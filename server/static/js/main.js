/* eslint-disable */
$(document).ready(() => {
  $('.share-block-button').on('click', e => {
    e.preventDefault()
    $('.share-block').toggleClass('active')
  })

  // footer callback div
  $('.footer-callback-button').on('click', e => {
    e.preventDefault()
    e.stopPropagation()
    if (!$('.callback-wrap').hasClass('active')) {
      $('.callback-wrap').addClass('active')
    }
    $(document).one('click', function closeMenu(e) {
      if ($('.callback-wrap').has(e.target).length === 0) {
        $('.callback-wrap').removeClass('active')
      } else {
        $(document).one('click', closeMenu)
      }
    })
  })

  // footer callback input focus
  $('.callback-wrap .input-row input').on('change', function () {
    if ($(this).val()) {
      $(this).addClass('active')
    } else {
      $(this).removeClass('active')
    }
  })

  // textarea on click make new line
  $('.textarea-autoheight')
    .each(function() {
      this.setAttribute(
        'style',
        `height:${this.scrollHeight}px;overflow-y:hidden;`
      )
    })
    .on('input', function() {
      this.style.height = '40px'
      this.style.height = `${this.scrollHeight}px`
    })

  // admin chat effects
  if ($(window).width() > 767) {
    $('.admin-chat-toggle, .admin-chat-control').on('click', e => {
      e.preventDefault()
      if ($('.admin-chat-wrap, .chat-input-wrapper').hasClass('hide')) {
        $('.admin-chat-wrap, .chat-input-wrapper').removeClass('hide')
      } else {
        $('.admin-chat-wrap, .chat-input-wrapper').addClass('hide')
      }
    })
  }

  if ($(window).width() < 767) {
    $('.admin-chat-toggle, .close-chat, .core-skill').on('click', e => {
      e.preventDefault()
      if ($('.chat-holder').hasClass('active')) {
        $('.chat-holder').removeClass('active')
      } else {
        $('.chat-holder').addClass('active')
      }
    })
  }

  // admin sidebar
  $('.sidebar-trigger').on('click', e => {
    e.preventDefault()

    const adminSidebar = $('.admin-sidebar')
    const adminContainer = $('.admin-container')

    if (adminSidebar.hasClass('sidebar-sm')) {
      adminSidebar.removeClass('sidebar-sm')
      adminContainer.removeClass('container-sm')
    } else {
      adminSidebar.addClass('sidebar-sm')
      adminContainer.addClass('container-sm')
    }
  })

  if ($(window).width() < 1200) {
    $('.sidebar-trigger').click()
  }

  // confirm modal
  $('.delete-button, .close-confirm-modal').on('click', e => {
    e.preventDefault()
    e.stopPropagation()
    if ($('.confirm-modal-wrapper').hasClass('show')) {
      $('.confirm-modal-wrapper').removeClass('show')
    } else {
      $('.confirm-modal-wrapper').addClass('show')
    }

    $(document).one('click', function closeMenu(e) {
      if ($('.confirm-modal-wrapper').has(e.target).length === 0) {
        $('.confirm-modal-wrapper').removeClass('show')
      } else {
        $(document).one('click', closeMenu)
      }
    })
  })

  // login registration
  $('.go-to-login').click(() => {
    $('.form-enter-section').removeClass('move-position')
    $('.signIn').addClass('active-sx')
    $('.signUp').addClass('inactive-dx')
    $('.signUp').removeClass('active-dx')
    $('.signIn').removeClass('inactive-sx')
  })

  $('.go-to-register').click(() => {
    $('.form-enter-section').addClass('move-position')
    $('.signUp').addClass('active-dx')
    $('.signIn').addClass('inactive-sx')
    $('.signIn').removeClass('active-sx')
    $('.signUp').removeClass('inactive-dx')
  })

  $('.video-bg').click()

  $('.dropdown-select-lang .dropdown-menu a').click(function() {
    $('.selected-lang').html(
      `${$(this).text()} <span class="mdi mdi-chevron-down"></span>`
    )

    if ($(this).hasClass('selected')) {
      $(this).removeClass('selected')
    } else {
      $('.dropdown-select-lang .dropdown-menu a').removeClass('selected')
      $(this).addClass('selected')
    }
  })

  // video page slider
  $('.hp-main-text').slick({
    arrows: false,
    dots: false,
    fade: true,
    autoplay: true,
    autoplaySpeed: 7000,
    speed: 900,
    infinite: true
  })

  // mobile menu
  $('.mobile-menu-button').on('click', e => {
    e.preventDefault()

    if ($('body').hasClass('overflow-h')) {
      $('body').removeClass('overflow-h')
    } else {
      $('body').addClass('overflow-h')
    }

    if ($('.main-nav').hasClass('open')) {
      $('.main-nav').removeClass('open')
    } else {
      $('.main-nav').addClass('open')
    }

    if ($('.mobile-menu-mask').hasClass('open')) {
      $('.mobile-menu-mask').removeClass('open')
    } else {
      $('.mobile-menu-mask').addClass('open')
    }

    if ($('.mobile-menu-button').hasClass('open')) {
      $('.mobile-menu-button').removeClass('open')
    } else {
      $('.mobile-menu-button').addClass('open')
    }
  })

  $('.mobile-menu-mask').on('click', e => {
    e.preventDefault()

    if ($('body').hasClass('overflow-h')) {
      $('body').removeClass('overflow-h')
    }
    if ($('.main-nav').hasClass('open')) {
      $('.main-nav').removeClass('open')
    }
    if ($('.mobile-menu-mask').hasClass('open')) {
      $('.mobile-menu-mask').removeClass('open')
    }
    if ($('.mobile-menu-button').hasClass('open')) {
      $('.mobile-menu-button').removeClass('open')
    }
  })

  $('.step-tags').on('click', 'a', function(e) {
    e.preventDefault()
    $(this.dataset.target).click()
  })

  // landing page top slider
  const slider = $('.slider')

  slider.slick({
    draggable: true,
    arrows: false,
    dots: true,
    fade: true,
    speed: 900,
    infinite: true,
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    touchThreshold: 100
  })

  slider.on('beforeChange', (event, slick, currentSlide, nextSlide) => {
    const stepLinks = $('.step-tags a')
    stepLinks.removeClass('active')
    $(stepLinks[nextSlide]).addClass('active')
  })

  // aos init
  if ($(window).width() > 750) {
    AOS.init()
  }

  if ($(window).width() < 750) {
    $('img, div, section').removeAttr('data-aos')
  }

  // don't close dropdown on click by element inside
  $(document).on('click', '.more-filter-dropdown .dropdown-menu', e => {
    e.stopPropagation()
  })

  // contract effects
  $('.show-hide-button').on('click', function(e) {
    e.preventDefault()
    if ($(this).hasClass('active')) {
      $(this).removeClass('active')
    } else {
      $(this).addClass('active')
    }

    if ($('.columns-wrap').hasClass('active')) {
      $('.columns-wrap').removeClass('active')
    } else {
      $('.columns-wrap').addClass('active')
    }
  })

  $('.activate').on('click', function(e) {
    e.preventDefault()
    if ($(this).hasClass('active')) {
      $(this).removeClass('active')
    } else {
      $(this).addClass('active')
    }

    if ($('.doc-details').hasClass('show')) {
      $('.doc-details').removeClass('show')
    } else {
      $('.doc-details').addClass('show')
    }
  })

  $('.doc-detail-close').on('click', e => {
    e.preventDefault()
    $('.doc-details').removeClass('show')
    $('.activate').removeClass('active')
  })

  if ($(window).width() < 1100) {
    $('.doc-project').on('click', e => {
      e.preventDefault()

      if ($('.columns-wrap').hasClass('active')) {
        $('.columns-wrap').removeClass('active')
      } else {
        $('.columns-wrap').addClass('active')
      }
    })
  }

  $(() => {
    $('[data-toggle="tooltip"]').tooltip()
  })

  // $('.chat-show-mobile').on('click', () => {
  //   showChat()
  // })

  $('.close-chat').on('click', () => {
    $('.chat-holder').removeClass('active')
    $('body').removeClass('overflow-h')
    $('main').removeClass('z-index101')
  })

  if ($(window).width() < 767) {
    function showChat() {
      if ($('.chat-holder').hasClass('active')) {
        $('.chat-holder').removeClass('active')
        $('body').removeClass('overflow-h')
        $('main').removeClass('z-index101')
      } else {
        $('.chat-holder').addClass('active')
        $('body').addClass('overflow-h')
        $('main').addClass('z-index101')
      }
    }
  }

  $('.col-options').on('click', function() {
    $(this).toggleClass('active')
    $('.moderation-buttons').toggleClass('show')
  })

  if ($('.consulting-nav-wrap').length) {
    const fixmeTop = $('.consulting-nav-wrap').offset().top
    $(window).scroll(() => {
      const currentScroll = $(window).scrollTop()
      if (currentScroll >= fixmeTop) {
        $('.consulting-nav-wrap').addClass('fixed')
      } else {
        $('.consulting-nav-wrap').removeClass('fixed')
      }
    })

    $(document).on('click', '.consulting-nav a', function(e) {
      e.preventDefault()
      $('html, body').animate(
        {
          scrollTop: $($.attr(this, 'href')).offset().top - 40
        },
        500
      )
    })
  }

  $('#video-modal').on('hidden.bs.modal', function() {
    const player = videojs('video')
    player.pause()
  })

  $('#video-modal').on('show.bs.modal', function() {
    const player = videojs('video')
    player.play()
  })
})
