jQuery(document).ready(($) => {
  const mainHeader = $('.main-header')


  const belowNavHeroContent = $('.sub-nav-hero')


  let headerHeight = mainHeader.height()

  // set scrolling variables
  let scrolling = false


  let previousTop = 0


  const scrollDelta = 10


  const scrollOffset = 150

  $(window).on('scroll', () => {
    if (!scrolling) {
      scrolling = true
      !window.requestAnimationFrame
        ? setTimeout(autoHideHeader, 250)
        : requestAnimationFrame(autoHideHeader)
    }
  })

  $(window).on('resize', () => {
    headerHeight = mainHeader.height()
  })

  function autoHideHeader() {
    const currentTop = $(window).scrollTop()

    belowNavHeroContent.length > 0
      ? checkStickyNavigation(currentTop) // secondary navigation below intro
      : checkSimpleNavigation(currentTop)

    previousTop = currentTop
    scrolling = false
  }

  function checkSimpleNavigation(currentTop) {
    // there's no secondary nav or secondary nav is below primary nav
    if (previousTop - currentTop > scrollDelta) {
      // if scrolling up...
      mainHeader.removeClass('is-hidden')
    } else if (
      currentTop - previousTop > scrollDelta
      && currentTop > scrollOffset
    ) {
      // if scrolling down...
      mainHeader.addClass('is-hidden')
    }
  }

  // change header styles
  $(window).scroll(() => {
    const scroll = $(window).scrollTop()

    if (scroll >= 30) {
      $('.main-header').addClass('header-scroll')
    } else {
      $('.main-header').removeClass('header-scroll')
    }
  })
})
