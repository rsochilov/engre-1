from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import ImageCard, LandingPage

admin.site.register(LandingPage, SingletonModelAdmin)
admin.site.register(ImageCard)
