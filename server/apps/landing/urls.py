from django.urls import path, re_path
from .views import LandingView, landing_error

urlpatterns = [
    path("", LandingView.as_view(), name="landing"),
]
