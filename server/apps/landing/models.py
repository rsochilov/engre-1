from core.models import WatchableMixin
from django.db import models
from solo.models import SingletonModel


class LandingPage(WatchableMixin, SingletonModel):
    video = models.FileField()
    cards = models.ManyToManyField("ImageCard", related_name="page")


class ImageCard(WatchableMixin):
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to="landing/cards")
    ordering = models.SmallIntegerField(default=50)

    class Meta:
        ordering = ["id"]
