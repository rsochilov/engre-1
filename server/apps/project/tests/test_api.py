from typing import Tuple, List
import psycopg2
from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate
from accounts.models import User
from ..views import ServiceListAPIView, ProjectListAPIView


class ServiceListAPIViewTestCase(TestCase):
    conn_str = "dbname=marketplace user=marketplace host=localhost"
    order_by_list = ["recommended", "date", "popularity"]
    uri_part = "/projects-pm/services-api/?page=1&category__slug={}&order_by={}"
    api_request_factory = APIRequestFactory()

    @classmethod
    def setUpClass(cls):
        cls.user = User.objects.get(email__contains="kudrenko")
        categories_info = cls._get_all_categories()
        cls.categories = {ci[1]: ci[0] for ci in categories_info}

    @classmethod
    def tearDownClass(cls):
        pass

    def test_service_list_view(self):
        for cat in self.categories.keys():
            self.check_view_response(cat)

    def check_view_response(self, category: str):
        for order_by in self.order_by_list:
            if order_by in ["relevance", "rating"]:
                continue
            response = self._get_response(category, order_by)
            self.assertEqual(response.status_code, 200)
            results = self._get_data_from_response(response, "results")

            received_ids = [r["id"] for r in results]
            actual_ids = self._get_ordered_services_ids(category, order_by)

    def _get_response(self, category: str, order_by: str):
        uri = self.uri_part.format(category, order_by)
        request = self.api_request_factory.get(uri)
        force_authenticate(request, user=self.user)
        return ServiceListAPIView.as_view()(request)

    def _get_data_from_response(self, response, key: str) -> dict:
        data = response.data.get(key)
        if not data:
            self.fail(f"Invalid response type: no '{key}' key in response data")
        return data

    @classmethod
    def _get_all_categories(cls) -> Tuple[int, str]:
        query = "SELECT id,slug FROM project_category;"
        with psycopg2.connect(cls.conn_str) as conn:
            with conn.cursor() as cursor:
                cursor.execute(query)
                return cursor.fetchall()

    def _get_ordered_services_ids(self, category: str, order_by: str) -> List[int]:
        category_id = self.categories[category]
        replaced_order_by = self._replace_order_by(order_by)
        query = f"SELECT id FROM project_intellectualservice WHERE category_id = {category_id} ORDER BY {self._replace_order_by(order_by)} LIMIT 10;"
        with psycopg2.connect(self.conn_str) as conn:
            with conn.cursor() as cursor:
                cursor.execute(query)
                return [r[0] for r in cursor.fetchall()]

    def _replace_order_by(self, order_by: str) -> str:
        if order_by == "date":
            return "created_at"
        if order_by == "popularity":
            return "views"
        if order_by == "recommended":
            return "is_recommended"
        return order_by

    def _get_rows_count(self, table_name: str) -> int:
        query = f"SELECT COUNT(*) FROM {table_name};"
        if not table_name:
            raise Exception("Empty table_name in _get_rows_count")
        with psycopg2.connect(self.conn_str) as conn:
            with conn.cursor() as cursor:
                cursor.execute(query)
                return cursor.fetchone()[0]
