# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots["test_update_service 1"] = {
    "data": {
        "updateIntellectualService": {
            "errors": None,
            "service": {
                "categories": {
                    "edges": [
                        {"node": {"name": "category6"}},
                        {"node": {"name": "category8"}},
                        {"node": {"name": "category10"}},
                    ]
                },
                "description": "updated desc",
                "hourlyRate": 400.0,
                "name": "updated bane",
                "tags": {
                    "edges": [
                        {"node": {"name": "tag5"}},
                        {"node": {"name": "tag6"}},
                        {"node": {"name": "tag7"}},
                        {"node": {"name": "tag8"}},
                        {"node": {"name": "tag9"}},
                    ]
                },
            },
        }
    }
}

snapshots["test_delete_service 1"] = {
    "data": {"deleteIntellectualService": {"errors": None, "success": True}}
}

snapshots["test_create_project 1"] = {
    "data": {
        "createProject": {
            "errors": None,
            "project": {
                "budget": 1000.0,
                "categories": {
                    "edges": [
                        {"node": {"name": "category12"}},
                        {"node": {"name": "category14"}},
                        {"node": {"name": "category16"}},
                    ]
                },
                "dateFinish": "2018-02-01",
                "dateStart": "2018-01-01",
                "description": "test description",
                "document": "http://localhost:8000/media/projects/documents/file18.pdf",
                "name": "test project",
                "picture": "http://localhost:8000/media/projects/images/file19.png",
                "pm": {"firstName": "john8", "lastName": "johnson8"},
                "service": {
                    "id": "SW50ZWxsZWN0dWFsU2VydmljZVR5cGU6NA==",
                    "name": "service2",
                },
                "status": 1,
                "tags": {
                    "edges": [
                        {"node": {"name": "tag10"}},
                        {"node": {"name": "tag11"}},
                        {"node": {"name": "tag12"}},
                        {"node": {"name": "tag13"}},
                        {"node": {"name": "tag14"}},
                    ]
                },
                "video": "http://localhost:8000/media/projects/videos/file17.mp4",
            },
        }
    }
}

snapshots["test_create_service 1"] = {
    "data": {
        "createIntellectualService": {
            "errors": None,
            "service": {
                "categories": {
                    "edges": [
                        {"node": {"name": "category0"}},
                        {"node": {"name": "category2"}},
                        {"node": {"name": "category4"}},
                    ]
                },
                "description": "test desc",
                "hourlyRate": 500.0,
                "name": "test",
                "tags": {
                    "edges": [
                        {"node": {"name": "tag0"}},
                        {"node": {"name": "tag1"}},
                        {"node": {"name": "tag2"}},
                        {"node": {"name": "tag3"}},
                        {"node": {"name": "tag4"}},
                    ]
                },
                "video": "http://localhost:8000/media/service_videos/file3.mp4",
            },
        }
    }
}

snapshots["test_update_project_without_service_and_pm 1"] = {
    "data": {
        "updateProject": {
            "errors": None,
            "project": {
                "budget": 1000.0,
                "dateFinish": "2018-02-01",
                "dateStart": "2018-01-01",
                "description": "updated description",
                "document": "http://localhost:8000/media/projects/documents/file24.pdf",
                "name": "updated name",
                "picture": "http://localhost:8000/media/projects/images/file25.png",
                "pm": {"firstName": "john11", "lastName": "johnson11"},
                "service": {
                    "id": "SW50ZWxsZWN0dWFsU2VydmljZVR5cGU6NQ==",
                    "name": "service3",
                },
                "status": 1,
                "tags": {
                    "edges": [
                        {"node": {"name": "tag15"}},
                        {"node": {"name": "tag16"}},
                        {"node": {"name": "tag17"}},
                        {"node": {"name": "tag18"}},
                        {"node": {"name": "tag19"}},
                    ]
                },
                "video": "http://localhost:8000/media/projects/videos/file23.mp4",
            },
        }
    }
}

snapshots["test_create_issue_group 1"] = {
    "data": {
        "createIssueGroup": {
            "errors": None,
            "issueGroup": {"name": "test group", "project": {"name": "project2"}},
        }
    }
}

snapshots["test_update_issue_group 1"] = {
    "data": {
        "updateIssueGroup": {
            "errors": None,
            "issueGroup": {
                "name": "updated name",
                "project": {"name": "updated project"},
            },
        }
    }
}

snapshots["test_create_issue 1"] = {
    "data": {
        "createIssue": {
            "errors": None,
            "issue": {
                "assignedUsers": {
                    "edges": [
                        {"node": {"firstName": "john27", "lastName": "johnson27"}},
                        {"node": {"firstName": "john28", "lastName": "johnson28"}},
                    ]
                },
                "description": "test desc",
                "dueDate": "2019-01-01",
                "group": {"name": "issue group1"},
                "name": "test issue",
            },
        }
    }
}

snapshots["test_update_issue 1"] = {
    "data": {
        "updateIssue": {
            "errors": None,
            "issue": {
                "assignedUsers": {
                    "edges": [
                        {"node": {"firstName": "john32", "lastName": "johnson32"}},
                        {"node": {"firstName": "john33", "lastName": "johnson33"}},
                    ]
                },
                "description": "updated desc",
                "dueDate": "2019-02-02",
                "group": {"name": "issue group2"},
                "name": "updated issue",
            },
        }
    }
}

snapshots["test_delete_mutation 1"] = {
    "data": {"deleteIssue": {"errors": None, "success": True}}
}

snapshots["test_create_comment 1"] = {
    "data": {
        "createComment": {
            "comment": {
                "issue": {"name": "issue2"},
                "text": "test comment",
                "user": {"firstName": "john41", "lastName": "johnson41"},
            },
            "errors": None,
        }
    }
}

snapshots["test_update_comment 1"] = {
    "data": {"updateComment": {"comment": {"text": "updated text"}, "errors": None}}
}

snapshots["test_delete_comment 1"] = {
    "data": {"deleteComment": {"errors": None, "success": True}}
}

snapshots["test_create_issue_atachment 1"] = {
    "data": {
        "createIssueAttachment": {
            "attachment": {
                "file": "http://localhost:8000/media/projects/issues/attachments/file68.png",
                "issue": {"name": "issue5"},
            },
            "errors": None,
        }
    }
}

snapshots["test_delete_issue_attachment 1"] = {
    "data": {"deleteIssueAttachment": {"errors": None, "success": True}}
}

snapshots["test_update_project_with_service_and_pm 1"] = {
    "errors": [{"message": "Object of type 'BytesIO' is not JSON serializable"}]
}
