import shutil

import pytest
from company.factories import CompanyFactory
from core.factories import (FileFactory, LanguageFactory, SubCategoryFactory,
                            TagFactory, UserFactory)
from core.utils import Context, to_global_id
from django.conf import settings
from graphene.test import Client
from marketplace.schema import schema
from project.factories import (CommentFactory, IntellectualServiceFactory,
                               IssueAttachmentFactory, IssueFactory,
                               IssueGroupFactory, ProjectFactory)
from project.models import Comment, IntellectualService, Issue, IssueAttachment


def teardown_module(module):
    if settings.TESTING_SETTINGS:
        shutil.rmtree(settings.MEDIA_ROOT)


@pytest.mark.django_db
def test_create_service(snapshot):
    user = UserFactory()
    client = Client(schema)
    company = CompanyFactory()
    categories = SubCategoryFactory.create_batch(3)
    tags = TagFactory.create_batch(5)
    video = FileFactory(content_type="video/mp4")

    executed = client.execute("""
       mutation createIntelService($serviceData: IntellectualServiceInput!) {
           createIntellectualService(serviceData: $serviceData) {
               errors
               service {
                   name
                   description
                   hourlyRate
                   video
                   tags {
                       edges {
                           node {
                               name
                           }
                       }
                   }
                   categories {
                       edges {
                           node {
                               name
                           }
                       }
                   }
               }
           }
       }
    """, variable_values={
        'serviceData': {
            'name': 'test',
            'description': 'test desc',
            'company': to_global_id(company),
            'categories': to_global_id(categories, many=True),
            'tags': to_global_id(tags, many=True),
            'hourlyRate': 500,
            'video': {'file': video.file, 'name': video.name}
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_update_service(snapshot):
    user = UserFactory()
    client = Client(schema)
    company = CompanyFactory()
    categories = SubCategoryFactory.create_batch(3)
    tags = TagFactory.create_batch(5)
    service = IntellectualServiceFactory(name='test', description='test desc')
    video = FileFactory(content_type="video/mp4")

    executed = client.execute("""
       mutation updateIntelService($serviceId: ID!, $serviceData: IntellectualServiceInput!) {
           updateIntellectualService(serviceId: $serviceId, serviceData: $serviceData) {
               errors
               service {
                   name
                   description
                   hourlyRate
                   tags {
                       edges {
                           node {
                               name
                           }
                       }
                   }
                   categories {
                       edges {
                           node {
                               name
                           }
                       }
                   }
               }
           }
       }
    """, variable_values={
        'serviceId': to_global_id(service),
        'serviceData': {
            'name': 'updated bane',
            'description': 'updated desc',
            'company': to_global_id(company),
            'categories': to_global_id(categories, many=True),
            'tags': to_global_id(tags, many=True),
            'hourlyRate': 400,
            'video': {'file': video.file, 'name': video.name}
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_delete_service(snapshot):
    client = Client(schema)
    user = UserFactory()
    service = IntellectualServiceFactory()

    executed = client.execute("""
        mutation deleteIntelService($serviceId: ID!) {
            deleteIntellectualService(serviceId: $serviceId) {
                errors
                success
            }
        }
    """, variable_values={'serviceId': to_global_id(service)}, context_value=Context(user=user))

    with pytest.raises(IntellectualService.DoesNotExist):
        IntellectualService.objects.get(pk=service.id)

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_create_project(snapshot):
    client = Client(schema)
    user = UserFactory()
    pm = UserFactory()
    service = IntellectualServiceFactory()
    languages = LanguageFactory.create_batch(3)
    tags = TagFactory.create_batch(5)
    categories = SubCategoryFactory.create_batch(3)

    video = FileFactory(content_type="video/mp4")
    document = FileFactory(content_type="application/pdf")
    picture = FileFactory(content_type="image/png")

    project_name = 'test project'

    executed = client.execute("""
        mutation createProject($projectData: ProjectInput!) {
            createProject(projectData: $projectData) {
                errors
                project {
                    name
                    description
                    pm {
                        firstName
                        lastName
                    }
                    service {
                        id
                        name
                    }
                    budget
                    dateStart
                    dateFinish
                    status
                    pm {
                        firstName
                        lastName
                    }
                    tags {
                        edges {
                            node {
                                name
                            }
                        }
                    }
                    categories {
                        edges {
                            node {
                                name
                            }
                        }
                    }
                    picture
                    document
                    video
                }
            }
        }
    """, variable_values={
        'projectData': {
            'name': project_name,
            'description': 'test description',
            'service': to_global_id(service),
            'tags': to_global_id(tags, many=True),
            'pm': to_global_id(pm),
            'budget': 1000,
            'dateStart': '2018-01-01',
            'dateFinish': '2018-02-01',
            'languages': to_global_id(languages, many=True),
            'categories': to_global_id(categories, many=True),
            'picture': {'file': picture.file, 'name': picture.name},
            'document': {'file': document.file, 'name': document.name},
            'video': {'file': video.file, 'name': video.name},
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_update_project_without_service_and_pm(snapshot):
    client = Client(schema)
    user = UserFactory()
    pm = UserFactory()
    service = IntellectualServiceFactory()
    languages = LanguageFactory.create_batch(3)
    tags = TagFactory.create_batch(5)
    categories = SubCategoryFactory.create_batch(3)

    video = FileFactory(content_type="video/mp4")
    document = FileFactory(content_type="application/pdf")
    picture = FileFactory(content_type="image/png")

    project = ProjectFactory(pm=None, service=None)

    project_name = 'updated name'

    executed = client.execute("""
        mutation updateProject($projectId: ID!, $projectData: ProjectInput!) {
            updateProject(projectId: $projectId, projectData: $projectData) {
                errors
                project {
                    name
                    description
                    pm {
                        firstName
                        lastName
                    }
                    service {
                        id
                        name
                    }
                    budget
                    dateStart
                    dateFinish
                    status
                    pm {
                        firstName
                        lastName
                    }
                    tags {
                        edges {
                            node {
                                name
                            }
                        }
                    }
                    picture
                    document
                    video
                }
            }
        }
    """, variable_values={
        'projectId': to_global_id(project),
        'projectData': {
            'name': project_name,
            'description': 'updated description',
            'service': to_global_id(service),
            'tags': to_global_id(tags, many=True),
            'pm': to_global_id(pm),
            'budget': 1000,
            'dateStart': '2018-01-01',
            'dateFinish': '2018-02-01',
            'categories': to_global_id(categories, many=True),
            'languages': to_global_id(languages, many=True),
            'picture': {'file': picture.file, 'name': picture.name},
            'document': {'file': document.file, 'name': document.name},
            'video': {'file': video.file, 'name': video.name},
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_update_project_with_service_and_pm(snapshot):
    client = Client(schema)
    user = UserFactory()
    pm = UserFactory()
    service = IntellectualServiceFactory()
    languages = LanguageFactory.create_batch(3)
    tags = TagFactory.create_batch(5)

    video = FileFactory(content_type="video/mp4")
    document = FileFactory(content_type="application/pdf")
    picture = FileFactory(content_type="image/png")

    project = ProjectFactory()

    project_name = 'updated name'

    executed = client.execute("""
        mutation updateProject($projectId: ID!, $projectData: ProjectInput!) {
            updateProject(projectId: $projectId, projectData: $projectData) {
                errors
                project {
                    name
                    description
                    pm {
                        firstName
                        lastName
                    }
                    service {
                        id
                        name
                    }
                    budget
                    dateStart
                    dateFinish
                    status
                    pm {
                        firstName
                        lastName
                    }
                    tags {
                        edges {
                            node {
                                name
                            }
                        }
                    }
                    picture
                    document
                    video
                }
            }
        }
    """, variable_values={
        'projectId': to_global_id(project),
        'projectData': {
            'name': project_name,
            'description': 'updated description',
            'service': to_global_id(service),
            'tags': to_global_id(tags, many=True),
            'pm': to_global_id(pm),
            'budget': 1000,
            'dateStart': '2018-01-01',
            'dateFinish': '2018-02-01',
            'languages': to_global_id(languages, many=True),
            'picture': {'file': picture.file, 'name': picture.name},
            'document': {'file': document.file, 'name': document.name},
            'video': {'file': video.file, 'name': video.name},
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_create_issue_group(snapshot):
    client = Client(schema)

    user = UserFactory()
    project = ProjectFactory()

    executed = client.execute("""
        mutation createIssueGroup($issueGroupData: IssueGroupInput!) {
            createIssueGroup(issueGroupData: $issueGroupData) {
                errors
                issueGroup {
                    name
                    project {
                        name
                    }
                }
            }
        }
    """, variable_values={
        'issueGroupData': {
            'name': 'test group',
            'project': to_global_id(project),
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_update_issue_group(snapshot):
    client = Client(schema)

    user = UserFactory()
    project = ProjectFactory(name='updated project')
    issue_group = IssueGroupFactory()

    executed = client.execute("""
        mutation updateIssueGroup($issueGroupId: ID!, $issueGroupData: IssueGroupInput!) {
            updateIssueGroup(issueGroupId: $issueGroupId, issueGroupData: $issueGroupData) {
                errors
                issueGroup {
                    name
                    project {
                        name
                    }
                }
            }
        }
    """, variable_values={
        'issueGroupId': to_global_id(issue_group),
        'issueGroupData': {
            'name': 'updated name',
            'project': to_global_id(project),
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_create_issue(snapshot):
    client = Client(schema)

    user = UserFactory()
    users = UserFactory.create_batch(2)
    issue_group = IssueGroupFactory()

    executed = client.execute("""
        mutation createIssue($issueData: IssueInput!) {
            createIssue(issueData: $issueData) {
                errors
                issue {
                    name
                    description
                    group {
                        name
                    }
                    dueDate
                    assignedUsers {
                        edges {
                            node {
                                firstName
                                lastName
                            }
                        }
                    }
                }
            }
        }
    """, variable_values={
        'issueData': {
            'name': 'test issue',
            'description': 'test desc',
            'group': to_global_id(issue_group),
            'dueDate': '2019-01-01',
            'assignedUsers': to_global_id(users, many=True)
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_update_issue(snapshot):
    client = Client(schema)

    user = UserFactory()
    users = UserFactory.create_batch(2)
    issue_group = IssueGroupFactory()
    issue = IssueFactory()

    executed = client.execute("""
        mutation updateIssue($issueId: ID!, $issueData: IssueInput!) {
            updateIssue(issueId: $issueId, issueData: $issueData) {
                errors
                issue {
                    name
                    description
                    group {
                        name
                    }
                    dueDate
                    assignedUsers {
                        edges {
                            node {
                                firstName
                                lastName
                            }
                        }
                    }
                }
            }
        }
    """, variable_values={
        'issueId': to_global_id(issue),
        'issueData': {
            'name': 'updated issue',
            'description': 'updated desc',
            'group': to_global_id(issue_group),
            'dueDate': '2019-02-02',
            'assignedUsers': to_global_id(users, many=True)
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_delete_mutation(snapshot):
    client = Client(schema)

    user = UserFactory()
    issue = IssueFactory()

    executed = client.execute("""
        mutation deleteIssue($issueId: ID!) {
            deleteIssue(issueId: $issueId) {
                errors
                success
            }
        }
    """, variable_values={'issueId': to_global_id(issue)}, context_value=Context(user=user))

    with pytest.raises(Issue.DoesNotExist):
        Issue.objects.get(pk=issue.id)

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_create_comment(snapshot):
    client = Client(schema)

    user = UserFactory()
    issue = IssueFactory()

    executed = client.execute("""
        mutation createComment($commentData: CommentInput!) {
            createComment(commentData: $commentData) {
                errors
                comment {
                    text
                    issue {
                        name
                    }
                    user {
                        firstName
                        lastName
                    }
                }
            }
        }
    """, variable_values={
        'commentData': {
            'text': 'test comment',
            'issue': to_global_id(issue),
            'user': to_global_id(user),
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_update_comment(snapshot):
    client = Client(schema)

    user = UserFactory()
    comment = CommentFactory()

    executed = client.execute("""
        mutation updateComment($commentId: ID!, $text: String!) {
            updateComment(commentId: $commentId, text: $text) {
                errors
                comment {
                    text
                }
            }
        }
    """, variable_values={
        'commentId': to_global_id(comment),
        'text': 'updated text'
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_delete_comment(snapshot):
    client = Client(schema)

    user = UserFactory()
    comment = CommentFactory()

    executed = client.execute("""
        mutation deleteComment($commentId: ID!) {
            deleteComment(commentId: $commentId) {
                errors
                success
            }
        }
    """, variable_values={'commentId': to_global_id(comment)}, context_value=Context(user=user))

    with pytest.raises(Comment.DoesNotExist):
        Comment.objects.get(pk=comment.id)

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_create_issue_atachment(snapshot):
    client = Client(schema)

    user = UserFactory()
    issue = IssueFactory()
    attachment = FileFactory()

    executed = client.execute("""
        mutation createIssueAttachment($data: IssueAttachmentInput) {
            createIssueAttachment(data: $data) {
                errors
                attachment {
                    file
                    issue {
                        name
                    }
                }
            }
        }
    """, variable_values={
        'data': {
            'file': {'file': attachment.file, 'name': attachment.name},
            'issue': to_global_id(issue)
        }
    }, context_value=Context(user=user))

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_delete_issue_attachment(snapshot):
    client = Client(schema)

    user = UserFactory()
    attachment = IssueAttachmentFactory()

    executed = client.execute("""
        mutation deleteIssueAttachment($attachmentId: ID!) {
            deleteIssueAttachment(attachmentId: $attachmentId) {
                errors
                success
            }
        }
    """, variable_values={'attachmentId': to_global_id(attachment)}, context_value=Context(user=user))

    with pytest.raises(IssueAttachment.DoesNotExist):
        IssueAttachment.objects.get(pk=attachment.id)

    snapshot.assert_match(executed)
