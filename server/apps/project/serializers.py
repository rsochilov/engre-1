from accounts.models import User
from accounts.serializers import UserSerializer
from chat.models import ChatRoom
from company.models import Review
from company.serializers import CompanySerializer
from core.serializers import NameSlugSerializer
from project.models import Industry
from rest_framework import serializers

from .models import IntellectualService, Project, Task, TaskAttachment, TaskComment


class AssignedUserSerializer(serializers.ModelSerializer):

    company = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ("id", "name", "company", "picture")

    def get_company(self, obj):
        if hasattr(obj, "work"):
            return obj.work.company.name

        return ""


class AbstractIntellectualServiceSerializer(serializers.ModelSerializer):
    tags = NameSlugSerializer(many=True)
    tag_list = serializers.SerializerMethodField()
    languages = NameSlugSerializer(many=True)
    core_skills = NameSlugSerializer(many=True)
    category = NameSlugSerializer(read_only=True)
    company = CompanySerializer()
    industry = NameSlugSerializer(read_only=True)
    url = serializers.SerializerMethodField()

    def get_tag_list(self, obj):
        return ", ".join(obj.tags.values_list("name", flat=True))

    def get_url(self, obj):
        return obj.get_absolute_url()

    class Meta:
        abstract = True


class ServiceSerializer(AbstractIntellectualServiceSerializer):
    class Meta:
        model = IntellectualService
        fields = "__all__"


class PartialServiceSerializer(serializers.ModelSerializer):
    company = CompanySerializer()
    """
    IntellectualService serializer for prioritized services
    Default ServiceSerializer is slow with order_by is_priority statement
    """

    class Meta:
        model = IntellectualService
        fields = "__all__"


class ServiceCompanySerializer(AbstractIntellectualServiceSerializer):
    company = None

    class Meta:
        model = IntellectualService
        fields = "__all__"


class ProjectSerializer(AbstractIntellectualServiceSerializer):
    assigned_users_data = serializers.SerializerMethodField(read_only=True)
    service = ServiceSerializer(read_only=True)
    chat_room_with_pm = serializers.SerializerMethodField()
    chat_room_with_contractor = serializers.SerializerMethodField()
    has_open_request = serializers.SerializerMethodField()

    def get_url(self, obj):
        return obj.get_absolute_url()

    class Meta:
        model = Project
        # fields = "__all__"
        exclude = ("assigned_users",)
        extra_fields = "assigned_users_data"

    def get_assigned_users_data(self, obj):
        return AssignedUserSerializer(obj.assigned_users.all(), many=True).data

    def get_chat_room_with_pm(self, obj):
        request = self.context.get("request")
        if not request:
            return None

        if obj.pm:
            if obj.pm.id == request.user.id:
                return None

            try:
                if not request.user.is_anonymous:
                    return (
                        ChatRoom.objects.filter(users=obj.pm).get(users=request.user).id
                    )
            except ChatRoom.DoesNotExist:
                return None

        return None

    def get_chat_room_with_contractor(self, obj):
        request = self.context.get("request")
        if not request:
            return None

        if obj.service:
            try:
                return (
                    ChatRoom.objects.filter(
                        users=obj.company.workers.filter(is_owner=True).first().user.id
                    )
                    .get(
                        users=obj.service.company.workers.filter(is_owner=True)
                        .first()
                        .user.id
                    )
                    .id
                )
            except ChatRoom.DoesNotExist:
                return None

        return None

    def get_has_open_request(self, obj):
        return obj.projectrequest_set.filter(
            is_accepted=False, is_rejected=False
        ).exists()


class ReviewSerializer(serializers.ModelSerializer):
    project = ProjectSerializer()
    user_data = serializers.SerializerMethodField()

    class Meta:
        model = Review
        fields = "__all__"
        extra_fields = ("user_data",)

    def get_user_data(self, obj):
        return UserSerializer(obj.user).data


class TaskSerializer(serializers.ModelSerializer):
    comments_count = serializers.SerializerMethodField(read_only=True)
    attachments_count = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Task
        fields = "__all__"

    def get_comments_count(self, obj):
        return obj.comments.count()

    def get_attachments_count(self, obj):
        return obj.attachments.count()


class TaskCommentSerializer(serializers.ModelSerializer):
    user_data = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = TaskComment
        fields = "__all__"
        extra_fields = "user_data"

    def get_user_data(self, obj):
        return UserSerializer(obj.user).data


class TaskAttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskAttachment
        fields = "__all__"


class IndustrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Industry
        fields = "__all__"
