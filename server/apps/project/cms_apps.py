from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


@apphook_pool.register
class ProjectApphook(CMSApp):
    name = _("Project Apphook")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.urls"]


@apphook_pool.register
class ProjectsPMApphook(CMSApp):
    name = _("Projects PM Apphook")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.urls_pm_projects"]


@apphook_pool.register
class ServicesPMApphook(CMSApp):
    name = _("Services PM Apphook")

    def get_urls(self, page=None, language=None, **kwargs):
        return ["project.urls_pm_services"]
