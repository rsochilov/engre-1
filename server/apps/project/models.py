import os

from accounts.models import User
from chat.models import ChatRoom
from company.models import Company
from core.managers import CommonManager
from core.models import (
    ACTIVE,
    NOT_ACTIVE,
    STATUSES,
    ArchivableMixin,
    Language,
    NameSlugModel,
    OrderingMixin,
    Tag,
    WatchableMixin,
)
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver


def get_project_images_path(instance, filename):
    return os.path.join("projects", "images", filename)


def get_project_videos_path(instance, filename):
    return os.path.join("projects", "videos", filename)


def get_project_documents_path(instance, filename):
    return os.path.join("projects", "documents", filename)


def get_task_attachment_path(instance, filename):
    return os.path.join("projects", "tasks", "attachments", filename)


def get_video_path(instance, filename):
    return os.path.join(f"{instance._meta.model_name}s", "videos", filename)


def get_logo_path(instance, filename):
    return os.path.join(f"{instance._meta.model_name}s", "logos", filename)


def get_document_path(instance, filename):
    return os.path.join(f"{instance._meta.model_name}s", "documents", filename)


class Category(NameSlugModel, OrderingMixin):
    class Meta:
        ordering = ["ordering"]


class Industry(NameSlugModel, OrderingMixin):
    class Meta:
        ordering = ["ordering"]


class CoreSkill(NameSlugModel):
    pass


class WorkAbstract(WatchableMixin, ArchivableMixin):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)
    description = models.TextField()
    company = models.ForeignKey(
        Company, related_name="%(class)ss", on_delete=models.PROTECT
    )
    category = models.ForeignKey(
        Category, related_name="%(class)ss", on_delete=models.PROTECT
    )
    core_skills = models.ManyToManyField(CoreSkill, related_name="%(class)ss")
    industry = models.ForeignKey(
        "project.Industry",
        related_name="%(class)ss",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    languages = models.ManyToManyField(Language)
    tags = models.ManyToManyField(Tag)
    logo = models.ImageField(upload_to=get_logo_path, null=True)
    document = models.FileField(upload_to=get_document_path, null=True, blank=True)
    video = models.FileField(upload_to=get_video_path, null=True, blank=True)
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=NOT_ACTIVE,)
    video_link = models.TextField(max_length=1000, null=True, blank=True)
    views = models.PositiveIntegerField(default=0)

    class Meta:
        abstract = True
        ordering = ["pk"]

    def save(self, *args, **kwargs):
        self.slug = "-".join(self.name.lower().split(" "))
        return super(WorkAbstract, self).save(*args, **kwargs)


class IntellectualService(WorkAbstract):
    hourly_rate = models.DecimalField(decimal_places=2, max_digits=14)
    is_recommended = models.BooleanField(default=False)
    is_priority = models.BooleanField(
        default=False, verbose_name="High Priority", db_index=True
    )

    objects = CommonManager()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("service-details", args=[str(self.company.slug), str(self.slug)])

    def save(self, *args, **kwargs):
        self.slug = "-".join(self.name.lower().split(" "))
        self.status = ACTIVE
        super(IntellectualService, self).save(*args, **kwargs)


class Project(WorkAbstract):
    PLATFORM, DIRECT = 1, 2
    MANAGEMENT_OPTIONS = (
        (PLATFORM, "platform"),
        (DIRECT, "direct"),
    )

    service = models.ForeignKey(
        IntellectualService,
        related_name="projects",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    assigned_users = models.ManyToManyField(
        "accounts.User", related_name="projects", blank=True
    )
    budget = models.DecimalField(decimal_places=2, max_digits=10)
    date_start = models.DateField(null=True, blank=True)
    date_finish = models.DateField(null=True, blank=True)
    pm = models.ForeignKey(
        "accounts.ProjectManager", null=True, on_delete=models.SET_NULL, blank=True
    )
    management_option = models.PositiveSmallIntegerField(choices=MANAGEMENT_OPTIONS)

    objects = CommonManager()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("project-details", args=[str(self.company.slug), str(self.slug)])


class ProjectUsers(models.Model):
    """Users, that accepted for project
    """

    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Task(WatchableMixin):
    name = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    due_date = models.DateTimeField(null=True, blank=True)
    assigned_users = models.ManyToManyField(User, related_name="tasks", blank=True)
    project = models.ForeignKey(
        "project.Project", related_name="tasks", on_delete=models.CASCADE
    )
    progress = models.PositiveSmallIntegerField(default=0)

    objects = CommonManager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["created_at"]


class TaskComment(WatchableMixin):
    text = models.TextField()
    user = models.ForeignKey(User, related_name="comments", on_delete=models.CASCADE)
    task = models.ForeignKey(
        "project.Task", related_name="comments", on_delete=models.CASCADE
    )

    objects = CommonManager()

    def __str__(self):
        return self.task.name


class TaskAttachment(WatchableMixin):
    file = models.FileField(upload_to=get_task_attachment_path)
    name = models.CharField(max_length=255, null=True, blank=True)
    task = models.ForeignKey(
        "project.Task", related_name="attachments", on_delete=models.CASCADE, null=True
    )
    comment = models.ForeignKey(
        "project.TaskComment",
        related_name="attachments",
        on_delete=models.CASCADE,
        null=True,
    )

    objects = CommonManager()

    def __str__(self):
        return self.task.name

    def save(self, *args, **kwargs):
        self.name = self.file.name
        return super(TaskAttachment, self).save(*args, **kwargs)


@receiver(post_save, sender=Project, dispatch_uid="project.create_chat")
def create_chat(sender, instance, **kwargs):
    if instance.pm:
        owner = instance.company.get_owner()
        pm = instance.pm

        if not ChatRoom.objects.filter(users=pm).filter(users=owner).exists():
            chat_room = ChatRoom.objects.create()
            chat_room.users.set([owner, pm])
