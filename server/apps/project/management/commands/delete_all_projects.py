from django.core.management.base import BaseCommand
from loguru import logger

from project.models import Project


class Command(BaseCommand):
    help = "Deletes all projects from database"

    def handle(self, *args, **options):
        logger.warning(
            "This command will delete ALL projects from database. Are you sure? [y/N]"
        )

        if input().rstrip() == "y":
            Project.objects.all().delete()
            logger.success("All projects were deleted")
