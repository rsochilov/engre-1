from os import path
import pickle
from typing import List, Iterator

from django.core.management.base import BaseCommand
from loguru import logger

from project.models import IntellectualService


class Command(BaseCommand):
    help = "PRODUCTION SERVER COMMAND! Saves services from selected file dumped_services.bin to database"

    def add_arguments(self, parser):
        parser.add_argument("dumped_services_filepath", nargs="+", type=str)

    def handle(self, *args, **options):
        data_file = options["dumped_services_filepath"][0]
        if not path.isfile(data_file):
            logger.critical(f'Unable to locate file "{data_file}"')
            exit(-1)
        try:
            updated_services = self.load_services_from_file(data_file)
        except Exception as ex:
            logger.exception(ex)
            exit(-1)

        for service in updated_services:
            try:
                service.save()
                logger.success(f'Service "{service}" updated')
            except Exception as identifier:
                logger.critical(identifier)

    def load_services_from_file(self, filepath: str) -> List[IntellectualService]:
        with open(filepath, "rb") as file:
            return pickle.load(file)
