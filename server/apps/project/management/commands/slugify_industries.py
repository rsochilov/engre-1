from django.utils.text import slugify
from django.core.management.base import BaseCommand
from loguru import logger

from project.models import Industry


class Command(BaseCommand):
    help = "Creates slugs for industries without slugs"

    def handle(self, *args, **options):
        industries = Industry.objects.filter(slug="")
        if not industries:
            logger.info("No industries with empty slug found")
            return

        for i in industries:
            i.slug = slugify(i.name)
            i.save(update_fields=["slug"])
            logger.success(f'Industry "{i}" slug updated')
