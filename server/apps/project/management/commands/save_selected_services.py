from os import path
import pickle
from typing import List, Iterator

import xlrd
from django.core.management.base import BaseCommand
from loguru import logger

from project.models import IntellectualService


class Command(BaseCommand):
    help = "STAGE SERVER COMMAND! Saves selected services from xslx file into dumped_services.bin"
    save_file_name = "dumped_services.bin"

    def add_arguments(self, parser):
        parser.add_argument("services_data_filepath", nargs="+", type=str)

    def handle(self, *args, **options):
        data_file = options["services_data_filepath"][0]
        if not path.isfile(data_file):
            logger.critical(f'Unable to locate file "{data_file}"')
            exit(-1)
        services = self.try_get_services_from_xl_file(data_file)
        try:
            self.serialize_services_to_file(services)
        except IntellectualService.DoesNotExist as ex:
            logger.exception(ex)
            exit(-1)

    def try_get_services_from_xl_file(
        self, data_file: str
    ) -> List[IntellectualService]:
        services = []
        for service_name in self.get_service_name_from_xl(data_file):
            try:
                services.append(IntellectualService.objects.get(name=service_name))
            except IntellectualService.DoesNotExist:
                logger.error(f'Service "{service_name.name}" not found')
        return services

    def serialize_services_to_file(self, services_list: List[IntellectualService]):
        with open(self.save_file_name, "ab") as file:
            pickle.dump(services_list, file)

    def get_service_name_from_xl(self, filepath: str) -> Iterator[str]:
        sheet = xlrd.open_workbook(filepath).sheet_by_index(0)
        for row_num in range(0, sheet.nrows):
            vals = sheet.row_values(row_num)
            if vals[0].strip() == "":
                break
            # Сервисы, у которых в соседнем столбце нет никаких значений
            # остались без изменения и игнорируются
            if vals[1] == "":
                continue
            else:
                yield vals[0].strip()
