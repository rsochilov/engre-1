from os import path
from django.core.management.base import BaseCommand

from project.models import IntellectualService
from loguru import logger


class Command(BaseCommand):
    help = "Sets prioritized services from file"

    def add_arguments(self, parser):
        parser.add_argument("names_path", nargs="+", type=str)

    def handle(self, *args, **options):
        names_file = options["names_path"][0]
        if not path.isfile(names_file):
            logger.critical(f'Unable to locate file "{names_file}"')
            exit(-1)

        for line in open(names_file, "r"):
            name = line.rstrip()
            try:
                service = IntellectualService.objects.get(name=name)
            except IntellectualService.DoesNotExist:
                logger.warning(f'Service "{name}" doesn\'t exist')
                continue
            service.is_priority = True
            service.save(update_fields=["is_priority"])
