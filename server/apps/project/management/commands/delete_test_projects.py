from django.core.management.base import BaseCommand
from loguru import logger

from project.models import Project


class Command(BaseCommand):
    help = "Deletes all projects except projects in projects_names list. See Command listing for details"
    projects_names = [
        "Hardware development and research",
        "Modeling and develop automated line",
        "Automation and development",
        "Interesting project",
        "Reasonable superb project",
        "Development Services for Website",
    ]

    def handle(self, *args, **options):
        test_projects = Project.objects.exclude(name__in=self.projects_names)
        if not test_projects:
            logger.info("No test project found, exiting")
            return
        logger.info(
            f"Found: {test_projects.count()} test projects:\n{test_projects}. Deleting..."
        )

        test_projects.delete()
        logger.success("Done!")
