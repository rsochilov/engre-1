from django.core.management.base import BaseCommand

from project.models import IntellectualService
from project.management.mixins import DeleteByDescriptionMixin


class Command(BaseCommand, DeleteByDescriptionMixin):
    description_length = 80
    db_model = IntellectualService

    def handle(self, *args, **options):
        self.delete_by_description_length()
