from django.db.models import Model
from django.db.models.functions import Length
from loguru import logger


class DeleteByDescriptionMixin:
    def delete_by_description_length(self):
        if self.db_model is None:
            raise Exception("No db model provided!")
        if not self.description_length or self.description_length <= 0:
            raise Exception("Invalid length")

        test_objects = self.db_model.objects.annotate(
            descr_len=Length("description")
        ).filter(descr_len__lte=self.description_length)

        if not test_objects:
            logger.warning(f"No test {self.db_model.__name__}s found, exiting")
            return

        logger.info(f"Found:\n{test_objects}\nDeleting..")
        test_objects.delete()
        logger.success(f"All test {self.db_model.__name__}s were deleted")
