from datetime import datetime

import factory

from company.factories import CompanyFactory
from core.factories import UserFactory


class IntellectualServiceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "project.IntellectualService"
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda n: "service%s" % n)
    description = factory.Sequence(lambda n: "description%s" % n)
    company = factory.SubFactory(CompanyFactory)
    hourly_rate = 500


class ProjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "project.Project"
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda n: "project%s" % n)
    description = factory.Sequence(lambda n: "description%s" % n)
    service = factory.SubFactory(IntellectualServiceFactory)
    budget = 500
    date_start = datetime(2018, 1, 1)
    date_finish = datetime(2018, 2, 1)
    status = 1
    pm = factory.SubFactory(UserFactory)


class IssueGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "project.IssueGroup"
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda n: "issue group%s" % n)
    project = factory.SubFactory(ProjectFactory)


class IssueFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "project.Issue"
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda n: "issue%s" % n)
    description = factory.Sequence(lambda n: "description%s" % n)
    due_date = "2019-01-01"
    group = factory.SubFactory(IssueGroupFactory)


class CommentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "project.Comment"

    text = factory.Sequence(lambda n: "comment%s" % n)
    user = factory.SubFactory(UserFactory)
    issue = factory.SubFactory(IssueFactory)


class IssueAttachmentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "project.IssueAttachment"

    file = factory.django.FileField(filename="test_file.pdf")
    issue = factory.SubFactory(IssueFactory)
