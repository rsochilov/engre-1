# Generated by Django 2.1.4 on 2019-05-30 12:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("project", "0010_auto_20190530_0925"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="category", options={"ordering": ["ordering"]},
        ),
        migrations.AlterModelOptions(
            name="industry", options={"ordering": ["ordering"]},
        ),
        migrations.RemoveIndex(
            model_name="category", name="project_cat_slug_bcdf93_idx",
        ),
        migrations.RemoveIndex(
            model_name="industry", name="project_ind_slug_adca95_idx",
        ),
    ]
