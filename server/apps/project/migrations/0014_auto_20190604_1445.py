# Generated by Django 2.1.4 on 2019-06-04 14:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("project", "0013_auto_20190603_1220"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="service",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="projects",
                to="project.IntellectualService",
            ),
        ),
    ]
