# Generated by Django 2.1.4 on 2019-02-06 11:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("project", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="task",
            name="name",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
