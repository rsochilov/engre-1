from core.utils import send_mail
from django.conf import settings
from django.template.loader import render_to_string
from marketplace.celery import app


@app.task
def send_partnership_email(data):
    html_message = render_to_string("partnership/emails/partnership.html", data)

    send_mail(
        settings.DEFAULT_FROM_EMAIL,
        f"{data.get('email')} / {data.get('service')}",
        data.get("partnership_email")
        if data.get("partnership_email") is not None
        else settings.PARTNERSHIP_EMAIL,
        html_message,
    )


@app.task
def send_partnership_email_to_user(data):
    html_message = render_to_string("partnership/emails/partnership_user.html", data)

    send_mail(
        settings.DEFAULT_FROM_EMAIL,
        f'We are proceeding you request №{data.get("request_number")}',
        data.get("email"),
        html_message,
    )
