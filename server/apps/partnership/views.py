from core.mixins import VueTemplateMixin
from core.serializers import NameSlugSerializer
from django.core.paginator import Paginator
from django.views.generic import DetailView, ListView
from django.views.generic.base import TemplateView
from django_filters.rest_framework import DjangoFilterBackend
from project.models import Industry
from rest_framework import generics
from rest_framework.filters import OrderingFilter
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response

from .models import ForInvestor, Software
from .serializers import (
    ForInvestorObjSerializer,
    InterestedRequestSerializer,
    PartnershipRequestSerializer,
)

from seo_tags.mixins import MetaTagTemplateMixin


class SoftwareList(MetaTagTemplateMixin, ListView):
    model = Software
    template_name = "partnership/software.html"
    context_object_name = "objects"

    def get_queryset(self):
        return Software.objects.all()


class SoftwareDetail(MetaTagTemplateMixin, DetailView):
    model = Software
    template_name = "partnership/software_detail.html"


class SoftwareSearch(MetaTagTemplateMixin, ListView):
    model = Software
    template_name = "partnership/software.html"
    context_object_name = "objects"

    def get_queryset(self):
        q = self.request.GET.get("q", None)
        if q:
            return Software.objects.filter(name__icontains=q)
        else:
            return Software.objects.all()

    def get_context_data(self, **kwargs):
        context = super(SoftwareSearch, self).get_context_data(**kwargs)
        context["search_query"] = self.request.GET.get("q", "")
        return context


class ForInvestorsTemplateView(VueTemplateMixin, TemplateView):
    template_name = "partnership/investors.html"

    def get_props(self):
        startups = ForInvestorObjSerializer(ForInvestor.objects.all(), many=True).data
        industries = NameSlugSerializer(Industry.objects.all(), many=True).data

        return {
            "startups": startups[:5],
            "industries": industries,
        }


class ForInvestorsListAPIView(generics.ListAPIView):
    queryset = ForInvestor.objects.all()
    serializer_class = ForInvestorObjSerializer
    pagination_class = LimitOffsetPagination
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filter_fields = ("amount", "industry")
    ordering = ("-created_at",)
    ordering_fields = ("created_at", "rating", "interested_requests")


class InterestedRequestAPIView(generics.UpdateAPIView):
    queryset = ForInvestor.objects.all()
    serializer_class = InterestedRequestSerializer


class ForInvestorsDetailView(VueTemplateMixin, DetailView):
    template_name = "partnership/investors-detailed.html"
    model = ForInvestor

    def get_props(self):
        startup = self.get_object()

        return {"startupId": startup.id}


class CreatePartnershipRequest(generics.CreateAPIView):
    serializer_class = PartnershipRequestSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        self.perform_create(serializer)

        return Response(status=201)
