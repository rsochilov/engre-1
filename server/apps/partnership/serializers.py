from core.models import PartnershipEmail
from core.serializers import NameSlugSerializer
from core.tasks import EngreEmails
from django.db.models import F
from rest_framework import serializers

from . import tasks
from .models import ForInvestor, PartnershipRequest


class ForInvestorObjSerializer(serializers.ModelSerializer):
    industry = NameSlugSerializer()
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return obj.get_absolute_url()

    class Meta:
        model = ForInvestor
        fields = "__all__"


class InterestedRequestSerializer(serializers.ModelSerializer):
    name = serializers.CharField(write_only=True)
    email = serializers.EmailField(write_only=True)
    phone = serializers.CharField(write_only=True)

    class Meta:
        model = ForInvestor
        fields = ("name", "email", "phone")

    def update(self, instance, validated_data):
        EngreEmails.send_interesting_email(
            validated_data, "partnership/emails/interesting.html"
        )

        instance.interested_requests = F("interested_requests") + 1

        return super().update(instance, validated_data)


class PartnershipRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartnershipRequest
        fields = "__all__"

    def create(self, validated_data):
        service = self.initial_data.get("service")
        instance = PartnershipRequest.objects.create(**validated_data)

        data = {
            "name": instance.name,
            "service": f"Partnership({service})",
            "request_number": instance.id,
            "email": instance.email,
            "company": instance.company,
            "phone": instance.phone,
            "text": instance.text,
        }

        if instance:
            tasks.send_partnership_email.delay(data)
            tasks.send_partnership_email_to_user.delay(data)
            if PartnershipEmail.objects.count() == 1:
                data["partnership_email"] = PartnershipEmail.get_solo().email
                tasks.send_partnership_email.delay(data)

        return instance
