from django.conf.urls import url

from . import views

urlpatterns = [
    url(r"learn-more/", views.CreateEducationRequest.as_view(), name="learn-more")
]
