from core.models import EducationEmail
from rest_framework import serializers

from . import tasks
from .models import EducationRequest


class EducationRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = EducationRequest
        fields = "__all__"

    def create(self, validated_data):
        service = self.initial_data.get("service")
        instance = EducationRequest.objects.create(**validated_data)
        data = {
            "name": instance.name,
            "service": f"Education({service})",
            "request_number": instance.id,
            "email": instance.email,
            "company": instance.company,
            "phone": instance.phone,
            "text": instance.text,
        }

        if instance:
            tasks.send_learn_more_email.delay(data)
            tasks.send_learn_more_info_to_user.delay(data)
            if EducationEmail.objects.count() == 1:
                data["learn_more_email"] = EducationEmail.get_solo().email
                tasks.send_learn_more_email.delay(data)

        return instance
