from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from .serializers import EducationRequestSerializer


class CreateEducationRequest(CreateAPIView):
    serializer_class = EducationRequestSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        self.perform_create(serializer)

        return Response(status=201)
