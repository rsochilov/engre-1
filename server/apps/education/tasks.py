from core.utils import send_mail
from django.conf import settings
from django.template.loader import render_to_string
from marketplace.celery import app


@app.task
def send_learn_more_email(data):
    html_message = render_to_string("education/emails/learn_more.html", data)

    send_mail(
        settings.DEFAULT_FROM_EMAIL,
        f"{data.get('email')} / {data.get('service')}",
        data.get("learn_more_email")
        if data.get("learn_more_email") is not None
        else settings.EDUCATION_LEARN_MORE_EMAIL,
        html_message,
    )


@app.task
def send_learn_more_info_to_user(data):
    html_message = render_to_string(
        "education/emails/learn_more_user.html",
        {"name": data.get("name"), "service": data.get("service")},
    )

    send_mail(
        settings.DEFAULT_FROM_EMAIL,
        f"We are proceeding you request №{data.get('request_number')}",
        data.get("email"),
        html_message,
    )
