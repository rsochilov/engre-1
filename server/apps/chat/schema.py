import graphene
from graphene_django.types import DjangoObjectType

from .models import ChatRoom


class ChatRoomType(DjangoObjectType):
    class Meta:
        model = ChatRoom


class Query(graphene.ObjectType):
    chat_rooms = graphene.List(ChatRoomType)

    def resolve_chat_rooms(self, info, **kwargs):
        return ChatRoom.objects.all()
