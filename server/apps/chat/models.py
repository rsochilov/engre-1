from accounts.models import User
from core.models import ArchivableMixin, WatchableMixin
from django.db import models

SENDING, SENT, READ, ERROR = 1, 2, 3, 4
MESSAGE_STATUSES = ((SENT, "Sent"), (READ, "Read"))


class ChatRoom(ArchivableMixin, WatchableMixin):
    blocked = models.BooleanField(default=False)
    blocked_by = models.ForeignKey(
        User, related_name="blocked_chats", on_delete=models.CASCADE, null=True
    )
    users = models.ManyToManyField(User, related_name="chats")
    blocked_users = models.ManyToManyField(User, related_name="blocked_in_chats")
    hidden_for = models.ForeignKey(
        User, related_name="hidden_chats", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return " - ".join(self.users.values_list("name", flat=True))

    def get_recipient(self, sender):
        try:
            return self.users.get(~models.Q(id=sender.id))
        except User.DoesNotExist:
            return None

    @property
    def unread_messages_count(self):
        return self.messages.filter(~models.Q(status=READ)).count()


def chat_room_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/chat_<id>/<filename>
    return "chat_{0}/{1}".format(instance.chat_room_id, filename)


class Message(WatchableMixin):
    MESSAGE, ATTACHMENT, START_REQUEST, START_WAITING = range(1, 5)
    START_ACCEPT, START_REJECT, SYSTEM, WARNING, INFO, SYSTEM_WARNING = range(5, 11)
    FINISH_REQUEST = 11

    WAITING_TEXT = "Waiting for confirmation"
    ACCEPT_TEXT = "Confirmation accepted"
    REJECT_TEXT = "Confirmation rejected"

    TYPES = (
        (MESSAGE, "message"),
        (ATTACHMENT, "attachment"),
        (START_REQUEST, "start project request"),
        (START_WAITING, "start project waiting"),
        (START_ACCEPT, "start project accept"),
        (START_REJECT, "start project reject"),
        (FINISH_REQUEST, "finish project request"),
    )  # yapf:disable

    type = models.PositiveSmallIntegerField(choices=TYPES, default=MESSAGE)
    chat_room = models.ForeignKey(
        ChatRoom, related_name="%(class)ss", on_delete=models.CASCADE
    )
    sender = models.ForeignKey(
        User, related_name="sent_messages", on_delete=models.CASCADE, null=True
    )
    message = models.TextField()
    status = models.PositiveSmallIntegerField(choices=MESSAGE_STATUSES, default=SENT)
    attachment = models.FileField(upload_to=chat_room_path)
    hidden_for = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    request = models.ForeignKey(
        "chat.ProjectRequest",
        on_delete=models.CASCADE,
        null=True,
        related_name="messages",
    )
    readed_at = models.DateTimeField(null=True)

    @property
    def recipient(self):
        if self.sender:
            return self.chat_room.users.get(~models.Q(id=self.sender.id))

        return None

    def __str__(self):
        return "{} - {}: {}".format(self.sender, self.recipient, self.message)

    class Meta:
        ordering = ["-created_at"]


class ProjectRequest(WatchableMixin):
    START, FINISH = 1, 2
    TYPES = ((START, "Start request"), (FINISH, "Finish request"))

    project = models.ForeignKey("project.Project", on_delete=models.CASCADE)
    is_accepted = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    type = models.PositiveSmallIntegerField(choices=TYPES)
