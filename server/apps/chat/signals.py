# from asgiref.sync import async_to_sync
# from channels.layers import get_channel_layer
# from django.db.models.signals import post_save
# from django.dispatch import receiver

# from .models import Message
# from notifications_api.models import Notification
# from notifications_api.serializers import NotificationSerializer


# @receiver(post_save, sender=Message, dispatch_uid="chat.set_chat_visible")
# def set_chat_visible(sender, instance, **kwargs):
#     recipient = instance.recipient

#     channel_layer = get_channel_layer()
#     new_notification = Notification.objects.create(
#         user=instance.recipient,
#         notification_type=Notification.CHAT,
#         text=f'Message from {instance.sender}'
#     )
#     data = NotificationSerializer(new_notification).data

#     async_to_sync(channel_layer.group_send)(
#         f'notific_{recipient.id}',
#         {
#             'type': 'new_notification',
#             'body': data
#         }
#     )
