from django.urls import path

from . import views

app_name = "chat"

urlpatterns = [
    path(
        "create-message/", views.CreateMessageAPIView.as_view(), name="create-message"
    ),
    path("update-message/<int:pk>/", views.UpdateMessageAPIView.as_view()),
    path("delete-message/<int:pk>/", views.DeleteMessageAPIView.as_view()),
    path(
        "create-attachment/",
        views.CreateAttachmentAPIView.as_view(),
        name="create-attachment",
    ),
    path("update-request/<int:pk>/", views.UpdateProjectRequestAPIView.as_view()),
    path("update-chat/<int:pk>/", views.UpdateChatRoomAPIView.as_view()),
    path("message-list/", views.MessagesListAPIView.as_view(), name="message-list"),
    path("archive-chat/<int:pk>/", views.ArchiveChatAPIView.as_view()),
    path(
        "redirect-to-chat/",
        views.RedirectToChatAPIView.as_view(),
        name="redirect-to-chat",
    ),
]
