import shutil
import tempfile

import pytest
from PIL import Image

from channels.testing import WebsocketCommunicator
from chat.models import READ, SENT, ChatRoom, Message
from core.factories import UserFactory
from django.conf import settings
from graphql_relay import to_global_id
from marketplace.routing import application
from rest_framework import status
from rest_framework.test import APIClient


def get_temporary_image(temp_file):
    size = (200, 200)
    color = (255, 0, 0, 0)
    image = Image.new("RGBA", size, color)
    image.save(temp_file, "png")
    return temp_file


def teardown_module(module):
    if settings.TESTING_SETTINGS:
        shutil.rmtree(settings.MEDIA_ROOT)


@pytest.fixture
def create_connection():
    async def _connect(sender=None, recipient=None):
        token = None
        token_name = "auth_token"

        if not recipient:
            # if no recipient then create a fake one
            recipient = UserFactory()

        if sender:
            token = sender.profile.active_token

        guid = to_global_id("UserType", recipient.id)

        cookie = "{}={};".format(token_name, token)
        headers = [(b"cookie", cookie.encode())]
        url = "/ws/chat/{}".format(guid)

        communicator = WebsocketCommunicator(application, url, headers)

        connected, _ = await communicator.connect()

        return connected, communicator

    return _connect


@pytest.mark.django_db
@pytest.mark.asyncio
async def test_connection_without_auth(create_connection):
    connected, communicator = await create_connection()

    assert not connected
    await communicator.disconnect()


@pytest.mark.django_db
@pytest.mark.asyncio
async def test_connection_with_auth(create_connection):
    user = UserFactory()

    connected, communicator = await create_connection(sender=user)

    assert connected
    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_message_and_chat_room_creation(create_connection):
    sender = UserFactory()
    recipient = UserFactory()

    _, sender_communicator = await create_connection(sender, recipient)
    _, recipient_communicator = await create_connection(recipient, sender)

    message = {"type": "chat", "message": "test"}

    # check message and chat room creation
    await sender_communicator.send_json_to(message)
    await recipient_communicator.receive_json_from(timeout=2)
    await sender_communicator.receive_json_from(timeout=2)

    assert Message.objects.filter(from_user=sender, to_user=recipient).exists()
    assert ChatRoom.objects.filter(users__in=[sender, recipient]).exists()

    # check if new message will be attach to the same chat room
    await sender_communicator.send_json_to(message)
    await recipient_communicator.receive_json_from(timeout=2)
    await sender_communicator.receive_json_from(timeout=2)

    assert (
        ChatRoom.objects.filter(users__in=[sender, recipient]).distinct().count() == 1
    )

    await sender_communicator.disconnect()
    await recipient_communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_message_delivery_between_users(create_connection):
    sender = UserFactory()
    recipient = UserFactory()

    sender_connected, sender_communicator = await create_connection(sender, recipient)
    recipient_connected, recipient_communicator = await create_connection(
        recipient, sender
    )

    assert sender_connected
    assert recipient_connected

    message = {"type": "chat", "message": "test"}

    # test message delivery user1 -> user2
    await sender_communicator.send_json_to(message)
    sender_response = await sender_communicator.receive_json_from(timeout=2)
    recipient_response = await recipient_communicator.receive_json_from(timeout=2)

    assert sender_response == {
        "message": "test",
        "from_user": to_global_id("UserType", sender.id),
    }
    assert recipient_response == {
        "message": "test",
        "from_user": to_global_id("UserType", sender.id),
    }

    message = {"type": "chat", "message": "test2"}

    # test message delivery user2 -> user1
    await recipient_communicator.send_json_to(message)
    sender_response = await sender_communicator.receive_json_from(timeout=2)
    recipient_response = await recipient_communicator.receive_json_from(timeout=2)

    assert sender_response == {
        "message": "test2",
        "from_user": to_global_id("UserType", recipient.id),
    }
    assert recipient_response == {
        "message": "test2",
        "from_user": to_global_id("UserType", recipient.id),
    }

    await sender_communicator.disconnect()
    await recipient_communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_message_delivery_status(create_connection):
    sender = UserFactory()
    recipient = UserFactory()
    sender_connected, sender_communicator = await create_connection(sender, recipient)
    recipient_connected, recipient_communicator = await create_connection(
        recipient, sender
    )

    assert sender_connected
    assert recipient_connected

    message = {"type": "chat", "message": "test"}

    # test message delivery status when recipient online
    await sender_communicator.send_json_to(message)
    await sender_communicator.receive_json_from(timeout=2)
    await recipient_communicator.receive_json_from(timeout=2)

    assert (
        Message.objects.filter(from_user=sender, to_user=recipient).last().status
        == READ
    )

    await recipient_communicator.disconnect()

    # test message delivery status when recipient offline
    await sender_communicator.send_json_to(message)
    await sender_communicator.receive_json_from(timeout=2)

    assert (
        Message.objects.filter(from_user=sender, to_user=recipient).last().status
        == SENT
    )

    # connect recipient again
    recipient_connected, recipient_communicator = await create_connection(
        recipient, sender
    )

    assert (
        Message.objects.filter(from_user=sender, to_user=recipient).last().status
        == READ
    )

    await sender_communicator.disconnect()
    await recipient_communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_connection_when_user_blocked(create_connection):
    sender = UserFactory()
    recipient = UserFactory()

    # add sender to recipient's blocked list
    recipient.profile.blocked_users.add(sender)

    connected, communicator = await create_connection(sender, recipient)

    assert not connected

    await communicator.disconnect()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_attachments(create_connection):
    sender = UserFactory()
    recipient = UserFactory()

    _, sender_communicator = await create_connection(sender, recipient)
    _, recipient_communicator = await create_connection(recipient, sender)

    chat_room = ChatRoom.objects.create()
    chat_room.users.add(sender, recipient)

    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION="JWT " + sender.profile.active_token)
    url = "/chat/upload-file/{}".format(to_global_id("ChatRoomType", chat_room.id))

    temp_file = tempfile.NamedTemporaryFile()

    attachment = get_temporary_image(temp_file)
    http_response = client.post(url, {"attachment": attachment}, format="multipart")

    assert http_response.status_code == status.HTTP_200_OK
    assert chat_room.attachments.count() == 1

    await sender_communicator.disconnect()
    await recipient_communicator.disconnect()
