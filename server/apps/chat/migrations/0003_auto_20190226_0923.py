# Generated by Django 2.1.4 on 2019-02-26 09:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


def updated_created_by(apps, schema_editor):
    ChatRoom = apps.get_model("chat", "ChatRoom")

    for instance in ChatRoom.objects.all():
        instance.created_by = instance.users.first()
        instance.save()


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("chat", "0002_auto_20190220_0944"),
    ]

    operations = [
        migrations.RunPython(
            updated_created_by, reverse_code=migrations.RunPython.noop
        ),
        migrations.AlterModelOptions(
            name="message", options={"ordering": ["created_at"]},
        ),
        migrations.AddField(
            model_name="chatroom",
            name="created_by",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.CASCADE,
                to=settings.AUTH_USER_MODEL,
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="chatroom",
            name="is_draft",
            field=models.BooleanField(default=False),
        ),
    ]
