# Generated by Django 2.1.4 on 2019-05-24 07:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("project", "0009_auto_20190502_0910"),
        ("chat", "0038_chatroom_hidden_for"),
    ]

    operations = [
        migrations.CreateModel(
            name="CloseProjectRequest",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created_at", models.DateTimeField(auto_now_add=True, null=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                ("is_accepted", models.BooleanField(default=False)),
                ("is_rejected", models.BooleanField(default=False)),
                (
                    "project",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="project.Project",
                    ),
                ),
            ],
            options={"abstract": False,},
        ),
    ]
