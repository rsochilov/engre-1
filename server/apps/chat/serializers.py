from datetime import date, datetime

from django.db.models import Q
from django.conf import settings
from django.urls import reverse
from asgiref.sync import async_to_sync
from rest_framework import serializers
from channels.layers import get_channel_layer

from .models import ChatRoom, Message, ProjectRequest
from accounts.models import User
from core.models import ACTIVE, CLOSED, NOT_ACTIVE, StopWord
from core.utils import Notifier
from project.models import IntellectualService, Project, ProjectUsers
from notifications_api.models import Notification


class UserSerializer(serializers.ModelSerializer):
    last_seen = serializers.SerializerMethodField()
    online = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ("id", "name", "picture", "last_seen", "online")

    def get_last_seen(self, obj):
        last_seen = obj.last_seen()

        if last_seen:
            return last_seen.isoformat()

        return None

    def get_online(self, obj):
        return obj.online()


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ("id", "name", "budget")


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = IntellectualService
        fields = ("id", "name", "hourly_rate")


class ProjectRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectRequest
        fields = ("id", "project", "is_accepted", "is_rejected")

    def update(self, instance, validated_data):

        request = self.context.get("request")
        channel_layer = get_channel_layer()
        project_request = super().update(instance, validated_data)
        chat_room_id = set(
            project_request.messages.values_list("chat_room", flat=True)
        ).pop()
        chat_room = ChatRoom.objects.get(pk=chat_room_id)
        recipient = chat_room.get_recipient(request.user)

        if (
            validated_data.get("is_accepted")
            and instance.type == ProjectRequest.START
            and not recipient.is_pm
            and not self.initial_data.get("service_id")
        ):
            raise serializers.ValidationError("No service_id provided")

        for user in request.user, recipient:
            async_to_sync(channel_layer.group_send)(
                str(user.id),
                {
                    "type": "messages_update",
                    "chat": chat_room_id,
                    "body": {
                        "messages": list(
                            project_request.messages.values_list("id", flat=True)
                        ),
                        "request": {
                            "is_accepted": project_request.is_accepted,
                            "is_rejected": project_request.is_rejected,
                        },
                    },
                },
            )

        if validated_data.get("is_accepted"):
            message = Message.objects.create(
                type=Message.START_ACCEPT,
                message=Message.ACCEPT_TEXT,
                chat_room=chat_room,
                sender=request.user,
                request=project_request,
            )

            project = validated_data.get("project")

            if instance.type == ProjectRequest.START:
                project.date_start = date.today()
                if recipient.is_pm:
                    project.pm = recipient.projectmanager
                else:
                    service_id = self.initial_data.get("service_id")
                    project.service = IntellectualService.objects.get(pk=service_id)

                project.status = ACTIVE
            elif instance.type == ProjectRequest.FINISH:
                project.date_finish = date.today()
                project.status = CLOSED

            project.save()
            ProjectUsers.objects.create(project=project, user=request.user)
            ProjectUsers.objects.create(project=project, user=recipient)

        elif validated_data.get("is_rejected"):
            message = Message.objects.create(
                type=Message.START_REJECT,
                message=Message.REJECT_TEXT,
                chat_room=chat_room,
                sender=request.user,
                request=project_request,
            )

        for user in request.user, recipient:
            async_to_sync(channel_layer.group_send)(
                str(user.id),
                {
                    "type": "message",
                    "chat": chat_room_id,
                    "body": {**MessageSerializer(message).data},
                },
            )

        return project_request


class MessageSerializer(serializers.ModelSerializer):
    request = ProjectRequestSerializer(required=False)
    attachment = serializers.CharField(required=False)
    sender_id = serializers.IntegerField()
    fake_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Message
        fields = [
            "id",
            "message",
            "sender_id",
            "created_at",
            "status",
            "attachment",
            "type",
            "request",
            "chat_room",
            "fake_id",
            "hidden_for",
        ]
        read_only_fields = ("hidden_for",)

    def validate(self, data):
        request = self.context.get("request")
        chat_room = data.get("chat_room")

        if chat_room:
            recipient = chat_room.get_recipient(request.user)
            channel_layer = get_channel_layer()

            if (
                recipient.is_customer
                and chat_room.unread_messages_count >= 3
                and chat_room.messages.count() == 3
            ):
                async_to_sync(channel_layer.group_send)(
                    str(request.user.id),
                    {
                        "type": "room_update",
                        "chat": chat_room.id,
                        "body": {"blocked": True, "blocked_by": None},
                    },
                )
                chat_room.blocked_users.add(request.user)
                raise serializers.ValidationError("Customer has 3 unread messages")

            elif recipient.is_contractor and chat_room.unread_messages_count >= 3:
                chat_room.blocked_users.clear()
                async_to_sync(channel_layer.group_send)(
                    str(recipient.id),
                    {
                        "type": "room_update",
                        "chat": chat_room.id,
                        "body": {"blocked": False, "blocked_by": None},
                    },
                )
        return data

    def create(self, validated_data):
        request = self.context.get("request")
        msg_type = validated_data.get("type")
        channel_layer = get_channel_layer()
        fake_id = validated_data.pop("fake_id")
        chat_room = validated_data.get("chat_room")

        if msg_type == Message.START_REQUEST or msg_type == Message.FINISH_REQUEST:
            # if start project reqest then create a StartProjectRequest instance
            # and create info message about waiting for project start confirmation

            if msg_type == Message.START_REQUEST:
                project_request_type = ProjectRequest.START
            elif msg_type == Message.FINISH_REQUEST:
                project_request_type = ProjectRequest.FINISH

            request_data = validated_data.pop("request")
            project_request = ProjectRequest.objects.create(
                type=project_request_type, project=request_data["project"]
            )
            validated_data["request"] = project_request
            waiting_msg = Message(
                chat_room=chat_room,
                message=Message.WAITING_TEXT,
                type=Message.START_WAITING,
                request=project_request,
                sender_id=validated_data.get("sender_id"),
            )

            message = super().create(validated_data)
            waiting_msg.save()

        else:
            message = super().create(validated_data)

        recipient = chat_room.get_recipient(request.user)

        for user in request.user, recipient:
            async_to_sync(channel_layer.group_send)(
                str(user.id),
                {
                    "type": "message",
                    "chat": chat_room.id,
                    "body": {**MessageSerializer(message).data, "fake_id": fake_id,},
                },
            )

        self.create_notification(message, chat_room)

        # send info about waiting for project start confirmation
        if msg_type == Message.START_REQUEST:
            for user in request.user, recipient:
                async_to_sync(channel_layer.group_send)(
                    str(user.id),
                    {
                        "type": "message",
                        "chat": chat_room.id,
                        "body": {
                            **MessageSerializer(waiting_msg).data,
                            "fake_id": fake_id + 1,
                        },
                    },
                )

        for word in StopWord.objects.values_list("word", flat=True):
            if message.message.find(word) > -1:
                if Message.objects.filter(
                    chat_room=chat_room, type=Message.SYSTEM_WARNING
                ).exists():
                    chat_room.blocked = True
                    chat_room.save()

                    for user in recipient, request.user:
                        async_to_sync(channel_layer.group_send)(
                            str(user.id),
                            {
                                "type": "room_update",
                                "chat": chat_room.id,
                                "body": {"blocked": True,},
                            },
                        )
                else:
                    warning = Message.objects.create(
                        type=Message.SYSTEM_WARNING,
                        message="The first warning. You violate the conditions of using chat.",
                        chat_room=chat_room,
                        hidden_for=recipient,
                    )

                    async_to_sync(channel_layer.group_send)(
                        str(request.user.id),
                        {
                            "type": "message",
                            "chat": chat_room.id,
                            "body": {**MessageSerializer(warning).data,},
                        },
                    )
                break  # exit from loop
        return message

    def create_notification(self, instance: Message, chat_room: ChatRoom):
        Notifier.create_notification(
            user=instance.recipient,
            notification_body=str(chat_room.id),
            text=f'You have unread messages from {instance.sender}!\nFollow this link to check your chats: https://{settings.SITE_URL}{reverse("chat")}',
            notification_type=Notification.CHAT,
        )

    def update(self, instance, validated_data):
        request = self.context.get("request")
        channel_layer = get_channel_layer()
        recipient = instance.chat_room.get_recipient(request.user)

        for user in request.user, recipient:
            async_to_sync(channel_layer.group_send)(
                str(user.id),
                {
                    "type": "messages_update",
                    "chat": instance.chat_room.id,
                    "body": {
                        "messages": [instance.id],
                        "status": validated_data.get("status"),
                    },
                },
            )
        validated_data["readed_at"] = datetime.now()
        return super().update(instance, validated_data)


class ChatRoomSerializer(serializers.ModelSerializer):
    messages = serializers.SerializerMethodField()
    recipient = serializers.SerializerMethodField()
    projects = serializers.SerializerMethodField()
    services = serializers.SerializerMethodField()
    has_open_request = serializers.SerializerMethodField()

    class Meta:
        model = ChatRoom
        fields = (
            "id",
            "created_at",
            "messages",
            "recipient",
            "projects",
            "services",
            "has_open_request",
            "blocked",
            "blocked_by",
            "blocked_users",
            "hidden_for",
        )

    def get_messages(self, obj):
        return MessageSerializer(
            list(reversed(obj.messages.all()[:20])), many=True
        ).data

    def get_recipient(self, obj):
        request = self.context.get("request")

        if request:
            return UserSerializer(obj.get_recipient(request.user)).data

        return None

    def get_projects(self, obj):
        projects = []
        user = self.context.get("request").user
        recipient = obj.get_recipient(user)

        if user.is_pm or user.is_contractor or not recipient:
            return []

        if recipient.is_pm:
            projects = user.work.company.projects.filter(
                pm=recipient, status=NOT_ACTIVE
            )

        elif recipient.is_contractor:
            projects = user.work.company.projects.filter(
                management_option=Project.DIRECT, status=NOT_ACTIVE
            )

        return ProjectSerializer(projects, many=True).data

    def get_services(self, obj):
        services = []
        user = self.context.get("request").user
        recipient = obj.get_recipient(user)

        if user.is_customer or not recipient:
            return []

        if recipient.is_customer and not user.is_pm:
            services = user.work.company.intellectualservices.all()

        if recipient.is_customer and user.is_pm:
            for worker in user.projectmanager.membership.all():
                for service in worker.company.intellectualservices.all():
                    services.append(service)

        return ServiceSerializer(services, many=True).data

    def get_has_open_request(self, obj):
        return obj.messages.filter(
            Q(request__isnull=False)
            & Q(request__is_accepted=False)
            & Q(request__is_rejected=False)
        ).exists()

    def update(self, instance, validated_data):
        request = self.context.get("request")
        room = super().update(instance, validated_data)
        channel_layer = get_channel_layer()

        recipient = room.get_recipient(request.user)

        for user in request.user, recipient:
            async_to_sync(channel_layer.group_send)(
                str(user.id),
                {
                    "type": "room_update",
                    "chat": room.id,
                    "body": {
                        "blocked": room.blocked,
                        "blocked_by": getattr(room.blocked_by, "id", None),
                    },
                },
            )

        message = None

        if validated_data.get("blocked"):
            message = Message.objects.create(
                type=Message.WARNING,
                message=f"The chat was blocked by {request.user.name}",
                chat_room=room,
            )

        elif validated_data.get("blocked") is False:
            message = Message.objects.create(
                type=Message.INFO,
                message=f"The chat was unblocked by {request.user.name}",
                chat_room=room,
            )

        if message:
            for user in request.user, recipient:
                async_to_sync(channel_layer.group_send)(
                    str(user.id),
                    {
                        "type": "message",
                        "chat": room.id,
                        "body": {**MessageSerializer(message).data},
                    },
                )

        return room


class AttachmentSerializer(serializers.ModelSerializer):
    chat_room_id = serializers.IntegerField(write_only=True)
    fake_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Message
        fields = (
            "id",
            "attachment",
            "sender_id",
            "chat_room_id",
            "status",
            "type",
            "fake_id",
        )

    def create(self, validated_data):
        channel_layer = get_channel_layer()
        request = self.context.get("request")

        validated_data["sender"] = request.user
        validated_data["type"] = Message.ATTACHMENT
        fake_id = validated_data.pop("fake_id")

        message = Message.objects.create(**validated_data)
        recipient = message.chat_room.get_recipient(request.user)

        for user in request.user, recipient:
            async_to_sync(channel_layer.group_send)(
                str(user.id),
                {
                    "type": "attachment",
                    "chat": message.chat_room_id,
                    "body": {
                        "id": message.id,
                        "type": message.type,
                        "fake_id": fake_id,
                        "attachment": message.attachment.url,
                        "created_at": message.created_at.isoformat(),
                        "status": message.status,
                        "sender_id": request.user.id,
                    },
                },
            )

        return message


class ArchiveChatRoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatRoom
        fields = ("id",)

    def update(self, instance, validated_data):
        request = self.context.get("request")

        request.user.archived_chats.add(instance)

        return super().update(instance, validated_data)
