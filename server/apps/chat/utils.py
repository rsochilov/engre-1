from channels.db import database_sync_to_async

from .models import ChatRoom


class ClientError(Exception):
    def __init__(self, code):
        super().__init__(code)
        self.code = code


@database_sync_to_async
def get_rooms_or_error(user, room_id=None):
    """
    Tries to fetch a rooms for the user.
    """
    if not user.is_authenticated:
        raise ClientError("USER_HAS_TO_LOGIN")

    if room_id:
        try:
            room = ChatRoom.objects.get(id=room_id)
            if not room.users.filter(id=user.id).exists():
                raise ClientError("USER_INVALID")
        except ChatRoom.DoesNotExist:
            raise ClientError("ROOMS_INVALID")
        return room
    else:
        rooms = ChatRoom.objects.filter(users=user)
        if not rooms.exists():
            raise ClientError("ROOMS_INVALID")
        return rooms
