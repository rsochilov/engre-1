from django.apps import AppConfig


class NotificationsApiConfig(AppConfig):
    name = "notifications_api"
