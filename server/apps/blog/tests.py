import glob
import json
import os

from blog.models import BlogArticle, BlogComment, BlogCommentRating
from blog.schema import BlogArticleType, CategoryType
from core.models import Category, Language, Tag
from core.schema import LanguageType, TagType
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse
from graphql_jwt.shortcuts import get_token
from graphql_relay import to_global_id
from kombu.utils import json


class BlogTestCase(TestCase):
    def setUp(self):
        self.test_username = "testuser1"
        self.test_password = "12345678"
        self.test_email = "marketplace.ddi.test1@gmail.com"
        self.test_user = User.objects.create_user(
            username=self.test_username,
            password=self.test_password,
            email=self.test_email,
        )
        token = get_token(self.test_user)
        self.test_user.profile.active_token = token
        self.test_user.profile.save()
        self.test_user_glob_id = to_global_id(User.__name__, self.test_user.id)

        _tag = [Tag.objects.create(name="tag"), Tag.objects.create(name="test")]
        self.glob_tags = json.dumps(
            [to_global_id(TagType.__name__, i.id) for i in _tag]
        )

        _languages = [
            Language.objects.create(name="ENG"),
            Language.objects.create(name="FR"),
        ]
        self.glob_language_EN = to_global_id(LanguageType.__name__, _languages[0].id)
        self.glob_languages = json.dumps(
            [to_global_id(LanguageType.__name__, i.id) for i in _languages]
        )

        _category = Category.objects.create(name="cat")
        self.glob_category = to_global_id(CategoryType.__name__, _category.id)

        _picture = SimpleUploadedFile(
            "test_file.png", b"file_content", content_type="image/png"
        )

        _title = "title_"

        _content = "content" * 25

        self.test_blog_article1 = BlogArticle.objects.create(
            author=self.test_user, title=_title * 2, content=_content, picture=_picture
        )
        self.test_blog_article_glob1 = to_global_id(
            BlogArticleType.__name__, self.test_blog_article1.id
        )
        self.test_blog_article1.languages.add(*_languages)
        self.test_blog_article1.tags.add(*_tag)
        self.test_blog_article1.categories.add(_category)
        self.test_blog_article1.save()

        self.test_blog_article2 = BlogArticle.objects.create(
            author=self.test_user, title=_title * 3, content=_content, picture=_picture
        )
        self.test_blog_article_glob2 = to_global_id(
            BlogArticleType.__name__, self.test_blog_article2.id
        )
        self.test_blog_article2.languages.add(*_languages)
        self.test_blog_article2.tags.add(*_tag)
        self.test_blog_article2.categories.add(_category)
        self.test_blog_article2.save()

        comment1_0 = BlogComment.objects.create(
            blog_article=self.test_blog_article1,
            user=self.test_user,
            comment="comment1_0",
            parent=None,
        )
        BlogComment.objects.create(
            blog_article=self.test_blog_article1,
            user=self.test_user,
            comment="comment2_0",
            parent=None,
        )
        comment1_1 = BlogComment.objects.create(
            blog_article=self.test_blog_article1,
            user=self.test_user,
            comment="comment1_1",
            parent=comment1_0,
        )
        BlogComment.objects.create(
            blog_article=self.test_blog_article1,
            user=self.test_user,
            comment="comment1_2",
            parent=comment1_0,
        )
        BlogComment.objects.create(
            blog_article=self.test_blog_article1,
            user=self.test_user,
            comment="comment1_3",
            parent=comment1_0,
        )
        comment1_1_1 = BlogComment.objects.create(
            blog_article=self.test_blog_article1,
            user=self.test_user,
            comment="comment1_1_1",
            parent=comment1_1,
        )
        comment1_1_1_1 = BlogComment.objects.create(
            blog_article=self.test_blog_article1,
            user=self.test_user,
            comment="comment1_1_1_1",
            parent=comment1_1_1,
        )

        for i, k in enumerate(range(1, 25)):
            _user = User.objects.create_user(
                username=f"rate_user{k}",
                password=self.test_password,
                email=f"rate_user_email{k}@gmail.com",
            )
            _user.save()
            token = get_token(_user)
            _user.profile.active_token = token
            _user.profile.save()
            BlogCommentRating.objects.create(
                user=_user, blog_comment=comment1_0, rate=(i % 2)
            )

        self.test_comment_for_unrate = comment1_1_1_1
        self.test_parent_comment_glob_id = to_global_id(
            BlogComment.__name__, comment1_1_1_1.id
        )

        self.test_title_for_add = "test_new_title" * 3
        self.test_content_for_add = "test_new_content" * 20
        self.test_picture_for_add = SimpleUploadedFile(
            "test_new_file.png", b"file_content", content_type="image/png"
        )

        self.test_title_for_update = "test_updated_title" * 3
        self.test_content_for_update = "test_updated_content" * 20
        self.test_picture_for_update = SimpleUploadedFile(
            "test_updated_file.png", b"file_content", content_type="image/png"
        )

        _updated_category = Category.objects.create(name="updated_cat")
        self.glob_updated_category = to_global_id(
            CategoryType.__name__, _updated_category.id
        )

        _updated_tag = [
            Tag.objects.create(name="updated_tag"),
            Tag.objects.create(name="updated_test"),
        ]
        self.glob_updated_tags = json.dumps(
            [to_global_id(TagType.__name__, i.id) for i in _updated_tag]
        )

    def tearDown(self):
        for filename in glob.glob(
            os.path.join(settings.MEDIA_ROOT, "blog_images/test_file*")
        ):
            os.remove(filename)

    def test_import_apps(self):
        try:
            import blog.apps

            raise ImportError
        except ImportError:
            self.assertTrue(True)

    def test_get_blog_article_by_id(self):
        query = (
            """
          query {
            blogArticle(id: "%s") {
              title
              content
              author {
                username
              }
            }
          }
        """
            % self.test_blog_article_glob2
        )
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_blog_article_by_wrong_id(self):
        query = (
            """
          query {
            blogArticle(id: "%s") {
              title
              content
              author {
                username
              }
            }
          }
        """
            % "x"
            * 20
        )
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertIn("errors", response.json())
        self.assertEqual(response.status_code, 400)

    def test_get_all_blog_articles(self):
        query = """
          query {
            allBlogArticles {
              edges {
                node {
                  id
                  title
                  picture
                  author {
                    username
                    email
                  }
                  tags {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                  categories {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                  languages {
                     edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                }
              }
            }
          }
        """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_blog_articles_by_categories(self):
        query = """
          query {
            allBlogArticles(categories_Name : "cat") {
              edges {
                node {
                  id
                  title
                  picture
                  author {
                    username
                    email
                  }
                  tags {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                  categories {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                  languages {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                }
              }
            }
          }
        """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_blog_articles_by_tags(self):
        query = """
          query {
            allBlogArticles(tags_Name_In: "test,tag") {
              edges {
                node {
                  id
                  title
                  picture
                  author {
                    username
                    email
                  }
                  tags {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                  categories {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                  languages {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                }
              }
            }
          }
        """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_blog_articles_by_language(self):
        query = """
          query {
            allBlogArticles(languages_Name: "ENG") {
              edges {
                node {
                  id
                  title
                  picture
                  author {
                    username
                    email
                  }
                  tags {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                  categories {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                  languages {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                }
              }
            }
          }
        """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_blog_articles_by_author_id(self):
        query = (
            """
          query {
            allBlogArticles(author: "%s") {
              edges {
                node {
                  id
                  title
                  picture
                  author {
                    username
                    email
                  }
                  tags {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                  categories {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                  languages {
                    edges {
                      node {
                        name
                        id
                      }
                    }
                  }
                }
              }
            }
          }
        """
            % self.test_user_glob_id
        )

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()["data"]["allBlogArticles"]["edges"]), 2)

    def test_create_blog_article(self):
        query = """
          mutation {
            createBlogArticle (
            title: "%s", content: "%s", languages: %s, tags: %s, categories: "%s" , picture: {file: "%s", name: "%s"}) {
              errors
              blogArticle {
                id
                title
                content
                tags {
                  edges {
                    node {
                      name
                      id
                    }
                  }
                }
                categories {
                  edges {
                    node {
                      name
                      id
                    }
                  }
                }
                languages {
                  edges {
                    node {
                      name
                      id
                    }
                  }
                }
              }
            }
          }
        """ % (
            self.test_title_for_add,
            self.test_content_for_add,
            self.glob_languages,
            self.glob_tags,
            self.glob_category,
            self.test_picture_for_add.file,
            self.test_picture_for_add.name,
        )

        token = self.test_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(response.json()["data"]["createBlogArticle"]["errors"])
        self.assertEqual(
            response.json()["data"]["createBlogArticle"]["blogArticle"]["title"],
            self.test_title_for_add,
        )

    def test_update_blog_article(self):
        query = """
          mutation {
            updateBlogArticle (
            blogArticleGlobId: "%s" ,title: "%s", content: "%s", languages: %s, tags: %s, categories: "%s" ,
            picture: {file: "%s", name: "%s"}) {
              errors
              blogArticle {
                id
                title
                content
                tags {
                  edges {
                    node {
                      name
                      id
                    }
                  }
                }
                categories {
                  edges {
                    node {
                      name
                      id
                    }
                  }
                }
                languages {
                  edges {
                    node {
                      name
                      id
                    }
                  }
                }
              }
            }
          }
        """ % (
            self.test_blog_article_glob1,
            self.test_title_for_update,
            self.test_content_for_update,
            self.glob_languages,
            self.glob_updated_tags,
            self.glob_updated_category,
            self.test_picture_for_update.file,
            self.test_picture_for_update.name,
        )

        token = self.test_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(response.json()["data"]["updateBlogArticle"]["errors"])
        self.assertEqual(
            response.json()["data"]["updateBlogArticle"]["blogArticle"]["title"],
            self.test_title_for_update,
        )

    def test_get_blog_article_comments(self):
        query = (
            """
          query {
            blogArticleComments(blogArticleGlobId: "%s")
          }
        """
            % self.test_blog_article_glob1
        )

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_add_blog_article_comment_to_root(self):
        query = (
            """
          mutation {
            createBlogComment (blogArticleGlobId : "%s", comment: "fresh_root_comment") {
              errors
              blogComment {
                id
                comment
                createdAt
                parent {
                  id
                }
              }
            }
          }
        """
            % self.test_blog_article_glob1
        )

        token = self.test_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        self.assertIsNone(response.json()["data"]["createBlogComment"]["errors"])
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_add_blog_article_comment_to_comment(self):
        query = """
          mutation {
            createBlogComment(blogArticleGlobId : "%s", comment: "comment1_1_1_1_1", parentCommentGlobId: "%s") {
              errors
              blogComment {
                id
                comment
                createdAt
                parent {
                  id
                }
              }
            }
          }
        """ % (
            self.test_blog_article_glob1,
            self.test_parent_comment_glob_id,
        )
        token = self.test_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        self.assertIsNone(response.json()["data"]["createBlogComment"]["errors"])
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_rate_blog_comment(self):
        query = (
            """
          mutation {
            rateBlogComment(blogCommentGlobId: "%s") {
              errors
              blogComment {
                id
                comment
                rating
                user {
                  username
                }
              }
            }
          }
        """
            % self.test_parent_comment_glob_id
        )

        token = self.test_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        self.assertIsNone(response.json()["data"]["rateBlogComment"]["errors"])
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)

    def test_unrate_blog_comment(self):
        before = self.test_comment_for_unrate.blog_comment_rating.filter(rate=1).count()
        self.test_comment_for_unrate.blog_comment_rating.create(
            user=self.test_user, rate=1
        )

        query = (
            """
          mutation {
            rateBlogComment(blogCommentGlobId: "%s") {
              errors
              blogComment {
                id
                comment
                rating
                user {
                  username
                }
              }
            }
          }
        """
            % self.test_parent_comment_glob_id
        )

        token = self.test_user.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        after = self.test_comment_for_unrate.blog_comment_rating.filter(rate=1).count()
        self.assertIsNone(response.json()["data"]["rateBlogComment"]["errors"])
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(before, after)
