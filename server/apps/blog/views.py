from os import path
from django.core.paginator import Paginator
from django.template.loader import render_to_string
from django.views.generic import View
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from rest_framework import generics
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .filters import (
    BlogArticleFilterSet,
    BlogArticleSettingsFilterSet,
    BlogQuestionFilterSet,
)
from .models import Answer, BlogArticle, BlogCategory, Comment, Question
from .pagination import BlogPagination
from .serializers import (
    AnswerSerializer,
    ArticleSerializer,
    BlogArticleSerializer,
    BlogCategorySerializer,
    CommentSerializer,
    DislikeAnswerSerializer,
    DislikeCommentSerializer,
    DislikeQuestionSerializer,
    InterestArticleSerializer,
    LikeAnswerSerializer,
    LikeCommentSerializer,
    LikeQuestionSerializer,
    QuestionConfigurationSerializer,
    QuestionSerializer,
    RateQuestionSerializer,
)
from accounts.models import Blogger
from accounts.serializers import BloggerSerializer, UserSerializer
from seo_tags.mixins import BlogArticleMetaTagTemplateMixin
from core.mixins import VueTemplateMixin
from core.utils import get_image_from_base64, send_mail, SitemapArticleWriter


class BlogTemplateView(VueTemplateMixin, TemplateView):
    template_name = "blogs/list.html"

    def get_props(self):
        if self.request.user.is_authenticated:
            user = UserSerializer(self.request.user).data
        else:
            user = None

        context = {"request": self.request, "filters": self.request.GET}

        bloggers = BloggerSerializer(
            Blogger.objects.all(), many=True, context=context
        ).data

        categories = BlogCategorySerializer(
            BlogCategory.objects.all(), many=True, context=context
        ).data

        question_paginator = Paginator(Question.objects.visible(self.request.user), 5)

        questions = QuestionSerializer(
            question_paginator.get_page(1).object_list, many=True, context=context
        ).data
        has_more_questions = question_paginator.get_page(1).has_next()

        return {
            "categories": categories,
            "bloggers": bloggers,
            "user": user,
            "questions": questions,
            "hasMoreQuestions": has_more_questions,
        }


class BlogArticleDetailView(
    BlogArticleMetaTagTemplateMixin, VueTemplateMixin, DetailView
):
    template_name = "blogs/article.html"
    model = BlogArticle

    def get_props(self):
        context = {"request": self.request}

        if self.request.user.is_authenticated:
            user = UserSerializer(self.request.user).data
        else:
            user = None

        return {
            "user": user,
            "article": ArticleSerializer(self.object, context=context).data,
            "comments": CommentSerializer(
                Comment.objects.filter(article=self.object, parent__isnull=True),
                many=True,
                context=context,
            ).data,
        }


class BlogArticleListAPIView(generics.ListAPIView):
    queryset = BlogArticle.objects.all()
    serializer_class = ArticleSerializer
    filterset_class = BlogArticleFilterSet
    pagination_class = BlogPagination


class ArticleListAPIView(generics.ListAPIView):
    queryset = BlogArticle.objects.all()
    serializer_class = BlogArticleSerializer
    filterset_class = BlogArticleSettingsFilterSet
    pagination_class = BlogPagination


class LikeCommentAPIView(generics.UpdateAPIView):
    queryset = Comment.objects.all()
    serializer_class = LikeCommentSerializer
    permission_classes = (IsAuthenticated,)


class DislikeCommentAPIView(generics.UpdateAPIView):
    queryset = Comment.objects.all()
    serializer_class = DislikeCommentSerializer
    permission_classes = (IsAuthenticated,)


class InterestArticleAPIView(generics.UpdateAPIView):
    queryset = BlogArticle.objects.all()
    serializer_class = InterestArticleSerializer
    permission_classes = (IsAuthenticated,)


class CreateCommentAPIView(generics.CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticated,)


class LikeQuestionAPIView(generics.UpdateAPIView):
    queryset = Question.objects.all()
    serializer_class = LikeQuestionSerializer
    permission_classes = (IsAuthenticated,)


class DislikeQuestionAPIView(generics.UpdateAPIView):
    queryset = Question.objects.all()
    serializer_class = DislikeQuestionSerializer
    permission_classes = (IsAuthenticated,)


class CreateQuestionAPIView(generics.CreateAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = (IsAuthenticated,)


class UpdateQuestionConfiguration(generics.UpdateAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionConfigurationSerializer
    permission_classes = (IsAuthenticated,)


class QuestionListAPIView(generics.ListAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    filterset_class = BlogQuestionFilterSet
    pagination_class = LimitOffsetPagination


class RateQuestionAPIView(generics.UpdateAPIView):
    queryset = Question.objects.all()
    serializer_class = RateQuestionSerializer


class CreateAnswerAPIView(generics.CreateAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = (IsAuthenticated,)


class AnswerListAPIView(generics.ListAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    filter_fields = ("question",)


class LikeAnswerAPIView(generics.UpdateAPIView):
    queryset = Answer.objects.all()
    serializer_class = LikeAnswerSerializer
    permission_classes = (IsAuthenticated,)


class DislikeAnswerAPIView(generics.UpdateAPIView):
    queryset = Answer.objects.all()
    serializer_class = DislikeAnswerSerializer
    permission_classes = (IsAuthenticated,)


class ArticleViewSet(
    generics.CreateAPIView, generics.UpdateAPIView, generics.DestroyAPIView
):
    serializer_class = BlogArticleSerializer

    def get_object(self):
        return BlogArticle.objects.get(pk=self.request.data.get("pk"))

    def create(self, request, *args, **kwargs):
        picture = request.data.get("picture")

        if picture:
            del request.data["picture"]

        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        instance = self.perform_create(serializer)
        instance.picture = get_image_from_base64(instance.title, picture)
        instance.save()

        new_blog_abs_url = instance.get_absolute_url()
        url = path.join(request.get_host(), new_blog_abs_url)
        SitemapArticleWriter.insert_article_info(url, instance)

        return Response(status=201)

    def update(self, request, *args, **kwargs):
        picture = None
        if request.data.get("picture", None):
            picture = request.data.get("picture")
            del request.data["picture"]

        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        self.perform_update(serializer)

        if picture:
            instance.picture = get_image_from_base64(instance.title, picture)
            instance.save()

        return Response(status=200)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)

        return Response(status=204)

    def perform_create(self, serializer):
        return serializer.save()


class ArchiveArticleView(generics.UpdateAPIView):
    def get_object(self):
        return BlogArticle.objects.get(pk=self.request.data.get("pk"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_archived = not instance.is_archived
        instance.save()

        return Response(
            status=200, data={"instance": BlogArticleSerializer(instance).data},
        )


class ContactBloggerAPIView(APIView):
    def post(self, request, *args, **kwargs):
        html_message = render_to_string(
            "blog/ask_blogger.html", {"text": request.data.get("text")}
        )

        status = send_mail(
            request.data.get("user"),
            request.data.get("subject"),
            request.data.get("blogger"),
            html_message,
        )

        if status:
            return Response(status=200)

        return Response(status=400)
