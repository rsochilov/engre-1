from django.core.paginator import Paginator

from accounts.models import Blogger
from accounts.serializers import BloggerSerializer, UserSerializer
from core.serializers import (
    DislikeSerializer,
    InterestSerializer,
    LikedDislikedSerializer,
    LikeSerializer,
    NameSerializer,
    NameSlugSerializer,
)
from rest_framework import serializers

from .filters import BlogArticleFilterSet, BlogArticleSettingsFilterSet
from .models import (
    Answer,
    BlogArticle,
    BlogCategory,
    Comment,
    Question,
    QuestionConfiguration,
)


class RecursiveField(serializers.Serializer):
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class CommentSerializer(LikedDislikedSerializer, serializers.ModelSerializer):
    reply_set = RecursiveField(many=True, read_only=True)
    user = UserSerializer(read_only=True)
    parent_id = serializers.IntegerField(required=False)

    class Meta:
        model = Comment
        fields = (
            "id",
            "comment",
            "reply_set",
            "created_at",
            "user",
            "id",
            "is_liked",
            "is_disliked",
            "liked_count",
            "disliked_count",
            "article",
            "parent_id",
        )

    def create(self, validated_data):
        request = self.context.get("request")

        validated_data["user"] = request.user

        return super(CommentSerializer, self).create(validated_data)


class LikeCommentSerializer(LikeSerializer, CommentSerializer):
    pass


class DislikeCommentSerializer(DislikeSerializer, CommentSerializer):
    pass


class BaseArticleSerializer(serializers.Serializer):
    interested_users_count = serializers.SerializerMethodField()
    answers_count = serializers.SerializerMethodField()
    comments_count = serializers.SerializerMethodField()

    def get_interested_users_count(self, obj):
        return obj.interested_users.count()

    def get_answers_count(self, obj):
        return obj.comments.filter(parent__isnull=True).count()

    def get_comments_count(self, obj):
        return obj.comments.count()


class ArticleSerializer(BaseArticleSerializer, serializers.ModelSerializer):
    url = serializers.SerializerMethodField()
    author = serializers.SerializerMethodField()
    category = NameSerializer()

    def get_url(self, obj):
        return obj.get_absolute_url()

    def get_author(self, obj):
        return BloggerSerializer(obj.author, context=self.context).data

    class Meta:
        model = BlogArticle
        fields = "__all__"

    def to_representation(self, obj):
        request = self.context.get("request")

        if request:
            obj.views += 1
            obj.save()

        return super(ArticleSerializer, self).to_representation(obj)


class InterestArticleSerializer(InterestSerializer, serializers.ModelSerializer):
    class Meta:
        model = BlogArticle
        fields = ("interested_users_count", "interested_users")


class BlogCategorySerializer(NameSlugSerializer, serializers.Serializer):
    articles = serializers.SerializerMethodField()
    has_more_articles = serializers.SerializerMethodField()

    def get_articles(self, obj):
        filters = self.context.get("filters", {})
        articles_paginator = Paginator(
            BlogArticleFilterSet(filters, obj.articles.all()).qs, 6
        )

        return ArticleSerializer(
            articles_paginator.get_page(1).object_list, many=True, context=self.context
        ).data

    def get_has_more_articles(self, obj):
        articles_paginator = Paginator(obj.articles.all(), 6)

        return articles_paginator.get_page(1).has_next()

    class Meta:
        model = BlogCategory
        fields = "__all__"


class BlogArticleSerializer(serializers.ModelSerializer):
    category = serializers.SerializerMethodField(read_only=True)
    category_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = BlogArticle
        fields = "__all__"

    def get_category(self, obj):
        if obj.category:
            return NameSerializer(obj.category).data

        return None


class BloggerArticleSerializer(serializers.Serializer):
    articles = serializers.SerializerMethodField()
    has_more_articles = serializers.SerializerMethodField()

    def get_articles(self, obj):
        filters = self.context.get("filters", {})
        articles_paginator = Paginator(
            BlogArticleSettingsFilterSet(filters, obj.blog_articles.all()).qs, 6
        )

        return BlogArticleSerializer(
            articles_paginator.get_page(1).object_list, many=True, context=self.context
        ).data

    def get_has_more_articles(self, obj):
        articles_paginator = Paginator(obj.blog_articles.all(), 6)

        return articles_paginator.get_page(1).has_next()


class AnswerSerializer(serializers.ModelSerializer, LikedDislikedSerializer):
    reply_set = RecursiveField(many=True, read_only=True)
    user = UserSerializer(read_only=True)
    article = serializers.SerializerMethodField()

    class Meta:
        model = Answer
        fields = "__all__"

    def get_article(self, obj):
        return obj.question.id

    def create(self, validated_data):
        request = self.context.get("request")
        user = request.user

        if user and user.is_authenticated:
            validated_data["user"] = user

        if validated_data.get("article"):
            validated_data["question"] = validated_data["article"]

        return super(AnswerSerializer, self).create(validated_data)


class QuestionSerializer(LikedDislikedSerializer, serializers.ModelSerializer):
    category = NameSerializer(read_only=True)
    interested_users_count = serializers.SerializerMethodField()
    is_rated = serializers.SerializerMethodField()
    answers_count = serializers.SerializerMethodField()
    category_id = serializers.IntegerField(write_only=True)
    user_id = serializers.IntegerField(write_only=True)
    user = UserSerializer(read_only=True)

    def get_interested_users_count(self, obj):
        return obj.interested_users.count()

    def get_is_rated(self, obj):
        request = self.context.get("request")

        return obj.interested_users.filter(id=request.user.id).exists()

    def get_answers_count(self, obj):
        return obj.answers.filter(parent__isnull=True).count()

    class Meta:
        model = Question
        fields = "__all__"


class LikeQuestionSerializer(LikeSerializer, QuestionSerializer):
    pass


class DislikeQuestionSerializer(DislikeSerializer, QuestionSerializer):
    pass


class QuestionConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ("id",)

    def update(self, instance, validated_data):
        request = self.context.get("request")
        user = request.user

        if user and user.is_authenticated:
            try:
                configuration = user.blog_questions_configuration
            except QuestionConfiguration.DoesNotExist:
                configuration = QuestionConfiguration.objects.create(user=user)

            configuration.hidden_questions.add(instance)

        return instance


class RateQuestionSerializer(serializers.ModelSerializer):
    interested_users_count = serializers.SerializerMethodField()

    def get_interested_users_count(self, obj):
        return obj.interested_users.count()

    class Meta:
        model = Question
        fields = ("interested_users_count",)

    def update(self, instance, validated_data):
        request = self.context.get("request")
        user = request.user

        if user and user.is_authenticated:
            if instance.interested_users.filter(pk=user.pk).exists():
                instance.interested_users.remove(user)
            else:
                instance.interested_users.add(user)

        return instance


class LikeAnswerSerializer(LikeSerializer, AnswerSerializer):
    pass


class DislikeAnswerSerializer(DislikeSerializer, AnswerSerializer):
    pass
