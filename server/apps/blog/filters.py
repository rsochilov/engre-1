import django_filters
from core.filters import CharArrayFilter, NumberInFilter
from dateutil import parser
from django.contrib.postgres.search import SearchVector
from django.db.models import Q
from django_filters import OrderingFilter

from .models import BlogArticle, Question


class BlogArticleFilterSet(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")
    created_at = django_filters.CharFilter(method="date_filter")
    ordering = OrderingFilter(fields=(("views", "popularity"),))
    dates = CharArrayFilter(method="dates_filter")

    class Meta:
        model = BlogArticle
        fields = ("category__slug", "author")

    def search_filter(self, queryset, name, value):
        words = value.split()
        result = queryset.none()

        for word in words:
            search = queryset.annotate(
                search=SearchVector(
                    "title", "content", "excerpt", "category__name", "tags__name",
                )
            ).filter(Q(search=word) | Q(search__icontains=word))

            result = result | search

        return result.distinct()

    def date_filter(self, qs, name, value):
        try:
            date = parser.parse(value)
        except ValueError:
            return qs

        return qs.filter(
            created_at__year=date.year,
            created_at__month=date.month,
            created_at__day=date.day,
        )

    def dates_filter(self, qs, name, value):
        try:
            date_from = parser.parse(value[0])
            date_to = parser.parse(value[1])
        except ValueError:
            return qs

        return qs.filter(Q(created_at__gte=date_from) & Q(created_at__lte=date_to))


class BlogQuestionFilterSet(django_filters.FilterSet):
    category_in = NumberInFilter(field_name="category", lookup_expr="in")

    class Meta:
        model = Question
        fields = {"title": ["icontains"]}


class BlogArticleSettingsFilterSet(django_filters.FilterSet):
    class Meta:
        model = BlogArticle
        fields = {
            "author": ["exact"],
            "status": ["exact"],
            "title": ["icontains"],
            "is_archived": ["exact"],
        }
