from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r"contact-blogger/",
        views.ContactBloggerAPIView.as_view(),
        name="contact-blogger",
    ),
    url(
        r"blog-articles-list/",
        views.BlogArticleListAPIView.as_view(),
        name="blog-articles-list-api",
    ),
    url(
        r"articles/(?P<slug>[-a-zA-Z0-9_]+)/",
        views.BlogArticleDetailView.as_view(),
        name="blog-article-details",
    ),
    url(
        r"like-comment/(?P<pk>[0-9]+)/",
        views.LikeCommentAPIView.as_view(),
        name="like-comment",
    ),
    url(
        r"dislike-comment/(?P<pk>[0-9]+)/",
        views.DislikeCommentAPIView.as_view(),
        name="dislike-comment",
    ),
    url(
        r"interest-article/(?P<pk>[0-9]+)/",
        views.InterestArticleAPIView.as_view(),
        name="like-article",
    ),
    url(
        r"like-question/(?P<pk>[0-9]+)/",
        views.LikeQuestionAPIView.as_view(),
        name="like-question",
    ),
    url(
        r"dislike-question/(?P<pk>[0-9]+)/",
        views.DislikeQuestionAPIView.as_view(),
        name="dislike-question",
    ),
    url(
        r"create-blog-comment/",
        views.CreateCommentAPIView.as_view(),
        name="create-blog-comment",
    ),
    url(
        r"create-blog-question/",
        views.CreateQuestionAPIView.as_view(),
        name="create-blog-question",
    ),
    url(
        r"hide-blog-question/(?P<pk>[0-9]+)/",
        views.UpdateQuestionConfiguration.as_view(),
    ),
    url(
        r"question-list-api/",
        views.QuestionListAPIView.as_view(),
        name="blog-question-list",
    ),
    url(
        r"rate-question/(?P<pk>[0-9]+)/",
        views.RateQuestionAPIView.as_view(),
        name="rate-blog-question",
    ),
    url(
        r"create-blog-answer/",
        views.CreateAnswerAPIView.as_view(),
        name="create-blog-answer",
    ),
    url(
        r"blog-answers-list/",
        views.AnswerListAPIView.as_view(),
        name="blog-answers-list",
    ),
    url(
        r"like-blog-answer/(?P<pk>[0-9]+)/",
        views.LikeAnswerAPIView.as_view(),
        name="like-blog-answer",
    ),
    url(
        r"dislike-blog-answer/(?P<pk>[0-9]+)/",
        views.DislikeAnswerAPIView.as_view(),
        name="dislike-blog-answer",
    ),
    url(
        r"(?P<slug>[-a-zA-Z0-9_]+)/", views.BlogTemplateView.as_view(), name="blog-list"
    ),
]
