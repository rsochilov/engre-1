from django.contrib import admin

from .models import BlogArticle, BlogCategory, Comment, Question

admin.site.register(BlogCategory)
admin.site.register(BlogArticle)
admin.site.register(Comment)
admin.site.register(Question)
