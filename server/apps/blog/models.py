from accounts.models import Blogger, User
from core.managers import CommonManager
from core.models import (
    ARTICLE_STATUSES,
    PUBLISHED,
    AbstractArticle,
    AbstractComment,
    ArchivableMixin,
    LikeDislikeMixin,
    NameSlugModel,
    RateableMixin,
    WatchableMixin,
)
from django.db import models

from .managers import QuestionManager


class BlogCategory(NameSlugModel, ArchivableMixin, WatchableMixin):
    description = models.TextField()

    def __str__(self):
        return self.name


class BlogArticle(AbstractArticle, RateableMixin):
    author = models.ForeignKey(
        Blogger, related_name="blog_articles", on_delete=models.PROTECT
    )
    category = models.ForeignKey(
        BlogCategory, related_name="articles", on_delete=models.PROTECT
    )
    views = models.PositiveIntegerField(default=0)
    picture = models.ImageField(upload_to="blogs/pictures", null=True)
    slug = models.SlugField(max_length=255, null=True, blank=True)
    status = models.PositiveSmallIntegerField(
        choices=ARTICLE_STATUSES, default=PUBLISHED
    )
    created_at = models.DateTimeField(null=True, blank=True)

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("blog-article-details", args=[str(self.slug)])

    def get_answers_count(self):
        return self.comments.filter(parent__isnull=True).count()

    def like(self, user_id):
        if self.interested_users.filter(pk=user_id).exists():
            self.interested_users.remove(user_id)
        else:
            self.interested_users.add(user_id)

    def save(self, *args, **kwargs):
        self.slug = "-".join([w.lower() for w in self.title.split(" ")])

        return super(BlogArticle, self).save(*args, **kwargs)


class Comment(AbstractComment, LikeDislikeMixin):
    article = models.ForeignKey(
        BlogArticle, related_name="comments", on_delete=models.CASCADE
    )

    objects = CommonManager()

    @property
    def rating(self):
        rating = self.liked.count() - self.disliked.count()

        if rating > 0:
            return f"+{rating}"

        return str(rating)


class Question(WatchableMixin, ArchivableMixin, RateableMixin, LikeDislikeMixin):

    user = models.ForeignKey(
        User, related_name="blog_questions", on_delete=models.PROTECT
    )
    category = models.ForeignKey(
        BlogCategory, related_name="questions", on_delete=models.PROTECT
    )
    title = models.CharField(max_length=200)
    text = models.TextField()

    objects = QuestionManager()

    class Meta:
        ordering = ("-created_at",)

    def __str__(self):
        return self.title

    @property
    def rating(self):
        rating = self.liked.count() - self.disliked.count()

        if rating > 0:
            return f"+{rating}"

        return str(rating)


class Answer(AbstractComment, LikeDislikeMixin):
    question = models.ForeignKey(
        Question, related_name="answers", on_delete=models.CASCADE
    )

    @property
    def rating(self):
        rating = self.liked.count() - self.disliked.count()

        if rating > 0:
            return f"+{rating}"

        return str(rating)


class QuestionConfiguration(models.Model):
    user = models.OneToOneField(
        User, related_name="blog_questions_configuration", on_delete=models.CASCADE
    )
    hidden_questions = models.ManyToManyField(
        Question, related_name="hide_configurations"
    )
