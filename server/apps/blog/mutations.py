import graphene
from accounts.models import Blogger
from accounts.schema import UserTypeUnion
from core.types import Mutation
from core.utils import retrieve_object
from graphql_jwt.decorators import login_required
from news.models import Comment as NewsComment
from news.models import NewsArticle
from news.schema import NewsCommentType

from .inputs import BlogArticleInput, BlogQuestionInput
from .models import Answer, BlogArticle
from .models import Comment as BlogComment
from .models import Question, QuestionConfiguration
from .schema import (
    ArticleType,
    BlogAnswerType,
    BlogArticleType,
    BlogCommentType,
    BlogQuestionType,
    Comment,
    CommentType,
    Post,
)


class CreateBlogQuestionMutation(Mutation):
    class Arguments:
        blog_question_data = BlogQuestionInput()

    question = graphene.Field(BlogQuestionType)

    @login_required
    def mutate(self, info, blog_question_data):
        blog_question_data["user"] = info.context.user

        question = Question.objects.super_create(**blog_question_data)

        return CreateBlogQuestionMutation(question=question)


class CreateCommentMutation(Mutation):
    class Arguments:
        article = graphene.ID(required=True)
        parent = graphene.ID()
        comment = graphene.String(required=True)
        type = Comment(required=True)

    comment = graphene.Field(CommentType)
    article = graphene.Field(ArticleType)

    @login_required
    def mutate(self, info, **kwargs):
        type = kwargs.pop("type")

        if type == Comment.BLOG:
            comment = BlogComment.objects.super_create(user=info.context.user, **kwargs)
        elif type == Comment.NEWS:
            comment = NewsComment.objects.super_create(user=info.context.user, **kwargs)

        elif type == Comment.ANSWER:
            comment = Answer.objects.super_create(user=info.context.user, **kwargs)

        article = comment.article

        return CreateCommentMutation(comment=comment, article=article)


class AnswerResult(graphene.Union):
    class Meta:
        types = (BlogCommentType, NewsCommentType, BlogAnswerType, BlogQuestionType)


class LikeComment(Mutation):
    class Arguments:
        comment_id = graphene.ID(required=True)
        type = Comment(required=True)

    comment = graphene.Field(AnswerResult)

    @login_required
    def mutate(self, info, comment_id, type):
        if type == Comment.BLOG:
            comment = retrieve_object(comment_id, BlogComment)

        elif type == Comment.NEWS:
            comment = retrieve_object(comment_id, NewsComment)

        elif type == Comment.QUESTION:
            comment = retrieve_object(comment_id, Question)

        elif type == Comment.ANSWER:
            comment = retrieve_object(comment_id, Answer)

        if comment.disliked.filter(id=info.context.user.id).exists():
            comment.disliked.remove(info.context.user)
        else:
            comment.liked.add(info.context.user)

        return LikeComment(comment=comment)


class DislikeComment(Mutation):
    class Arguments:
        comment_id = graphene.ID(required=True)
        type = Comment(required=True)

    comment = graphene.Field(AnswerResult)

    @login_required
    def mutate(self, info, comment_id, type):
        if type == Comment.BLOG:
            comment = retrieve_object(comment_id, BlogComment)

        elif type == Comment.NEWS:
            comment = retrieve_object(comment_id, NewsComment)

        elif type == Comment.QUESTION:
            comment = retrieve_object(comment_id, Question)

        elif type == Comment.ANSWER:
            comment = retrieve_object(comment_id, Answer)

        if comment.liked.filter(id=info.context.user.id).exists():
            comment.liked.remove(info.context.user)
        else:
            comment.disliked.add(info.context.user)

        return DislikeComment(comment=comment)


class WatchArticle(Mutation):
    class Arguments:
        article_id = graphene.ID(required=True)
        article_type = Post(required=True)

    article = graphene.Field(ArticleType)

    def mutate(self, info, article_id, article_type):
        if article_type == Post.BLOG:
            article = retrieve_object(article_id, BlogArticle)

        elif article_type == Post.NEWS:
            article = retrieve_object(article_id, NewsArticle)

        article.views += 1
        article.save()

        return WatchArticle(article=article)


class LikeArticle(Mutation):
    class Arguments:
        article_id = graphene.ID(required=True)
        article_type = Post(required=True)

    article = graphene.Field(ArticleType)

    @login_required
    def mutate(self, info, article_id, article_type):
        if article_type == Post.BLOG:
            article = retrieve_object(article_id, BlogArticle)

        elif article_type == Post.NEWS:
            article = retrieve_object(article_id, NewsArticle)

        elif article_type == Post.QUESTION:
            article = retrieve_object(article_id, Question)

        if article.interested_users.filter(id=info.context.user.id).exists():
            article.interested_users.remove(info.context.user)
        else:
            article.interested_users.add(info.context.user)

        return LikeArticle(article=article)


class HideQuestionMutation(Mutation):
    class Arguments:
        question_id = graphene.ID(required=True)

    result = graphene.Boolean()

    @login_required
    def mutate(self, info, question_id):
        question = retrieve_object(question_id, Question)
        user = info.context.user

        try:
            user.blog_questions_configuration.hidden_questions.add(question)

        except QuestionConfiguration.DoesNotExist:
            questions_configuration = QuestionConfiguration.objects.create(user=user)
            questions_configuration.hidden_questions.add(question)

        return HideQuestionMutation(result=True)


class SubscribeToBlogger(Mutation):
    class Arguments:
        blogger_id = graphene.ID(required=True)

    user = graphene.Field(UserTypeUnion)

    @login_required
    def mutate(self, info, blogger_id):
        user = info.context.user
        blogger = retrieve_object(blogger_id, Blogger)

        if user.blogger_subscriptions.filter(id=blogger.id).exists():
            user.blogger_subscriptions.remove(blogger)
        else:
            user.blogger_subscriptions.add(blogger)

        return SubscribeToBlogger(user=user)


class CreateBlogArticle(Mutation):
    class Arguments:
        article_data = BlogArticleInput(required=True)

    article = graphene.Field(BlogArticleType)

    @login_required
    def mutate(self, info, article_data):
        article = BlogArticle.objects.super_create(**article_data)

        return CreateBlogArticle(article=article)


class Mutation(graphene.ObjectType):
    create_comment = CreateCommentMutation.Field()
    create_blog_question = CreateBlogQuestionMutation.Field()
    like_comment = LikeComment.Field()
    dislike_comment = DislikeComment.Field()
    watch_article = WatchArticle.Field()
    like_article = LikeArticle.Field()
    hide_blog_question = HideQuestionMutation.Field()
    subscribe_to_blogger = SubscribeToBlogger.Field()
    create_blog_article = CreateBlogArticle.Field()
