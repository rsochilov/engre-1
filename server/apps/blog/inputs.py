import graphene


class BlogQuestionInput(graphene.InputObjectType):
    category = graphene.ID(reqiored=True)
    title = graphene.String(required=True)
    text = graphene.String(required=True)


class BlogArticleInput(graphene.InputObjectType):
    category = graphene.ID(required=True)
    title = graphene.String(required=True)
    excerpt = graphene.String(required=True)
    content = graphene.String(required=True)
    author = graphene.ID(required=True)
