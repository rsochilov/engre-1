import graphene
from accounts.schema import BloggerType
from core.schema import RateableMixin
from core.utils import get_media_absolute_url
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from news.schema import NewsArticleType, NewsCommentType

from .filters import BlogArticleFilterSet, BlogQuestionFilterSet
from .models import Answer, BlogArticle, BlogCategory
from .models import Comment as BlogComment
from .models import Question


class Comment(graphene.Enum):
    BLOG = 1
    NEWS = 2
    QUESTION = 3
    ANSWER = 4


class Post(graphene.Enum):
    BLOG = 1
    NEWS = 2
    QUESTION = 3


class BlogArticleType(DjangoObjectType):
    picture = graphene.String()
    video = graphene.String()
    interested = graphene.Int()
    tag_list = graphene.String()
    comments = DjangoFilterConnectionField(lambda: BlogCommentType)
    comments_count = graphene.Int()
    answers_count = graphene.Int()
    author = graphene.Field(BloggerType)

    class Meta:
        model = BlogArticle
        interfaces = (graphene.relay.Node,)

    def resolve_interested(self, info):
        return self.interested_users.count()

    def resolve_tag_list(self, info):
        return ", ".join(self.tags.all().values_list("name", flat=True))

    def resolve_comments_count(self, info):
        return self.comments.count()

    def resolve_answers_count(self, info):
        return self.comments.filter(parent__isnull=True).count()

    def resolve_picture(self, info):
        return get_media_absolute_url(self.picture)

    def resolve_video(self, info):
        return get_media_absolute_url(self.video)


class BlogCategoryType(DjangoObjectType):
    picture = graphene.String()
    articles = DjangoFilterConnectionField(
        BlogArticleType, filterset_class=BlogArticleFilterSet
    )

    def resolve_picture(self, info):
        return get_media_absolute_url(self.picture)

    class Meta:
        model = BlogCategory
        interfaces = (graphene.relay.Node,)
        filter_fields = ("name",)


class BlogCommentType(DjangoObjectType, RateableMixin):
    parent = graphene.Field(lambda: BlogCommentType)

    class Meta:
        model = BlogComment
        interfaces = (graphene.relay.Node,)
        filter_fields = []


class BlogQuestionType(DjangoObjectType, RateableMixin):
    answers_count = graphene.Int()
    interested = graphene.Int()

    def resolve_answers_count(self, info):
        return self.answers.count()

    def resolve_interested(self, info):
        return self.interested_users.count()

    class Meta:
        model = Question
        interfaces = (graphene.relay.Node,)


class BlogAnswerType(DjangoObjectType, RateableMixin):
    class Meta:
        model = Answer
        interfaces = (graphene.relay.Node,)
        filter_fields = []


class ArticleType(graphene.types.union.Union):
    class Meta:
        types = (NewsArticleType, BlogArticleType, BlogQuestionType)


class CommentType(graphene.types.union.Union):
    class Meta:
        types = (NewsCommentType, BlogCommentType, BlogAnswerType)


class Query(graphene.ObjectType):
    blog_categories = DjangoFilterConnectionField(BlogCategoryType)
    blog_category = graphene.Field(
        BlogCategoryType, slug=graphene.String(required=True)
    )
    blog_articles = DjangoFilterConnectionField(
        BlogArticleType, filterset_class=BlogArticleFilterSet
    )
    blog_article = graphene.relay.Node.Field(BlogArticleType)
    blog_comments = DjangoFilterConnectionField(BlogCommentType)
    blog_questions = DjangoFilterConnectionField(
        BlogQuestionType, filterset_class=BlogQuestionFilterSet
    )
    blog_answers = DjangoFilterConnectionField(BlogAnswerType)

    def resolve_blog_category(sefl, info, slug):
        try:
            return BlogCategory.objects.get(slug=slug)
        except BlogCategory.DoesNotExist:
            return None

    def resolve_blog_questions(self, info, **kwargs):
        if info.context.user.is_authenticated:
            return Question.objects.exclude(hide_configurations__user=info.context.user)

        return Question.objects.all()
