from django.core.management.base import BaseCommand
from core.management.add_classes import BuggedContentUpdateMixin

from blog.models import BlogArticle


class Command(BaseCommand, BuggedContentUpdateMixin):
    help = "Updates all blogs, that has invalid href in content <a> tags"
    db_model = BlogArticle

    def handle(self, *args, **options):
        self.update_bugged_content()
