from core.utils import send_mail
from django.conf import settings
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from marketplace.celery import app


@app.task
def send_feedback_creation_email(user_id):
    user = User.objects.get(pk=user_id)
    html_message = render_to_string("feedback/emails/feedback_created")
    send_mail(
        settings.DEFAULT_FROM_EMAIL,
        "Engre: Feedback have been created",
        user.email,
        html_message,
    )


@app.task
def send_feedback_action_email(user_id, action):
    _context = {"action": action}
    user = User.objects.get(pk=user_id)
    html_message = render_to_string("feedback/emails/feedback_action", context=_context)
    send_mail(
        settings.DEFAULT_FROM_EMAIL,
        "Engre: Feedback have been {}".format(action.lower()),
        user.email,
        html_message,
    )
