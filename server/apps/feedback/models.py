from company.models import Company
from core.models import ModeratableMixin
from django.db import models


class AbstractFeedback(models.Model):
    company_customer = models.ForeignKey(
        Company,
        related_name="%(app_label)s_%(class)s_feedback",
        on_delete=models.CASCADE,
    )
    comment = models.CharField(max_length=255)

    class Meta:
        abstract = True


class Platform(AbstractFeedback, ModeratableMixin):
    pass


class Executor(AbstractFeedback):
    company_executor = models.ForeignKey(
        Company, related_name="feedback", on_delete=models.CASCADE
    )
