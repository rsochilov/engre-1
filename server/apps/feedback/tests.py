from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from graphql_jwt.shortcuts import get_token
from graphql_relay import to_global_id

from company.factories import CompanyFactory
from company.models import Ownership
from company.schema import CompanyType
from core.models import Category
from feedback.models import Executor, Platform
from feedback.schema import PlatformFeedbackType
from market_location.models import City, Country


class PlatformFeedbackTestCase(TestCase):
    def setUp(self):
        _category = Category.objects.create(name="Aircraft construction")
        _country = Country.objects.create()
        _city = City.objects.create(country=_country)
        _ownership = Ownership.objects.create(name="TEST_CUSTOMER")

        self.user_customer_list = list()
        self.company_customer_list = list()
        self.platform_feedbacks = list()

        for i in range(1, 30):
            _test_user_customer = User.objects.create_user(
                username="test_customer_username{}".format(i),
                password="123456",
                email="test_customer_email{}@gmail.com".format(i),
            )
            token = get_token(_test_user_customer)
            _test_user_customer.profile.active_token = token
            _test_user_customer.profile.save()
            self.user_customer_list.append(_test_user_customer)

            _company_customer = CompanyFactory()
            self.company_customer_list.append(_company_customer)
            if i % 2 == 0:
                _status = "waiting_for_approval"
            else:
                _status = "published"
            _customer_platform_feedback = Platform.objects.create(
                company_customer=_company_customer, comment="Good Job", status=_status
            )
            self.platform_feedbacks.append(_customer_platform_feedback)

    def tearDown(self):
        pass

    def test_get_all_platform_feedbacks(self):
        query = """
            query {
                    allPlatformFeedbacks {
                    edges {
                        node {
                            id
                            comment
                            status
                        }
                    }
                }
            }
        """
        token = self.user_customer_list[0].profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())

    def test_get_all_published_feedbacks(self):
        query = """
            query {
                    allPlatformFeedbacks(status: "published") {
                    edges {
                        node {
                            id
                            comment
                            status
                        }
                    }
                }
            }
        """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())

    def test_add_platform_feedback(self):
        query = """
            mutation {
                createPlatformFeedback(comment: "New Good Comment") {
                    platformFeedback {
                        id
                        comment
                        status
                    }
                }
            }
        """
        token = self.user_customer_list[0].profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())

    def test_update_platform_feedback(self):
        query = """
            mutation {
                updatePlatformFeedback(platformFeedbackGlobId: "%s",comment: "Kruto delaete. Molodci. Pacani vobshe rebyata", status: "published") {
                    platformFeedback {
                        id
                        comment
                        status
                    }
                }
            }
        """ % to_global_id(
            PlatformFeedbackType.__name__, self.platform_feedbacks[0].id
        )
        token = self.user_customer_list[0].profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())

    def test_refuse_feedback(self):
        query = """
        mutation {
            updatePlatformFeedback(platformFeedbackGlobId: "%s", status: "refused") {
                platformFeedback {
                    id
                    comment
                    status
                }
            }
        }
        """ % to_global_id(
            PlatformFeedbackType.__name__, self.platform_feedbacks[0].id
        )
        token = self.user_customer_list[0].profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())


class ExecutorFeedbackTestCase(TestCase):
    def setUp(self):
        _category = Category.objects.create(name="Aircraft construction")
        _country = Country.objects.create()
        _location = City.objects.create(country=_country)
        _ownership = Ownership.objects.create(name="TEST_CUSTOMER")

        self.user_executor_list = list()
        self.company_executor_list = list()
        self.user_customer_list = list()
        self.company_customer_list = list()
        self.executor_feedback = list()
        for i in range(1, 30):
            test_user_executor = User.objects.create_user(
                username="test_executor_username{}".format(i),
                password="123456",
                email="test_executor_email{}@gmail.com".format(i),
            )
            test_user_executor.save()
            token = get_token(test_user_executor)
            test_user_executor.profile.active_token = token
            self.user_executor_list.append(test_user_executor)

            company_executor = CompanyFactory()

            self.company_executor_list.append(company_executor)

            test_user_customer = User.objects.create_user(
                username="test_customer_username{}".format(i),
                password="123456",
                email="test_customer_email{}@gmail.com".format(i),
            )
            test_user_customer.save()
            token = get_token(test_user_customer)
            test_user_customer.profile.active_token = token
            test_user_customer.profile.save()
            self.user_customer_list.append(test_user_customer)

            company_customer = CompanyFactory()
            self.company_customer_list.append(company_customer)
            _customer_executor_feedback = Executor.objects.create(
                company_customer=company_customer,
                company_executor=company_executor,
                comment="Good Job",
            )
            self.executor_feedback.append(_customer_executor_feedback)

    def tearDown(self):
        pass

    def test_get_all_executors_feedbacks(self):
        query = """
        query {
            allExecutorFeedbacks {
                edges {
                node {
                    id
                    comment
                    companyCustomer {
                            name
                        }
                    companyExecutor {
                            name
                        }
                    }
                }
            }
        }
        """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())

    def test_get_specified_executor_feedbacks(self):
        query = """
        query {
            allExecutorFeedbacks(companyExecutor: "%s") {
                edges {
                node {
                    id
                    comment
                    companyCustomer {
                            name
                        }
                    }
                }
            }
            }
        """ % to_global_id(
            CompanyType.__name__, self.company_executor_list[0].id
        )
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())

    def test_add_executor_feedback(self):
        query = """
        mutation {
            createExecutorFeedback(companyExecutorGlobId: "%s", comment: "New Good Comment") {
                executorFeedback {
                    id
                    comment
                    companyExecutor {
                        name
                    }
                    companyCustomer {
                        name
                    }
                }
            }
        }
        """ % to_global_id(
            CompanyType.__name__, self.company_executor_list[1].id
        )
        token = self.user_customer_list[0].profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data={"query": query}, **headers)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())
