from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.test import TestCase
from django.urls import reverse

from .models import Language, Category, Message


class DBLanguageTestCase(TestCase):
    def test_add(self):
        Language.objects.create(code="en", name="English")
        self.assertEqual(Language.objects.count(), 1)


class DBCategoryTestCase(TestCase):
    def test_add(self):
        Category.objects.create(code="mainpage", name="Main page")
        self.assertEqual(Category.objects.count(), 1)

    def test_add_invalid_code(self):
        obj = Category(code="1.1", name="Comments")
        with self.assertRaises(ValidationError):
            obj.clean_fields()


class DBMessageTestCase(TestCase):
    def setUp(self):
        self.language = Language.objects.create(code="en", name="English",)
        self.category = Category.objects.create(code="mainpage", name="Main page",)

    def test_add(self):
        Message.objects.create(
            language=self.language,
            category=self.category,
            code="welcome",
            text="Welcome!",
        )
        self.assertEqual(Message.objects.count(), 1)

    def test_add_another(self):
        Message.objects.create(
            language=self.language,
            category=self.category,
            code="welcome",
            text="Welcome!",
        )
        with self.assertRaises(IntegrityError):
            Message.objects.create(
                language=self.language,
                category=self.category,
                code="welcome",
                text="Welcome!",
            )


class SchemaLanguageTestCase(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username="jane", email="jane@gmail.com", password="password",
        )

    def test_get(self):
        Language.objects.create(
            code="en", name="English",
        )
        query = """
            query {
              textLanguages {
                edges {
                  node {
                    code
                  }
                }
              }
            }
        """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        edge = data["data"]["textLanguages"]["edges"][0]
        self.assertEqual(edge["node"]["code"], "en")

    def test_add(self):
        self.user.is_superuser = True
        self.user.save()

        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createTextLanguage(code: "en", name: "English") {
                status
              }
            }
        """

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue(data["data"]["createTextLanguage"]["status"])
        self.assertEqual(Language.objects.count(), 1)

    def test_add_another(self):
        self.user.is_superuser = True
        self.user.save()

        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createTextLanguage(code: "en", name: "English") {
                status
              }
            }
        """

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue(data["data"]["createTextLanguage"]["status"])
        self.assertEqual(Language.objects.count(), 1)

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertFalse(data["data"]["createTextLanguage"]["status"])
        self.assertEqual(Language.objects.count(), 1)

    def test_add_without_superuser_status(self):
        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createTextLanguage(code: "en", name: "English") {
                status
              }
            }
        """

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIsNone(data["data"]["createTextLanguage"])
        self.assertEqual(Language.objects.count(), 0)


class SchemaCategoryTestCase(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username="jane", email="jane@gmail.com", password="password",
        )

    def test_get(self):
        Category.objects.create(
            code="mainpage", name="Main page",
        )
        query = """
            query {
              textCategories {
                edges {
                  node {
                    code
                  }
                }
              }
            }
        """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        edge = data["data"]["textCategories"]["edges"][0]
        self.assertEqual(edge["node"]["code"], "mainpage")

    def test_add(self):
        self.user.is_superuser = True
        self.user.save()

        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createTextCategory(code: "mainpage", name: "Main page") {
                status
              }
            }
        """

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue(data["data"]["createTextCategory"]["status"])
        self.assertEqual(Category.objects.count(), 1)

    def test_add_another(self):
        self.user.is_superuser = True
        self.user.save()

        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createTextCategory(code: "mainpage", name: "Main page") {
                status
              }
            }
        """

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue(data["data"]["createTextCategory"]["status"])
        self.assertEqual(Category.objects.count(), 1)

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertFalse(data["data"]["createTextCategory"]["status"])
        self.assertEqual(Category.objects.count(), 1)

    def test_add_without_superuser_status(self):
        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createTextCategory(code: "mainpage", name: "Main page") {
                status
              }
            }
        """

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIsNone(data["data"]["createTextCategory"])
        self.assertEqual(Category.objects.count(), 0)


class SchemaMessageTestCase(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username="jane", email="jane@gmail.com", password="password",
        )
        self.language = Language.objects.create(code="en", name="English",)
        self.category = Category.objects.create(code="mainpage", name="Main page",)

    def test_get(self):
        Message.objects.create(
            language=self.language,
            category=self.category,
            code="welcome",
            text="Welcome!",
        )
        query = """
            query {
              textMessages {
                edges {
                  node {
                    code
                  }
                }
              }
            }
        """
        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        edge = data["data"]["textMessages"]["edges"][0]
        self.assertEqual(edge["node"]["code"], "welcome")

    def test_add(self):
        self.user.is_superuser = True
        self.user.save()

        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createTextMessage(
                language: "en",
                category: "mainpage",
                code: "welcome",
                text: "Welcome!",
              ) {
                status
              }
            }
        """

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue(data["data"]["createTextMessage"]["status"])
        self.assertEqual(Message.objects.count(), 1)

    def test_add_another(self):
        self.user.is_superuser = True
        self.user.save()

        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createTextMessage(
                language: "en",
                category: "mainpage",
                code: "welcome",
                text: "Welcome!",
              ) {
                status
              }
            }
        """

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue(data["data"]["createTextMessage"]["status"])
        self.assertEqual(Message.objects.count(), 1)

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertFalse(data["data"]["createTextMessage"]["status"])
        self.assertEqual(Message.objects.count(), 1)

    def test_add_without_superuser_status(self):
        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createTextMessage(
                language: "en",
                category: "mainpage",
                code: "welcome",
                text: "Welcome!",
              ) {
                status
              }
            }
        """

        response = self.client.post(reverse("api"), data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIsNone(data["data"]["createTextMessage"])
        self.assertEqual(Message.objects.count(), 0)
