import graphene

from graphene_django.types import DjangoObjectType
from graphene_django.fields import DjangoConnectionField

from core.decorators import superuser_status_required
from core.utils import get_object_or_none

from .models import Language, Category, Message


class TextLanguageType(DjangoObjectType):
    class Meta:
        model = Language
        interfaces = (graphene.relay.Node,)


class CreateLanguage(graphene.Mutation):
    class Arguments:
        code = graphene.String()
        name = graphene.String()

    status = graphene.Boolean()
    language = graphene.Field(TextLanguageType)

    @superuser_status_required
    def mutate(self, info, code, name):
        db_obj, created = Language.objects.get_or_create(
            code=code, defaults={"name": name},
        )
        status = created
        return CreateLanguage(language=db_obj, status=status)


class UpdateLanguage(graphene.Mutation):
    class Arguments:
        code = graphene.String()
        name = graphene.String()

    status = graphene.Boolean()
    language = graphene.Field(TextLanguageType)

    @superuser_status_required
    def mutate(self, info, code, name):
        db_obj = get_object_or_none(Language, code=code)
        if db_obj:
            db_obj.name = name
            db_obj.save()
            status = True
        else:
            status = False
        return UpdateLanguage(language=db_obj, status=status)


class TextCategoryType(DjangoObjectType):
    class Meta:
        model = Category
        interfaces = (graphene.relay.Node,)


class CreateCategory(graphene.Mutation):
    class Arguments:
        code = graphene.String()
        name = graphene.String()

    status = graphene.Boolean()
    category = graphene.Field(TextCategoryType)

    @superuser_status_required
    def mutate(self, info, code, name):
        db_obj, created = Category.objects.get_or_create(
            code=code, defaults={"name": name},
        )
        status = created
        return CreateCategory(category=db_obj, status=status)


class UpdateCategory(graphene.Mutation):
    class Arguments:
        code = graphene.String()
        name = graphene.String()

    status = graphene.Boolean()
    category = graphene.Field(TextCategoryType)

    @superuser_status_required
    def mutate(self, info, code, name):
        db_obj = get_object_or_none(Category, code=code)
        if db_obj:
            db_obj.name = name
            db_obj.save()
            status = True
        else:
            status = False
        return UpdateCategory(category=db_obj, status=status)


class TextMessageType(DjangoObjectType):
    class Meta:
        model = Message
        interfaces = (graphene.relay.Node,)


class CreateMessage(graphene.Mutation):
    class Arguments:
        language = graphene.String()
        category = graphene.String()
        code = graphene.String()
        text = graphene.String()

    status = graphene.Boolean()
    message = graphene.Field(TextMessageType)

    @superuser_status_required
    def mutate(self, info, language, category, code, text):
        db_obj, created = Message.objects.get_or_create(
            language_id=language,
            category_id=category,
            code=code,
            defaults={"text": text},
        )
        status = created
        return CreateMessage(message=db_obj, status=status)


class UpdateMessage(graphene.Mutation):
    class Arguments:
        language = graphene.String()
        category = graphene.String()
        code = graphene.String()
        text = graphene.String()

    status = graphene.Boolean()
    message = graphene.Field(TextMessageType)

    @superuser_status_required
    def mutate(self, info, language, category, code, text):
        db_obj = get_object_or_none(
            Message, language_id=language, category_id=category, code=code,
        )
        if db_obj:
            db_obj.text = text
            db_obj.save()
            status = True
        else:
            status = False
        return UpdateMessage(message=db_obj, status=status)


class DeleteMessage(graphene.Mutation):
    class Arguments:
        language = graphene.String()
        category = graphene.String()
        code = graphene.String()

    status = graphene.Boolean()

    @superuser_status_required
    def mutate(self, info, language, category, code):
        db_obj = get_object_or_none(
            Message, language_id=language, category_id=category, code=code,
        )
        if db_obj:
            db_obj.delete()
            status = True
        else:
            status = False
        return DeleteMessage(status=status)


class Mutation(graphene.ObjectType):
    create_text_language = CreateLanguage.Field()
    update_text_language = UpdateLanguage.Field()
    create_text_category = CreateCategory.Field()
    update_text_category = UpdateCategory.Field()
    create_text_message = CreateMessage.Field()
    update_text_message = UpdateMessage.Field()
    delete_text_message = DeleteMessage.Field()


class Query(graphene.ObjectType):
    text_languages = DjangoConnectionField(TextLanguageType)
    text_categories = DjangoConnectionField(TextCategoryType)
    text_messages = DjangoConnectionField(TextMessageType)

    def resolve_languages(self, info):
        return Language.objects.all()

    def resolve_categories(self, info):
        return Category.objects.all()

    def resolve_messages(self, info):
        return Message.objects.all()
