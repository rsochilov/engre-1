from os import path
from django.db.models import Q
from django.utils.text import slugify
from django.http.response import HttpResponse
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from rest_framework import status
from rest_framework.generics import UpdateAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import DocumentSerializer
from .utils import DocumentPDFConverter
from company.models import Document, Signature
from accounts.models import User
from project.models import ProjectUsers, Project
from accounts.tasks import send_email


class UpdateDocumentsView(UpdateAPIView):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = (IsAuthenticated,)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.status != Document.NOSIGN:
            return Response(
                data={"detail": "Already signed up by user"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if instance.is_archived:
            return Response(
                data={"detail": "Document is archieved"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        project = instance.project
        user = request.user
        if not user_has_access(user, project):
            return Response(status=status.HTTP_403_FORBIDDEN)
        serializer = self.get_serializer(instance, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(
                data={"detail": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )


class CreateDocumentsView(CreateAPIView):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = (IsAuthenticated,)


class ResignDocumentView(UpdateAPIView):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = (IsAuthenticated,)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.status == Document.NOSIGN:
            return Response(
                data={"detail": "Unable to resign unsigned document"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if instance.status == Document.FULLSIGNED:
            return Response(
                data={"detail": "Unable to resign fullsigned document"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if not user_has_access(request.user, instance.project):
            return Response(status=status.HTTP_403_FORBIDDEN)

        q_filter = Q(project=instance.project) & ~Q(user=request.user)

        instance.status = Document.NOSIGN
        instance.save()

        try:
            recipient = (
                Signature.objects.select_related("user")
                .get(document=instance)
                .user.email
            )
        except Signature.DoesNotExist:
            return Response(
                data={"detail": "Project was never signed"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        self.send_resign_email(request, recipient, instance)
        Signature.objects.filter(document=instance).delete()
        return Response(status=status.HTTP_200_OK)

    def send_resign_email(self, request, recipient: str, document: Document):
        email_body = render_to_string(
            "documents_api/resign_email.html",
            {"document": document, "host": request.get_host()},
        )
        send_email.delay("Engre document un-sign", email_body, [recipient])


class DownloadDocumentView(RetrieveAPIView):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if not user_has_access(request.user, instance.project):
            return Response(status=status.HTTP_403_FORBIDDEN)
        name = f"{slugify(instance.name)}.pdf"
        filepath = DocumentPDFConverter.write_to_file(instance, name)
        fs = FileSystemStorage("/tmp")
        with fs.open(path.basename(filepath)) as file:
            response = HttpResponse(file, content_type="application/pdf")
            response["Content-Disposition"] = f'attachment; filename="{name}"'
            return response
        return super().retrieve(request, *args, **kwargs)


def user_has_access(user: User, project: Project) -> bool:
    return (
        user.is_pm
        and project.pm.pk == user.pk
        or user.is_customer
        and project.company.pk == user.work.company.pk
        or user.is_contractor
        and project.service
        and user.work.company.intellectualservices.filter(
            pk=project.service.pk
        ).exists()
    )
