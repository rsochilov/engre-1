from django.db.models import Q
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIRequestFactory, force_authenticate

from .views import UpdateDocumentsView, CreateDocumentsView, ResignDocumentView
from .serializers import DocumentSerializer
from accounts.models import User
from company.models import Document, Worker, Signature
from project.models import Project, ProjectUsers


class UpdateDocumentsApiTests(TestCase):
    @classmethod
    def setUpClass(cls):
        prj = Project.objects.all().first()
        cls.test_user = Worker.objects.filter(company__id=prj.company.pk).first().user
        cls.document = Document.objects.create(
            name="test doc",
            project=prj,
            body="lalala",
            document_type=2,
            version="1.1",
            status=1,
        )
        cls.api_factory = APIRequestFactory()

    @classmethod
    def tearDownClass(cls):
        pass

    def test_update_document(self):
        self.document.name = "lalalalalala"
        doc_data = DocumentSerializer(self.document).data

        request = self.api_factory.put(
            reverse("documents-update", kwargs={"pk": self.document.pk}), data=doc_data
        )
        force_authenticate(request, user=self.test_user)

        response = UpdateDocumentsView().as_view()(request, pk=self.document.pk)
        self.assertEqual(response.status_code, 200, response.data)
        new_version = float(Document.objects.get(pk=self.document.pk).version)
        expected_version = float(self.document.version) + 0.01
        self.assertEqual(new_version, expected_version)

    def test_update_signed_document(self):

        self.document.status = Document.BYCONTRACTOR
        self.document.save()

        self.document.name = "lalalalalala"
        doc_data = DocumentSerializer(self.document).data

        request = self.api_factory.put(
            reverse("documents-update", kwargs={"pk": self.document.pk}), data=doc_data
        )
        force_authenticate(request, user=self.test_user)

        response = UpdateDocumentsView().as_view()(request, pk=self.document.pk)
        self.assertEqual(response.status_code, 400, response.data)

    def test_update_document_no_rights(self):
        prj = Project.objects.all().order_by("-id")[0]
        doc = Document.objects.create(
            name="test doc",
            project=prj,
            body="lalala",
            document_type=2,
            version="1.1",
            status=1,
        )
        doc.name = "lalalala"
        doc_data = DocumentSerializer(doc).data

        request = self.api_factory.put(
            reverse("documents-update", kwargs={"pk": doc.pk}), data=doc_data
        )
        force_authenticate(request, user=self.test_user)

        response = UpdateDocumentsView().as_view()(request, pk=doc.pk)
        self.assertEqual(response.status_code, 403, response.data)

    def test_update_document_invalid_version_no_numbers(self):
        self.document.name = "lalalalalala"
        self.document.version = "aa"

        doc_data = DocumentSerializer(self.document).data

        request = self.api_factory.put(
            reverse("documents-update", kwargs={"pk": self.document.pk}), data=doc_data
        )
        force_authenticate(request, user=self.test_user)

        response = UpdateDocumentsView().as_view()(request, pk=self.document.pk)
        self.assertEqual(response.status_code, 400, response.data)

    def test_update_document_invalid_version_invalid_number(self):
        self.document.name = "lalalalalala"
        self.document.version = "0.12"

        doc_data = DocumentSerializer(self.document).data

        request = self.api_factory.put(
            reverse("documents-update", kwargs={"pk": self.document.pk}), data=doc_data
        )
        force_authenticate(request, user=self.test_user)

        response = UpdateDocumentsView().as_view()(request, pk=self.document.pk)
        self.assertEqual(response.status_code, 400, response.data)


class CreateDocumentApiTests(TestCase):
    def test_create_document(self):
        prj = Project.objects.all().first()
        user = Worker.objects.filter(company__id=prj.company.pk).first().user
        doc = Document(
            name="created test doc",
            project=prj,
            body="hohoho",
            document_type=2,
            version="0.01",
            status=1,
        )
        data = DocumentSerializer(doc).data

        api_factory = APIRequestFactory()
        request = api_factory.post(reverse("documents-create"), data=data)
        force_authenticate(request, user=user)
        response = CreateDocumentsView.as_view()(request)
        self.assertEqual(response.status_code, 201, response.data)


class ResignDocumentApiTests(TestCase):
    @classmethod
    def setUpClass(cls):
        prj = Project.objects.all().first()
        cls.test_user = Worker.objects.filter(company__id=prj.company.pk).first().user
        cls.second_test_user = User.objects.filter(~Q(pk=cls.test_user.pk)).first()

        cls.document = Document.objects.create(
            name="test doc",
            project=prj,
            body="lalala",
            document_type=2,
            version="1.1",
            status=1,
        )
        cls.test_signature = Signature.objects.create(
            document=cls.document, user=cls.second_test_user
        )
        ProjectUsers.objects.create(project=prj, user=cls.test_user)
        ProjectUsers.objects.create(project=prj, user=cls.second_test_user)

    @classmethod
    def tearDownClass(cls):
        ProjectUsers.objects.filter(
            user__in=[cls.test_user, cls.second_test_user]
        ).delete()
        Signature.objects.filter(document=cls.document).delete()
        cls.document.delete()

    def test_resign_unsigned_document(self):
        api_factory = APIRequestFactory()
        request = api_factory.patch(
            reverse("documents-resign", kwargs={"pk": self.document.pk})
        )
        force_authenticate(request, user=self.test_user)
        response = ResignDocumentView.as_view()(request, pk=self.document.pk)
        self.assertEqual(response.status_code, 400, response.data)

    def test_resign_fullsigned_document(self):
        api_factory = APIRequestFactory()
        old_status = self.document.status
        self.document.status = Document.FULLSIGNED
        self.document.save()
        request = api_factory.put(
            reverse("documents-resign", kwargs={"pk": self.document.pk})
        )
        force_authenticate(request, user=self.test_user)
        response = ResignDocumentView.as_view()(request, pk=self.document.pk)
        self.assertEqual(response.status_code, 400, response.data)
        self.document.status = old_status
        self.document.save()

    def test_resign_document_user_not_allowed(self):
        not_allowed_user = User.objects.filter(~Q(pk=self.test_user.pk)).last()
        self.document.status = Document.BYCUSTOMER
        self.document.save()

        api_factory = APIRequestFactory()
        request = api_factory.patch(
            reverse("documents-resign", kwargs={"pk": self.document.pk})
        )
        force_authenticate(request, user=not_allowed_user)
        response = ResignDocumentView.as_view()(request, pk=self.document.pk)
        self.assertEqual(response.status_code, 403, response.data)

    def test_resign_document_no_errors(self):
        self.document.status = Document.BYCUSTOMER
        self.document.save()
        api_factory = APIRequestFactory()
        request = api_factory.patch(
            reverse("documents-resign", kwargs={"pk": self.document.pk})
        )
        force_authenticate(request, user=self.test_user)
        response = ResignDocumentView.as_view()(request, pk=self.document.pk)
        self.assertEqual(response.status_code, 200, response.data)
