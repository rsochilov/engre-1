from accounts.serializers import UserSerializer
from core.mixins import VueTemplateMixin
from django.views.generic import TemplateView
from news.models import NewsCategory
from news.serializers import NewsCategorySerializer

from .models import ForCustomersPage


class ForCustomersView(VueTemplateMixin, TemplateView):
    template_name = "for_customers.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context["for_customers"] = ForCustomersPage.objects.get()
        except ForCustomersPage.DoesNotExist:
            pass
        return context

    def get_props(self):
        context = {"user": self.request.user}

        if self.request.user.is_authenticated:
            user = UserSerializer(self.request.user).data
        else:
            user = None

        return {
            "categories": NewsCategorySerializer(
                NewsCategory.objects.all(), many=True, context=context
            ).data,
            "user": user,
        }
