from accounts.serializers import ContentManagerSerializer, UserSerializer
from blog.serializers import BaseArticleSerializer, RecursiveField
from core.serializers import (
    DislikeSerializer,
    InterestSerializer,
    LikedDislikedSerializer,
    LikeSerializer,
    NameSerializer,
    NameSlugSerializer,
    SubscribeSerializer,
)
from django.core.paginator import Paginator
from rest_framework import serializers

from .filters import NewsArticleFilterSet
from .models import Comment, NewsArticle, NewsCategory


class ArticleSerializer(BaseArticleSerializer, serializers.ModelSerializer):
    category = NameSerializer()
    url = serializers.SerializerMethodField()
    author = ContentManagerSerializer(read_only=True)
    images = serializers.SerializerMethodField()

    class Meta:
        model = NewsArticle
        fields = "__all__"

    def get_url(self, obj):
        return obj.get_absolute_url()

    def get_images(self, obj):
        return [preview.image.url for preview in obj.images.all()]


class NewsCategorySerializer(NameSlugSerializer, serializers.ModelSerializer):
    is_subscribed = serializers.SerializerMethodField()
    articles = serializers.SerializerMethodField()
    has_more_articles = serializers.SerializerMethodField()

    def get_is_subscribed(self, obj):
        user = self.context.get("user")

        if user and obj.subscribers.filter(pk=user.pk).exists():
            return True

        return False

    def get_articles(self, obj):
        filters = self.context.get("filters", {})
        articles_paginator = Paginator(
            NewsArticleFilterSet(filters, obj.articles.all()).qs, 4
        )

        return ArticleSerializer(
            articles_paginator.get_page(1).object_list, many=True
        ).data

    def get_has_more_articles(self, obj):
        filters = self.context.get("filters", {})
        articles_paginator = Paginator(
            NewsArticleFilterSet(filters, obj.articles.all()).qs, 4
        )

        return articles_paginator.get_page(1).has_next()

    class Meta:
        model = NewsCategory
        fields = (
            "id",
            "name",
            "slug",
            "is_subscribed",
            "articles",
            "has_more_articles",
            "description",
            "picture",
        )


class CommentSerializer(LikedDislikedSerializer, serializers.ModelSerializer):
    reply_set = RecursiveField(many=True, read_only=True)
    user = UserSerializer(read_only=True)
    parent_id = serializers.IntegerField(required=False)

    class Meta:
        model = Comment
        fields = (
            "id",
            "comment",
            "reply_set",
            "created_at",
            "user",
            "id",
            "liked",
            "disliked",
            "is_liked",
            "is_disliked",
            "liked_count",
            "disliked_count",
            "article",
            "parent_id",
        )

    def create(self, validated_data):
        request = self.context.get("request")

        validated_data["user"] = request.user

        return super(CommentSerializer, self).create(validated_data)


class InterestArticleSerializer(InterestSerializer, serializers.ModelSerializer):
    class Meta:
        model = NewsArticle
        fields = ("interested_users_count", "interested_users")


class LikeCommentSerializer(LikeSerializer, CommentSerializer):
    pass


class DislikeCommentSerializer(DislikeSerializer, CommentSerializer):
    pass


class SubscribeCategorySerializer(SubscribeSerializer, serializers.ModelSerializer):
    class Meta:
        model = NewsCategory
        fields = ("is_subscribed",)


class SubscribeAllCategoriesSerializer(serializers.Serializer):
    categories = serializers.ListField(serializers.IntegerField())

    def save(self, request, categories):
        for category_id in categories:
            category = NewsCategory.objects.get(pk=category_id)

            if category.subscribers.filter(pk=request.user.pk).exists():
                category.subscribers.remove(request.user)
            else:
                category.subscribers.add(request.user)
