from accounts.serializers import UserSerializer
from core.mixins import VueTemplateMixin
from core.serializers import NameSerializer
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from market_location.models import Country
from rest_framework import generics
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .filters import NewsArticleFilterSet
from .models import Comment, NewsArticle, NewsCategory
from .serializers import (
    ArticleSerializer,
    CommentSerializer,
    DislikeCommentSerializer,
    InterestArticleSerializer,
    LikeCommentSerializer,
    NewsCategorySerializer,
    SubscribeAllCategoriesSerializer,
    SubscribeCategorySerializer,
)
from seo_tags.mixins import NewsArticleMetaTagTemplateMixin


class NewsView(VueTemplateMixin, TemplateView):
    template_name = "news/list.html"

    def get_props(self):
        context = {"user": self.request.user, "filters": self.request.GET}
        countries = NameSerializer(Country.objects.all(), many=True).data

        if self.request.user.is_authenticated:
            user = UserSerializer(self.request.user).data
        else:
            user = None

        return {
            "categories": NewsCategorySerializer(
                NewsCategory.objects.all(), many=True, context=context
            ).data,
            "countries": countries,
            "user": user,
        }


class NewsArticleListAPIView(generics.ListAPIView):
    queryset = NewsArticle.objects.all()
    serializer_class = ArticleSerializer
    filterset_class = NewsArticleFilterSet

    def list(self, request, *args, **kwargs):
        category_slug = request.GET.get("category__slug")

        if category_slug:
            return self.list_categorized()
        else:
            return self.list_no_category()

    def list_categorized(self) -> Response:
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return Response({"articles": serializer.data})

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def list_no_category(self):
        page = []
        news_queryset = self.filter_queryset(self.get_queryset())
        for category in NewsCategory.objects.all():
            queryset = news_queryset.filter(category=category)
            paginated = self.paginate_queryset(queryset)
            serializer = self.get_serializer(paginated, many=True)
            page += serializer.data

        resp = {"articles": page}
        return Response(resp)


class NewsArticleDetailView(
    NewsArticleMetaTagTemplateMixin, VueTemplateMixin, DetailView
):
    template_name = "news/article.html"
    model = NewsArticle

    def get_props(self):
        context = {"request": self.request}

        if self.request.user.is_authenticated:
            user = UserSerializer(self.request.user).data
        else:
            user = None

        return {
            "user": user,
            "article": ArticleSerializer(self.object, context=context).data,
            "comments": CommentSerializer(
                Comment.objects.filter(article=self.object, parent__isnull=True),
                many=True,
                context=context,
            ).data,
        }


class InterestArticleAPIView(generics.UpdateAPIView):
    queryset = NewsArticle.objects.all()
    serializer_class = InterestArticleSerializer
    permission_classes = (IsAuthenticated,)


class CreateCommentAPIView(generics.CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticated,)


class LikeCommentAPIView(generics.UpdateAPIView):
    queryset = Comment.objects.all()
    serializer_class = LikeCommentSerializer
    permission_classes = (IsAuthenticated,)


class DislikeCommentAPIView(generics.UpdateAPIView):
    queryset = Comment.objects.all()
    serializer_class = DislikeCommentSerializer
    permission_classes = (IsAuthenticated,)


class SubscribeCategoryAPIView(generics.UpdateAPIView):
    queryset = NewsCategory
    serializer_class = SubscribeCategorySerializer
    permission_classes = (IsAuthenticated,)


class SubscribeAllCategoriesAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializer = SubscribeAllCategoriesSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save(request, **request.data)
            return Response(status=200)

        return Response(serializer.errors, status=400)
