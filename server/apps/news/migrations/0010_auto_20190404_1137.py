# Generated by Django 2.1.4 on 2019-04-04 11:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("news", "0009_auto_20190404_1111"),
    ]

    operations = [
        migrations.AlterField(
            model_name="newsarticle",
            name="tags",
            field=models.ManyToManyField(
                blank=True, related_name="news_articles", to="core.Tag"
            ),
        ),
    ]
