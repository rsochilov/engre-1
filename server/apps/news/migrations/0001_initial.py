# Generated by Django 2.1.4 on 2019-02-05 12:27

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("accounts", "0001_initial"),
        ("core", "0001_initial"),
        ("market_location", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Comment",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created_at", models.DateTimeField(auto_now_add=True, null=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                ("comment", models.TextField()),
            ],
            options={"ordering": ["-created_at"], "abstract": False,},
        ),
        migrations.CreateModel(
            name="NewsArticle",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("is_archived", models.BooleanField(default=False)),
                ("created_at", models.DateTimeField(auto_now_add=True, null=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                ("title", models.CharField(max_length=255)),
                ("excerpt", models.TextField()),
                ("content", models.TextField()),
                (
                    "video",
                    models.FileField(blank=True, null=True, upload_to="article_videos"),
                ),
                (
                    "status",
                    models.PositiveSmallIntegerField(
                        choices=[
                            (1, "active"),
                            (2, "closed"),
                            (3, "archived visible"),
                            (4, "archived invisible"),
                            (5, "not active"),
                        ],
                        default=1,
                    ),
                ),
                ("views", models.PositiveIntegerField(default=0)),
                (
                    "images",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.CharField(max_length=255), null=True, size=3
                    ),
                ),
                (
                    "author",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="news_articles",
                        to="accounts.ContentManager",
                    ),
                ),
            ],
            options={"ordering": ["-created_at"], "abstract": False,},
        ),
        migrations.CreateModel(
            name="NewsCategory",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=255)),
                ("slug", models.SlugField(max_length=255)),
                ("is_archived", models.BooleanField(default=False)),
                ("created_at", models.DateTimeField(auto_now_add=True, null=True)),
                ("updated_at", models.DateTimeField(auto_now=True)),
                ("description", models.TextField()),
                ("picture", models.ImageField(upload_to="news_categories")),
                (
                    "subscribers",
                    models.ManyToManyField(
                        related_name="news_categories", to="accounts.User"
                    ),
                ),
            ],
            options={"abstract": False,},
        ),
        migrations.AddField(
            model_name="newsarticle",
            name="category",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="articles",
                to="news.NewsCategory",
            ),
        ),
        migrations.AddField(
            model_name="newsarticle",
            name="country",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="news_articles",
                to="market_location.Country",
            ),
        ),
        migrations.AddField(
            model_name="newsarticle",
            name="interested_users",
            field=models.ManyToManyField(
                blank=True, related_name="interested_news_articles", to="accounts.User"
            ),
        ),
        migrations.AddField(
            model_name="newsarticle",
            name="languages",
            field=models.ManyToManyField(
                related_name="news_articles", to="core.Language"
            ),
        ),
        migrations.AddField(
            model_name="newsarticle",
            name="tags",
            field=models.ManyToManyField(related_name="news_articles", to="core.Tag"),
        ),
        migrations.AddField(
            model_name="comment",
            name="article",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="comments",
                to="news.NewsArticle",
            ),
        ),
        migrations.AddField(
            model_name="comment",
            name="disliked",
            field=models.ManyToManyField(
                related_name="disliked_news_comments", to="accounts.User"
            ),
        ),
        migrations.AddField(
            model_name="comment",
            name="liked",
            field=models.ManyToManyField(
                related_name="liked_news_comments", to="accounts.User"
            ),
        ),
        migrations.AddField(
            model_name="comment",
            name="parent",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="reply_set",
                to="news.Comment",
            ),
        ),
        migrations.AddField(
            model_name="comment",
            name="user",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="news_comments",
                to="accounts.User",
            ),
        ),
        migrations.AddIndex(
            model_name="newscategory",
            index=models.Index(fields=["slug"], name="news_newsca_slug_5c0a14_idx"),
        ),
    ]
