from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r"articles-list-api/",
        views.NewsArticleListAPIView.as_view(),
        name="news-articles-list-api",
    ),
    url(
        r"articles/(?P<slug>[-a-zA-Z0-9_]+)/",
        views.NewsArticleDetailView.as_view(),
        name="news-article-details",
    ),
    url(
        r"interest-article/(?P<pk>[0-9]+)/",
        views.InterestArticleAPIView.as_view(),
        name="interest-news-article",
    ),
    url(
        r"create-comment/",
        views.CreateCommentAPIView.as_view(),
        name="create-news-comment",
    ),
    url(
        r"like-comment/(?P<pk>[0-9]+)/",
        views.LikeCommentAPIView.as_view(),
        name="like-news-comment",
    ),
    url(
        r"dislike-comment/(?P<pk>[0-9]+)/",
        views.DislikeCommentAPIView.as_view(),
        name="dislike-comment",
    ),
    url(
        r"subscribe-category/(?P<pk>[0-9]+)/",
        views.SubscribeCategoryAPIView.as_view(),
        name="subscribe-news-category",
    ),
    url(
        r"subscribe-all-categories/",
        views.SubscribeAllCategoriesAPIView.as_view(),
        name="subscribe-all-categories",
    ),
    url(r"(?P<slug>[-a-zA-Z0-9_]+)/", views.NewsView.as_view(), name="news-list"),
]
