from django.contrib import admin

from .models import NewsArticle, NewsCategory

admin.site.register(NewsCategory)
admin.site.register(NewsArticle)
