from accounts.models import ContentManager, User
from core.managers import CommonManager
from core.models import (
    AbstractArticle,
    AbstractComment,
    ArchivableMixin,
    LikeDislikeMixin,
    NameSlugModel,
    OrderingMixin,
    WatchableMixin,
)
from django.contrib.postgres.fields import ArrayField
from django.db import models
from market_location.models import Country


class NewsCategory(NameSlugModel, ArchivableMixin, WatchableMixin, OrderingMixin):
    description = models.TextField()
    picture = models.ImageField(upload_to="news_categories")
    subscribers = models.ManyToManyField(
        User, related_name="news_categories", blank=True
    )

    class Meta:
        ordering = ["ordering"]

    def __str__(self):
        return self.name


class NewsPreviewImage(models.Model):
    WIDE, LONG_FILLED, LONG = range(1, 4)
    FORMATS = ((WIDE, "wide"), (LONG_FILLED, "long filled"), (LONG, "long"))

    format_type = models.PositiveSmallIntegerField(choices=FORMATS)
    image = models.ImageField()
    article = models.ForeignKey(
        "news.NewsArticle", related_name="images", on_delete=models.CASCADE
    )

    class Meta:
        ordering = ("format_type",)


class NewsArticle(AbstractArticle):
    slug = models.SlugField(max_length=255)
    country = models.ForeignKey(
        Country, related_name="news_articles", null=True, on_delete=models.PROTECT
    )

    category = models.ForeignKey(
        NewsCategory, related_name="articles", on_delete=models.PROTECT
    )

    author = models.ForeignKey(
        ContentManager, related_name="news_articles", on_delete=models.PROTECT
    )
    views = models.PositiveIntegerField(default=0)
    interested_users = models.ManyToManyField(
        User, related_name="interested_news_articles", blank=True
    )

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("news-article-details", args=[str(self.slug)])


class Comment(AbstractComment, LikeDislikeMixin):
    article = models.ForeignKey(
        NewsArticle, related_name="comments", on_delete=models.CASCADE
    )
    liked = models.ManyToManyField(User, related_name="liked_news_comments", blank=True)
    disliked = models.ManyToManyField(
        User, related_name="disliked_news_comments", blank=True
    )

    objects = CommonManager()
