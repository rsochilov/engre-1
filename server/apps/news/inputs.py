import graphene
from core.inputs import FileInput, TagInput


class NewsArticleInput(graphene.InputObjectType):
    category = graphene.ID(required=True)
    title = graphene.String(required=True)
    excerpt = graphene.String(required=True)
    content = graphene.String(required=True)
    author = graphene.ID(required=True)
    tags = graphene.List(TagInput)
    images = graphene.List(FileInput)
