import django_filters
from dateutil import parser
from django.contrib.postgres.search import SearchVector
from django.db.models import Q
from django_filters import OrderingFilter

from .models import Comment, NewsArticle


class NewsArticleFilterSet(django_filters.FilterSet):
    search = django_filters.CharFilter(method="search_filter")
    date = django_filters.CharFilter(method="date_filter")
    ordering = OrderingFilter(fields=(("views", "popularity"),))

    class Meta:
        model = NewsArticle
        fields = ("category__slug", "country")

    def search_filter(self, queryset, name, value):
        words = value.split()
        result = queryset.none()

        for word in words:
            search = queryset.annotate(
                search=SearchVector(
                    "title", "content", "country__name", "category__name", "tags__name",
                )
            ).filter(Q(search=word) | Q(search__icontains=word))

            result = result | search

        return result.distinct()

    def date_filter(self, qs, name, value):
        try:
            date = parser.parse(value)
        except ValueError:
            return qs

        return qs.filter(
            created_at__year=date.year,
            created_at__month=date.month,
            created_at__day=date.day,
        )


class NewsCommentFilterSet(django_filters.FilterSet):
    ordering = django_filters.OrderingFilter(fields=("liked", "disliked",))

    class Meta:
        model = Comment
        fields = {"parent": ["isnull", "exact"], "article": ["exact"]}
