from django.urls import path
from . import views

urlpatterns = [
    path(
        "<int:pk>/role/", views.GetUserRoleRetrieveView.as_view(), name="user_by_role"
    ),
    path("reviews/", views.UserReviewView.as_view(), name="review_by_user"),
    path("services/", views.UserServicesListAPIView.as_view(), name="user-services"),
    path("projects/", views.UserProjectsListAPIView.as_view(), name="user-projects"),
]
