from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.models import User
from company.models import Review
from company.serializers import ReviewSerializer
from project.models import IntellectualService, Project
from projects_api.serializers import IntellectualServiceSerializer, ProjectSerializer


class GetUserRoleRetrieveView(RetrieveAPIView):
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, *args, **kwargs):
        user = self.get_object()

        if user.is_pm:
            return Response(data={"role": -1})
        else:
            return Response(data={"role": user.role})


class UserReviewView(ListAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Review.objects.filter(user=self.request.user)


class UserServicesListAPIView(ListAPIView):
    queryset = IntellectualService.objects.all()
    pagination_class = None
    serializer_class = IntellectualServiceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return IntellectualService.objects.filter(
            company=self.request.user.get_company()
        )


class UserProjectsListAPIView(ListAPIView):
    queryset = Project.objects.all()
    pagination_class = None
    serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Project.objects.filter(company=self.request.user.get_company())
