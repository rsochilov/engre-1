from django.urls import path
from . import views

urlpatterns = [
    path(
        "<int:pk>/documents/",
        views.RetrieveDocumentByProjectAPIView.as_view(),
        name="projects-documents",
    ),
]
