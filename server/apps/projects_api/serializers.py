from rest_framework.serializers import ModelSerializer
from project.models import Project
from project.models import IntellectualService


class ProjectSerializer(ModelSerializer):
    class Meta:
        model = Project
        fields = "__all__"


class IntellectualServiceSerializer(ModelSerializer):
    class Meta:
        model = IntellectualService
        fields = "__all__"
