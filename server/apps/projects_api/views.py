from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from project.models import Project
from company.models import Document
from documents_api.serializers import DocumentSerializer
from .serializers import ProjectSerializer


class RetrieveDocumentByProjectAPIView(RetrieveAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, *args, **kwargs):
        project = self.get_object()
        serializer = DocumentSerializer(
            Document.objects.filter(project=project), many=True
        )
        return Response(data=serializer.data, status=status.HTTP_200_OK)
