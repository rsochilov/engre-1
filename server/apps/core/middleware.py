import re
from django.conf import settings


class IsRequestMobile:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        mobile_agent_re = re.compile(r".*(iphone|mobile|androidtouch)", re.IGNORECASE)

        if mobile_agent_re.match(request.META["HTTP_USER_AGENT"]):
            request.is_mobile = True
        else:
            request.is_mobile = False

        response = self.get_response(request)

        return response


class XRobotsMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        response["X-Robots-Tag"] = ",".join(settings.XROBOTSTAGS)
        return response
