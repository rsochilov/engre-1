from blog.models import BlogArticle, BlogCategory
from company.models import Company
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.text import slugify
from news.models import NewsArticle, NewsCategory
from project.models import Category, IntellectualService, Project


@receiver(pre_save, sender=Category, dispatch_uid="set_category_slug")
@receiver(pre_save, sender=BlogCategory, dispatch_uid="set_blog_category_slug")
@receiver(pre_save, sender=BlogArticle, dispatch_uid="set_blog_article_slug")
@receiver(pre_save, sender=NewsCategory, dispatch_uid="set_news_category_slug")
@receiver(pre_save, sender=NewsArticle, dispatch_uid="set_news_article_slug")
@receiver(pre_save, sender=Company, dispatch_uid="set_company_slug")
@receiver(pre_save, sender=IntellectualService, dispatch_uid="set_service_slug")
@receiver(pre_save, sender=Project, dispatch_uid="set_project_slug")
def populate_slug_field(sender, instance, **kwargs):
    if hasattr(instance, "name"):
        instance.slug = slugify(instance.name)
    elif hasattr(instance, "title"):
        instance.slug = slugify(instance.title)
