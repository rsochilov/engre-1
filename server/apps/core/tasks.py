from __future__ import absolute_import, unicode_literals

from accounts.models import User
from accounts.tasks import send_email
from django.conf import settings
from django.template.loader import render_to_string


class EngreEmails:
    default_from_email = settings.DEFAULT_FROM_EMAIL
    default_subject = "Engre"
    default_to_email = settings.EDUCATION_LEARN_MORE_EMAIL

    @classmethod
    def send_interesting_email(cls, data, template, _from="", subject="", _to=""):
        html_message = render_to_string(template, data)

        emails = list(
            User.objects.filter(is_superuser=True).values_list("email", flat=True)
        )

        send_email.delay(
            subject or cls.default_subject,
            html_message,
            emails,
            _from or cls.default_from_email,
        )
