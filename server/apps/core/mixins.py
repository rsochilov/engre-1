import json

from django.utils.html import escapejs
from seo_tags.mixins import MetaTagTemplateMixin


class VueTemplateMixin(MetaTagTemplateMixin):
    def get_dumped_data(self):
        """
        Returns a JSON escaped dumped data
        """
        return escapejs(json.dumps({"props": self.get_props()}))

    def get_props(self):
        """
        Returns an object that will be serialized as JSON by json.dumps().
        """
        return {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["data"] = self.get_dumped_data()
        return context
