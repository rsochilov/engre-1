import factory
from django.core.files.uploadedfile import SimpleUploadedFile


class FileFactory(factory.Factory):
    class Meta:
        model = SimpleUploadedFile

    name = factory.Sequence(lambda n: "file%s" % n)
    content = b"content"
    content_type = "image/png"

    @factory.post_generation
    def set_name(obj, created, extracted, **kwargs):
        if not created:
            return

        if obj.content_type == "image/png":
            ext = "png"
        elif obj.content_type == "video/mp4":
            ext = "mp4"
        elif obj.content_type == "application/pdf":
            ext = "pdf"

        obj.name = f"{obj.name}.{ext}"


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "core.Category"
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda n: "category%s" % n)
    description = factory.Sequence(lambda n: "description%s" % n)


class SubCategoryFactory(CategoryFactory):
    ancestor = factory.SubFactory(CategoryFactory)


class TagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "core.Tag"

    name = factory.Sequence(lambda n: "tag%s" % n)


class LanguageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "core.Language"

    name = factory.Sequence(lambda n: "language%s" % n)
