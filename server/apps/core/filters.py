import django_filters
from core.utils import retrieve_object
from graphene_django.forms import GlobalIDMultipleChoiceField
from graphql_relay.node.node import from_global_id


class CharArrayFilter(django_filters.BaseCSVFilter, django_filters.CharFilter):
    pass


class GlobalIdArrayField(GlobalIDMultipleChoiceField):
    def clean(self, items):
        return [from_global_id(item)[-1] for item in items if item] if items else None


class BaseInGlobIdFilter(django_filters.BaseInFilter):
    def filter(self, qs, value):
        local_id_values = [
            retrieve_object(each, self.model._meta.get_field(self.field_name).rel.to)
            for each in value
            if each
        ]
        return super(BaseInGlobIdFilter, self).filter(qs, local_id_values)


class IdArrayFilter(django_filters.Filter):
    field_class = GlobalIdArrayField

    def __init__(self, *args, **kwargs):
        kwargs.setdefault("lookup_expr", "in")
        super().__init__(*args, **kwargs)


class NumberInFilter(django_filters.BaseInFilter, django_filters.NumberFilter):
    pass
