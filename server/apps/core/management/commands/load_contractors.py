from typing import List, Tuple
import django
from loguru import logger

from accounts.models import User
from company.models import Company, Ownership, Worker
from core.models import Language, Tag
from core.management.base import BaseLoaderCommand, StatisticsCounters
from django.core.management.base import BaseCommand
from market_location.models import City, Country
from marketplace.gdriveparser import ContractorParser, Downloader
from openpyxl import load_workbook
from project.models import Category, CoreSkill, Industry, IntellectualService


class ServiceInitData:
    item = dict
    industry: Industry
    category: Category
    company: Company
    core_skills: List[CoreSkill]
    tags: List[Tag]

    def __init__(self, item, industry, category, company, core_skills, tags):
        self.item = item
        self.industry = industry
        self.category = category
        self.company = company
        self.core_skills = core_skills
        self.tags = tags

    def __str__(self):
        res = "item\n"
        for k, v in self.item.items():
            res += f"{k}: {v}\n"
        return f"{res}\nindustry.pk: {self.industry.pk}\ncategory.pk: {self.category.pk}\ncompany.pk: {self.company.pk}\n"


class Command(BaseLoaderCommand):
    table_name = "table.xlsx"
    table_id = "1xLqgzrs2fg1BwKAwPn7RWD0VHuJ10oribx8FfwR84pY"
    default_password = "qweqweqwe"

    def create_worker_if_not_exists(self, user: User, company: Company) -> Worker:
        try:
            return Worker.objects.get(user__pk=user.pk)
        except Worker.DoesNotExist:
            return Worker.objects.create(user=user, company=company, is_owner=True)

    def get_or_create_service(
        self, init_data: ServiceInitData
    ) -> Tuple[IntellectualService, bool]:
        """Creates IntellectualService isntance if not exists

        Arguments:
            init_data {ServiceInitData}

        Returns:
            Tuple[IntellectualService, bool] -- IntellectualService, was_created
        """
        try:
            return (
                IntellectualService.objects.get(
                    name=init_data.item["service_name"],
                    industry__pk=init_data.industry.pk,
                    category__pk=init_data.category.pk,
                    company__pk=init_data.company.pk,
                ),
                False,
            )
        except IntellectualService.DoesNotExist:
            logger.warning(f"No Service with name: {init_data.item['service_name']}")
            service = IntellectualService.objects.create(
                name=init_data.item["service_name"],
                industry=init_data.industry,
                category=init_data.category,
                description=init_data.item["service_description"],
                hourly_rate=init_data.item["rate"],
                company=init_data.company,
            )
            service.core_skills.set(init_data.core_skills)
            service.tags.set(init_data.tags)
            service.save()
            return service, True
        except IntellectualService.MultipleObjectsReturned:
            service_count = IntellectualService.objects.filter(
                name=init_data.item["service_name"],
                industry__pk=init_data.industry.pk,
                category__pk=init_data.category.pk,
                company__pk=init_data.company.pk,
            ).count()
            raise IntellectualService.MultipleObjectsReturned(
                f"Multiple IntellectualService \
                got with name {init_data.item['service_name']}: {service_count}"
            )

    def get_or_create_city(self, item: dict) -> City:
        try:
            return City.objects.get(name=item["location"])
        except City.DoesNotExist:
            return City.objects.create(
                name=item["location"], country=Country.objects.get(name="Ukraine")
            )

    def save_contractors_data(self, data, *args):
        stats = StatisticsCounters()
        for item in data:
            user, created = User.objects.get_or_create(
                email=item["email"], is_active=True
            )

            ownership, created = Ownership.objects.get_or_create(name=item["ownership"])

            location = self.get_or_create_city(item)

            language, created = Language.objects.get_or_create(
                name__iexact=item["languages"]
            )
            # TODO: date_of_creation is year, why 2006-03-20?
            date_creation = f'{item["date_of_creation"]}-03-20'
            slogan = item["slogan"][:255]

            company, created = Company.objects.get_or_create(
                owner=item["ceo"],
                name=item["company"],
                ownership=ownership,
                location=location,
                date_creation=date_creation,
                description=item["description"],
                website=item["website"],
                company_size=item["company_size"],
                role=Company.CONTRACTOR,
                slogan=slogan,
                flexible_working_hours=item["flexible_working_hours"],
            )
            if created:
                logger.warning(f"Company {company.name} was created")
            company.languages.add(language)

            if created and item["logo"]:
                company.logo.save(item["logo"].name, item["logo"].file)

            self.create_worker_if_not_exists(user, company)

            user.set_password(self.default_password)
            user.save()

            # SERVICE
            industry, created = Industry.objects.get_or_create(name=item["industry"])
            category, created = Category.objects.get_or_create(
                name=item["service_category"]
            )

            core_skills = []
            for core_skill_name in item["core_skills"]:
                core_skill, created = CoreSkill.objects.get_or_create(
                    name=core_skill_name
                )
                core_skills.append(core_skill)

            tags = []
            for tag_name in item["tags"]:
                tag, created = Tag.objects.get_or_create(name=tag_name)
                tags.append(tag)

            init_data = ServiceInitData(
                item, industry, category, company, core_skills, tags
            )
            try:
                _, created = self.get_or_create_service(init_data)
            except IntellectualService.MultipleObjectsReturned as err:
                logger.error(err)
                stats.errors += 1
                continue
            if created:
                stats.added += 1
                logger.success(f'Successfully uploaded "{item["service_name"]}"')
            else:
                stats.skipped += 1
                logger.info(f'Service {item["service_name"]} already exists, skipping')
        logger.success(f"Data was successfully uploaded for contractors\n{stats}")

    def handle(self, *args, **options):
        self.save_data = self.save_contractors_data
        self.handle_command(ContractorParser, None, "image")
