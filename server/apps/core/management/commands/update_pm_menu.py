from django.core.management.base import BaseCommand
from loguru import logger

from core.models import PageRenderingCondition


class Command(BaseCommand):
    help = "Updates pm permissions for chat and documents pages"
    # id of int service permissions for chat and documents
    ids = [152, 164]

    def handle(self, *args, **options):
        perms = PageRenderingCondition.objects.filter(id__in=self.ids)
        if not perms:
            logger.error(f"Permissions with ids {self.ids} not found")
        perms.update(strict=False)
        logger.success("Permissions updated!")
