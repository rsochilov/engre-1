from django.utils.text import slugify
from about.models import Achievement
from core.management.base import BaseLoaderCommand
from marketplace.gdriveparser import AchievementsParser


class Command(BaseLoaderCommand):
    help = "Loads about rows to the DB"

    def create_instance(self, row) -> Achievement:
        return Achievement(
            title=row["title"],
            description=row["description"],
            year=row["year"],
            slug=slugify(row["title"]),
            activity_id=row["activity_id"],
        )

    def check_instance_exists(self, instance: Achievement) -> bool:
        try:
            return Achievement.objects.get(slug=instance.slug)
        except Achievement.MultipleObjectsReturned as err:
            achievements_count = Achievement.objects.filter(slug=instance.slug).count()
            raise Achievement.MultipleObjectsReturned(
                f"Multiple Achievements got with \
                slug '{instance.slug}': {achievements_count}"
            )
        except Achievement.DoesNotExist:
            pass

    def handle(self, *args, **options):
        self.handle_command(AchievementsParser, Achievement, "image")
