from django.utils.text import slugify
from django.db.models import Q
from openpyxl import load_workbook

from about.models import Person
from core.management.base import BaseLoaderCommand
from marketplace.gdriveparser import PersonParser


class Command(BaseLoaderCommand):
    help = "Loads about persons to the DB"

    def create_instance(self, row) -> Person:
        return Person(
            name=row["name"],
            short_bio=row["short_bio"],
            bio=row["bio"],
            century_id=row["century_id"],
            activity_id=row["activity_id"],
            year_of_birth=row["year_of_birth"],
            year_of_death=row["year_of_death"],
        )

    def check_instance_exists(self, instance: Person) -> bool:
        filter_query = (
            Q(name=instance.name)
            & Q(year_of_birth=instance.year_of_birth)
            & Q(year_of_death=instance.year_of_death)
        )
        try:
            return Person.objects.get(filter_query)
        except Person.MultipleObjectsReturned:
            people_count = Person.objects.filter(filter_query).count()
            raise Person.MultipleObjectsReturned(
                f"Multiple Persons got with name '{instance.name}' \
                and birt_year: {instance.year_of_birth}: {people_count}"
            )
        except Person.DoesNotExist:
            pass

    def handle(self, *args, **options):
        self.handle_command(PersonParser, Person, "photo")
