from django.db.models import Model as DbModel
from django.utils.text import slugify
from openpyxl import load_workbook
from loguru import logger

from accounts.models import ContentManager
from core.management.base import BaseLoaderCommand, StatisticsCounters
from marketplace.gdriveparser import NewsParser
from news.models import NewsArticle, NewsPreviewImage
from seo_tags.models import NewsArticleMetaTags


class Command(BaseLoaderCommand):
    help = "Loads news articles to the DB"
    _metatag_model = NewsArticleMetaTags

    def try_get_article(
        self, article_data: dict, content_manager: ContentManager
    ) -> bool:
        try:
            return NewsArticle.objects.get(
                author_id=content_manager.pk,
                title=article_data["title"],
                category_id=article_data["category_id"],
                country_id=article_data["country_id"],
            )
        except NewsArticle.DoesNotExist:
            return False
        except NewsArticle.MultipleObjectsReturned:
            newsarticle_count = NewsArticle.objects.filter(
                author_id=content_manager.pk,
                title=article_data["title"],
                category_id=article_data["category_id"],
                country_id=article_data["country_id"],
            ).count()
            raise NewsArticle.MultipleObjectsReturned(
                "Multiple NewsArticles got "
                + +f"with title '{article_data['title']}: {newsarticle_count}"
            )

    def save_news_articles(self, data, *args):
        stats = StatisticsCounters()
        for item in data:
            content_manager = ContentManager.objects.first()
            try:
                instance = self.try_get_article(item, content_manager)
                if instance:
                    logger.info(
                        f'News Article \'{item["title"]}\' already exists, skipping...'
                    )
                    stats.skipped += 1
                    self.create_seo_tags(item, instance)
                    super().insert_sitemap_info(instance)
                    continue
            except NewsArticle.MultipleObjectsReturned as err:
                stats.errors += 1
                logger.error(err)
                continue

            new_article = NewsArticle.objects.create(
                title=item["title"],
                slug=slugify(item["title"]),
                excerpt=item["excerpt"],
                content=item["content"],
                category_id=item["category_id"],
                country_id=item["country_id"],
                author=content_manager,
            )

            for index, image in enumerate(item["previews"]):
                preview = NewsPreviewImage.objects.create(
                    format_type=index, article=new_article
                )

                preview.image.save(image.name, image.file)

            new_article.created_at = item["created_at"]
            new_article.save()
            self.create_seo_tags(item, new_article)
            super().insert_sitemap_info(new_article)
            stats.added += 1
            logger.success(f'Successfully uploaded "{item["title"]}" article')
        logger.success(f"Data successfully uploaded for NewsArticles\n{stats}")

    def handle(self, *args, **options):
        self.save_data = self.save_news_articles
        self.handle_command(NewsParser, NewsArticle, "image")
