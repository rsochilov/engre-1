from django.utils.text import slugify
from openpyxl import load_workbook
from loguru import logger

from marketplace.gdriveparser import StartupParser
from core.management.base import BaseLoaderCommand, StatisticsCounters
from partnership.models import ForInvestor


class Command(BaseLoaderCommand):
    help = "Loads news rows to the DB"
    table_id = "1otPEhdqdhRdb-WsXdvbVOSBHfgTcddtq--_vMvBjOps"

    def create_instance(self, row: dict) -> ForInvestor:
        return ForInvestor(
            name=row["title"],
            slug=slugify(row["title"]),
            excerpt=row["excerpt"],
            text=row["text"],
            industry_id=row["industry_id"],
            amount=row["amount"],
            created_at=row["created_at"],
        )

    def check_instance_exists(self, instance: ForInvestor) -> bool:
        try:
            return ForInvestor.objects.get(name=instance.name)
        except ForInvestor.DoesNotExist:
            return
        except ForInvestor.MultipleObjectsReturned:
            count = ForInvestor.objects.filter(name=instance.name).count()
            raise ForInvestor.MultipleObjectsReturned(
                f"Multiple ForInvestor got " + +f"with name: {instance.name}: {count}"
            )

    def save_for_investors(self, data, *args):
        stats = StatisticsCounters()
        for row in data:
            instance = self.create_instance(row)
            if self.check_instance_exists(instance):
                stats.skipped += 1
                logger.info(f"Instance '{instance}' already exists, skipping...")
                continue

            if row.get("preview"):
                instance.image.save(row["preview"].name, row["preview"].file)

            instance.save()
            stats.added += 1
            logger.success(f'Successfully uploaded "{row["title"]}" row')
        logger.success(f"Data successfully uploaded for startups\n{stats}")

    def handle(self, *args, **options):
        self.save_data = self.save_for_investors
        self.handle_command(StartupParser, ForInvestor, "preview")
