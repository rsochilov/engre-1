from django.utils.text import slugify

from about.models import CommonProject
from core.management.base import BaseLoaderCommand
from marketplace.gdriveparser import CommonProjectsParser


class Command(BaseLoaderCommand):
    help = "Loads common projects information from google docs tables to db"

    def create_instance(self, row) -> CommonProject:
        return CommonProject(
            title=row["title"],
            description=row["description"],
            year=row["year"],
            slug=slugify(row["title"]),
            industry_id=row["industry_id"],
        )

    def check_instance_exists(self, instance: CommonProject) -> bool:
        try:
            return CommonProject.objects.get(slug=instance.slug)
        except CommonProject.MultipleObjectsReturned:
            commonproject_count = CommonProject.objects.filter(
                slug=instance.slug
            ).count()
            raise CommonProject.MultipleObjectsReturned(
                f"Multiple CommonProjects got \
                with slug '{instance.slug}': {commonproject_count}"
            )
        except CommonProject.DoesNotExist:
            pass

    def handle(self, *args, **options):
        self.handle_command(CommonProjectsParser, CommonProject, "image")
