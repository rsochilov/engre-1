import re

# deprecated
class Printable:
    def print(self, message: str):
        self.stdout.write(self.style.HTTP_INFO(f"INFO: ") + message)

    def print_success(self, message: str):
        self.stdout.write(self.style.SUCCESS(f"SUCCESS: ") + message)

    def print_warning(self, message: str):
        self.stdout.write(self.style.WARNING(f"WARNING: ") + message)

    def print_error(self, message: str):
        self.stdout.write(self.style.ERROR(f"ERROR: ") + message)


class StatisticsCounters:
    def __init__(self):
        self.added = self.skipped = self.updated = self.errors = 0

    def __str__(self):
        return f"Stats:\nAdded: {self.added}\nSkipped: {self.skipped}\nErrors: {self.errors}"


class BuggedContentUpdateMixin(Printable):
    pattern = "&amp;sa=D&amp;ust="

    def update_bugged_content(self):
        if not self.db_model:
            self.print_error("No db_model provided! Exiting")
            return
        rgx = re.compile(self.pattern + "\d+")
        bugged_objects = self.db_model.objects.filter(content__contains=self.pattern)
        if not bugged_objects:
            self.print("No bugged blogs found. Exiting")
            return

        for obj in bugged_objects:
            obj.content = rgx.sub("", obj.content)
            obj.save(update_fields=["content"])
            self.print_success(f"{self.db_model.__name__} '{obj}' updated")

        self.print(f"Updated {bugged_objects.count()} objects")
