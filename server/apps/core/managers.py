from django.db import models

from .inputs import FileInput, TagInput, TimezoneInput
from .timezones import timezones
from .utils import get_image_from_base64, retrieve_list, retrieve_object


class CommonManager(models.Manager):
    ONE_TO_RELATIONSHIPS = ["OneToOneField", "ForeignKey"]
    MANY_TO_MANY_RELATIONSHIP = "ManyToManyField"

    def _set_data(self, instance, **kwargs):
        for field_name, value in kwargs.items():
            setattr(instance, field_name, value)

    def _process(self, func=None, instance=None, instance_id=None, *args, **kwargs):
        data = {}

        if not instance and instance_id:
            instance = retrieve_object(instance_id, self.model)

        if instance and not isinstance(instance, self.model):
            raise TypeError(
                f"The object is not instance of {self.model.__name__} model"
            )

        for field_name, value in kwargs.items():
            data[field_name] = {"many_to_many": False, "is_file": False}
            field = self.model._meta.get_field(field_name)
            field_type = field.get_internal_type()

            if isinstance(value, str) and field_type in self.ONE_TO_RELATIONSHIPS:
                # handle OneToOneField & ForeignKey
                data[field_name]["value"] = retrieve_object(value, field.related_model)

            elif isinstance(value, TimezoneInput):
                # handle timezone

                data[field_name]["value"] = timezones[value.name]

            elif (
                isinstance(value, list) and field_type is self.MANY_TO_MANY_RELATIONSHIP
            ):

                # handle ManyToManyField
                if value and isinstance(value[0], TagInput):
                    # handle Tags
                    data[field_name]["value"] = field.related_model.objects.bulk_create(
                        [field.related_model(name=tag.name) for tag in value if tag]
                    )
                    data[field_name]["many_to_many"] = True
                else:
                    data[field_name]["value"] = retrieve_list(
                        value, field.related_model
                    )
                    data[field_name]["many_to_many"] = True

            elif isinstance(value, FileInput):
                # handle files

                try:
                    data[field_name]["value"] = get_image_from_base64(
                        value["name"], value["file"]
                    )
                except Exception:
                    del data[field_name]
            else:
                # handle scalar type

                data[field_name]["value"] = value

        simple_data = {
            k: v["value"]
            for k, v in data.items()
            if not v["many_to_many"] and not v["is_file"]
        }

        many_to_many_data = {
            k: v["value"] for k, v in data.items() if v["many_to_many"]
        }

        files_data = {k: v["value"] for k, v in data.items() if v["is_file"]}

        if func:
            instance = func(**simple_data)
        else:
            self._set_data(instance, **simple_data)

        for field_name, value in many_to_many_data.items():
            getattr(instance, field_name).set(value)

        for field_name, value in files_data.items():
            setattr(instance, field_name, value)
            getattr(instance, field_name).save()

        return instance

    def super_create(self, *args, **kwargs):
        return self._process(func=self.create, *args, **kwargs)

    def super_update(self, instance=None, instance_id=None, *args, **kwargs):
        if instance or instance_id:
            service = self._process(
                instance=instance, instance_id=instance_id, *args, **kwargs
            )
            service.save()
            return service

        return None
