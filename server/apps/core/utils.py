from datetime import datetime
from typing import Tuple, List, Dict
import xml.etree.ElementTree as ET
import base64
import binascii
import mimetypes
import os
import re
import time
from email.mime.base import MIMEBase

from bs4 import BeautifulSoup
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

import boto3
import sendgrid
from celery.task.control import revoke as task_revoke
from django.conf import settings
from django_redis import get_redis_connection
from django.core.exceptions import (
    ObjectDoesNotExist,
    MultipleObjectsReturned,
    ValidationError,
)
from django.core.files.base import ContentFile
from django.utils.translation import gettext_lazy as _
from graphql.error import GraphQLLocatedError
from graphql_relay import from_global_id
from graphql_relay import to_global_id as global_id
from sendgrid.helpers.mail import Content, Email, Mail

from .exceptions import CacherException

from notifications_api.models import Notification
from user_settings.models import NotificationsSettings
from notifications_api.serializers import NotificationSerializer
from accounts.tasks import send_email

# Default MIME type to use on attachments (if it is not explicitly given
# and cannot be guessed).
DEFAULT_ATTACHMENT_MIME_TYPE = "application/octet-stream"


class Context:
    """ For testing """

    def __init__(self, **kwargs):
        for field_name, value in kwargs.items():
            setattr(self, field_name, value)


def retrieve_object(global_id, obj_type):
    try:
        _type, local_id = from_global_id(global_id)
    except (
        binascii.Error,
        GraphQLLocatedError,
        UnicodeDecodeError,
        obj_type.DoesNotExist,
    ):
        raise ValidationError(_(f"{obj_type.__name__} does not exist"))
    return obj_type.objects.get(id=local_id)


def retrieve_list(id_list, obj_type=None):
    if obj_type:
        model_name = obj_type.__name__
    else:
        model_name = "Instance"

    try:
        objects = [from_global_id(i)[1] for i in id_list]
    except (binascii.Error, GraphQLLocatedError, UnicodeDecodeError):
        raise ValidationError(_(f"{model_name} does not exist"))
    return objects


def camel2snake(name):
    return name[0].lower() + re.sub(
        r"(?!^)[A-Z]", lambda x: "_" + x.group(0).lower(), name[1:]
    )


def to_global_id(obj, many=False):
    if many:
        return [global_id(f"{o._meta.model.__name__}Type", o.id) for o in obj]
    return global_id(f"{obj._meta.model.__name__}Type", obj.id)


def get_media_absolute_url(media_obj):
    try:
        # return settings.MEDIA_URL + media_obj.url
        return os.path.join(settings.MEDIA_URL, "media", media_obj.name)
    except ValueError:
        return None


def get_object_or_none(model, **kwargs):
    try:
        return model.objects.get(**kwargs)
    except ObjectDoesNotExist:
        return None


def send_mail(from_email, subject, to_email, content):
    print(f"\n{content}\n")
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)
    mail = Mail(
        Email(from_email), subject, Email(to_email), Content("text/html", content)
    )
    response = sg.client.mail.send.post(request_body=mail.get())

    if response.status_code == 202:
        return True

    return False


def get_image_from_base64(name, fileStr):
    _, img_str = fileStr.split(";base64,")
    return ContentFile(base64.b64decode(img_str), name)


def create_attachment(filename, b64_encoded_content):
    mimetype = mimetypes.guess_type(filename)[0] or DEFAULT_ATTACHMENT_MIME_TYPE
    basetype, subtype = mimetype.split("/", 1)
    attachment = MIMEBase(basetype, subtype)
    try:
        filename.encode("ascii")
    except UnicodeEncodeError:
        filename = ("utf-8", "", filename)
    attachment.add_header("Content-Transfer-Encoding", "base64")
    attachment.add_header(
        "Content-Disposition", "attachment", filename=filename,
    )
    attachment.set_payload(b64_encoded_content)
    return attachment


def upload_file(path, name, file):
    """ uploads a file to S3 and returns absolute url """

    s3 = boto3.client("s3")
    bucket_name = settings.AWS_STORAGE_BUCKET_NAME

    timestamp = int(time.time())
    file_name, ext = name.split(".")

    image_path = f"{path}/{file_name}_{timestamp}.{ext}"

    s3.upload_fileobj(file, bucket_name, image_path, ExtraArgs={"ACL": "public-read",})

    return os.path.join(settings.MEDIA_URL, image_path)


class Cacher:
    __verify_hash = "engre_verify_emails"
    __conn = get_redis_connection()
    __ttl_verify_email = 3600 * 24

    @classmethod
    def add_invite_info(
        cls, uid: str, email: str, project_id: int, inviter_id: int
    ) -> int:
        """
        Added user verification info to redis
        Returns:
            int: redis rpush result
        """
        cls.__conn.rpush(uid, email)
        cls.__conn.rpush(uid, project_id)
        cls.__conn.rpush(uid, inviter_id)
        cls.__conn.expire(uid, cls.__ttl_verify_email)
        return cls.__conn.rpush(uid, project_id)

    @classmethod
    def get_invite_info(cls, uid: str) -> Tuple[str, int, int]:
        """
        Retreives invite info from redis
        Args:
            uid (str): uid, that provided by user
        Raises:
            CacherException: when cached uid doesn't found

        Returns:
            Tuple[str, int, int]: email, project_id, inviter_id
        """
        if not cls.__conn.exists(uid):
            raise CacherException("Uid not found")
        email = cls.__conn.lindex(uid, 0).decode()
        project_id = int(cls.__conn.lindex(uid, 1).decode())
        inviter_id = int(cls.__conn.lindex(uid, 2).decode())
        return email, project_id, inviter_id

    @classmethod
    def remove_verify_info(cls, uid: str) -> int:
        return cls.__conn.delete(uid)


class SitemapArticleWriter:
    """
    Writes new blogs and news articles data into settings.SITEMAP_FILE
    """

    template = "<url><loc>{}</loc><lastmod>{}-{}-{}</lastmod><changefreq>daily</changefreq><priority>0.80</priority></url>"

    @classmethod
    def cache_file(cls):
        if not os.path.exists(settings.SITEMAP_FILE):
            raise FileNotFoundError(f'"{settings.SITEMAP_FILE}" not found')
        with open(settings.SITEMAP_FILE, "r") as file:
            cls.sitemap_text = "".join(file.readlines())

    @classmethod
    def insert_article_info(cls, uri: str, instance):
        if hasattr(cls, "sitemap_text") and uri in cls.sitemap_text:
            return
        if not os.path.exists(settings.SITEMAP_FILE):
            raise FileNotFoundError(f'"{settings.SITEMAP_FILE}" not found')
        tree = ET.parse(settings.SITEMAP_FILE)
        root = tree.getroot()

        new_str = cls.template.format(
            uri,
            instance.updated_at.year,
            instance.updated_at.month,
            instance.updated_at.day,
        )
        root.append(ET.fromstring(new_str))
        tree.write(settings.SITEMAP_FILE)


class ArticleHtmlParser:
    """
    For gathering title and description from blogs or news articles
    """

    @staticmethod
    def get_headline(article) -> str:
        bs = BeautifulSoup(article.content, "html.parser")
        if bs.h1:
            return bs.h1.text.strip()
        return ""

    @staticmethod
    def get_image_url(article) -> str:
        bs = BeautifulSoup(article.content, "html.parser")
        if bs.img:
            return bs.img["src"]
        return ""


class Notifier:
    redis_hash_name = "email_notifications"

    @classmethod
    def create_notification(
        cls, user, notification_body: str, text: str, notification_type: int
    ):
        """
        Creates notification object, generates email send task with delay from settings

        Args:
            user (User): User instance
            text (str): notification text
        """

        new_notification = Notification.objects.create(
            user=user, notification_type=Notification.CHAT, body=notification_body,
        )
        data = NotificationSerializer(new_notification).data
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            f"notific_{user.id}", {"type": "new_notification", "body": data}
        )

        try:
            if not cls._check_user_settings(user, notification_type):
                return
        except ObjectDoesNotExist:
            print(f"CRITICAL ERROR: {user} doesn't have notification settings")
            return
        except MultipleObjectsReturned:
            print(
                f"CRITICAL ERROR: {user} has multiple notification settings, will select first"
            )
            return

        task_id = send_email.apply_async(
            ("Engre notification", text, [user.email]),
            countdown=settings.EMAIL_TASK_DELAY,
        )
        conn = get_redis_connection()
        conn.hset(
            cls.redis_hash_name,
            cls._generate_hash_field_name(new_notification.pk, notification_type),
            task_id,
        )

    @classmethod
    def update_notifications(cls, notifications_data: Dict[int, int], user):
        ids = [nd["id"] for nd in notifications_data]
        Notification.objects.filter(user=user, pk__in=ids).update(viewed=datetime.now())
        conn = get_redis_connection()
        for nd in notifications_data:
            cls._delete_notification_task(conn, nd["id"], nd["type"])

    @classmethod
    def _check_user_settings(cls, user, notification_type: int) -> bool:
        if notification_type == Notification.CHAT:
            values = "new_messages"
        elif notification_type == Notification.PROJECT:
            values = "project_updates"
        else:
            values = "events"

        notify_settings = NotificationsSettings.objects.values(values).get(user=user)

        return (
            notification_type == Notification.CHAT
            and notify_settings[values]
            or notification_type == Notification.PROJECT
            and notify_settings[values]
            or notification_type == Notification.ETC
            and notify_settings[values]
        )

    @classmethod
    def _generate_hash_field_name(
        cls, notification_id: int, notification_type: int
    ) -> str:
        return f"{notification_type}_{notification_id}"

    @classmethod
    def _delete_notification_task(
        cls, conn, notification_id: int, notification_type: int
    ):
        """Gets task id from redis by notification_id, deletes task from celery and redis

        Args:
            notification_id (int): id of Notification instance
        """
        hash_field = cls._generate_hash_field_name(notification_id, notification_type)
        task_id = conn.hget(cls.redis_hash_name, hash_field)
        if task_id:
            conn.hdel(cls.redis_hash_name, hash_field)
            try:
                task_revoke(task_id, terminate=True)
            except Exception as exc:
                print(f"EXCEPTION OCCURED during task revoke:\n{exc}")
