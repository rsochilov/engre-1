import graphene


class FileInput(graphene.InputObjectType):
    name = graphene.String()
    file = graphene.String()


class TimezoneInput(graphene.InputObjectType):
    name = graphene.String()


class ProfileInput(graphene.InputObjectType):
    name = graphene.String(required=True)
    location = graphene.ID(required=True)
    current_position = graphene.ID(required=True)
    picture = graphene.InputField(FileInput)
    timezone = graphene.InputField(TimezoneInput, required=True)


class TagInput(graphene.InputObjectType):
    name = graphene.String(required=True)
