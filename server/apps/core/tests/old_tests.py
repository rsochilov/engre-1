# """
# Core Tests. Auth system, Password change/reset.
# Example error response:
#     {
#       "errors": [
#         {
#           "message": "You do not have permission to perform this action",
#           "locations": [
#             {
#               "line": 2,
#               "column": 3
#             }
#           ]
#         }
#       ],
#       "data": {
#         "refuseToken": null
#       }
#     }
# """


# import logging
# import sys


# from django.contrib.auth.models import User
# from django.contrib.auth.tokens import default_token_generator
# from django.core.files.uploadedfile import SimpleUploadedFile

# from django.urls import reverse
# from django.utils.encoding import force_bytes
# from django.utils.http import urlsafe_base64_encode
# from graphql_jwt.shortcuts import get_token
# from graphql_relay import to_global_id

# from core.schema import UserType
# from market_location.models import City, Country
# from market_location.schema import CityType

# sys.tracebacklimit = 0
# logging.disable(logging.CRITICAL)


# class AuthTestCase(TestCase):
#     def setUp(self):
#         self.test_username = 'testuser1'
#         self.test_password = '12345678'
#         self.test_email = 'marketplace.ddi.test1@gmail.com'
#         self.test_user1 = User.objects.create_user(
#             username=self.test_username, email=self.test_email)
#         self.test_user1.set_password(self.test_password)
#         self.test_user1.save()

#     def test_login(self):
#         data = {
#             "query": '''
#             mutation {
#               login(email: "%s", password: "%s") {
#                 token
#               }
#             }
#           ''' % (self.test_email, self.test_password)
#         }
#         response = self.client.post(reverse('api'), data=data)

#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertIn('token', response.json()['data']['login'])
#         self.test_user1 = User.objects.get(id=self.test_user1.id)
#         self.assertIsNotNone(self.test_user1.profile.active_token)

#         return response.json()['data']['login']['token']

#     def test_multi_login(self):
#         for _ in range(3):
#             self.test_login()

#     def test_login_with_wrong_user(self):
#         data = {
#             "query": '''
#             mutation {
#               login(email: "%s", password: "%s") {
#                 token
#               }
#             }
#           ''' % ('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', self.test_password)
#         }
#         response = self.client.post(reverse('api'), data=data)
#         self.assertEqual(response.status_code, 200)
#         self.assertIsNone(response.json()['data']['login']['token'])

#     def test_logout_after_login(self):
#         token = self.test_login()
#         data = {
#             "query": '''
#             mutation {
#               refuseToken {
#                 token
#               }
#             }
#           '''
#         }
#         headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
#         response = self.client.post(reverse('api'), data=data, **headers)

#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.test_user1 = User.objects.get(id=self.test_user1.id)
#         self.assertIsNone(self.test_user1.profile.active_token)

#         return token

#     def test_login_after_logout(self):
#         token = self.test_logout_after_login()

#         data = {
#             "query": '''
#             mutation {
#               refuseToken {
#                 token
#               }
#             }
#           '''
#         }
#         headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
#         response = self.client.post(reverse('api'), data=data, **headers)
#         self.assertEqual(response.status_code, 200)
#         self.test_user1 = User.objects.get(id=self.test_user1.id)
#         self.assertIsNone(self.test_user1.profile.active_token)

#     def test_refresh_token(self):
#         token = self.test_login()
#         data = {
#             "query": '''
#             mutation {
#               refreshToken(token: "%s") {
#                 token
#               }
#             }
#           ''' % (token,)
#         }
#         response = self.client.post(reverse('api'), data=data)

#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertIn('token', response.json()['data']['refreshToken'])

#     def test_verify_token(self):
#         token = self.test_login()
#         data = {
#             "query": '''
#             mutation {
#               verifyToken(token: "%s") {
#                 payload
#               }
#             }
#           ''' % (token,)
#         }
#         response = self.client.post(reverse('api'), data=data)

#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertIn('payload', response.json()['data']['verifyToken'])
#         self.assertEqual(self.test_username, response.json()[
#                          'data']['verifyToken']['payload']['username'])


# # @override_settings(EMAIL_BACKEND='django.core.mail.backends.smtp.EmailBackend')
# class PasswordResetTestCase(TestCase):
#     def setUp(self):
#         self.test_username = 'testuser1'
#         self.test_password = '12345678'
#         self.test_new_password = '87654321'
#         self.test_email = 'marketplace.ddi.test1@gmail.com'
#         self.test_user1 = User.objects.create_user(
#             username=self.test_username, password=self.test_password, email=self.test_email)
#         self.test_user1.save()

#     def test_get_reset_password_email_link(self):
#         data = {
#             "query": '''
#             mutation {
#               passwordReset(email: "%s") {
#                 status
#               }
#             }
#           ''' % (self.test_email, )
#         }
#         response = self.client.post(reverse('api'), data=data)

#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())

#     def test_get_reset_password_email_link_with_wrong_email(self):
#         data = {
#             "query": '''
#             mutation {
#               passwordReset(email: "%s") {
#                 errors
#                 status
#               }
#             }
#           ''' % ("x"*255+"@gmail.com", )
#         }
#         response = self.client.post(reverse('api'), data=data)

#         self.assertEqual(response.status_code, 200)
#         self.assertIsNotNone(
#             response.json()['data']['passwordReset']['errors'])

#     def test_verify_reset_password_email_link(self):
#         data = {
#             "query": '''
#             query {
#               passwordResetVerify {
#                 status
#               }
#             }
#           '''
#         }
#         _kwargs = {
#             "uidb64": urlsafe_base64_encode(force_bytes(self.test_user1.pk)),
#             "token": default_token_generator.make_token(self.test_user1)
#         }
#         response = self.client.post(
#             reverse('password_reset_confirm', kwargs=_kwargs), data=data)
#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())

#     def test_multy_verify_reset_password_email_link(self):
#         for _ in range(3):
#             self.test_verify_reset_password_email_link()

#     def test_verify_reset_password_email_link_with_wrong_uid(self):
#         data = {
#             "query": '''
#             query {
#               passwordResetVerify {
#                 status
#               }
#             }
#           '''
#         }
#         _kwargs = {
#             "uidb64": "xxx",
#             "token": default_token_generator.make_token(self.test_user1)
#         }
#         response = self.client.post(
#             reverse('password_reset_confirm', kwargs=_kwargs), data=data)
#         self.assertEqual(response.status_code, 200)
#         self.assertIn('errors', response.json())

#     def test_verify_reset_password_email_link_with_wrong_token(self):
#         data = {
#             "query": '''
#             query {
#               passwordResetVerify {
#                 status
#               }
#             }
#           '''
#         }
#         _kwargs = {
#             "uidb64": urlsafe_base64_encode(force_bytes(self.test_user1.pk)),
#             "token": "xxx-xxx"
#         }
#         response = self.client.post(
#             reverse('password_reset_confirm', kwargs=_kwargs), data=data)
#         self.assertEqual(response.status_code, 200)
#         self.assertIn('errors', response.json())

#     def test_confirm_reset_password_email_link(self):
#         data = {
#             "query": '''
#             mutation {
#               passwordResetConfirm(newPassword: "%s", repeatPassword: "%s") {
#                 status
#               }
#             }
#           ''' % (self.test_new_password, self.test_new_password)
#         }
#         _kwargs = {
#             "uidb64": urlsafe_base64_encode(force_bytes(self.test_user1.pk)),
#             "token": default_token_generator.make_token(self.test_user1)
#         }
#         response = self.client.post(
#             reverse('password_reset_confirm', kwargs=_kwargs), data=data)
#         self.test_user1 = User.objects.get(id=self.test_user1.id)

#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertTrue(self.test_user1.check_password(self.test_new_password))

#     def test_confirm_reset_password_email_link_with_wrong_uid(self):
#         data = {
#             "query": '''
#             mutation {
#               passwordResetConfirm(newPassword: "%s", repeatPassword: "%s") {
#                 status
#               }
#             }
#           ''' % (self.test_new_password, self.test_new_password)
#         }
#         _kwargs = {
#             "uidb64": "xxx",
#             "token": default_token_generator.make_token(self.test_user1)
#         }
#         response = self.client.post(
#             reverse('password_reset_confirm', kwargs=_kwargs), data=data)
#         self.test_user1 = User.objects.get(id=self.test_user1.id)

#         self.assertEqual(response.status_code, 200)
#         self.assertIn('errors', response.json())
#         self.assertFalse(self.test_user1.check_password(
#             self.test_new_password))

#     def test_confirm_reset_password_email_link_with_wrong_token(self):
#         data = {
#             "query": '''
#             mutation {
#               passwordResetConfirm(newPassword: "%s", repeatPassword: "%s") {
#                 status
#               }
#             }
#           ''' % (self.test_new_password, self.test_new_password)
#         }
#         _kwargs = {
#             "uidb64": urlsafe_base64_encode(force_bytes(self.test_user1.pk)),
#             "token": "xxx-xxx"
#         }
#         response = self.client.post(
#             reverse('password_reset_confirm', kwargs=_kwargs), data=data)
#         self.test_user1 = User.objects.get(id=self.test_user1.id)

#         self.assertEqual(response.status_code, 200)
#         self.assertIn('errors', response.json())
#         self.assertFalse(self.test_user1.check_password(
#             self.test_new_password))

#     def test_confirm_reset_password_email_link_with_wrong_password(self):
#         data = {
#             "query": '''
#             mutation {
#               passwordResetConfirm(newPassword: "%s", repeatPassword: "%s") {
#                 status
#               }
#             }
#           ''' % (self.test_new_password, self.test_password)
#         }
#         _kwargs = {
#             "uidb64": urlsafe_base64_encode(force_bytes(self.test_user1.pk)),
#             "token": default_token_generator.make_token(self.test_user1)
#         }
#         response = self.client.post(
#             reverse('password_reset_confirm', kwargs=_kwargs), data=data)
#         self.test_user1 = User.objects.get(id=self.test_user1.id)

#         self.assertEqual(response.status_code, 200)
#         self.assertIn('errors', response.json())
#         self.assertFalse(self.test_user1.check_password(
#             self.test_new_password))


# class SignupTestCase(TestCase):

#     def setUp(self):
#         self.test_email = 'marketplace.ddi.test1@gmail.com'
#         self.test_username = 'testuser1'
#         self.test_password = '12345678'
#         self.is_newsletter = True
#         country = Country.objects.create()
#         city = City.objects.create(country=country)
#         self.test_location_glob_id = to_global_id(CityType.__name__, city.id)

#     def test_create_user_from_the_origin_form(self, with_error=False):
#         data = {
#           "query": '''
#             mutation {
#               signup(email: "%s", username: "%s", password: "%s", isNewsletter: %s) {
#                 errors
#                 user {
#                   id
#                 }
#               }
#             }
#           ''' % (self.test_email, self.test_username, self.test_password, 'true')
#         }
#         response = self.client.post(reverse('api'), data=data, format='json')
#         self.assertEqual(response.status_code, 200)
#         if with_error:
#             self.assertIsNotNone(response.json()['data']['signup']['errors'])
#         else:
#             self.assertNotIn('errors', response.json())
#             self.assertIn('user', response.json()['data']['signup'])
#             self.assertIsNotNone(
#                 response.json()['data']['signup']['user']['id'])
#             self.assertTrue(User.objects.all().count() > 0)

#     def test_create_user_from_the_origin_form_twice(self):
#         self.test_create_user_from_the_origin_form()
#         self.test_create_user_from_the_origin_form(with_error=True)

#     def test_create_user_from_the_origin_form_with_wrong_passwords(self):
#         data = {
#             "query": '''
#             mutation {
#               signup(
#               email: "%s", username: "%s", password1: "%s",  password2: "%s", isNewsletter: %s, locationGlobId: "%s") {
#                 errors
#                 user {
#                   id
#                 }
#               }
#             }
#           ''' % (self.test_email, self.test_username, self.test_password,
#                  self.test_password+"xxx", self.is_newsletter, self.test_location_glob_id)
#         }
#         response = self.client.post(reverse('api'), data=data, format='json')
#         self.assertEqual(response.status_code, 200)
#         self.assertIsNotNone(response.json()['data']['signup']['errors'])
#         self.assertTrue(User.objects.filter().count() == 0)


# class UserTestCase(TestCase):
#     def setUp(self):
#         self.test_email = 'marketplace.ddi.test1@gmail.com'
#         self.test_email2 = 'marketplace.ddi.test2@gmail.com'
#         self.test_username = 'testuser1'
#         self.test_username2 = 'testuser2'
#         self.test_password = '12345678'
#         self.test_new_password = '87654321'
#         self.is_newsletter = 'true'
#         country = Country.objects.create()
#         self.test_city = City.objects.create(country=country)
#         self.test_location_glob_id = to_global_id(
#             CityType.__name__, self.test_city.id)
#         self.biography = 'Some bio'
#         self.picture = SimpleUploadedFile(
#             "test_file.mp4", b"file_content", content_type="video/mp4")
#         self.test_user1 = User.objects.create_user(
#             username=self.test_username, password=self.test_password, email=self.test_email)
#         self.test_user1.save()

#         token = get_token(self.test_user1)
#         self.test_user1.profile.active_token = token
#         self.test_user1.profile.save()

#     def test_get_users(self):
#         data = {
#           "query": '''
#             query {
#               users {
#                 edges {
#                   node {
#                     id
#                     email
#                   }
#                 }
#               }
#             }
#           '''
#         }
#         response = self.client.post(reverse('api'), data=data)
#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertEqual(1, len(response.json()['data']['users']['edges']))

#     def test_get_user_by_id(self):
#         user_global_id = to_global_id(UserType.__name__, self.test_user1.id)
#         data = {
#           "query": '''
#             query {
#               user(id: "%s") {
#                 id
#                 email
#               }
#             }
#           ''' % (user_global_id,)
#         }
#         response = self.client.post(reverse('api'), data=data)
#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertEqual(self.test_email, response.json()
#                          ['data']['user']['email'])
#         self.assertEqual(user_global_id, response.json()['data']['user']['id'])

#     def test_get_user_with_wrong_id(self):
#         data = {
#           "query": '''
#             query {
#               user(id: "%s") {
#                 id
#                 email
#               }
#             }
#           ''' % ('xxxxxxxxxxx',)
#         }
#         response = self.client.post(reverse('api'), data=data)
#         self.assertEqual(response.status_code, 200)
#         self.assertIsNone(response.json()['data']['user'])

#     def test_get_users_by_filtered_email(self):
#         data = {
#           "query": '''
#             query {
#               users(email_Icontains:"%s") {
#                 edges {
#                   node {
#                     id
#                     email
#                   }
#                 }
#               }
#             }
#           ''' % (self.test_email[:4],)
#         }
#         response = self.client.post(reverse('api'), data=data)
#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertEqual(1, len(response.json()['data']['users']['edges']))

#     def test_get_users_by_wrong_filtered_email(self):
#         data = {
#           "query": '''
#             query {
#               users(email_Icontains:"%s") {
#                 edges {
#                   node {
#                     id
#                     email
#                   }
#                 }
#               }
#             }
#           ''' % ("xxxxxxxxxxxxxxx",)
#         }
#         response = self.client.post(reverse('api'), data=data)
#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertEqual(0, len(response.json()['data']['users']['edges']))

#     def test_get_me(self):
#         data = {
#           "query": '''
#             query {
#               me {
#                 id
#                 email
#               }
#             }
#           '''
#         }
#         headers = {
#             "HTTP_AUTHORIZATION": f"JWT {self.test_user1.profile.active_token}"}
#         response = self.client.post(reverse('api'), data=data, **headers)
#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertIsNotNone(response.json()['data']['me'])
#         self.assertEqual(self.test_email, response.json()
#                          ['data']['me']['email'])

#     def test_update_profile(self):
#         data = {
#           "query": '''
#             mutation {
#               updateProfile(
#               email:"%s", username:"%s", locationGlobId:"%s", biography:"%s", picture:{file: "%s", name: "%s"}, isNewsletter: %s) {
#                 user {
#                   id
#                   email
#                   profile {
#                     isNewsletter
#                     location {
#                       name
#                     }
#                     biography
#                     picture
#                   }
#                 }
#               }
#             }
#           ''' % (self.test_email2, self.test_username2, self.test_location_glob_id, self.biography, self.picture.file,
#                  self.picture.name, self.is_newsletter)
#         }
#         headers = {
#             "HTTP_AUTHORIZATION": f"JWT {self.test_user1.profile.active_token}"}
#         response = self.client.post(reverse('api'), data=data, **headers)
#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertIsNotNone(response.json()['data']['updateUser']['user'])
#         self.assertEqual(self.test_email2, response.json()[
#                          'data']['updateUser']['user']['email'])
#         self.assertEqual(self.test_email2, User.objects.first().email)
#         self.assertEqual(
#             self.biography, User.objects.first().profile.biography)

#     def test_update_profile_with_empty_data(self):
#         data = {
#           "query": '''
#             mutation {
#               updateProfile {
#                 user {
#                   id
#                   email
#                 }
#               }
#             }
#           '''
#         }
#         headers = {
#             "HTTP_AUTHORIZATION": f"JWT {self.test_user1.profile.active_token}"}
#         response = self.client.post(reverse('api'), data=data, **headers)
#         self.assertEqual(response.status_code, 200)
#         self.assertNotIn('errors', response.json())
#         self.assertIsNotNone(response.json()['data']['updateUser']['user'])
#         self.assertEqual(self.test_email, response.json()[
#                          'data']['updateUser']['user']['email'])
#         self.assertEqual(self.test_email, User.objects.first().email)

#     def test_update_profile_with_wrong_location(self):
#         data = {
#           "query": '''
#             mutation {
#               updateProfile(
#               email:"%s", username:"%s", locationGlobId:"%s", biography:"%s", picture:{file: "%s", name: "%s"}) {
#                 errors
#                 user {
#                   id
#                   email
#                   profile {
#                     isNewsletter
#                     location {
#                       name
#                     }
#                     biography
#                     picture
#                   }
#                 }
#               }
#             }
#           ''' % (self.test_email2, self.test_username2, 'xxxxxxxxxxxxxxxxxxxx', self.biography, self.picture.file,
#                  self.picture.name)
#         }
#         headers = {
#             "HTTP_AUTHORIZATION": f"JWT {self.test_user1.profile.active_token}"}
#         response = self.client.post(reverse('api'), data=data, **headers)
#         self.assertEqual(response.status_code, 200)
#         self.assertIsNotNone(response.json()['data']['updateUser']['errors'])
#         self.assertEqual(self.test_email, User.objects.first().email)
