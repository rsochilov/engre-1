from django.urls import reverse
from django.test import TestCase
from cms.test_utils.testcases import CMSTestCase
from django.test.utils import override_settings
from rest_framework.test import APIRequestFactory

from core.views import phone_email_send as phone_email_send_view


class PhoneEmailTest(CMSTestCase):
    @override_settings(ROOT_URLCONF="core.tests.urls")
    def test_phone_email_valid(self):
        data = {"phone": "+380987655441"}
        factory = APIRequestFactory()
        request = factory.post(reverse("core:phone_call"), data)
        response = phone_email_send_view(request)
        self.assertEqual(response.status_code, 200, f"{response.data}")

    @override_settings(ROOT_URLCONF="core.tests.urls")
    def test_phone_email_invalid_number(self):
        factory = APIRequestFactory()
        invalid_numbers = [
            "121345+213",
            "213a123",
            "",
            "one_two_three",
            "+3_213_213_232",
        ]
        for number in invalid_numbers:
            data = {"phone": number}
            request = factory.post(reverse("core:phone_call"), data)
            response = phone_email_send_view(request)
            self.assertEqual(response.status_code, 400, f"{response.data}")
            self.assertEqual(response.data["detail"], "Invalid phone number")

    @override_settings(ROOT_URLCONF="core.tests.urls")
    def test_phone_email_invalid_data(self):
        data = {"invalid": "data"}
        factory = APIRequestFactory()
        request = factory.post(reverse("core:phone_call"), data)
        response = phone_email_send_view(request)
        self.assertEqual(response.status_code, 400, f"{response.data}")
        self.assertEqual(response.data["detail"], "No phone provided")
