from io import StringIO
from unittest import skip

from django.test import TestCase
from django.core.management import call_command

from about.models import FieldOfActivity


class LoadAboutAchievementsCommandTestCase(TestCase):
    # @classmethod
    # def setUpClass(cls):
    #     for i in range(1, 100):
    #         FieldOfActivity.objects.get_or_create(pk=i, name=f'name_{i}')

    # @classmethod
    # def tearDownClass(cls):
    #     FieldOfActivity.objects.all().delete()

    @skip("Too slow, no need right now")
    def test_command_output(self):
        out = StringIO()
        call_command("load_about_achievements", stdout=out)
        self.assertNotIn("Exception", out.getvalue())
        self.assertNotIn("Error", out.getvalue())
