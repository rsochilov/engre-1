from django.urls import reverse
from django.test import TestCase, Client
from cms.test_utils.testcases import CMSTestCase
from django.test.utils import override_settings


class XrobotsMiddlewareTest(CMSTestCase):
    # @override_settings(ROOT_URLCONF='core.tests.urls')
    def test_headers_contains_xrobots(self):
        client = Client(HTTP_USER_AGENT="Mozilla/5.0")
        resp = client.get("/")
        self.assertEqual(resp.status_code, 200)
        tag = resp.get("X-Robots-Tag")
        self.assertTrue(tag)
        self.assertEqual(tag, "all")
