import graphene
from accounts.models import User
from core.types import Mutation
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ValidationError
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext_lazy as _
from graphene_django.types import DjangoObjectType


class PasswordResetVerify(DjangoObjectType):
    status = graphene.String()

    class Meta:
        model = User

    def resolve_status(self, info, **kwargs):
        return "Success"


class PasswordResetVerifyQuery(graphene.ObjectType):
    password_reset_verify = graphene.Field(PasswordResetVerify)

    @staticmethod
    def validate_uidb64(uidb64):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            raise ValidationError(_("Invalid uid value"))
        return user

    @staticmethod
    def validate_token(token, user):
        if not default_token_generator.check_token(user, token):
            raise ValidationError(_("Invalid token value"))
        return token

    def resolve_password_reset_verify(self, info, **kwargs):
        user = PasswordResetVerifyQuery.validate_uidb64(info)
        PasswordResetVerifyQuery.validate_token(info, user)
        return user


class PasswordResetConfirm(Mutation):
    status = graphene.String()

    class Arguments:
        new_password = graphene.String(required=True)
        repeat_password = graphene.String(required=True)
        uidb64 = graphene.String(required=True)
        token = graphene.String(required=True)

    def mutate(self, info, new_password, repeat_password, uidb64, token):
        user = PasswordResetVerifyQuery.validate_uidb64(uidb64)
        PasswordResetVerifyQuery.validate_token(token, user)
        set_password_form = SetPasswordForm(
            user=user,
            data={"new_password1": new_password, "new_password2": repeat_password},
        )
        if not set_password_form.is_valid():
            raise ValidationError(set_password_form.errors)
        set_password_form.save()
        return PasswordResetConfirm(status=_("Success"))


class PasswordResetMutations(graphene.ObjectType):
    password_reset_confirm = PasswordResetConfirm.Field()


schema = graphene.Schema(
    query=PasswordResetVerifyQuery, mutation=PasswordResetMutations
)
