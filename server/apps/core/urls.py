from django.conf.urls import url
from django.urls import path
from .views import temp_file_upload, phone_email_send

app_name = "core"
urlpatterns = [
    url("temp-file-upload/", temp_file_upload, name="temp-file-upload"),
    url("phone_call/", phone_email_send, name="phone_call"),
]
