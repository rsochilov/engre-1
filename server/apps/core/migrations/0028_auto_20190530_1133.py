# Generated by Django 2.1.4 on 2019-05-30 11:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0027_auto_20190530_1130"),
    ]

    operations = [
        migrations.AlterField(
            model_name="pagerenderingcondition",
            name="condition",
            field=models.CharField(
                choices=[
                    ("is_authenticated", "Visible for authenticated"),
                    ("is_customer", "Visible for customer"),
                    ("is_contractor", "Visible for contractor"),
                    ("is_profile_completed", "Visible if profile completed"),
                    (
                        "has_intellectual_services",
                        "Visible if has intellectual services",
                    ),
                    ("is_pm", "Visible for PM"),
                    ("is_owner", "Visible for company owner"),
                    ("is_blogger", "Visible for blogger"),
                    ("without_role", "Visible for users without role yet"),
                ],
                max_length=50,
            ),
        ),
    ]
