# Generated by Django 2.1.4 on 2019-02-26 10:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0009_auto_20190226_1045"),
    ]

    operations = [
        migrations.AddField(
            model_name="intellectualservicessettings",
            name="projects_count",
            field=models.PositiveIntegerField(default=0),
        ),
    ]
