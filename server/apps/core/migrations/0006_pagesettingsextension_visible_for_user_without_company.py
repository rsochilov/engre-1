# Generated by Django 2.1.4 on 2019-02-21 15:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        (
            "core",
            "0005_pagesettingsextension_visible_for_user_with_uncompleted_profile",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="pagesettingsextension",
            name="visible_for_user_without_company",
            field=models.BooleanField(default=True, verbose_name="uncompleted company"),
        ),
    ]
