import graphene
from core.inputs import FileInput, TimezoneInput


class CompanyInput(graphene.InputObjectType):
    name = graphene.String(required=True)
    logo = graphene.InputField(FileInput)
    location = graphene.ID()
    ownership = graphene.ID()
    date_creation = graphene.Date()
    website = graphene.String(required=True)
    vanity_url = graphene.String()
    languages = graphene.List(graphene.ID)
    role = graphene.Int()
    company_size = graphene.Int()
    timezone = graphene.InputField(TimezoneInput)
    owner = graphene.String()


class CompanyInfoInput(graphene.InputObjectType):
    slogan = graphene.String()
    description = graphene.String()
    video = graphene.InputField(FileInput)
    picture = graphene.InputField(FileInput)
    presentation = graphene.InputField(FileInput)


class PartnershipInput(graphene.InputObjectType):
    name = graphene.String(required=True)
    description = graphene.String(required=False)
    user = graphene.ID(required=True)
    company = graphene.ID(required=True)
