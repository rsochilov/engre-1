from .models import Company, Worker


def is_customer(user):
    try:
        if user.work.company.role == Company.CUSTOMER:
            return True

        return False
    except Worker.DoesNotExist:
        return True
