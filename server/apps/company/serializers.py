from Crypto.PublicKey import RSA
from django.db.models import Q
from django.template.loader import render_to_string

from accounts.models import User
from accounts.serializers import UserShortSerializer
from core.serializers import NameSerializer, NameSlugSerializer
from core.timezones import new_timezones
from market_location.serializers import CitySerializer, LocationSerializer
from rest_framework import serializers

from .models import (
    Company,
    CompanyPosition,
    Document,
    Ownership,
    Review,
    Signature,
    Worker,
)
from .tasks import send_user_invitation
from accounts.tasks import send_email
from project.models import ProjectUsers


class CompanyPositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanyPosition
        fields = "__all__"


class OwnershipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ownership
        fields = "__all__"


class BaseCompanySerializer(serializers.ModelSerializer):
    language_list = serializers.SerializerMethodField(read_only=True)
    ownership = NameSerializer()
    location = LocationSerializer()
    form_location = serializers.SerializerMethodField(read_only=True)
    ceo = serializers.CharField(source="owner")

    def get_language_list(self, obj):
        return obj.get_language_list()

    def get_form_location(self, obj):
        return CitySerializer(obj.location).data

    class Meta:
        model = Company
        fields = (
            "id",
            "name",
            "location",
            "language_list",
            "ownership",
            "ceo",
            "role",
            "form_location",
        )


class WorkerSerializer(serializers.ModelSerializer):
    company = BaseCompanySerializer()
    user = UserShortSerializer()

    class Meta:
        model = Worker
        fields = "__all__"


class CompanySerializer(BaseCompanySerializer, serializers.ModelSerializer):
    reviews_count = serializers.SerializerMethodField(read_only=True)
    timezone = serializers.SerializerMethodField()
    languages = serializers.SerializerMethodField()
    workers = WorkerSerializer(many=True)
    url = serializers.SerializerMethodField()
    intellectualservices = serializers.SerializerMethodField()
    is_info_completed = serializers.SerializerMethodField(read_only=True)
    rating = serializers.SerializerMethodField(read_only=True)
    owner = serializers.SerializerMethodField()

    def get_reviews_count(self, obj):
        return obj.reviews.count()

    def get_rating(self, obj):
        return obj.rating

    def get_timezone(self, obj):
        if obj.timezone:
            for timezone in new_timezones:
                if timezone["tz_db"] == obj.timezone.zone:
                    return timezone

        return None

    def get_languages(self, obj):
        if obj.languages:
            return NameSlugSerializer(obj.languages.all(), many=True).data

        return None

    def get_intellectualservices(self, obj):
        from project.serializers import ServiceCompanySerializer

        if hasattr(obj, "intellectualservices") and obj.intellectualservices.exists():
            return ServiceCompanySerializer(
                obj.intellectualservices.all(), many=True
            ).data

    def get_url(self, obj):
        return obj.get_absolute_url()

    def get_is_info_completed(self, obj):
        if (
            not obj.slogan
            or not obj.description
            or not obj.picture
            or not obj.video
            or not obj.presentation
        ):
            return False

        return True

    def get_owner(self, obj):
        try:
            return NameSerializer(obj.get_owner()).data
        except AttributeError:
            return None

    class Meta:
        model = Company
        fields = "__all__"
        extra_fields = ["company_rating"]


class DocumentSerializer(serializers.ModelSerializer):
    project = serializers.SerializerMethodField()
    is_signed = serializers.SerializerMethodField()

    class Meta:
        model = Document
        fields = "__all__"

    def get_project(self, obj):
        from project.serializers import ProjectSerializer

        return ProjectSerializer(obj.project).data

    def get_is_signed(self, obj):
        return obj.is_signed


class SignDocumentSerializer(serializers.ModelSerializer):
    public_key = serializers.FileField(write_only=True)
    is_signed = serializers.SerializerMethodField()

    class Meta:
        model = Document
        fields = ("id", "is_signed", "public_key")

    def get_is_signed(self, obj):
        return obj.is_signed

    def update(self, instance, validated_data):
        request = self.context.get("request")
        user = request.user
        self._validate_document_status(user, instance)
        public_key_file = validated_data.get("public_key")

        try:
            public_key = RSA.import_key(public_key_file.read())
        except ValueError:
            raise serializers.ValidationError("Invalid secret key!")

        key = user.get_private_key()

        if key:
            if user.check_public_key(public_key):
                signature = Signature.objects.create(document=instance, user=user)
                signature.sign(key)
                signature.save()
                self._set_document_status(user, instance)
                self._send_email(instance, user)
            else:
                raise serializers.ValidationError("Invalid secret key!")
        else:
            raise serializers.ValidationError("Your account does not verified!")

        return super(SignDocumentSerializer, self).update(instance, validated_data)

    def _validate_document_status(self, user: User, document: Document):
        if (
            document.status == document.FULLSIGNED
            or user.is_customer
            and document.status == document.BYCUSTOMER
            or user.is_contractor
            and document.status == document.BYCONTRACTOR
            or user.is_pm
            and document.status == document.BYPM
        ):
            raise serializers.ValidationError("Already signed")

    def _set_document_status(self, user: User, document: Document):
        if document.status != document.NOSIGN:
            document.status = document.FULLSIGNED
        elif user.is_customer:
            document.status = document.BYCUSTOMER
        elif user.is_pm:
            document.status = document.BYPM
        else:
            document.status = document.BYCONTRACTOR
        document.save()

    def _send_email(self, document: Document, user: User):

        try:
            recipient = (
                Signature.objects.select_related("user")
                .get(Q(document=document) & ~Q(user=user))
                .user.email
            )
            template = "documents_api/sign_doc_email.html"
        except Signature.DoesNotExist:
            try:
                recipient = (
                    ProjectUsers.objects.select_related("user")
                    .get(Q(project=document.project) & ~Q(user=user))
                    .user.email
                )
                template = "documents_api/first_signed_doc_email.html"
            except ProjectUsers.DoesNotExist:
                err = serializers.ValidationError(
                    "Mail send critical error: User from project not found"
                )
                err.status = 500
                raise err
        context = {
            "document": document,
            "user": user,
            "host": self.context.get("request").get_host(),
        }
        body = render_to_string(template, context)
        send_email.delay("Engre document sign", body, [recipient])


class CreateUpdateCompanySerializer(serializers.ModelSerializer):
    languages = serializers.ListField()
    user = serializers.IntegerField()

    class Meta:
        model = Company
        fields = (
            "id",
            "name",
            "location",
            "timezone",
            "owner",
            "ownership",
            "date_creation",
            "website",
            "company_size",
            "languages",
            "logo",
            "role",
            "user",
            "picture",
            "video",
            "presentation",
            "slogan",
            "description",
            "video_link",
        )

    def create(self, validated_data):
        user = validated_data.pop("user")
        languages = validated_data.pop("languages")
        instance = Company.objects.create(**validated_data)

        if instance:
            instance.languages.add(*[int(lan) for lan in languages[0].split(",")])
            Worker.objects.create(user_id=user, company=instance, is_owner=True)

        return instance

    def update(self, instance, validated_data):
        languages = None
        if validated_data.get("languages"):
            languages = validated_data.pop("languages")

        updated_instance = super().update(instance, validated_data)

        if languages:
            updated_instance.languages.clear()
            updated_instance.languages.add(
                *[int(lan) for lan in languages[0].split(",")]
            )

        return updated_instance


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = "__all__"


class InviteUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(write_only=True)

    class Meta:
        model = Company
        fields = ("id", "email")

    def update(self, instance, validated_data):
        request = self.context.get("request")
        email = validated_data.get("email")

        if not email:
            raise serializers.ValidationError("Invitee email is required!")

        user = User.objects.filter(email=email).first()

        if not user:
            user = User.objects.create(email=email)

        if not hasattr(user, "work"):
            work = Worker.objects.create(user=user)
            user.work = work
            user.save()

        if not request.user.is_pm and user.is_owner:
            raise serializers.ValidationError("You can't invite an owner of a company")
        elif request.user.is_pm and not user.is_owner:
            raise serializers.ValidationError("The user is not owner of a company")
        elif request.user.is_pm and user.is_customer:
            raise serializers.ValidationError("The user is not a contractor")

        send_user_invitation.delay(request.user.pk, user.pk, instance.pk)

        return super(InviteUserSerializer, self).update(instance, validated_data)


class DeleteMemberFromCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Worker
        fields = ("id",)

    def update(self, instance, validated_data):
        instance.company = None
        instance.save()

        return super().update(instance, validated_data)


class DeleteMemberFromPmSerializer(serializers.ModelSerializer):
    pm_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Worker
        fields = ("id", "pm_id")

    def update(self, instance, validated_data):
        pm_id = validated_data.get("pm_id")

        instance.project_managers.remove(pm_id)

        return super().update(instance, validated_data)
