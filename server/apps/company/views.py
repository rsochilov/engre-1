import json
from accounts.models import User
from accounts.tasks import send_support_email

from core.mixins import VueTemplateMixin
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, reverse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic.edit import FormView
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .forms import InviteUserForm
from .models import Company, Document, Worker
from .serializers import (
    ReviewSerializer,
    CreateUpdateCompanySerializer,
    DeleteMemberFromCompanySerializer,
    DeleteMemberFromPmSerializer,
    InviteUserSerializer,
    SignDocumentSerializer,
    WorkerSerializer,
)
from .tokens import invite_user_token

INTERNAL_INVITE_URL_TOKEN = "done"
INTERNAL_INVITE_SESSION_TOKEN = "_password_set_token"


class CompanyViewSet(generics.CreateAPIView, generics.UpdateAPIView):
    serializer_class = CreateUpdateCompanySerializer

    def get_object(self):
        return Company.objects.get(pk=self.request.data.get("pk"))

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        instance = self.perform_create(serializer)
        instance.timezone = request.data.get("timezone")
        instance.save()

        url = (
            reverse("projects-list")
            if int(request.data.get("role")) == 2
            else reverse("services-list")
        )

        return HttpResponse(
            json.dumps({"success": True, "url": url}), content_type="application/json"
        )

    def update(self, request, *args, **kwargs):
        timezone = request.data.get("timezone")

        instance = self.get_object()
        if timezone:
            instance.timezone = timezone
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        self.perform_update(serializer)

        url = (
            reverse("projects-list") if instance.role == 2 else reverse("services-list")
        )

        return HttpResponse(
            json.dumps({"success": True, "url": url}), content_type="application/json"
        )

    def perform_create(self, serializer):
        return serializer.save()


class ReviewViewSet(generics.CreateAPIView):
    serializer_class = ReviewSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        self.perform_create(serializer)
        send_support_email.delay(
            subject=f"New review from {request.user}",
            body=f"{request.user} created new review, check it out!",
        )
        return Response(data={"success": True,})


class SignDocumentAPIView(generics.UpdateAPIView):
    queryset = Document.objects.all()
    serializer_class = SignDocumentSerializer
    permission_classes = (IsAuthenticated,)


class InviteUserAPIView(generics.UpdateAPIView):
    queryset = Company.objects.all()
    serializer_class = InviteUserSerializer
    permission_classes = (IsAuthenticated,)


class InviteUserConfirmView(VueTemplateMixin, FormView):
    form_class = InviteUserForm
    token_generator = invite_user_token
    template_name = "company/invite-user-confirm.html"

    @method_decorator(sensitive_post_parameters())
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        assert "uidb64" in kwargs and "cidb64" in kwargs and "token" in kwargs

        self.validlink = False
        self.user = self.get_user(kwargs["uidb64"])
        self.inviter = self.get_user(kwargs["invidb64"])
        self.company = self.get_company(kwargs["cidb64"])

        if self.user is not None and self.company is not None:
            token = kwargs["token"]
            if token == INTERNAL_INVITE_URL_TOKEN:
                session_token = self.request.session.get(INTERNAL_INVITE_SESSION_TOKEN)
                if self.token_generator.check_token(self.user, session_token):
                    if self.user.password == "":
                        # if user has no password then display set password form
                        self.validlink = True
                        return super().dispatch(*args, **kwargs)

                    if self.inviter.is_pm:
                        self.invite_user_to_membership()
                    else:
                        self.invite_user_to_company()

                    return render(
                        self.request,
                        "company/invite-user-done.html",
                        self.get_context_data(),
                    )
            else:
                if self.token_generator.check_token(self.user, token):
                    # Store the token in the session and redirect to the
                    # password set form at a URL without the token. That
                    # avoids the possibility of leaking the token in the
                    # HTTP Referer header.

                    self.request.session[INTERNAL_INVITE_SESSION_TOKEN] = token
                    redirect_url = self.request.path.replace(
                        token, INTERNAL_INVITE_URL_TOKEN
                    )
                    return HttpResponseRedirect(redirect_url)

        # Display the "invalid link" page.
        return self.render_to_response(self.get_context_data())

    def get_user(self, uidb64):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        return user

    def invite_user_to_company(self):
        self.user.work.company = self.company
        self.user.is_active = True
        self.user.work.save()
        self.user.save()

    def invite_user_to_membership(self):
        self.inviter.projectmanager.membership.add(self.user.work)

    def get_company(self, cidb64):
        try:
            cid = force_text(urlsafe_base64_decode(cidb64))
            company = Company.objects.get(pk=cid)
        except (TypeError, ValueError, OverflowError, Company.DoesNotExist):
            company = None

        return company

    def get_props(self):
        return {"submitText": "set"}

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.user

        return kwargs

    def form_valid(self, form):
        form.save(self.company.id, self.inviter)
        del self.request.session[INTERNAL_INVITE_SESSION_TOKEN]
        return render(
            self.request, "company/invite-user-done.html", self.get_context_data()
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.validlink:
            context["validlink"] = True
        else:
            context["validlink"] = False

        context["inviter"] = self.inviter

        return context


class ToggleActivenessWorkerAPIView(generics.UpdateAPIView):
    queryset = Worker.objects.all()
    serializer_class = WorkerSerializer
    permission_classes = (IsAuthenticated,)


class DeleteMemberFromCompanyAPIView(generics.UpdateAPIView):
    queryset = Worker.objects.all()
    serializer_class = DeleteMemberFromCompanySerializer
    permission_classes = (IsAuthenticated,)


class DeleteMemberFromPmAPIView(generics.UpdateAPIView):
    queryset = Worker.objects.all()
    serializer_class = DeleteMemberFromPmSerializer
    permission_classes = (IsAuthenticated,)
