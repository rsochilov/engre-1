from datetime import datetime

import factory

from core.factories import FileFactory, UserFactory
from market_location.factories import CityFactory


class OwnershipFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "company.Ownership"

    name = factory.Sequence(lambda n: "ownership%s" % n)


class CompanyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "company.Company"
        django_get_or_create = ("name",)

    owner = factory.SubFactory(UserFactory)
    name = factory.Sequence(lambda n: "company%s" % n)
    location = factory.SubFactory(CityFactory)
    ownership = factory.SubFactory(OwnershipFactory)
    date_creation = datetime.now()
    website = factory.Sequence(lambda n: "website%s" % n)
    vanity_url = factory.Sequence(lambda n: "vanity url%s" % n)
    description = factory.Sequence(lambda n: "description%s" % n)
    logo = factory.django.FileField(from_func=FileFactory)
    video = factory.django.FileField(
        from_func=lambda: FileFactory(content_type="video/mp4")
    )
    presentation = factory.django.FileField(
        from_func=lambda: FileFactory(content_type="application/pdf")
    )
    company_size = 50
    role = 1

    @factory.post_generation
    def worker_set(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for worker in extracted:
                self.workers.add(worker)


class WorkerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "company.Worker"

    title = factory.Sequence(lambda n: "worker title%s" % n)
    user = factory.SubFactory(UserFactory)
    company = factory.SubFactory(CompanyFactory)


class PartnershipFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "company.Partnership"

    name = factory.Sequence(lambda n: "partnership%s" % n)
    description = factory.Sequence(lambda n: "description%s" % n)
    user = factory.SubFactory(UserFactory)
    company = factory.SubFactory(CompanyFactory)
