from django.core.management.base import BaseCommand
from django.utils.text import slugify
from loguru import logger

from company.models import Ownership


class Command(BaseCommand):
    help = "Creates slugs for company_ownership table rows"

    def handle(self, *args, **options):
        bugged_ownerships = Ownership.objects.filter(slug__exact="")
        for own in bugged_ownerships:
            own.slug = slugify(own.name)
            own.save(update_fields=["slug"])
            logger.success(f"Company ownership '{own}' slug updated")
        logger.info(f"Updated {bugged_ownerships.count()} ownership(-s)")
