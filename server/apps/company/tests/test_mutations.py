import shutil

import pytest

from core.factories import FileFactory, LanguageFactory, UserFactory
from core.utils import Context, to_global_id
from django.conf import settings
from django.test import override_settings
from graphene.test import Client
from market_location.factories import CityFactory
from marketplace.schema import schema

from ..factories import CompanyFactory, OwnershipFactory, WorkerFactory
from ..models import Company


def teardown_module(module):
    if settings.TESTING_SETTINGS:
        shutil.rmtree(settings.MEDIA_ROOT)


@pytest.mark.django_db
def test_create_ownership(snapshot):
    client = Client(schema)

    user = UserFactory()

    executed = client.execute(
        """
        mutation ($name: String!) {
            createOwnership(name: $name) {
                ownership {
                    name
                }
            }
        }
    """,
        variable_values={"name": "test ownership"},
        context_value=Context(user=user),
    )

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_create_company(snapshot):
    client = Client(schema)

    user = UserFactory()
    owner = UserFactory()
    location = CityFactory()
    ownership = OwnershipFactory()
    languages = LanguageFactory.create_batch(3)

    logo = FileFactory()
    video = FileFactory(content_type="video/mp4")
    presentation = FileFactory(content_type="application/pdf")

    executed = client.execute(
        """
        mutation ($companyData: CompanyInput!) {
            createCompany(companyData: $companyData) {
                errors
                company {
                    owner {
                        username
                    }
                    name
                    logo
                    video
                    presentation
                    location
                    ownership {
                        name
                    }
                    dateCreation
                    website
                    vanityUrl
                    description
                    languages {
                        edges {
                            node {
                                name
                            }
                        }
                    }
                    role
                }
            }
        }
    """,
        variable_values={
            "companyData": {
                "name": "test company",
                "logo": {"file": logo.file, "name": logo.name},
                "video": {"file": video.file, "name": video.name},
                "presentation": {"file": presentation.file, "name": presentation.name},
                "location": to_global_id(location),
                "ownership": to_global_id(ownership),
                "dateCreation": "2018-01-01",
                "website": "company.org",
                "vanityUrl": "vanityUrl",
                "description": "company desc",
                "languages": to_global_id(languages, many=True),
                "role": Company.EMPLOYER,
                "owner": to_global_id(owner),
            }
        },
        context_value=Context(user=user),
    )

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_update_company(snapshot):
    client = Client(schema)

    user = UserFactory()
    owner = UserFactory()
    location = CityFactory()
    ownership = OwnershipFactory()
    languages = LanguageFactory.create_batch(3)
    company = CompanyFactory()

    logo = FileFactory()
    video = FileFactory(content_type="video/mp4")
    presentation = FileFactory(content_type="application/pdf")

    executed = client.execute(
        """
        mutation ($companyId: ID!, $companyData: CompanyInput!) {
            updateCompany(companyId: $companyId, companyData: $companyData) {
                errors
                company {
                    owner {
                        username
                    }
                    name
                    logo
                    video
                    presentation
                    location
                    ownership {
                        name
                    }
                    dateCreation
                    website
                    vanityUrl
                    description
                    languages {
                        edges {
                            node {
                                name
                            }
                        }
                    }
                    role
                }
            }
        }
    """,
        variable_values={
            "companyId": to_global_id(company),
            "companyData": {
                "name": "updated name",
                "logo": {"file": logo.file, "name": logo.name},
                "video": {"file": video.file, "name": video.name},
                "presentation": {"file": presentation.file, "name": presentation.name},
                "location": to_global_id(location),
                "ownership": to_global_id(ownership),
                "dateCreation": "2018-02-02",
                "website": "newcompany.org",
                "vanityUrl": "new vanityUrl",
                "description": "updated desc",
                "languages": to_global_id(languages, many=True),
                "role": Company.CUSTOMER,
                "owner": to_global_id(owner),
            },
        },
        context_value=Context(user=user),
    )

    snapshot.assert_match(executed)


@pytest.mark.django_db
@override_settings(CELERY_ALWAYS_EAGER=True, CELERY_EAGER_PROPAGATES_EXCEPTIONS=True)
def test_send_user_invitation(snapshot):
    client = Client(schema)

    user = UserFactory()
    user_to_invite = UserFactory()
    company = CompanyFactory()

    executed = client.execute(
        """
        mutation ($email: String!, $companyId: ID!) {
            inviteUser(email: $email, companyId: $companyId) {
                status
            }
        }
    """,
        variable_values={
            "email": user_to_invite.email,
            "companyId": to_global_id(company),
        },
        context_value=Context(user=user),
    )

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_send_user_invitation_to_the_same_user_in_company(snapshot):
    client = Client(schema)

    user = UserFactory()
    company = CompanyFactory(worker_set=WorkerFactory.create_batch(3))
    worker = WorkerFactory()

    executed = client.execute(
        """
        mutation ($email: String!, $companyId: ID!) {
            inviteUser(email: $email, companyId: $companyId) {
                errors
                status
            }
        }
    """,
        variable_values={
            "email": worker.user.email,
            "companyId": to_global_id(company),
        },
        context_value=Context(user=user),
    )

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_create_partnership(snapshot):
    client = Client(schema)

    user = UserFactory()
    company = CompanyFactory()

    executed = client.execute(
        """
        mutation ($partnershipData: PartnershipInput!) {
            createPartnership(partnershipData: $partnershipData) {
                errors
                partnership {
                    name
                    description
                    user {
                        username
                    }
                    company {
                        name
                    }
                }
            }
        }
    """,
        variable_values={
            "partnershipData": {
                "name": "test partnership",
                "description": "test desc",
                "user": to_global_id(user),
                "company": to_global_id(company),
            }
        },
        context_value=Context(user=user),
    )

    snapshot.assert_match(executed)


@pytest.mark.django_db
def test_create_language(snapshot):
    client = Client(schema)

    user = UserFactory()

    executed = client.execute(
        """
        mutation ($name: String!) {
            createLanguage(name: $name) {
                language {
                    name
                }
            }
        }
    """,
        variable_values={"name": "ENG"},
        context_value=Context(user=user),
    )

    snapshot.assert_match(executed)
