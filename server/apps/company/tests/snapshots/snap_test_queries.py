# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots["test_company 1"] = {
    "data": {
        "company": {
            "location": "city10, country10",
            "logo": "http://localhost:8000/media/logos/file30.png",
            "presentation": "http://localhost:8000/media/company_documents/file32.pdf",
            "role": 1,
            "video": "http://localhost:8000/media/videos/file31.mp4",
        }
    }
}
