import pytest

from core.utils import to_global_id
from graphene.test import Client
from marketplace.schema import schema

from ..factories import CompanyFactory


@pytest.mark.django_db
def test_company(snapshot):
    """
        Test query fields with custom resolvers:
        - logo
        - presentation
        - video
        - role
        - location
    """

    client = Client(schema)

    company = CompanyFactory()

    executed = client.execute(
        """
        query ($companyId: ID!) {
            company(id: $companyId) {
                logo
                presentation
                video
                role
                location
            }
        }
    """,
        variable_values={"companyId": to_global_id(company)},
    )

    snapshot.assert_match(executed)
