from django.contrib.auth.forms import SetPasswordForm


class InviteUserForm(SetPasswordForm):
    def save(self, company_id, inviter):
        user = super().save(commit=False)

        if inviter.is_pm:
            self.inviter.membership.add(user.work)
        else:
            user.work.company_id = company_id
            user.work.save()

        user.is_active = True
        user.save()
