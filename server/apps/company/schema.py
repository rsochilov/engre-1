import graphene
from core.timezones import get_timezone
from core.utils import get_media_absolute_url
from graphene_django import DjangoConnectionField
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from .models import Company, CompanyPosition, Ownership, Partnership, Review, Worker


class ReviewType(DjangoObjectType):
    class Meta:
        model = Review
        interfaces = (graphene.relay.Node,)
        filter_fields = ("company",)


class WorkerType(DjangoObjectType):
    class Meta:
        model = Worker
        interfaces = (graphene.relay.Node,)
        filter_fields = []


class CompanyPositionType(DjangoObjectType):
    class Meta:
        model = CompanyPosition
        interfaces = (graphene.relay.Node,)
        filter_fields = []


class CompanyType(DjangoObjectType):
    role = graphene.Int()
    video = graphene.String()
    logo = graphene.String()
    presentation = graphene.String()
    picture = graphene.String()
    language_list = graphene.String()
    timezone = graphene.String()
    company_size = graphene.Int()
    reviews_count = graphene.Int()
    has_projects = graphene.Boolean()
    has_services = graphene.Boolean()
    projects_count = graphene.Int()
    services_count = graphene.Int()

    def resolve_video(self, info):
        return get_media_absolute_url(self.video)

    def resolve_logo(self, info):
        return get_media_absolute_url(self.logo)

    def resolve_presentation(self, info):
        return get_media_absolute_url(self.presentation)

    def resolve_picture(self, info):
        return get_media_absolute_url(self.picture)

    def resolve_language_list(self, info):
        return ", ".join(self.languages.values_list("name", flat=True))

    def resolve_timezone(self, info):
        if not self.timezone:
            return None

        if isinstance(self.timezone, str):
            return get_timezone(self.timezone)

        return get_timezone(self.timezone.zone)

    def resolve_reviews_count(self, info):
        return self.reviews.count()

    # change after adding ability to archive
    def resolve_has_projects(self, info):
        return self.projects.count() > 0

    def resolve_has_services(self, info):
        return self.intellectualservices.count() > 0

    def resolve_projects_count(self, info):
        return self.projects.filter(is_archived=False).count()

    def resolve_services_count(self, info):
        return self.intellectualservices.count()

    class Meta:
        model = Company
        interfaces = (graphene.relay.Node,)
        filter_fields = {"id": ["exact",], "name": ["icontains", "exact"]}


class OwnershipType(DjangoObjectType):
    class Meta:
        model = Ownership
        interfaces = (graphene.relay.Node,)


class PartnershipType(DjangoObjectType):
    class Meta:
        model = Partnership
        interfaces = (graphene.relay.Node,)


class Query(graphene.ObjectType):
    companies = DjangoConnectionField(CompanyType)
    company = graphene.relay.Node.Field(CompanyType)
    ownerships = DjangoConnectionField(OwnershipType)
    ownership = graphene.relay.Node.Field(OwnershipType)
    partnerships = DjangoConnectionField(PartnershipType)
    partnership = graphene.relay.Node.Field(PartnershipType)
    reviews = DjangoFilterConnectionField(ReviewType)
