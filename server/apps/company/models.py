from datetime import datetime
from Crypto.Hash import SHA256
from Crypto.Signature import pkcs1_15

from accounts.models import User
from core.managers import CommonManager
from core.models import ArchivableMixin, Language, NameSlugModel, WatchableMixin
from django.db import models
from market_location.models import City
from timezone_field import TimeZoneField


class CompanyPosition(WatchableMixin):
    name = models.CharField(max_length=100)


class Ownership(NameSlugModel):
    objects = CommonManager()


class Document(WatchableMixin, ArchivableMixin):
    NOSIGN, BYCUSTOMER, BYCONTRACTOR, BYPM, FULLSIGNED = range(1, 6)
    TYPES_STATUS = (
        (NOSIGN, "Not signed"),
        (BYCUSTOMER, "Signed by customer"),
        (BYCONTRACTOR, "Signed by contractor"),
        (BYPM, "Signed by PM"),
        (FULLSIGNED, "Signed by all users"),
    )
    INVOICE, CONTRACT, SERVICE_ACCEPTANCE_ACT = range(1, 4)
    TYPES = (
        (INVOICE, "invoice"),
        (CONTRACT, "contract"),
        (SERVICE_ACCEPTANCE_ACT, "service acceptance act"),
    )

    name = models.CharField(max_length=128)
    project = models.ForeignKey(
        "project.Project", related_name="documents", null=True, on_delete=models.CASCADE
    )
    body = models.TextField()
    document_type = models.PositiveSmallIntegerField(choices=TYPES, default=CONTRACT)
    version = models.CharField(max_length=50)
    status = models.PositiveSmallIntegerField(choices=TYPES_STATUS, default=NOSIGN)

    objects = CommonManager()

    @property
    def is_signed(self):
        return self.status == Document.FULLSIGNED

    def check_signature(self, key):
        data = self.body.encode()
        digest = SHA256.new(data)

        try:
            pkcs1_15.new(key).verify(digest, self.signature.tobytes())
            return True
        except (ValueError, TypeError):
            return False


class Signature(models.Model):
    signature = models.BinaryField(max_length=2048, blank=True, null=True)
    document = models.ForeignKey(
        Document, related_name="signatures", on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        "accounts.User", related_name="signatures", on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True)

    def sign(self, key):
        data = self.document.body.encode()
        digest = SHA256.new(data)

        signature = pkcs1_15.new(key).sign(digest)
        self.signature = signature

        return signature


class Company(WatchableMixin):
    CONTRACTOR, CUSTOMER = (1, 2)
    TYPE_CHOICE = ((CONTRACTOR, "contractor"), (CUSTOMER, "customer"))
    SIZES = (
        (1, "5-15"),
        (2, "16-49"),
        (3, "50-199"),
        (4, "200-499"),
        (5, "500-999"),
        (6, "1000 and more"),
    )

    slug = models.SlugField(max_length=255)
    owner = models.CharField(max_length=255, null=True)
    name = models.CharField(max_length=255)
    logo = models.ImageField(null=True, blank=True, upload_to="logos")
    picture = models.ImageField(null=True, upload_to="company_pictures")
    video = models.FileField(null=True, blank=True, upload_to="videos")
    presentation = models.FileField(
        null=True, blank=True, upload_to="company_documents"
    )
    video_link = models.TextField(max_length=1000, null=True, blank=True)
    location = models.ForeignKey(City, null=True, on_delete=models.SET_NULL)
    ownership = models.ForeignKey(Ownership, null=True, on_delete=models.SET_NULL)
    date_creation = models.DateField(null=True)
    website = models.CharField(max_length=255)
    vanity_url = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    languages = models.ManyToManyField(Language)
    role = models.PositiveSmallIntegerField(choices=TYPE_CHOICE)
    flexible_working_hours = models.BooleanField(default=False)
    slogan = models.CharField(max_length=255, null=True)
    invisible = models.BooleanField(default=False)
    timezone = TimeZoneField(null=True)
    company_size = models.PositiveSmallIntegerField(
        choices=SIZES, null=True, blank=True
    )

    objects = CommonManager()

    def __str__(self):
        return self.name

    def get_language_list(self):
        return ", ".join(self.languages.values_list("name", flat=True))

    def get_absolute_url(self):
        from django.urls import reverse

        return reverse("company-details", args=[self.slug])

    @property
    def rating(self):
        import math

        rating_list = []
        for r in self.reviews.values("rating"):
            rating_list.append(r.get("rating"))

        try:
            return math.ceil(sum(rating_list) / len(rating_list))
        except ZeroDivisionError:
            return 0

    def get_owner(self):
        return self.workers.filter(is_owner=True).first().user

    def get_site_url(self):
        return self.website.split("//")[-1]


class Worker(WatchableMixin):
    current_position = models.ForeignKey(
        CompanyPosition, null=True, on_delete=models.SET_NULL
    )
    user = models.OneToOneField(User, related_name="work", on_delete=models.CASCADE)
    company = models.ForeignKey(
        "Company", related_name="workers", null=True, on_delete=models.CASCADE
    )
    is_owner = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_native_user = models.BooleanField(null=False, default=True)

    objects = CommonManager()

    def __str__(self):
        if self.company:
            return f"{self.user.name}-{self.company.name}"

        return self.user.name


class Partnership(WatchableMixin):
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    company = models.ForeignKey("Company", on_delete=models.CASCADE)

    objects = CommonManager()


class Review(WatchableMixin):
    project = models.ForeignKey(
        "project.Project",
        related_name="reviews",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    company = models.ForeignKey(
        Company,
        related_name="reviews",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    rating = models.PositiveSmallIntegerField()
    text = models.TextField()
    user = models.ForeignKey(
        "accounts.User", on_delete=models.CASCADE, blank=True, null=True
    )
    is_approved = models.BooleanField(
        null=False, default=False, verbose_name="Approved"
    )
    date_add = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user.name} - {self.rating}"

    @property
    def is_for_platform(self):
        if self.project and self.company:
            return False

        return True

    class Meta:
        unique_together = (
            "user",
            "project",
        )
