from django.contrib import admin

from .models import Achievement, Activity, Century, CommonProject, Person


@admin.register(Achievement)
class AchievementAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


@admin.register(CommonProject)
class CommonProjectAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Person)
admin.site.register(Activity)
admin.site.register(Century)
