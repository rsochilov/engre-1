from django.db.models import Q
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from company.models import Review
from core.mixins import VueTemplateMixin
from core.serializers import NameSerializer
from project.models import Industry
from rest_framework import generics
from seo_tags.mixins import (
    CommonProjectMetaTagTemplateMixin,
    AchievementMetaTagTemplateMixin,
)


from .models import (
    Achievement,
    Activity,
    Century,
    CommonProject,
    FieldOfActivity,
    Person,
)
from .pagination import PersonPagination
from .serializers import (
    AchievementSerializer,
    CommonProjectSerializer,
    PersonSerializer,
)


class PeopleView(VueTemplateMixin, TemplateView):
    template_name = "about/people.html"

    def get_props(self):
        return {
            "people": PersonSerializer(Person.objects.all(), many=True).data,
            "activities": NameSerializer(Activity.objects.all(), many=True).data,
            "centuries": NameSerializer(Century.objects.all(), many=True).data,
        }


class PeopleListAPIView(generics.ListAPIView):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer
    pagination_class = PersonPagination
    filterset_fields = ("century", "activity")


class AchievementListView(VueTemplateMixin, TemplateView):
    template_name = "about/achievements.html"

    def get_props(self):
        return {
            "achievements": AchievementSerializer(
                Achievement.objects.all(), many=True
            ).data,
            "activities": NameSerializer(FieldOfActivity.objects.all(), many=True).data,
        }


class AchievementDetailView(AchievementMetaTagTemplateMixin, DetailView):
    template_name = "about/achievement.html"
    model = Achievement


class AchievementListAPIView(generics.ListAPIView):
    queryset = Achievement.objects.all()
    serializer_class = AchievementSerializer
    filterset_fields = ("year", "activity")


class CommonProjectView(VueTemplateMixin, TemplateView):
    template_name = "about/common-projects.html"

    def get_props(self):
        return {
            "projects": CommonProjectSerializer(
                CommonProject.objects.all(), many=True
            ).data,
            "industries": NameSerializer(Industry.objects.all(), many=True).data,
        }


class CommonProjectListAPIView(generics.ListAPIView):
    queryset = CommonProject.objects.all()
    serializer_class = CommonProjectSerializer
    filterset_fields = ("year", "industry")


class CommonProjectDetailView(CommonProjectMetaTagTemplateMixin, DetailView):
    template_name = "about/common-project.html"
    model = CommonProject


class ReviewListView(ListView):
    model = Review
    template_name = "about/reviews.html"
    queryset = Review.objects.filter(is_approved=True).order_by("-date_add")
