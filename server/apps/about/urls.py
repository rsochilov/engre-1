from django.conf.urls import url
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    url(
        r"about-us/",
        TemplateView.as_view(template_name="about/about-us.html"),
        name="about-us",
    ),
    url(r"people/", views.PeopleView.as_view(), name="about-people-list"),
    url(r"api/people/", views.PeopleListAPIView.as_view(), name="about-people-api"),
    url(r"reviews/", views.ReviewListView.as_view(), name="about-reviews"),
    url(
        r"achievements/(?P<slug>[-a-zA-Z0-9_]+)",
        views.AchievementDetailView.as_view(),
        name="about-achievement",
    ),
    url(
        r"achievements/", views.AchievementListView.as_view(), name="about-achievements"
    ),
    url(
        r"api/achievements/",
        views.AchievementListAPIView.as_view(),
        name="about-achievements-api",
    ),
    url(
        r"common-projects/(?P<slug>[-a-zA-Z0-9_]+)",
        views.CommonProjectDetailView.as_view(),
        name="about-common-project",
    ),
    url(
        r"common-projects/",
        views.CommonProjectView.as_view(),
        name="about-common-projects",
    ),
    url(
        r"api/common-projects/",
        views.CommonProjectListAPIView.as_view(),
        name="about-common-projects-api",
    ),
]
