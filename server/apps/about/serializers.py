from core.serializers import NameSerializer
from rest_framework import serializers

from .models import Achievement, CommonProject, Person


class PersonSerializer(serializers.ModelSerializer):
    activity = serializers.SerializerMethodField(read_only=True)
    activity_id = serializers.IntegerField(write_only=True)
    century = serializers.SerializerMethodField(read_only=True)
    century_id = serializers.IntegerField(write_only=True)
    photo = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Person
        fields = "__all__"

    def get_activity(self, obj):
        if obj.activity:
            return NameSerializer(obj.activity).data

        return None

    def get_century(self, obj):
        if obj.century:
            return NameSerializer(obj.century).data

        return None

    def get_photo(self, obj):
        return obj.photo.url


class AchievementSerializer(serializers.ModelSerializer):
    activity = NameSerializer()

    class Meta:
        model = Achievement
        fields = "__all__"


class CommonProjectSerializer(serializers.ModelSerializer):
    industry = NameSerializer()

    class Meta:
        model = CommonProject
        fields = "__all__"
