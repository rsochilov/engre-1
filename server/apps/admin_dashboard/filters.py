import django_filters
from accounts.models import User


class UserManagementFilterSet(django_filters.FilterSet):
    class Meta:
        model = User
        fields = {"email": ["icontains"]}
