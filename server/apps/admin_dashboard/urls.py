from django.conf.urls import url

from . import views

app_name = "admin_dashboard"

urlpatterns = [
    url(r"dashboard/", views.DashboardView.as_view(), name="admin-index"),
    # projects tab urls
    url(r"projects/", views.ProjectsView.as_view(), name="admin-projects"),
    url(r"get-projects/", views.ProjectAPIView.as_view(), name="admin-get-projects"),
    url(
        r"send-warning-message/",
        views.SendWarningMessageAPIView.as_view(),
        name="send-warning-message",
    ),
    url(r"archive-project/", views.ProjectAPIView.as_view(), name="archive-project"),
    # content tab urls
    # about
    url(r"content/", views.ContentView.as_view(), name="admin-content"),
    url(r"create-person/", views.PersonViewSet.as_view(), name="create-person"),
    url(r"update-person/", views.PersonViewSet.as_view(), name="update-person"),
    url(r"remove-person/", views.PersonViewSet.as_view(), name="remove-person"),
    # blog
    url(
        r"get-blog-articles/", views.ArticleAPIView.as_view(), name="admin-get-articles"
    ),
    # news
    url(
        r"get-news-articles/",
        views.NewsArticleAPIView.as_view(),
        name="admin-get-news-articles",
    ),
    url(
        r"create-news-article/",
        views.NewsArticleViewSet.as_view(),
        name="create-news-article",
    ),
    url(
        r"update-news-article/",
        views.NewsArticleViewSet.as_view(),
        name="update-news-article",
    ),
    url(
        r"remove-news-article/",
        views.NewsArticleViewSet.as_view(),
        name="remove-news-article",
    ),
    # partnership urls
    url(
        r"get-for-investors/",
        views.ForInvestorViewSet.as_view(),
        name="admin-get-for-investors",
    ),
    url(
        r"create-for-investor/",
        views.ForInvestorViewSet.as_view(),
        name="create-for-investor",
    ),
    url(
        r"update-for-investor/",
        views.ForInvestorViewSet.as_view(),
        name="update-for-investor",
    ),
    url(
        r"remove-for-investor/",
        views.ForInvestorViewSet.as_view(),
        name="remove-for-investor",
    ),
    # careers tab urls
    url(r"get-jobs/", views.JobListAPIView.as_view(), name="admin-get-jobs"),
    url(r"create-job/", views.JobViewSet.as_view(), name="create-job"),
    url(r"update-job/", views.JobViewSet.as_view(), name="update-job"),
    url(r"remove-job/", views.JobViewSet.as_view(), name="remove-job"),
    # documents urls
    url(r"documents/", views.DocumentView.as_view(), name="admin-documents"),
    url(r"get-documents/", views.DocumentAPIView.as_view(), name="admin-get-documents"),
    url(r"get-pdf/", views.GeneratePDF.as_view(), name="admin-get-pdf"),
    # software urls
    url(r"software/", views.SoftwareView.as_view(), name="admin-software"),
    url(r"get-softwares/", views.SoftwareAPIView.as_view(), name="admin-get-softwares"),
    url(r"create-software/", views.SoftwareViewSet.as_view(), name="create-software"),
    url(r"update-software/", views.SoftwareViewSet.as_view(), name="update-software"),
    url(
        r"upload-software-file/",
        views.UploadSoftware.as_view(),
        name="upload-software-file",
    ),
    url(
        r"get-software-files/",
        views.SoftwareFileAPIView.as_view(),
        name="get-software-files",
    ),
    url(r"remove-software/", views.SoftwareViewSet.as_view(), name="remove-software"),
    # finances urls
    url(r"finances/", views.FinancesView.as_view(), name="admin-finances"),
    # user management urls
    url(
        r"user-management/",
        views.UserManagementView.as_view(),
        name="admin-user-management",
    ),
    url(r"get-users/", views.UserManagementViewSet.as_view(), name="admin-get-users"),
    url(
        r"activate-user/",
        views.UserManagementViewSet.as_view(),
        name="admin-activate-user",
    ),
    url(r"remove-user/", views.UserManagementViewSet.as_view(), name="remove-user"),
    # moderation urls
    url(r"moderation/", views.ModerationView.as_view(), name="admin-moderation"),
    url(r"create-user/", views.CreateUserAPIView.as_view(), name="admin-create-user"),
    # chat urls
    url(r"chat/", views.ChatView.as_view(), name="admin-chat"),
    # settings urls
    url(r"settings/", views.SettingsView.as_view(), name="admin-settings"),
    url(
        r"upload-user-picture/",
        views.UploadPictureAPIView.as_view(),
        name="upload-user-picture",
    ),
    url(
        r"set-new-password/",
        views.SetAdminPassword.as_view(),
        name="admin-set-new-password",
    ),
    url(
        r"get-categories/", views.CategoryViewSet.as_view(), name="admin-get-categories"
    ),
    url(
        r"get-industries/", views.IndustryViewSet.as_view(), name="admin-get-industries"
    ),
    url(
        r"update-categories-ordering/",
        views.CategoryOrderingAPIView.as_view(),
        name="admin-update-categories-ordering",
    ),
    url(
        r"update-category-name/",
        views.CategoryViewSet.as_view(),
        name="admin-update-category-name",
    ),
    url(
        r"update-industries-ordering/",
        views.IndustryOrderingAPIView.as_view(),
        name="admin-update-industries-ordering",
    ),
    url(
        r"update-industry-name/",
        views.IndustryViewSet.as_view(),
        name="admin-update-industry-name",
    ),
    url(
        r"get-stop-words/", views.StopWordViewSet.as_view(), name="admin-get-stop-words"
    ),
    url(
        r"create-stop-words/",
        views.StopWordViewSet.as_view(),
        name="admin-create-stop-words",
    ),
]
