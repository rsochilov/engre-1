from rest_framework import serializers

from .models import Job, JobRequest


class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = "__all__"


class JobRequestSerializer(serializers.ModelSerializer):
    cv = serializers.FileField(required=False)

    class Meta:
        model = JobRequest
        fields = "__all__"
