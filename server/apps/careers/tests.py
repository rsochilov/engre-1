from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase
from django.urls import reverse

from .models import Category, Job

from .tasks import send_apply_email


API_URL = reverse("api")


class SchemaCategoryTestCase(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username="jane", email="jane@gmail.com", password="password",
        )

    def test_get(self):
        Category.objects.create(name="R&D",)
        query = """
            query {
              careersCategories {
                edges {
                  node {
                    name
                  }
                }
              }
            }
        """
        response = self.client.post(API_URL, data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        edge = data["data"]["careersCategories"]["edges"][0]
        self.assertEqual(edge["node"]["name"], "R&D")

    def test_add(self):
        self.user.is_superuser = True
        self.user.save()

        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createCareersCategory(name: "R&D") {
                status
              }
            }
        """

        response = self.client.post(API_URL, data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue(data["data"]["createCareersCategory"]["status"])
        self.assertEqual(Category.objects.count(), 1)

    def test_add_without_superuser_status(self):
        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = """
            mutation {
              createCareersCategory(name: "R&D") {
                status
              }
            }
        """

        response = self.client.post(API_URL, data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIsNone(data["data"]["createCareersCategory"])
        self.assertEqual(Category.objects.count(), 0)


class SchemaJobTestCase(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username="jane", email="jane@gmail.com", password="password",
        )
        self.category = Category.objects.create(name="R&D",)

    def test_get(self):
        Job.objects.create(
            title="Expert",
            description="A good one",
            location="Kyiv, UA",
            category=self.category,
        )
        query = """
            query {
              careersJobs {
                edges {
                  node {
                    title
                  }
                }
              }
            }
        """
        response = self.client.post(API_URL, data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        edge = data["data"]["careersJobs"]["edges"][0]
        self.assertEqual(edge["node"]["title"], "Expert")

    def test_add(self):
        query = """
            query {
              careersCategories {
                edges {
                  node {
                    id
                  }
                }
              }
            }
        """
        response = self.client.post(API_URL, data={"query": query})
        data = response.json()
        edge = data["data"]["careersCategories"]["edges"][0]
        category_id = edge["node"]["id"]

        self.user.is_superuser = True
        self.user.save()

        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = (
            """
            mutation {
              createCareersJob(
                title: "Expert",
                location: "A good one",
                description: "Kyiv, UA",
                category: "%s",
                isExpired: false,
              ) {
                status
              }
            }
        """
            % category_id
        )

        response = self.client.post(API_URL, data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue(data["data"]["createCareersJob"]["status"])
        self.assertEqual(Job.objects.count(), 1)

    def test_add_without_superuser_status(self):
        query = """
            query {
              careersCategories {
                edges {
                  node {
                    id
                  }
                }
              }
            }
        """
        response = self.client.post(API_URL, data={"query": query})
        data = response.json()
        edge = data["data"]["careersCategories"]["edges"][0]
        category_id = edge["node"]["id"]

        self.client.force_login(
            self.user, backend="django.contrib.auth.backends.ModelBackend",
        )

        query = (
            """
            mutation {
              createCareersJob(
                title: "Expert",
                location: "A good one",
                description: "Kyiv, UA",
                category: "%s",
              ) {
                status
              }
            }
        """
            % category_id
        )

        response = self.client.post(API_URL, data={"query": query})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIsNone(data["data"]["createCareersJob"])
        self.assertEqual(Job.objects.count(), 0)


class SchemaSendApplyEmailTestCase(TestCase):
    def test_send(self):
        query = """
            mutation {
              sendApplyEmail(
                applyData: {
                  title: "Senior Procrastinator",
                  name: "Steve",
                  email: "steve@email.com",
                  resume: {
                    name: "resume.txt",
                    file: "data:text/plain;base64,dGVzdA==",
                  },
                }
              ) {
                status
              }
            }
        """

        with patch("careers.schema.send_apply_email.delay") as mock_function:
            response = self.client.post(API_URL, data={"query": query})
            self.assertTrue(mock_function.called)

        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertTrue(data["data"]["sendApplyEmail"]["status"])

    def test_send_without_required_fields(self):
        query = """
            mutation {
              sendApplyEmail(applyData: {}) {
                status
              }
            }
        """

        with patch("careers.schema.send_apply_email.delay") as mock_function:
            response = self.client.post(API_URL, data={"query": query})
            self.assertFalse(mock_function.called)

        self.assertEqual(response.status_code, 400)
        data = response.json()
        self.assertIn("errors", data)


class TaskSendApplyEmailTestCase(TestCase):
    def test_send(self):
        r = send_apply_email(
            apply_data={
                "title": "Senior Procrastinator",
                "name": "Steve",
                "email": "steve@email.com",
                "resume": {
                    "name": "resume.txt",
                    "file": "data:text/plain;base64,dGVzdA==",
                },
            },
        )
        self.assertEqual(len(mail.outbox), 1)
        msg = mail.outbox[0]
        self.assertEqual(msg.subject, "steve@email.com / Apply")
        self.assertEqual(msg.attachments[0].get_payload(), "dGVzdA==")
        self.assertIn("Steve", msg.body)
