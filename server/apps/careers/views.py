from core.mixins import VueTemplateMixin
from django.views.generic import DetailView
from django.views.generic.list import ListView
from rest_framework import generics

from .models import Category, Job, JobRequest
from .serializers import JobRequestSerializer, JobSerializer


class CareersListView(ListView):
    model = Category
    context_object_name = "categories"
    template_name = "careers/description.html"


class CareerDetailView(VueTemplateMixin, DetailView):
    model = Job
    context_object_name = "job"
    template_name = "careers/detail-job.html"

    def get_props(self):
        return {"job": JobSerializer(self.get_object()).data}


class JobRequestCreateAPIView(generics.CreateAPIView):
    queryset = JobRequest.objects.all()
    serializer_class = JobRequestSerializer
