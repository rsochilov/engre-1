Title: {{ apply_data.title }}
Name: {{ apply_data.name }}
Email: {{ apply_data.email }}{% if apply_data.phone %}
Phone: {{ apply_data.phone }}{% endif %}
