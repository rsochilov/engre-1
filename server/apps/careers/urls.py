from django.conf.urls import url

from . import views

urlpatterns = [
    url(r"job-request/", views.JobRequestCreateAPIView.as_view(), name="job-request"),
    url(
        r"(?P<slug>[-a-zA-Z0-9_]+)/",
        views.CareerDetailView.as_view(),
        name="career-detail",
    ),
    url(r"", views.CareersListView.as_view(), name="all-careers"),
]
