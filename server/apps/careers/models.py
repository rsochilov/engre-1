from django.db import models

from core.models import WatchableMixin


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Job(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(null=True, blank=True)
    location = models.CharField(max_length=255)
    short_description = models.TextField()
    rich_description = models.TextField()
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="jobs"
    )
    is_expired = models.BooleanField(default=False)
    image = models.ImageField(upload_to="job_images/", null=True, blank=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = "-".join(self.title.split(" "))

        return super().save(*args, **kwargs)


class JobRequest(WatchableMixin):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    phone = models.CharField(max_length=30)
    cv = models.FileField(upload_to="resume/")

    def __str__(self):
        return self.name
