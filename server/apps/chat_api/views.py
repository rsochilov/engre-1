from typing import Tuple, Iterable
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView,
    GenericAPIView,
)
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from chat.models import ChatRoom
from project.models import Project
from project.serializers import ProjectSerializer
from accounts.models import ProjectManager

from .mixins import GetChatRoomProjectsMixin


class ChatProjectsListApiView(ListAPIView, GetChatRoomProjectsMixin):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        try:
            projects = self.get_projects_by_chatroom(request, kwargs["chat_room_id"])
        except Exception as exception:
            print(type(exception), exception)
            return Response(
                data={"errors": str(exception)}, status=status.HTTP_400_BAD_REQUEST
            )
        serializer = self.get_serializer(projects, many=True)
        return Response(data=serializer.data)


class GetChatProjectApiView(RetrieveAPIView, GetChatRoomProjectsMixin):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            projects = self.get_projects_by_chatroom(request, kwargs["chat_room_id"])
            selected_project = projects.get(pk=kwargs["project_id"])
        except Project.DoesNotExist:
            return Response(data={"errors": "Project not found"}, status=404)
        except Exception as exception:
            return Response(
                data={"errors": str(exception)}, status=status.HTTP_400_BAD_REQUEST
            )

        serializer = self.get_serializer(selected_project)
        return Response(data=serializer.data)


class CreatePMChatApiView(GenericAPIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        pm_user = self.get_pm_user()
        if not pm_user:
            return Response(
                data={"detail": "PM not found"}, status=status.HTTP_404_NOT_FOUND
            )
        user = self.request.user
        pm_chat_room = ChatRoom.objects.filter(users=user).filter(users=pm_user)
        if pm_chat_room:
            return Response(status=status.HTTP_200_OK)

        new_room = ChatRoom.objects.create()
        new_room.users.add(user, pm_user)
        new_room.save()
        return Response(status=status.HTTP_201_CREATED)

    def get_pm_user(self) -> ProjectManager:
        return ProjectManager.objects.first()
