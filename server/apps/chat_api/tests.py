from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIRequestFactory, force_authenticate
from .views import CreatePMChatApiView

from accounts.models import User, ProjectManager
from chat.models import ChatRoom


class ChatApiTests(TestCase):
    def test_create_pmchat(self):
        admin_user = User.objects.get(email="admin@admin.com")
        factory = APIRequestFactory()
        request = factory.post(reverse("create_pm_chat"))
        force_authenticate(request, user=admin_user)

        view = CreatePMChatApiView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 201, response.data)
        created_room = ChatRoom.objects.filter(users=admin_user).filter(
            users=ProjectManager.objects.first()
        )
        self.assertTrue(created_room, "Chat room was not created!")

    def test_create_pm_chat_no_auth(self):
        factory = APIRequestFactory()
        request = factory.post(reverse("create_pm_chat"))

        view = CreatePMChatApiView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 401, response.data)
