from typing import Tuple, Iterable
from rest_framework.request import HttpRequest

from accounts.models import User
from core.models import NOT_ACTIVE
from chat.models import ChatRoom
from project.models import Project

from .exceptions import GetPMProjectsException, GetCustomerProjectsException


class GetChatRoomProjectsMixin:
    def get_projects_by_chatroom(
        self, request: HttpRequest, chat_room_id: int
    ) -> Iterable[Project]:
        main_user, second_user = self.get_users_from_chat_room(request, chat_room_id)
        if main_user.is_pm:
            return self.get_pm_projects(main_user, second_user)
        elif main_user.is_customer:
            return self.get_customer_projects(main_user, second_user)
        elif main_user.is_contractor:
            raise Exception("Contactor doesn't have rights for selecting project")

    def get_pm_projects(self, pm_user: User, second_user: User) -> Iterable[Project]:
        if second_user.is_customer:
            company = second_user.get_company()
            if not company:
                raise GetPMProjectsException(
                    f'Unable to get projects! User "{second_user}" doesn\'t have a company'
                )
            return Project.objects.filter(
                company=company, management_option=Project.PLATFORM, status=NOT_ACTIVE
            )
        elif second_user.is_contractor:
            company = pm_user.get_company()
            if not company:
                raise GetPMProjectsException(
                    f'Unable to get projects! PM "{pm_user}" doesn\'t have a company'
                )
            return Project.objects.filter(company=company, status=NOT_ACTIVE)
        elif second_user.is_pm:
            raise GetPMProjectsException(
                "PM doesn't have rights to select projects to other PM"
            )
        else:
            raise GetPMProjectsException("Unable to detect user type of chat")

    def get_customer_projects(
        self, customer: User, second_user: User
    ) -> Iterable[Project]:
        if second_user.is_pm:
            return Project.objects.none()
        elif second_user.is_contractor:
            company = customer.get_company()
            print(company.id)
            if not company:
                raise GetCustomerProjectsException(
                    f'Unable to get projects! User "{second_user}" doesn\'t have a company'
                )
            return Project.objects.filter(
                company=company, management_option=Project.DIRECT, status=NOT_ACTIVE
            )
        elif second_user.is_customer:
            raise GetPMProjectsException(
                "Customer doesn't have rights to select projects to other customer"
            )
        else:
            raise GetPMProjectsException("Unable to detect user type of chat")

    def get_users_from_chat_room(
        self, request: HttpRequest, chat_room_id: int
    ) -> Tuple[User]:
        chat_room = self.get_chat_room(chat_room_id)
        if not chat_room:
            raise Exception("Invalid chat room provided")

        if request.user not in chat_room.users.all():
            raise Exception("Chat room doesn't contain current user")

        other_user = self.get_other_user(chat_room, request.user)

        if not other_user:
            raise Exception("Critical! Unable to find other user from chat!")
        return request.user, other_user

    def get_chat_room(self, chat_room_id: int) -> ChatRoom:
        if not chat_room_id:
            return None
        try:
            return ChatRoom.objects.get(pk=chat_room_id)
        except ChatRoom.DoesNotExist:
            return None

    def get_other_user(self, room: ChatRoom, request_user: User) -> User:
        if not room:
            return None
        if not request_user:
            return None
        return room.users.exclude(pk=request_user.pk).first()
