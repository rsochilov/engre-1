import json
from uuid import uuid4

from django.conf import settings
from django.db.models import ObjectDoesNotExist, Q
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, reverse, render
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.views.generic import ListView, TemplateView
from rest_framework import generics

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status as rf_status

from accounts.models import Blogger, ProjectManager, User
from accounts.serializers import (
    BloggerDataSerializer,
    DeleteProfileSerializer,
    ProjectManagerSerializer,
    TimezoneSerializer,
    UserSerializer,
)
from accounts.tokens import account_activation_token
from accounts.tasks import send_email as send_email_task
from blog.models import BlogArticle, BlogCategory
from blog.serializers import BlogArticleSerializer, BloggerArticleSerializer
from chat.models import ChatRoom
from company.models import Document, Ownership, Worker, Company
from company.serializers import (
    CompanySerializer,
    DocumentSerializer,
    OwnershipSerializer,
    WorkerSerializer,
)
from core.mixins import VueTemplateMixin
from core.exceptions import CacherException
from core.models import Language, Tag
from core.serializers import NameSerializer, NameSlugSerializer
from core.timezones import new_timezones
from core.utils import Cacher, Notifier
from core.decorators import (
    not_pm,
    one_of_statuses_required,
    profile_completed_required,
    status_required,
)
from notifications_api.models import Notification
from market_location.models import City
from news.models import NewsCategory
from news.serializers import NewsCategorySerializer
from project.models import (
    Category,
    Industry,
    IntellectualService,
    Project,
    Task,
    TaskAttachment,
    TaskComment,
    ProjectUsers,
)
from project.serializers import (
    ProjectSerializer,
    ServiceSerializer,
    TaskAttachmentSerializer,
    TaskCommentSerializer,
    TaskSerializer,
)

from .models import NotificationsSettings
from .mixins import TryCreateMixin, TryUpdateMixin
from .serializers import (
    CitySerializer,
    CreateUpdateProjectSerializer,
    CreateUpdateServiceSerializer,
    NotificationsSettingsSerializer,
)


@method_decorator(login_required, name="dispatch")
class ProfileView(VueTemplateMixin, TemplateView):
    template_name = "settings/profile.html"

    def get_props(self):
        user = self.request.user
        return {
            "user": UserSerializer(user).data,
            "timezones": TimezoneSerializer(new_timezones, many=True).data,
            "cities": CitySerializer(
                City.objects.filter(country__name="Ukraine"), many=True
            ).data,
            "industries": NameSlugSerializer(Industry.objects.all(), many=True).data,
        }


class UpdateProfileView(generics.UpdateAPIView):
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return User.objects.get(pk=self.request.data.get("pk"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.location_id = request.data.get("location_id")
        instance.timezone = request.data.get("timezone")
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        self.perform_update(serializer)

        return HttpResponse(
            json.dumps({"success": True, "url": reverse("company")}),
            content_type="application/json",
        )


@method_decorator(login_required, name="dispatch")
@method_decorator(profile_completed_required, name="dispatch")
# @method_decorator(not_pm, name="dispatch")
class CompanyView(VueTemplateMixin, TemplateView):
    template_name = "settings/company.html"

    def get_props(self):
        user = self.request.user
        props = {
            "user": UserSerializer(user).data,
            "timezones": TimezoneSerializer(new_timezones, many=True).data,
            "cities": CitySerializer(
                City.objects.filter(country__name="Ukraine"), many=True
            ).data,
            "ownerships": OwnershipSerializer(Ownership.objects.all(), many=True).data,
            "languages": NameSlugSerializer(Language.objects.all(), many=True).data,
        }

        if hasattr(user, "work"):
            props["company"] = CompanySerializer(user.work.company).data

        return props


@method_decorator(login_required, name="dispatch")
@method_decorator(status_required("is_customer"), name="dispatch")
class CreateProjectView(VueTemplateMixin, TemplateView):
    template_name = "settings/project.html"

    def get_props(self):
        user = self.request.user
        props = {
            "user": UserSerializer(user).data,
            "categories": NameSlugSerializer(Category.objects.all(), many=True).data,
            "industries": NameSlugSerializer(Industry.objects.all(), many=True).data,
            "tags": NameSlugSerializer(Tag.objects.all(), many=True).data,
            "languages": NameSlugSerializer(
                user.work.company.languages.all(), many=True
            ).data,
        }

        if self.kwargs.get("pk") is not None:
            props["project"] = ProjectSerializer(
                Project.objects.get(pk=self.kwargs.get("pk"))
            ).data

        return props


# This one is used for creation
class ProjectViewSet(
    generics.CreateAPIView, generics.UpdateAPIView, TryCreateMixin, TryUpdateMixin
):
    serializer_class = CreateUpdateProjectSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return Project.objects.get(pk=self.request.data.get("pk"))

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if not serializer.is_valid():
            return self.generate_error_response(str(serializer.errors))

        return self.try_create(Project, serializer)

    def update(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
        except ObjectDoesNotExist:
            return Response(data={"detail": "Not found"}, status=404)
        serializer = self.get_serializer(instance, data=request.data, partial=True)

        if not serializer.is_valid():
            return JsonResponse(data={"errors": serializer.errors}, status=400)
        response = self.try_update(Project, serializer, instance)
        if response.status_code == 200:
            self.create_notification(instance)

        return response

    def create_notification(self, instance: Project):
        filter = Q(project=instance) & ~Q(user=self.request.user)
        try:
            user = ProjectUsers.objects.prefetch_related("user").get(filter).user
        except ObjectDoesNotExist:
            return

        Notifier.create_notification(
            user=user,
            notification_body=str(instance.pk),
            text=f'You have updates on project "{instance.name}"',
            notification_type=Notification.PROJECT,
        )


class ServiceViewSet(
    generics.CreateAPIView, generics.UpdateAPIView, TryCreateMixin, TryUpdateMixin
):
    serializer_class = CreateUpdateServiceSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return IntellectualService.objects.get(pk=self.request.data.get("pk"))

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        return self.try_create(IntellectualService, serializer)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        return self.try_update(IntellectualService, serializer, instance)


@method_decorator(login_required, name="dispatch")
@method_decorator(
    one_of_statuses_required(["is_customer", "is_pm", "is_contractor"]), name="dispatch"
)
class ProjectListView(VueTemplateMixin, ListView):
    template_name = "settings/projects_list.html"
    model = Project

    def get_props(self):
        user = self.request.user

        if user.is_pm:
            projects = self.get_queryset().filter(
                pm=user.projectmanager, management_option=1
            )
        elif user.is_customer:
            projects = self.get_queryset().filter(company=user.work.company)
        elif user.is_contractor:
            projects = self.get_queryset().filter(
                service__in=user.work.company.intellectualservices.all()
            )

        return {
            "user": UserSerializer(user).data,
            "projects": ProjectSerializer(
                projects, many=True, context={"request": self.request}
            ).data,
        }


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def archive_instance(request):
    instance_id = request.data.get("id")

    try:
        project = Project.objects.get(pk=instance_id)
        project.is_archived = not project.is_archived
        project.save()
    except Project.DoesNotExist:
        return HttpResponse(
            json.dumps({"success": False}), content_type="application/json"
        )

    return HttpResponse(
        json.dumps({"success": True, "url": reverse("projects-list")}),
        content_type="application/json",
    )


@method_decorator(login_required, name="dispatch")
@method_decorator(status_required("is_contractor"), name="dispatch")
class ServiceView(VueTemplateMixin, TemplateView):
    template_name = "settings/service.html"

    def get_props(self):
        user = self.request.user
        props = {
            "user": UserSerializer(user).data,
            "categories": NameSlugSerializer(Category.objects.all(), many=True).data,
            "industries": NameSlugSerializer(Industry.objects.all(), many=True).data,
            "tags": NameSlugSerializer(Tag.objects.all(), many=True).data,
            "languages": NameSlugSerializer(
                user.work.company.languages.all(), many=True
            ).data,
        }

        if self.kwargs.get("pk") is not None:
            props["service"] = ServiceSerializer(
                IntellectualService.objects.get(pk=self.kwargs.get("pk"))
            ).data

        return props


@method_decorator(login_required, name="dispatch")
@method_decorator(status_required("is_contractor"), name="dispatch")
class ServiceListView(VueTemplateMixin, ListView):
    template_name = "settings/services_list.html"
    model = IntellectualService

    def get_props(self):
        user = self.request.user
        return {
            "user": UserSerializer(user).data,
            "services": ServiceSerializer(
                self.get_queryset().filter(
                    company=User.objects.get(pk=user.pk).work.company
                ),
                many=True,
            ).data,
        }


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def archive_service(request):
    if request.method == "POST":
        try:
            service = IntellectualService.objects.get(pk=request.data.get("id"))
            service.is_archived = not service.is_archived
            service.save()

        except IntellectualService.DoesNotExist:
            return HttpResponse(
                json.dumps({"success": False}), content_type="application/json"
            )

        return HttpResponse(
            json.dumps({"success": True, "url": reverse("services-list")}),
            content_type="application/json",
        )


class UpdateServiceView(generics.UpdateAPIView):
    serializer_class = ServiceSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return IntellectualService.objects.get(pk=self.request.data.get("service"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return JsonResponse({"errors": str(serializer.errors)}, status=400)
        if IntellectualService.objects.filter(
            name=instance.name, company__pk=instance.company.id
        ).count():
            return JsonResponse({"errors": str(serializer.errors)}, status=400)
        self.perform_update(serializer)

        return HttpResponse(
            json.dumps({"message": True, "url": reverse("services-list")}),
            content_type="application/json",
        )


class UpdateProjectView(generics.UpdateAPIView):
    serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return Project.objects.get(pk=self.request.data.get("project"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        if Project.objects.filter(
            name=instance.name, company__pk=instance.company.id
        ).count():
            return JsonResponse({"errors": str(serializer.errors)}, status=400)
        self.perform_update(serializer)

        return HttpResponse(
            json.dumps({"message": True, "url": reverse("projects-list")}),
            content_type="application/json",
        )


class UpdateProjectTypeView(generics.UpdateAPIView):
    serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return Project.objects.get(pk=self.request.data.get("project"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        instance.management_option = 1 if instance.management_option == 2 else 2
        instance.save()

        return Response(data=self.get_serializer(instance).data, status=200)


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def update_instance_status(request):
    if request.data.get("instance") == "project":
        instance = Project.objects.get(pk=request.data.get("pk"))
    elif request.data.get("instance") == "service":
        instance = IntellectualService.objects.get(pk=request.data.get("pk"))

    if instance:
        instance.status = request.data.get("status")
        instance.save()

    return HttpResponse(
        json.dumps(
            {
                "success": True,
                "url": reverse("{}s-list".format(request.data.get("instance"))),
            }
        ),
        content_type="application/json",
    )


@method_decorator(login_required, name="dispatch")
@method_decorator(
    one_of_statuses_required(["is_pm", "is_customer", "is_contractor"]), name="dispatch"
)
# @method_decorator(status_required('has_intellectual_services'), name="dispatch")
class DocumentView(VueTemplateMixin, TemplateView):
    template_name = "settings/document.html"

    def get_props(self):
        user = self.request.user
        projects = None

        if user.is_pm:
            projects = Project.objects.filter(
                pm=user.projectmanager, management_option=1
            )
        elif user.is_customer:
            projects = Project.objects.filter(company=user.work.company)
        elif user.is_contractor:
            projects = Project.objects.filter(
                service__in=user.work.company.intellectualservices.all()
            )

        props = {
            "user": UserSerializer(user).data,
            "projects": ProjectSerializer(projects.order_by("-id"), many=True).data,
        }

        return props


@permission_classes((IsAuthenticated,))
@api_view(["POST"])
def create_document(request):
    data = {
        "name": request.data.get("name"),
        "body": request.data.get("body"),
        "project": Project.objects.get(pk=request.data.get("project")),
        "version": request.data.get("version"),
    }
    document, _ = Document.objects.update_or_create(
        pk=request.data.get("document"), defaults=data
    )

    if document:
        return HttpResponse(
            json.dumps({"success": True, "url": reverse("documents")}),
            content_type="application/json",
        )

    return HttpResponse(json.dumps({"success": False}), content_type="application/json")


@permission_classes((IsAuthenticated,))
@api_view(["GET"])
def get_project_documents(request):
    documents = Project.objects.get(pk=request.GET.get("project")).documents.all()

    return HttpResponse(
        json.dumps(
            {
                "success": True,
                "documents": DocumentSerializer(documents, many=True).data,
            }
        ),
        content_type="application/json",
    )


@permission_classes((IsAuthenticated,))
@api_view(["POST"])
def archive_document(request):
    document = Document.objects.get(pk=request.data.get("document"))
    document.is_archived = True
    document.save()

    return HttpResponse(
        json.dumps({"success": True, "url": reverse("documents")}),
        content_type="application/json",
    )


@method_decorator(login_required, name="dispatch")
class ProjectBoardView(VueTemplateMixin, TemplateView):
    template_name = "settings/project_board.html"

    def get_props(self):
        user = self.request.user
        project = Project.objects.get(pk=self.kwargs.get("pk"))

        return {
            "user": UserSerializer(user).data,
            "tasks": TaskSerializer(project.tasks, many=True).data,
            "project": ProjectSerializer(project).data,
        }


class TaskViewSet(
    generics.CreateAPIView,
    generics.UpdateAPIView,
    generics.ListAPIView,
    generics.DestroyAPIView,
):
    serializer_class = TaskSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return Task.objects.get(pk=self.request.data.get("pk"))

    def get(self, request, *args, **kwargs):
        data = self.get_serializer(
            Task.objects.filter(project_id=request.GET.get("project")), many=True
        ).data

        return HttpResponse(
            json.dumps({"success": True, "tasks": data}),
            content_type="application/json",
        )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        self.perform_create(serializer)

        return HttpResponse(
            json.dumps({"success": True, "task": serializer.data}),
            content_type="application/json",
        )

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        self.perform_update(serializer)

        return HttpResponse(
            json.dumps({"success": True, "task": serializer.data}),
            content_type="application/json",
        )

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)

        return HttpResponse(
            json.dumps({"success": True}), content_type="application/json"
        )


class CommentAPIView(
    generics.CreateAPIView,
    generics.UpdateAPIView,
    generics.ListAPIView,
    generics.DestroyAPIView,
):
    queryset = TaskComment.objects.all()
    serializer_class = TaskCommentSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return TaskComment.objects.get(pk=self.request.data.get("pk"))

    def get_queryset(self):
        return self.queryset.filter(task_id=self.request.GET.get("task"))

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        self.perform_create(serializer)

        return HttpResponse(
            json.dumps({"success": True, "comment": serializer.data}),
            content_type="application/json",
        )

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        self.perform_update(serializer)

        return HttpResponse(
            json.dumps({"success": True, "comment": serializer.data}),
            content_type="application/json",
        )

    def list(self, request, *args, **kwargs):
        return HttpResponse(
            json.dumps(
                {"comments": self.get_serializer(self.get_queryset(), many=True).data}
            ),
            content_type="application/json",
        )

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)

        return HttpResponse(
            json.dumps({"success": True}), content_type="application/json"
        )


class TaskAttachmentViewSet(
    generics.ListAPIView, generics.CreateAPIView, generics.DestroyAPIView
):
    serializer_class = TaskAttachmentSerializer
    queryset = TaskAttachment.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return TaskAttachment.objects.get(pk=self.request.data.get("pk"))

    def get_queryset(self):
        return self.queryset.filter(task_id=self.request.GET.get("task"))

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        self.perform_create(serializer)

        return HttpResponse(
            json.dumps({"success": True, "attachment": serializer.data}),
            content_type="application/json",
        )

    def list(self, request, *args, **kwargs):
        return HttpResponse(
            json.dumps(
                {
                    "attachments": self.get_serializer(
                        self.get_queryset(), many=True
                    ).data
                }
            ),
            content_type="application/json",
        )

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)

        return HttpResponse(
            json.dumps({"success": True}), content_type="application/json"
        )


@permission_classes((IsAuthenticated,))
@api_view(["POST"])
def send_invitation_email(request):
    email = request.data.get("email")
    project = Project.objects.get(pk=request.data.get("project"))

    try:
        invitee = User.objects.get(email=email)

        if project.assigned_users.filter(pk=invitee.id).exists():
            return Response(
                data={"detail": "User is already invited"},
                status=rf_status.HTTP_400_BAD_REQUEST,
            )
        else:
            if not invitee.is_contractor or invitee.is_pm:
                return Response(
                    data={"detail": "User is not a cotractor"},
                    status=rf_status.HTTP_400_BAD_REQUEST,
                )
            message = _generate_email_for_existing_user(invitee, request, project)
    except User.DoesNotExist:
        uid = str(uuid4())
        message = _generate_email_message_for_new_user(request, uid)
        Cacher.add_invite_info(uid, email, project.id, request.user.id)
    send_email_task.delay("Invitation to the project", message, [email])
    return Response(status=rf_status.HTTP_200_OK)


def _generate_email_message_for_new_user(request, uid: str) -> str:
    template = "user_settings/invite_new_user.html"
    context = {
        "domain": request.get_host(),
        "protocol": "https" if request.is_secure() else "http",
        "inviter": request.user.name,
        "uid": uid,
    }
    return render_to_string(template, context)


def _generate_email_for_existing_user(invitee: User, request, project: Project) -> str:
    template = "user_settings/invitation_email.html"

    context = {
        "domain": request.get_host(),
        "pid": urlsafe_base64_encode(force_bytes(project.pk)).decode(),
        "protocol": "https" if request.is_secure() else "http",
        "user": request.user,
        "uid": urlsafe_base64_encode(force_bytes(invitee.pk)).decode(),
        "token": account_activation_token.make_token(invitee),
    }

    return render_to_string(template, context)


def new_invitee_password_view(request, uid: str):
    context = {}
    try:
        Cacher.get_invite_info(uid)
        context["uid"] = uid
    except Exception as exc:
        context["error"] = "Email not found!"
    return render(request, "registration/invitee_password.html", context)


@api_view(["POST"])
def create_new_invitee_view(request):
    uid = request.data.get("uid")
    password = request.data.get("password")
    try:
        cached_email, project_id, inviter_id = Cacher.get_invite_info(uid)
        project = Project.objects.get(pk=project_id)
    except CacherException as exc:
        return Response(data={"detail": str(exc)}, status=rf_status.HTTP_404_NOT_FOUND)
    except Project.DoesNotExist:
        return Response(
            data={"detail": "Project not found"}, status=rf_status.HTTP_404_NOT_FOUND
        )
    except Exception as exc:
        return Response(
            data={"detail": str(exc)}, status=rf_status.HTTP_500_INTERNAL_SERVER_ERROR
        )

    invited_user = User.objects.create(email=cached_email)
    invited_user.set_password(password)
    invited_user.is_active = True
    invited_user.save()

    try:
        inviter = User.objects.get(pk=inviter_id)
        company = (
            Worker.objects.select_related("company").get(user__pk=inviter_id).company
        )
    except User.DoesNotExist:
        return Response(
            data={"detail": "Worker not found"}, status=rf_status.HTTP_404_NOT_FOUND
        )
    except Worker.DoesNotExist:
        return Response(
            data={"detail": "Worker not found"}, status=rf_status.HTTP_404_NOT_FOUND
        )
    except Company.DoesNotExist:
        return Response(
            data={"detail": "Company not found"}, status=rf_status.HTTP_404_NOT_FOUND
        )
    Worker.objects.create(
        user=invited_user, company=company, is_owner=False, is_native_user=False
    )

    send_email_task.delay(
        "Invited user joined your project",
        f"User {invited_user.email} joined project {project.name}, check it out!",
        [inviter.email],
    )
    Cacher.remove_verify_info(uid)
    project.assigned_users.add(invited_user)
    return Response(data={"email": cached_email}, status=rf_status.HTTP_201_CREATED)


def save_invitee(request, uid, pid, token):
    user_id = force_text(urlsafe_base64_decode(uid))
    project_id = force_text(urlsafe_base64_decode(pid))

    try:
        user = User.objects.get(pk=user_id)
        project = Project.objects.get(pk=project_id)
    except (User.DoesNotExist, Project.DoesNotExist):
        pass

    is_token_valid = account_activation_token.check_token(user, token)

    if is_token_valid:
        project.assigned_users.add(user)
        return redirect("project-board", pk=project.id)

    return redirect("sign-in")


@method_decorator(login_required, name="dispatch")
@method_decorator(profile_completed_required, name="dispatch")
class NotificationsTemplateView(VueTemplateMixin, TemplateView):
    template_name = "settings/notifications.html"

    def get_props(self):
        context = {"user": self.request.user}
        categories_subscribed = NewsCategory.objects.filter(
            subscribers__id=self.request.user.id
        ).count()
        categories_count = NewsCategory.objects.count()

        subscribed_all = categories_subscribed == categories_count

        settings = NotificationsSettingsSerializer(
            self.request.user.notifications_settings
        ).data

        return {
            "categories": NewsCategorySerializer(
                NewsCategory.objects.all(), many=True, context=context
            ).data,
            "user": UserSerializer(self.request.user).data,
            "subscribedAll": subscribed_all,
            "settings": settings,
        }


class UpdateNotificationsSettingsAPIView(generics.UpdateAPIView):
    queryset = NotificationsSettings
    serializer_class = NotificationsSettingsSerializer
    permission_classes = (IsAuthenticated,)


class DeleteProfileAPIView(generics.UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = DeleteProfileSerializer
    permission_classes = (IsAuthenticated,)


class UpdatePMView(generics.UpdateAPIView):
    serializer_class = ProjectManagerSerializer
    permission_class = (IsAuthenticated,)

    def get_object(self):
        return ProjectManager.objects.get(pk=self.request.data.get("pk"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.timezone = request.data.get("timezone")
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=400)
        self.perform_update(serializer)
        return Response(data={"success": True}, status=rf_status.HTTP_200_OK)


class UpdateBloggerView(generics.UpdateAPIView):
    serializer_class = BloggerDataSerializer
    permission_class = (IsAuthenticated,)

    def get_object(self):
        return Blogger.objects.get(pk=self.request.data.get("pk"))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.timezone = request.data.get("timezone")
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if not serializer.is_valid():
            return JsonResponse(data=serializer.errors, status=400)
        self.perform_update(serializer)

        return HttpResponse(
            json.dumps({"success": True}), content_type="application/json"
        )


@method_decorator(login_required, name="dispatch")
@method_decorator(status_required("is_pm"), name="dispatch")
class ReviewPMView(VueTemplateMixin, TemplateView):
    template_name = "settings/review_pm.html"

    def get_props(self):
        projects = Project.objects.filter(status=2)

        return {
            "user": UserSerializer(self.request.user).data,
            "projects": ProjectSerializer(projects, many=True).data,
        }


@method_decorator(login_required, name="dispatch")
@method_decorator(one_of_statuses_required(["is_owner", "is_pm"]), name="dispatch")
class MembershipTemplateView(VueTemplateMixin, TemplateView):
    template_name = "settings/membership.html"

    def get_props(self):
        user = self.request.user

        if user.is_pm:
            members = user.projectmanager.membership.all()
        else:
            company, members = user.get_company(), Worker.objects.none()

            if company:
                members = company.workers.exclude(user=user)

        return {
            "user": UserSerializer(user).data,
            "members": WorkerSerializer(members, many=True).data,
        }


@method_decorator(login_required, name="dispatch")
@method_decorator(status_required("is_blogger"), name="dispatch")
class ArticlesTemplateView(VueTemplateMixin, TemplateView):
    template_name = "settings/articles.html"

    def get_props(self):
        user = self.request.user

        return {
            "user": UserSerializer(user).data,
            "blog": BloggerArticleSerializer(user.blogger).data,
        }


@method_decorator(login_required, name="dispatch")
class ArticleTemplateView(VueTemplateMixin, TemplateView):
    template_name = "settings/article_form.html"

    def get_props(self):
        user = self.request.user
        data = {
            "user": UserSerializer(user).data,
            "categories": NameSerializer(BlogCategory.objects.all(), many=True).data,
        }

        if self.kwargs.get("pk") is not None:
            data["article"] = BlogArticleSerializer(
                BlogArticle.objects.get(pk=self.kwargs.get("pk"))
            ).data

        return data
