from company.models import Company
from core.serializers import NameSerializer, NameSlugSerializer
from core.timezones import get_timezone as get_timezone_display
from market_location.models import City
from market_location.serializers import LocationSerializer
from project.models import IntellectualService, Project
from rest_framework import serializers

from .models import NotificationsSettings


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ("id", "name_ascii")


class CompanySerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    ownership = NameSerializer()
    languages = serializers.SerializerMethodField()
    timezone = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = "__all__"

    def get_language_list(self, obj):
        return obj.get_language_list()

    def get_languages(self, obj):
        if obj.languages:
            return NameSlugSerializer(obj.languages.all(), many=True).data

        return None

    def get_timezone(self, obj):
        if obj.timezone:
            get_timezone_display(obj.timezone.zone)

        return None


class CreateUpdateProjectSerializer(serializers.ModelSerializer):
    core_skills = serializers.ListField()
    tags = serializers.ListField()
    languages = serializers.ListField()
    category_id = serializers.IntegerField(write_only=True)
    industry_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Project
        fields = (
            "id",
            "name",
            "budget",
            "date_start",
            "date_finish",
            "category_id",
            "industry_id",
            "core_skills",
            "tags",
            "languages",
            "logo",
            "description",
            "company",
            "management_option",
            "video",
            "document",
            "video_link",
        )

    def create(self, validated_data):
        core_skills = validated_data.pop("core_skills")
        tags = validated_data.pop("tags")
        languages = validated_data.pop("languages")

        instance = super().create(validated_data)
        instance.core_skills.add(*[int(skill) for skill in core_skills[0].split(",")])
        instance.tags.add(*[int(tag) for tag in tags[0].split(",")])
        instance.languages.add(*[int(lan) for lan in languages[0].split(",")])

        return instance

    def update(self, instance, validated_data):
        core_skills = validated_data.pop("core_skills")
        tags = validated_data.pop("tags")
        languages = validated_data.pop("languages")

        updated_instance = super().update(instance, validated_data)
        updated_instance.core_skills.clear()
        updated_instance.tags.clear()
        updated_instance.languages.clear()

        updated_instance.core_skills.add(
            *[int(skill) for skill in core_skills[0].split(",")]
        )
        updated_instance.tags.add(*[int(tag) for tag in tags[0].split(",")])
        updated_instance.languages.add(*[int(lan) for lan in languages[0].split(",")])

        return updated_instance


class CreateUpdateServiceSerializer(serializers.ModelSerializer):
    core_skills = serializers.ListField()
    tags = serializers.ListField()
    languages = serializers.ListField()
    category_id = serializers.IntegerField(write_only=True)
    industry_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = IntellectualService
        fields = (
            "id",
            "name",
            "hourly_rate",
            "category_id",
            "core_skills",
            "tags",
            "languages",
            "description",
            "logo",
            "company",
            "video",
            "document",
            "video_link",
            "industry_id",
        )

    def create(self, validated_data):
        core_skills = validated_data.pop("core_skills")
        tags = validated_data.pop("tags")
        languages = validated_data.pop("languages")

        instance = super().create(validated_data)
        instance.core_skills.add(*[int(skill) for skill in core_skills[0].split(",")])
        instance.tags.add(*[int(tag) for tag in tags[0].split(",")])
        instance.languages.add(*[int(lan) for lan in languages[0].split(",")])

        return instance

    def update(self, instance, validated_data):
        core_skills = validated_data.pop("core_skills")
        tags = validated_data.pop("tags")
        languages = validated_data.pop("languages")

        updated_instance = super().update(instance, validated_data)
        updated_instance.core_skills.clear()
        updated_instance.tags.clear()
        updated_instance.languages.clear()

        updated_instance.core_skills.add(
            *[int(skill) for skill in core_skills[0].split(",")]
        )
        updated_instance.tags.add(*[int(tag) for tag in tags[0].split(",")])
        updated_instance.languages.add(*[int(lan) for lan in languages[0].split(",")])

        return updated_instance


class NotificationsSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationsSettings
        fields = "__all__"
