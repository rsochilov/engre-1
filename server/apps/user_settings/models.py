from django.db import models


class NotificationsSettings(models.Model):
    project_updates = models.BooleanField(default=True)
    new_messages = models.BooleanField(default=True)
    events = models.BooleanField(default=False)
    user = models.OneToOneField(
        "accounts.User", on_delete=models.CASCADE, related_name="notifications_settings"
    )
