from django.db.models import Model as DjangoModel
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.shortcuts import reverse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.serializers import Serializer as DRFserializer
from accounts.tasks import send_support_email
from .models import NotificationsSettings


class TryCreateMixin:
    """
    Used for Project and IntellectualService creation
    """

    def generate_error_response(self, message: str, status_code: int = 400) -> Response:
        return Response(data={"errors": message}, status=status_code)

    # TODO переписать с добавление validate'a в сериализаторе, когда будет больше времени
    def try_create(self, model: DjangoModel, serializer: DRFserializer) -> Response:
        """Checks duplicated model name in db. If doesn't exists, creates model

        Arguments:
            model {DjangoModel} -- Project or IntellectualService model
            serializer {DRFserializer} -- model serializer with validated data

        Returns:
            Response
        """
        name = serializer.validated_data["name"]
        company_id = serializer.validated_data["company"].id
        try:
            model.objects.get(company__pk=company_id, name=name)
            return self.generate_error_response(
                f'{model.__name__} with name "{name}" already exists'
            )
        except MultipleObjectsReturned:
            return self.generate_error_response(
                f'{model.__name__} with name "{name}" already exists'
            )
        except ObjectDoesNotExist:
            self.perform_create(serializer)

            send_support_email.delay(
                subject=f'New {model.__name__} "{name}" was created', body=f""
            )
            return Response(
                data={"success": True, "url": reverse("projects-list")},
                status=status.HTTP_201_CREATED,
            )


class TryUpdateMixin:
    """
    Used for Project and IntellectualService update
    """

    def try_update(
        self,
        model: DjangoModel,
        serializer: DRFserializer,
        updated_instance: DjangoModel,
    ) -> Response:
        name = serializer.validated_data["name"]
        company_id = updated_instance.company.id
        id = updated_instance.id
        if (
            model.objects.filter(name=name, company__pk=company_id)
            .exclude(pk=id)
            .count()
        ):
            return Response(data={"errors": serializer.errors}, status=400)
        self.perform_update(serializer)
        return Response(data={"success": True, "url": reverse("projects-list")})
