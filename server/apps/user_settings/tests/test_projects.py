from datetime import date
from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate
from django.shortcuts import reverse

from accounts.models import User, ProjectManager
from company.models import Company
from core.models import Tag, Language
from project.models import Project, CoreSkill, Industry, Category
from user_settings.views import ProjectViewSet


class ProjectViewsTests(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.admin_user = User.objects.get(email="admin@admin.com")
        cls.new_project = {
            "name": "test_project",
            "budget": 123,
            "date_start": date.today(),
            "date_finish": date.today(),
            "category_id": 8,
            "industry_id": 4,
            "core_skills": [186],
            "tags": [2],
            "languages": [1],
            "description": "test_descr",
            "video_link": None,
            "company": 5795,
            "management_option": Project.PLATFORM,
        }

    @classmethod
    def tearDownClass(cls):
        pass

    def test_pm_chat_room_create(self):
        factory = APIRequestFactory()
        response = self.add_project(factory)

        self.assertEqual(response.status_code, 201)

        try:
            Project.objects.get(name=self.new_project["name"])
        except Project.DoesNotExist:
            self.fail("Project was not created")

    def test_create_project(self):
        factory = APIRequestFactory()
        response = self.add_project(factory)

        self.assertEqual(response.status_code, 201)

        try:
            Project.objects.get(name=self.new_project["name"])
        except Project.DoesNotExist:
            self.fail("Project was not created")

    def test_create_duplicated_project(self):
        factory = APIRequestFactory()
        response = self.add_project(factory)
        self.assertEqual(response.status_code, 201)

        response = self.add_project(factory)

        self.assertEqual(response.status_code, 400, "Error! Service was created twice")

    def test_update_project(self):
        new_proj = self.create_test_project()

        updated_project = self.new_project
        updated_project["budget"] = 123321
        updated_project["pk"] = new_proj.id

        factory = APIRequestFactory()

        request = factory.put(reverse("update-project"), data=updated_project)
        force_authenticate(request, self.admin_user)

        view = ProjectViewSet.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 200, response.data)

    def create_test_project(self) -> Project:
        new_proj = Project.objects.create(
            name=self.new_project["name"],
            budget=self.new_project["budget"],
            date_start=self.new_project["date_start"],
            date_finish=self.new_project["date_finish"],
            category=Category.objects.first(),
            industry=Industry.objects.first(),
            description=self.new_project["description"],
            company=Company.objects.get(pk=self.new_project["company"]),
            management_option=self.new_project["management_option"],
        )
        new_proj.core_skills.add(CoreSkill.objects.first())
        new_proj.languages.add(Language.objects.first())
        new_proj.tags.add(Tag.objects.first())
        new_proj.save()
        return new_proj

    def add_project(self, factory: APIRequestFactory, new_project: dict = None):
        view = ProjectViewSet.as_view()
        if new_project:
            request = self.get_request_with_auth(factory, "add-project", new_project)
        else:
            request = self.get_request_with_auth(
                factory, "add-project", self.new_project
            )
        return view(request)

    def get_request_with_auth(
        self, factory: APIRequestFactory, path_name: str, data: dict
    ):
        request = factory.post(reverse(path_name), data=data)
        force_authenticate(request, self.admin_user)
        return request
