from seo_tags.management.abstract_classes import ArticleCommand
from news.models import NewsArticle
from seo_tags.models import NewsArticleMetaTags


class Command(ArticleCommand):
    _model = NewsArticle
    _metatag_model = NewsArticleMetaTags
