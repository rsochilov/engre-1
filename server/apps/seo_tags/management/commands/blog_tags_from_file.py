from seo_tags.management.abstract_classes import ArticleCommand
from blog.models import BlogArticle
from seo_tags.models import BlogArticleMetaTags


class Command(ArticleCommand):
    _model = BlogArticle
    _metatag_model = BlogArticleMetaTags
