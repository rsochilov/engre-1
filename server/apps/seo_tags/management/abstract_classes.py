from os import path
from urllib.parse import urlparse
from typing import List, Iterator

import xlrd
from django.db.models import ObjectDoesNotExist
from django.core.management.base import BaseCommand
from loguru import logger

from core.utils import ArticleHtmlParser, SitemapArticleWriter
from seo_tags.models import AbstractArticleMetaTags


class ArticleTag:
    def __init__(self, vals):
        self.path = urlparse(vals[0]).path.strip("/")
        self.title = vals[1]
        self.description = vals[2]
        self.keywords = vals[3]
        self.image_uri = vals[4]
        self.image_alt = vals[5]


class ArticleCommand(BaseCommand):
    _model = None
    _metatag_model = None

    def add_arguments(self, parser):
        parser.add_argument("filepath", nargs="+", type=str)

    def handle(self, *args, **options):
        filepath = options["filepath"][0]
        if not path.isfile(filepath):
            logger.error(f'Unable to find file "{filepath}"')
            exit(-1)
        self.insert_tags(self.read_from_xl(filepath))

    def read_from_xl(self, filepath: str) -> Iterator[ArticleTag]:
        sheet = xlrd.open_workbook(filepath).sheet_by_index(0)
        for row_num in range(1, sheet.nrows):
            vals = sheet.row_values(row_num)
            if vals[0]:
                yield ArticleTag(vals)

    def insert_tags(self, tags: List[ArticleTag]):
        for tag_info in tags:
            slug = tag_info.path.split("articles/")[1].rstrip("/")
            article = None
            try:
                article = self._model.objects.get(slug=slug)
            except ObjectDoesNotExist:
                logger.error(f'Article not found: "{slug}"')
                continue
            headline = ArticleHtmlParser.get_headline(article)
            metatags, created = self._metatag_model.objects.get_or_create(
                article=article,
            )
            self.update_tags_data(metatags, tag_info, headline)

            if created:
                SitemapArticleWriter.insert_article_info(tag_info.path)
                logger.success(f'Tags for "{article}" created')
            else:
                logger.info(f'Tags for "{article}" updated')

    def update_tags_data(
        self, metatags: AbstractArticleMetaTags, tag_info: ArticleTag, headline: str
    ):
        metatags.path = tag_info.path
        metatags.title = tag_info.title
        metatags.description = tag_info.description
        metatags.keywords = tag_info.keywords
        metatags.headline = headline
        metatags.image_alt = tag_info.image_alt
        metatags.image_path = tag_info.image_uri
        metatags.save()
