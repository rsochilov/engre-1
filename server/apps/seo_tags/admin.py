from django.contrib import admin
from .models import HeadMetaTags, BlogArticleMetaTags, NewsArticleMetaTags

admin.site.register(HeadMetaTags)
admin.site.register(BlogArticleMetaTags)
admin.site.register(NewsArticleMetaTags)
