# Generated by Django 2.1.4 on 2020-05-08 14:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("seo_tags", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="headmetatags", options={"verbose_name": "Head meta tag"},
        ),
        migrations.AddField(
            model_name="headmetatags",
            name="robots",
            field=models.CharField(
                blank=True, default="noindex, follow", max_length=100
            ),
        ),
    ]
