from django.db.models import Model

from blog.models import BlogArticle
from news.models import NewsArticle
from about.models import CommonProject, Achievement
from core.utils import ArticleHtmlParser

from . import models


class TagDbInserter:
    def __init__(self, tag_model: models.AbstractMetaTags):
        self.tag_model = tag_model

    def insert_tags(self, data: dict, db_model: Model = None):
        if not db_model:
            return
        if type(db_model) in (BlogArticle, NewsArticle):
            self._insert_article_tags(data, db_model)
        elif type(db_model) == CommonProject:
            self._insert_common_project_tags(data, db_model)
        elif type(db_model) == Achievement:
            self._insert_achievement_tags(data, db_model)
        else:
            raise NotImplementedError(
                f"Method not implemented for type: {type(db_model)}"
            )

    def _insert_article_tags(self, data: dict, db_model: Model):
        metatags, created = self.tag_model.objects.get_or_create(article=db_model)
        self._insert_article_add_data(metatags, data, db_model)

    def _insert_common_project_tags(self, data: dict, db_model: Model):
        metatags, created = self.tag_model.objects.get_or_create(
            common_project=db_model
        )
        self._insert_article_add_data(metatags, data, db_model)

    def _insert_achievement_tags(self, data: dict, db_model: Model):
        metatags, created = self.tag_model.objects.get_or_create(achievement=db_model)
        self._insert_article_add_data(metatags, data, db_model)

    def _insert_article_add_data(
        self,
        metatags: models.AbstractArticleMetaTags,
        data: dict,
        db_model: Model = None,
    ):
        headline = ArticleHtmlParser.get_headline(db_model)
        metatags.path = db_model.get_absolute_url().strip("/")
        metatags.headline = headline
        metatags.title = data["title"]
        metatags.description = data["seo_tags"]["description"]
        metatags.keywords = data["seo_tags"]["keywords"]
        metatags.image_alt = data["seo_tags"]["image_alt"]
        metatags.image_path = data["seo_tags"]["image_url"]
        metatags.save()
