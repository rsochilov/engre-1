from django.db import models
from os import path
from django.conf import settings

from blog.models import BlogArticle
from news.models import NewsArticle
from about.models import Achievement, CommonProject

from .tag_renderers import MainTagRenderer, ArticleTagRenderer


class AbstractMetaTags(models.Model):
    # path is without first and last slashes!!
    path = models.CharField(max_length=150, blank=False, null=False, unique=True)
    title = models.CharField(max_length=100, blank=True, null=False)
    description = models.CharField(max_length=250, blank=True, null=False)
    keywords = models.CharField(max_length=200, blank=True, null=False)
    robots = models.CharField(
        max_length=100, blank=True, null=False, default="index, follow"
    )
    renderer = MainTagRenderer

    def render_head(self, host: str) -> str:
        return self.renderer.render_head(self, host)

    def render_body(self, host: str) -> str:
        return self.renderer.render_body(host)

    class Meta:
        abstract = True


class AbstractArticleMetaTags(AbstractMetaTags):
    headline = models.CharField(max_length=256, blank=True, null=False)
    image_path = models.CharField(max_length=256, blank=True, null=False)
    image_alt = models.CharField(max_length=100, blank=True, null=False)
    renderer = ArticleTagRenderer

    class Meta:
        abstract = True

    def render_body(self, host: str) -> str:
        return self.renderer.render_body(self, host)

    def __str__(self):
        return super().path


class HeadMetaTags(AbstractMetaTags):
    has_og_tags = models.BooleanField(default=True, null=False, blank=False)

    class Meta:
        verbose_name = "Head meta tag"

    def __str__(self):
        return self.path if self.path != "" else "Index"


class BlogArticleMetaTags(AbstractArticleMetaTags):
    article = models.ForeignKey(BlogArticle, on_delete=models.CASCADE, null=False)

    class Meta:
        verbose_name = "Blogs meta tag"


class NewsArticleMetaTags(AbstractArticleMetaTags):
    article = models.OneToOneField(NewsArticle, on_delete=models.CASCADE, null=False)

    class Meta:
        verbose_name = "News meta tag"


class CommonProjectMetaTags(AbstractArticleMetaTags):
    common_project = models.OneToOneField(
        CommonProject, on_delete=models.CASCADE, null=False
    )


class AchievementMetaTags(AbstractArticleMetaTags):
    achievement = models.OneToOneField(
        CommonProject, on_delete=models.CASCADE, null=False
    )
