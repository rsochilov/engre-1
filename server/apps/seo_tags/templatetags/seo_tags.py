from os import path
from django import template
from django.http import HttpRequest
from django.utils.html import escape

from seo_tags.models import HeadMetaTags
from seo_tags.tag_renderers import DefaultTagRenderer

register = template.Library()


def get_tags_from_context(context: dict) -> HeadMetaTags:
    return context.get("seo_tags")


def get_request_from_context(context: dict) -> HttpRequest:
    return context.get("seo_request")


def get_uri_from_request(request: HttpRequest) -> str:
    uri = f"{request.get_host()}{request.path}"
    return f"https://{uri}" if request.is_secure() else f"http://{uri}"


@register.simple_tag(takes_context=True)
def head_tags(context: dict, title: str = ""):
    """Renders seo tags if context contains seo_tags or seo_request

    Arguments:
        context {dict} -- context

    Keyword Arguments:
        title {str} -- cms title (default: {""})

    Returns:
        [str] -- Rendered metatags
    """

    request = get_request_from_context(context)

    if not request:
        return ""

    uri = get_uri_from_request(request)
    tags = get_tags_from_context(context)
    if tags:
        return tags.render_head(uri)
    return DefaultTagRenderer.render_head(uri)


@register.simple_tag(takes_context=True)
def body_tags(context: dict):
    """Renders seo body script if context contains seo_request and seo_tags

    Arguments:
        context {dict} -- context

    Returns:
        [str] -- Rendered script
    """

    request = get_request_from_context(context)
    if not request:
        return ""
    tags = get_tags_from_context(context)
    host = request.get_host()
    if tags:
        return tags.render_body(host)

    return DefaultTagRenderer.render_body(host)
