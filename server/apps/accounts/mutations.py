import json

import graphene
import graphql_jwt
import graphql_social_auth
import requests
from core import types
from django.conf import settings
from django.core.exceptions import ValidationError
from graphql_jwt.decorators import login_required
from graphql_jwt.shortcuts import get_token

from .inputs import BloggerProfileInput, ProfileInput, RegisterInput
from .models import Blogger, User
from .schema import BloggerType, UserType, UserTypeChoice, UserTypeUnion
from .utils import get_user_subclass


class ObtainLinkedInAccessToken(graphene.Mutation):
    class Arguments:
        code = graphene.String(required=True)

    access_token = graphene.String()

    def mutate(self, info, code):
        payload = {
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": settings.SOCIAL_AUTH_LINKEDIN_REDIRECT_URI,
            "client_id": settings.SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY,
            "client_secret": settings.SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET,
        }

        result = requests.get(
            settings.SOCIAL_AUTH_LINKEDIN_API_ENDPOINT, params=payload
        )

        data = json.loads(result.content)

        return ObtainLinkedInAccessToken(access_token=data.get("access_token"))


class SignUpMutation(types.Mutation):
    class Arguments:
        user_data = RegisterInput(required=True)
        user_type = UserTypeChoice(required=True)

    user = graphene.Field(UserType)
    token = graphene.String()

    def mutate(self, info, user_data, user_type):
        if User.objects.filter(email=user_data.email).exists():
            raise ValidationError("Email already taken!")

        if user_type == UserTypeChoice.USER:
            user = User.objects.super_create(**user_data)

        elif user_type == UserTypeChoice.BLOGGER:
            user = Blogger.objects.super_create(**user_data).user

        user.set_password(user_data.password)
        user.save()
        token = get_token(user)

        return SignUpMutation(user=user, token=token)


class ProfileUpdateMutation(types.Mutation):
    class Arguments:
        profile_data = ProfileInput()

    user = graphene.Field(UserType)

    @login_required
    def mutate(self, info, profile_data):
        user = User.objects.super_update(instance=info.context.user, **profile_data)

        return ProfileUpdateMutation(user=user)


class BloggerProfileUpdateMutation(types.Mutation):
    class Arguments:
        profile_data = BloggerProfileInput()

    blogger = graphene.Field(BloggerType)

    @login_required
    def mutate(self, info, profile_data):
        blogger = Blogger.objects.super_update(
            instance=info.context.user.blogger, **profile_data
        )

        return BloggerProfileUpdateMutation(blogger=blogger)


class ObtainJSONWebToken(graphql_jwt.JSONWebTokenMutation):
    user = graphene.Field(UserTypeUnion)

    @classmethod
    def resolve(cls, root, info):
        user = get_user_subclass(info.context.user)

        return cls(user=user)


class SocialAuth(graphql_social_auth.SocialAuthJWT):
    class Arguments:
        provider = graphene.String(required=True)
        access_token = graphene.String(required=True)
        user_type = UserTypeChoice(required=True)

    user = graphene.Field(UserTypeUnion)
    token = graphene.String()

    @classmethod
    def resolve(cls, root, info, social, **kwargs):
        user = get_user_subclass(social.user)

        if kwargs.get("user_type") == UserTypeChoice.BLOGGER:
            blogger_data = {
                "name": user.name,
                "email": user.email,
                "user": user,
            }
            Blogger.objects.super_create(**blogger_data)

        token = get_token(user)

        return cls(user=user, token=token)


class Mutation(graphene.ObjectType):
    token_auth = ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    social_auth = SocialAuth.Field()
    obtain_linkedin_access_token = ObtainLinkedInAccessToken.Field()
    sign_up = SignUpMutation.Field()
    update_profile = ProfileUpdateMutation.Field()
    update_blogger_profile = BloggerProfileUpdateMutation.Field()
