# Generated by Django 2.1.4 on 2019-04-12 10:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0013_auto_20190313_1035"),
    ]

    operations = [
        migrations.RemoveField(model_name="user", name="last_seen",),
    ]
