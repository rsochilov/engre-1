from accounts.models import Blogger


def _get_name(d: dict):
    return list(d["localized"].values())[0]


def get_name_from_linkedin_responce(response: dict) -> str:
    full_name = ""
    first_name = response.get("firstName")
    if first_name:
        full_name += _get_name(first_name)

    last_name = response.get("lastName")
    if last_name:
        return (
            full_name + " " + _get_name(last_name)
            if full_name
            else _get_name(last_name)
        )


def get_name(backend, user, response, *args, **kwargs):
    if backend.name == "facebook":
        if not user.name and response["name"]:
            user.name = response["name"]

    elif backend.name == "google-oauth2":
        if not user.name:
            if response.get("displayName"):
                user.name = response["displayName"]
            elif response.get("name"):
                user.name = response.get("name")
            else:
                print("Unable to get username from Google")

    elif backend.name == "linkedin-oauth2":
        if not user.name and response.get("firstName") or response.get("lastName"):
            user.name = get_name_from_linkedin_responce(response)

    user.save()


def blogger_create(backend, user, response, *args, **kwargs):
    strategy = kwargs.get("strategy")
    is_blogger = strategy.session_get("is_blogger")

    if is_blogger == "true":
        try:
            blogger = user.blogger
        except Blogger.DoesNotExist:
            blogger = Blogger.objects.create(
                user=user,
                email=user.email,
                name=user.name,
                location=user.location,
                timezone=user.timezone,
                date_joined=user.date_joined,
                is_active=user.is_active,
            )

            if user.picture:
                blogger.picture.save(user.picture.name, user.picture.file)


def activate_user(backend, user, response, *args, **kwargs):
    user.is_active = True
    user.save()
