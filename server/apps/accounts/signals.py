from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from chat.models import ChatRoom, Message
from company.models import Company, Worker
from user_settings.models import NotificationsSettings

from .models import ProjectManager, User
from .tasks import send_secret_key_email
from .tokens import private_key_link_token


@receiver(post_save, sender=User, dispatch_uid="accounts.create_notifications_settings")
def create_notifications_settings(sender, instance, **kwargs):
    try:
        instance.notifications_settings
    except NotificationsSettings.DoesNotExist:
        NotificationsSettings.objects.create(user=instance)


@receiver(post_save, sender=User, dispatch_uid="accounts.create_private_key")
def create_private_key(sender, instance, **kwargs):
    if instance.is_verified and not instance.private_key:
        instance.generate_new_private_key()
        token = private_key_link_token.make_token(instance)
        send_secret_key_email.delay(instance.pk, instance.email, token)


@receiver(post_save, sender=User, dispatch_uid="accounts.create_admin_chat")
def create_admin_chat(sender, instance, created, **kwargs):
    if created:
        try:
            admin = User.objects.get(email=settings.ADMIN_EMAIL)
            chat_room = ChatRoom.objects.create(blocked=True)
            chat_room.users.set([instance, admin])

            Message.objects.create(
                message=settings.GREETING_MESSAGE,
                sender=admin,
                chat_room=chat_room,
                type=Message.SYSTEM,
            )
        except User.DoesNotExist:
            print(
                f"ERROR! 'create_admin_chat' Unable to find admin with email {settings.ADMIN_EMAIL}"
            )


@receiver(
    post_save, sender=ProjectManager, dispatch_uid="accounts.assign_company_to_pm"
)
def assign_company_to_pm(sender, instance, created, **kwargs):
    if created:
        try:
            company = Company.objects.get(pk=1)
            Worker.objects.create(user=instance, company=company)
        except Company.DoesNotExist:
            pass
