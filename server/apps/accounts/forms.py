from django import forms
from django.core.exceptions import ValidationError


class LoginForm(forms.Form):

    email = forms.EmailField(required=True)
    password = forms.CharField(required=True)


class RegistrationForm(forms.Form):
    name = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    password = forms.CharField(required=True)
    re_password = forms.CharField(required=True)

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        re_password = cleaned_data.get("re_password")
        if password != re_password:
            raise ValidationError("Passwords no match")
