import graphene
from core.inputs import FileInput, TimezoneInput


class ProfileCommonFields:
    name = graphene.String()
    location = graphene.ID()
    timezone = graphene.InputField(TimezoneInput)
    picture = graphene.InputField(FileInput)


class RegisterInput(graphene.InputObjectType):
    name = graphene.String(required=True)
    email = graphene.String(required=True)
    password = graphene.String(required=True)


class ProfileInput(graphene.InputObjectType, ProfileCommonFields):
    pass


class BloggerProfileInput(graphene.InputObjectType, ProfileCommonFields):
    biography = graphene.String()
