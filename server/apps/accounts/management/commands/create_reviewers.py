from os import path

from django.core.management.base import BaseCommand
from django.conf import settings
from django.utils.text import slugify
from loguru import logger

from accounts.models import User
from company.models import Company


class Command(BaseCommand):
    help = "Creates reviewers"
    default_password = "12345qwerTy"
    picture_name_template = "users/pictures/{}_rev_avatar.jpg"
    company_logo_template = "logos/logo_rev_{}.jpg"

    def add_arguments(self, parser):
        parser.add_argument("names_path", nargs="+", type=str)

    def handle(self, *args, **options):
        names_file = options["names_path"][0]
        if not path.isfile(names_file):
            logger.error(f"Unable to find file: {names_file}")
            return
        for i, line in enumerate(open(names_file, "r")):
            name = line.rstrip()
            email = self.generate_email_from_name(name)

            user, created = User.objects.get_or_create(
                email=email, password=self.default_password
            )
            user.name = name
            self.update_user_picture(user, i + 1)
            msg = "created" if created else "updated"
            user.save(update_fields=["name", "picture"])
            logger.success(f"Account for {user} {msg}")

    def create_company(self, name: str, i: int) -> Company:
        company_name = f"{name}'s company"
        cmp, created = Company.objects.get_or_create(name=company_name, role=1)
        cmp.slug = slugify(company_name)
        cmp.owner = name
        cmp.logo = self.company_logo_template.format(i + 1)
        cmp.save()

    def update_user_picture(self, user: User, number: int) -> User:
        full_path = path.join(
            settings.MEDIA_ROOT, self.picture_name_template.format(number)
        )
        if not path.isfile(full_path):
            logger.warning(f"Unable to find user image: {full_path}")
            return
        user.picture = self.picture_name_template.format(number)

        user.save(update_fields=["picture"])
        return user

    def generate_email_from_name(self, name: str) -> str:
        return name.replace(" ", ".").lower() + "@engre.co"
