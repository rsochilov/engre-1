from django.core.management.base import BaseCommand
from loguru import logger

from blog.models import BlogArticle
from accounts.models import Blogger


class Command(BaseCommand):
    help = "Delete all bloggers except Tod Krank, Darryl Robertson and Lesia Kozachvska"
    used_bloggers_names = ["Tod Krank", "Darryl Roberson", "Lesia Kozachvska"]

    def handle(self, *args, **options):
        test_bloggers = Blogger.objects.exclude(user__name__in=self.used_bloggers_names)
        if not test_bloggers.count():
            logger.info("No test bloggers found, exiting")
            return
        logger.info(f"Bloggers found:\n{test_bloggers}")
        logger.info("Deleting all blog article, associated with test bloggers")
        BlogArticle.objects.filter(author__in=test_bloggers).delete()
        logger.info("Deleted!")
        logger.info("Deleting test bloggers")
        test_bloggers.delete()
        logger.success("All test bloggers were deleted")
