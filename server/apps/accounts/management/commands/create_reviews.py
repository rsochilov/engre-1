from os import path

from typing import Tuple
from django.core.management.base import BaseCommand
from loguru import logger

from accounts.models import User
from company.models import Review


class Command(BaseCommand):
    help = "Creates reviews from files"

    def add_arguments(self, parser):
        parser.add_argument("names_path", nargs="+", type=str)
        parser.add_argument("text_path", nargs="+", type=str)

    def handle(self, *args, **options):
        names_file = options["names_path"][0]
        text_file = options["text_path"][0]
        self.validate_pathes(names_file, text_file)

        reviews = "".join(open(text_file, "r")).split("\n\n")
        users_names = [line.rstrip() for line in open(names_file, "r").readlines()]

        if len(reviews) != len(users_names):
            logger.error(
                f"Reviews count ({len(reviews)}) != "
                + f"users count ({len(users_names)})"
            )
            return

        for i, reviewer_name in enumerate(users_names):
            user = self.get_user(reviewer_name)

            review, created = Review.objects.get_or_create(
                user=user, text=reviews[i], rating=5
            )
            msg = "created" if created else "already exists"
            logger.success(f'Review "{review}" {msg}')

    def get_user(self, reviewer_name: str) -> User:
        try:
            return User.objects.get(name=reviewer_name)
        except User.DoesNotExist:
            logger.error(f"Reviewer {reviewer_name} doesn't exists")
            exit(-1)

    def validate_pathes(self, *file_names: str):
        for name in file_names:
            if not path.isfile(name):
                logger.error(f'File "{name}" doesn\'t exists')
                exit(-1)
