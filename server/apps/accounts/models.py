import datetime

from Crypto.PublicKey import RSA
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.core.cache import cache
from django.db import models
from django.db.models import Count, Q
from django.utils import timezone as django_tz
from timezone_field import TimeZoneField

from market_location.models import City

from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    """ Some tips for User model
    role == 2 - customer
    """

    name = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    picture = models.ImageField(upload_to="users/pictures")
    location = models.ForeignKey(City, null=True, on_delete=models.SET_NULL)
    timezone = TimeZoneField(null=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    date_joined = models.DateField(auto_now_add=True, null=True)
    last_seen = models.DateTimeField(default=django_tz.now, blank=True)
    private_key = models.BinaryField(max_length=2048, blank=True, null=True)
    private_key_downloaded = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    archived_chats = models.ManyToManyField("chat.ChatRoom", related_name="archived_by")

    objects = UserManager()

    USERNAME_FIELD = "email"

    def __str__(self):
        return self.name

    @property
    def role(self):
        try:
            if self.has_company and self.work.company:
                return self.work.company.role
        except User._meta.get_field("work").related_model.DoesNotExist:
            return None

        return None

    @property
    def has_company(self):
        if hasattr(self, "work"):
            return bool(self.work.company)

    def get_company(self):
        try:
            if self.has_company:
                return self.work.company
        except User._meta.get_field("work").related_model.DoesNotExist:
            return None

    @property
    def is_profile_completed(self):
        if self.name and self.email and self.location and self.timezone:
            return True

        return False

    @property
    def active_projects(self):
        if hasattr(self, "projectmanager"):
            return self.projectmanager.project_set.filter(
                management_option=1, is_archived=False
            ).count()
        elif hasattr(self, "work"):
            return self.work.company.projects.filter(is_archived=False).count()

        return 0

    @property
    def active_services(self):
        if hasattr(self, "work"):
            return self.work.company.intellectualservices.filter(
                is_archived=False
            ).count()

        return 0

    @property
    def is_customer(self):
        return self.role == 2

    @property
    def is_contractor(self):
        return self.role == 1

    # "Фича", которая используется чтобы приглашённому пользователю, у которого нет учётки, в меню рендерились
    # только Projects и Profile. Детальнее смотреть core.cms_menus, core.models.PageSettingsExtension и PageRenderingCondition...
    # Ugly code for rendering menu for invited users, that didnt't create its accounts at this site.
    # Details in core.cms_menus, core.models.PageSettingsExtension и PageRenderingCondition...
    @property
    def is_native_user(self):
        if hasattr(self, "work"):
            return self.work.is_native_user

    @property
    def has_intellectual_services(self):
        if hasattr(self, "projectmanager"):
            return (
                self.projectmanager.project_set.filter(management_option=1).count() > 0
            )

        if hasattr(self, "work"):
            if self.work.company:
                if self.work.company.role == 2:
                    return self.work.company.projects.count() > 0
                elif self.work.company.role == 1:
                    return self.work.company.intellectualservices.count() > 0

        return False

    @property
    def is_pm(self):
        return hasattr(self, "projectmanager")

    @property
    def is_blogger(self):
        return hasattr(self, "blogger")

    @property
    def is_content_manager(self):
        return hasattr(self, "contentmanager")

    @property
    def is_owner(self):
        if hasattr(self, "work"):
            return self.work.is_owner

        return False

    @property
    def without_role(self):
        return (
            not self.is_blogger
            and not self.is_customer
            and not self.is_contractor
            and not self.is_pm
        )

    def generate_new_private_key(self):
        key = RSA.generate(2048)
        self.private_key = key.export_key()
        self.save()

    def get_private_key(self):
        if self.private_key:
            return RSA.importKey(self.private_key.tobytes())

        return None

    def get_public_key(self):
        if self.private_key:
            return self.get_private_key().publickey()

        return None

    def check_public_key(self, key):
        return self.get_public_key() == key

    def last_seen(self):
        return cache.get("seen_%s" % self.id)

    def online(self):
        if self.last_seen():
            now = datetime.datetime.now()
            if now > self.last_seen() + datetime.timedelta(
                seconds=settings.USER_ONLINE_TIMEOUT
            ):
                return False
            else:
                return True
        else:
            return False


class Blogger(User):
    subscribers = models.ManyToManyField(User, related_name="blogger_subscriptions")
    user = models.OneToOneField(User, parent_link=True, on_delete=models.CASCADE)
    biography = models.TextField(null=True)
    company = models.CharField(max_length=100, null=True, blank=True)

    @property
    def rating(self):
        rating = self.blog_articles.aggregate(rating=Count("interested_users")).get(
            "rating"
        )

        if rating > 0:
            return f"+{rating}"

        return str(rating)

    @property
    def active_articles(self):
        return self.blog_articles.filter(Q(is_archived=False) & Q(status=2)).count()


class ContentManager(User):
    @property
    def rating(self):
        rating = self.news_articles.aggregate(rating=Count("interested_users")).get(
            "rating"
        )

        if rating > 0:
            return f"+{rating}"

        return str(rating)


class ProjectManager(User):
    JUNIOR, MIDDLE, SENIOR = range(1, 4)
    SKILL_LEVELS = (
        (JUNIOR, "Junior"),
        (MIDDLE, "Middle"),
        (SENIOR, "Senior"),
    )

    is_project_manager = models.BooleanField(default=True)
    industry = models.ForeignKey(
        "project.Industry",
        on_delete=models.SET_NULL,
        related_name="pms",
        null=True,
        blank=True,
    )
    core_skills = models.ManyToManyField("project.CoreSkill", blank=True)
    weekly_availability = models.PositiveSmallIntegerField(null=True, blank=True)
    skills_level = models.PositiveSmallIntegerField(choices=SKILL_LEVELS, default=1)
    membership = models.ManyToManyField(
        "company.Worker", related_name="project_managers", blank=True
    )

    def __str__(self):
        return self.name

    @property
    def active_projects(self):
        return self.project_set.count()
