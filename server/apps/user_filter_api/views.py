from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from .serializers import SearchFilterSerializer
from .models import SearchFilter


class SearchFilterViewSet(ModelViewSet):
    queryset = SearchFilter.objects.all()
    serializer_class = SearchFilterSerializer
    pagination_class = None
    permission_classes = (IsAuthenticated,)
