from django.apps import AppConfig


class UserFilterApiConfig(AppConfig):
    name = "user_filter_api"
