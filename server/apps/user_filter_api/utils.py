from collections import defaultdict
from datetime import datetime
from typing import List, Iterator, Tuple

from django.db.models import Q, QuerySet

from project.models import Project, IntellectualService, WorkAbstract
from company.models import Company
from .models import SearchFilter
from core.models import NOT_ACTIVE, ACTIVE


class ServiceSelector:
    user_is_verified: bool = False

    def __init__(self, user_is_verified: bool = False):
        self.user_is_verified = user_is_verified

    def select_all_services(
        self, start_date: datetime, end_date: datetime
    ) -> Iterator[Tuple]:
        filters = SearchFilter.objects.prefetch_related(
            "user", "user__notifications_settings"
        ).filter(user__is_verified=self.user_is_verified)
        dd = defaultdict(Q)
        date_filter = Q(created_at__range=(start_date, datetime.now()))
        for f in filters:
            if f.user.is_customer:
                services_filter = (
                    date_filter & f.get_services_filter() & Q(status=ACTIVE)
                )
                dd[f.user] |= services_filter
            else:
                projects_filter = (
                    date_filter & f.get_projects_filter() & Q(status=NOT_ACTIVE)
                )
                dd[f.user] |= projects_filter
        for user in dd:
            yield user.email, IntellectualService.objects.filter(
                dd[user]
            ) if user.is_customer else Project.objects.filter(dd[user])

    def select_int_services(
        self, start_date: datetime, end_date: datetime
    ) -> Iterator[Tuple]:
        filters = SearchFilter.objects.prefetch_related("user").filter(
            user__notifications_settings__project_updates=True,
            user__is_verified=self.user_is_verified,
            user__work__company__role=Company.CUSTOMER,
        )
        dd = defaultdict(Q)
        date_status_filter = Q(created_at__range=(start_date, datetime.now())) & Q(
            status=ACTIVE
        )
        for f in filters:
            services_filter = date_status_filter & f.get_services_filter()
            dd[f.user] |= services_filter
        for user in dd:
            yield user, IntellectualService.objects.filter(dd[user])

    def select_projects(
        self, start_date: datetime, end_date: datetime
    ) -> Iterator[Tuple]:
        filters = SearchFilter.objects.prefetch_related("user").filter(
            user__notifications_settings__project_updates=True,
            user__is_verified=self.user_is_verified,
            user__work__company__role=Company.CONTRACTOR,
        )
        dd = defaultdict(Q)
        date_status_filter = Q(created_at__range=(start_date, datetime.now())) & Q(
            status=NOT_ACTIVE
        )
        for f in filters:
            projects_filter = date_status_filter & f.get_projects_filter()
            dd[f.user] |= projects_filter

        for user in dd:
            yield user, Project.objects.filter(dd[user])
