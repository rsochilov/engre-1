from rest_framework.serializers import (
    ModelSerializer,
    ValidationError,
    CharField,
    SerializerMethodField,
    SlugRelatedField,
)

from .models import SearchFilter
from project.models import CoreSkill, Industry, Category
from company.models import Ownership


class SearchFilterSerializer(ModelSerializer):
    coreskill = SlugRelatedField(
        slug_field="slug", queryset=CoreSkill.objects.all(), required=False
    )
    industry = SlugRelatedField(
        slug_field="slug", queryset=Industry.objects.all(), required=False
    )
    category = SlugRelatedField(
        slug_field="slug", queryset=Category.objects.all(), required=False
    )
    ownership = SlugRelatedField(
        slug_field="slug", queryset=Ownership.objects.all(), required=False
    )

    class Meta:
        model = SearchFilter
        exclude = ("user",)

    def create(self, validated_data):
        validated_data["user"] = self.context["request"].user
        return super().create(validated_data)
