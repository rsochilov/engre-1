# Generated by Django 2.1.4 on 2020-06-20 10:00

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("project", "0025_projectusers"),
        ("user_filter_api", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="searchfilter",
            name="industry",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="project.Industry",
            ),
        ),
        migrations.AlterField(
            model_name="searchfilter",
            name="category",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="project.Category",
            ),
        ),
        migrations.AlterField(
            model_name="searchfilter",
            name="company_size",
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name="searchfilter",
            name="coreskill",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="project.CoreSkill",
            ),
        ),
        migrations.AlterField(
            model_name="searchfilter",
            name="name",
            field=models.CharField(default="unnamed_filter", max_length=100),
        ),
        migrations.AlterField(
            model_name="searchfilter",
            name="ownership",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="company.Ownership",
            ),
        ),
        migrations.RemoveField(model_name="searchfilter", name="indusry",),
        migrations.AlterUniqueTogether(
            name="searchfilter", unique_together={("user", "name")},
        ),
    ]
