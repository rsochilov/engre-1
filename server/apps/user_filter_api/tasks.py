from collections import defaultdict
from datetime import datetime, timedelta
from time import sleep
from typing import List, Tuple

from django.conf import settings
from django.db.models import Q, QuerySet
from django.template.loader import render_to_string

from marketplace.celery import app
from accounts.models import User
from accounts.tasks import send_email_func
from company.models import Company, Ownership
from project.models import Project, IntellectualService, WorkAbstract

from .models import SearchFilter
from .utils import ServiceSelector


def send_selected_by_email(notification_data: Tuple, label: str):
    for user, services in notification_data:
        if not services:
            continue
        for s in services:
            s.abs_url = s.get_absolute_url()
        context = {
            "user": user,
            "services": services,
            "label": label,
            "site": settings.SITE_URL,
        }
        send_email_func(
            f"New {label} available on Engre.co",
            render_to_string("user_filter_api/notification_email.html", context),
            [user.email],
            is_html=True,
        )


def run_send_email_iteration(sleep_time: int, selector: ServiceSelector):
    start_date = datetime.now()
    sleep(sleep_time)
    end_date = datetime.now()
    send_selected_by_email(
        selector.select_int_services(start_date, end_date), "services"
    )
    send_selected_by_email(selector.select_projects(start_date, end_date), "projects")


def _start_selector_task(sleep_time: int):
    selector = ServiceSelector()
    if settings.DEBUG:
        run_send_email_iteration(sleep_time, selector)
        return
    while True:
        run_send_email_iteration(sleep_time, selector)


@app.task
def email_verified_users_by_project():
    _start_selector_task(settings.EMAIL_VERIFIED_NOTIFICATOR_SLEEP_TIME)


@app.task
def email_not_verified_users_by_project():
    _start_selector_task(settings.EMAIL_NOT_VERIFIED_NOTIFICATOR_SLEEP_TIME)
