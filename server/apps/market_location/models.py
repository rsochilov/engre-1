from cities_light.abstract_models import AbstractCity, AbstractCountry, AbstractRegion
from cities_light.receivers import connect_default_signals
from cities_light.settings import ICity
from cities_light.signals import city_items_post_import
from django.db import models


class Country(AbstractCountry):
    class Meta:
        indexes = [models.Index(fields=["name"])]


connect_default_signals(Country)


class Region(AbstractRegion):
    pass


connect_default_signals(Region)


class City(AbstractCity):
    timezone = models.CharField(max_length=40)


connect_default_signals(City)


def set_city_fields(sender, instance, items, **kwargs):
    instance.timezone = items[ICity.timezone]


city_items_post_import.connect(set_city_fields)
