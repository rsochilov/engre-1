from accounts.serializers import LocationSerializer as ProfileLocationSerializer
from core.serializers import NameSerializer
from rest_framework import serializers


class LocationSerializer(NameSerializer, serializers.Serializer):
    country = NameSerializer()


class CitySerializer(ProfileLocationSerializer):
    pass
