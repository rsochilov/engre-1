from django.apps import AppConfig


class MarketLocationConfig(AppConfig):
    name = "market_location"
