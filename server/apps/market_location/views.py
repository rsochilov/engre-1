from rest_framework import generics
from django.http import JsonResponse
from .filters import CityFilterSet
from .models import City
from .serializers import CitySerializer


class CityAPIView(generics.RetrieveAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer

    def get_object(self):
        return City.objects.filter(name=self.request.GET.get("city")).first()


class CityListAPIView(generics.ListAPIView):
    def list(self, request, *args, **kwargs):
        user = request.user
        query = City.objects.filter(country__pk=user.location.country.id)
        return JsonResponse(CitySerializer(query, many=True).data, safe=False)
