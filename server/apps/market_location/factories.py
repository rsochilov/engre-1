import factory


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "market_location.Country"

    name = factory.Sequence(lambda n: "country%s" % n)


class CityFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "market_location.City"

    name = factory.Sequence(lambda n: "city%s" % n)
    country = factory.SubFactory(CountryFactory)
