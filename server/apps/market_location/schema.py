import graphene
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from .models import City, Country, Region


class CountryType(DjangoObjectType):
    class Meta:
        model = Country
        interfaces = (graphene.relay.Node,)
        filter_fields = {
            "name": ["icontains"],
        }


class RegionType(DjangoObjectType):
    class Meta:
        model = Region
        interfaces = (graphene.relay.Node,)
        filter_fields = {
            "name": ["icontains"],
        }


class CityType(DjangoObjectType):
    pretty = graphene.String()

    def resolve_pretty(self, info):
        return f"{self.name}, {self.country.name}"

    class Meta:
        model = City
        interfaces = (graphene.relay.Node,)
        filter_fields = {
            "name": ["icontains", "istartswith"],
        }


class Query(graphene.ObjectType):
    countries = DjangoFilterConnectionField(CountryType)
    regions = DjangoFilterConnectionField(RegionType)
    cities = DjangoFilterConnectionField(CityType, country=graphene.String())

    def resolve_countries(self, info, **kwargs):
        return Country.objects.all()

    def resolve_regions(self, info, **kwargs):
        return Region.objects.all()

    def resolve_cities(self, info, country=None):
        if country:
            return City.objects.filter(country__name=country)

        return City.objects.all()
