import glob
import json
import os
import random

from core.models import Category, Language, Tag
from core.schema import CategoryType, LanguageType, TagType, UserType
from crowdfunding_project.models import CrowdfundingProject, CrowdfundingProjectRating
from crowdfunding_project.schema import CrowdProjectType
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models import Avg
from django.test import TestCase
from django.urls import reverse
from graphql_jwt.shortcuts import get_token
from graphql_relay import to_global_id
from market_location.models import City, Country
from market_location.schema import CityType


class CrowdfundingProjectTestCase(TestCase):
    def setUp(self):
        self.test_username = "testuser1"
        self.test_password = "12345678"
        self.test_email = "marketplace.ddi.test1@gmail.com"
        self.test_email2 = "kidomakai@gmail.com"
        self.test_user1 = User.objects.create_user(
            username=self.test_username,
            password=self.test_password,
            email=self.test_email,
        )
        self.user_glob_id = to_global_id(UserType.__name__, self.test_user1.id)
        self.test_user1.save()
        token = get_token(self.test_user1)
        self.test_user1.profile.active_token = token
        self.test_user1.profile.save()

        self.project_name = "testname1"
        self.description = "Some description"
        country = Country.objects.create()
        location = City.objects.create(country=country)
        self.location = to_global_id(CityType.__name__, location.id)
        self.date_start = "2018-01-01T20:10:00+00:00"
        self.date_finish = "2018-01-02T20:10:00+00:00"
        self.logo = SimpleUploadedFile(
            "test_file.png", b"file_content", content_type="image/png"
        )
        self.video = SimpleUploadedFile(
            "test_file.mp4", b"file_content", content_type="video/mp4"
        )
        self.document = SimpleUploadedFile(
            "test_file.pdf", b"file_content", content_type="application/pdf"
        )
        self.budget = random.uniform(1.5, 1.9) * 1000
        self.website = "https://website.com/"
        self.languages = json.dumps(
            [
                to_global_id(LanguageType.__name__, i.id)
                for i in [
                    Language.objects.create(name="ENG"),
                    Language.objects.create(name="FR"),
                ]
            ]
        )

        test_category = Category.objects.create(name="test_category")
        self.category = to_global_id(CategoryType.__name__, test_category.id)
        self.tags = json.dumps(
            [
                to_global_id(TagType.__name__, i.id)
                for i in [
                    Tag.objects.create(name="tag"),
                    Tag.objects.create(name="test"),
                ]
            ]
        )

        self.crowd_project = CrowdfundingProject.objects.create(
            name="testtest",
            description="test",
            date_start="2018-01-01T20:10:00+00:00",
            date_finish="2018-01-02T20:10:00+00:00",
            budget=123.12,
            website=self.website,
            owner=self.test_user1,
            category=test_category,
            location=location,
        )

        self.tags_for_update = json.dumps(
            [
                to_global_id(TagType.__name__, i.id)
                for i in [
                    Tag.objects.create(name="tag_updated"),
                    Tag.objects.create(name="test_updated"),
                ]
            ]
        )

        self.languages_for_update = json.dumps(
            [
                to_global_id(LanguageType.__name__, i.id)
                for i in [
                    Language.objects.create(name="UK"),
                    Language.objects.create(name="RU"),
                ]
            ]
        )

        CrowdfundingProjectRating.objects.create(
            crowd_project=self.crowd_project, user=self.test_user1, rate=5
        )

        for i, k in enumerate(range(1, 25)):
            _user = User.objects.create_user(
                username="rate_user{}".format(k),
                password=self.test_password,
                email="rate_user_email{}@gmail.com".format(k),
            )
            _user.save()
            token = get_token(_user)
            _user.profile.active_token = token
            _user.profile.save()
            CrowdfundingProjectRating.objects.create(
                user=_user, crowd_project=self.crowd_project, rate=(i % 5) + 1
            )

    def tearDown(self):
        for filename in glob.glob(
            os.path.join(settings.MEDIA_ROOT, "profile_pictures/test_file*")
        ):
            os.remove(filename)
        for filename in glob.glob(
            os.path.join(settings.MEDIA_ROOT, "logos/test_file*")
        ):
            os.remove(filename)
        for filename in glob.glob(
            os.path.join(settings.MEDIA_ROOT, "videos/test_file*")
        ):
            os.remove(filename)
        for filename in glob.glob(
            os.path.join(settings.MEDIA_ROOT, "profile_pictures/test_file*")
        ):
            os.remove(filename)

    def test_create_crowdfunding_project(self):
        data = {
            "query": """ mutation {
                createCrowdProject(name: "%s", description: "%s", dateStart: "%s",
                    dateFinish: "%s", website: "%s", languages: %s, video:{file: "%s", name: "%s"},
                    document:{file: "%s", name: "%s" }, picture:{file: "%s", name: "%s"}, tags: %s, budget: %s,
                    owner: "%s", category: "%s", location: "%s") {
                    crowdProject {
                        id
                        name
                        description
                        dateStart
                        dateFinish
                        website
                        languages {
                        edges {
                            node {
                                id
                                name
                                }
                            }
                        }
                        tags {
                        edges {
                            node {
                                id
                                name
                                }
                            }
                        }
                        budget
                        owner {
                            username
                            email
                        }
                        category {
                            name
                        }
                        location {
                            name
                        }
                    }
                  }
               }
                            """
            % (
                self.project_name,
                self.description,
                self.date_start,
                self.date_finish,
                self.website,
                self.languages,
                self.video.file,
                self.video.name,
                self.document.file,
                self.document.name,
                self.logo.file,
                self.logo.name,
                self.tags,
                self.budget,
                self.user_glob_id,
                self.category,
                self.location,
            )
        }
        token = self.test_user1.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(
            self.description,
            response.json()["data"]["createCrowdProject"]["crowdProject"][
                "description"
            ],
        )
        self.assertEqual(
            self.project_name,
            response.json()["data"]["createCrowdProject"]["crowdProject"]["name"],
        )

    def test_create_crowdfunding_project_without_authorization(self):
        data = {
            "query": """ mutation {
                createCrowdProject(name: "%s", description: "%s", dateStart: "%s",
                   dateFinish: "%s", website: "%s", languages: %s, video:{file: "%s", name: "%s"},
                   document:{file: "%s", name: "%s" }, picture:{file: "%s", name: "%s"}, tags: %s, budget: %s,
                   owner: "%s", category: "%s", location: "%s") {
                   crowdProject {
                       id
                       name
                       description
                   }
                 }
              }
                        """
            % (
                self.project_name,
                self.description,
                self.date_start,
                self.date_finish,
                self.website,
                self.languages,
                self.video.file,
                self.video.name,
                self.document.file,
                self.document.name,
                self.logo.file,
                self.logo.name,
                self.tags,
                self.budget,
                self.user_glob_id,
                self.category,
                self.location,
            )
        }
        response = self.client.post(reverse("api"), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertIn("errors", response.json())
        self.assertEqual(
            response.json()["errors"][0]["message"],
            "You do not have permission to perform this action",
        )

    def test_create_crowdfunding_project_with_wrong_language_id(self):
        wrong_language_global_id = "x" * 10
        data = {
            "query": """ mutation {
               createCrowdProject(name: "%s", description: "%s", dateStart: "%s",
                   dateFinish: "%s", website: "%s", languages: "%s", video:{file: "%s", name: "%s"},
                   document:{file: "%s", name: "%s" }, picture:{file: "%s", name: "%s"}, tags:
                   %s, budget: %s, owner: "%s",category: "%s", location: "%s") {
                   crowdProject {
                       id
                       name
                       description
                   }
                 }
              }
                                """
            % (
                self.project_name,
                self.description,
                self.date_start,
                self.date_finish,
                self.website,
                wrong_language_global_id,
                self.video.file,
                self.video.name,
                self.document.file,
                self.document.name,
                self.logo.file,
                self.logo.name,
                self.tags,
                self.budget,
                self.user_glob_id,
                self.category,
                self.location,
            )
        }
        token = self.test_user1.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertEqual(response.status_code, 200)
        self.assertIn("errors", response.json())

    def test_create_crowdfunding_project_with_wrong_location_id(self):
        wrong_location_global_id = "x" * 10
        data = {
            "query": """ mutation {
                createCrowdProject(name: "%s", description: "%s", dateStart: "%s",
                  dateFinish: "%s", website: "%s", languages: %s, video:{file: "%s", name: "%s"},
                  document:{file: "%s", name: "%s" }, picture:{file: "%s", name: "%s"}, tags:
                  %s, budget: %s, owner: "%s",category: "%s", location: "%s") {
                  crowdProject {
                      id
                      name
                      description
                  }
                }
             }
                       """
            % (
                self.project_name,
                self.description,
                self.date_start,
                self.date_finish,
                self.website,
                self.languages,
                self.video.file,
                self.video.name,
                self.document.file,
                self.document.name,
                self.logo.file,
                self.logo.name,
                self.tags,
                self.budget,
                self.user_glob_id,
                self.category,
                wrong_location_global_id,
            )
        }
        token = self.test_user1.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertEqual(response.status_code, 200)
        self.assertIn("errors", response.json())

    def test_get_crowdfunding_projects(self):
        data = {
            "query": """ query {
                  allCrowdProjects {
                    edges {
                      node {
                        id
                        website
                        name
                        description
                      }
                    }
                  }
                }
          """
        }
        response = self.client.post(reverse("api"), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())

    def test_get_crowdfunding_project_by_id(self):
        crowd_project_glob_id = to_global_id(
            CrowdProjectType.__name__, self.crowd_project.id
        )
        data = {
            "query": """ query {
                  crowdProject(id: "%s") {
                    id
                    website
                    name
                    description
                    rating
                  }
                }
              """
            % (crowd_project_glob_id,)
        }

        response = self.client.post(reverse("api"), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("errors", response.json())
        self.assertEqual(
            crowd_project_glob_id, response.json()["data"]["crowdProject"]["id"]
        )

    def test_get_crowdfunding_project_with_wrong_id(self):
        crowd_project_wrong_id = "x" * 15
        data = {
            "query": """ query {
                  crowdProject(id: "%s") {
                    id
                    website
                    name
                    description
                  }
                }
                """
            % (crowd_project_wrong_id,)
        }

        response = self.client.post(reverse("api"), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(response.json()["data"]["crowdProject"])

    def test_update_crowdfunding_project(self):
        crowd_project_glob_id = to_global_id(
            CrowdProjectType.__name__, self.crowd_project.id
        )
        data = {
            "query": """ mutation {
                updateCrowdProject(crowdProjectGlobId: "%s" ,name: "%s", description: "%s", dateStart: "%s",
                    dateFinish: "%s", website: "%s", languages: %s, video:{file: "%s", name: "%s"},
                    document:{file: "%s", name: "%s" }, picture:{file: "%s", name: "%s"}, tags: %s, budget: %s,
                    owner: "%s", category: "%s", location: "%s", status: "ar_vis") {
                    crowdProject {
                        id
                        name
                        description
                        status
                        category {
                            name
                        }
                        location {
                            name
                        }
                        languages {
                        edges {
                            node {
                                id
                                name
                                }
                            }
                        }
                    }
                  }
               }
                         """
            % (
                crowd_project_glob_id,
                self.project_name,
                self.description,
                self.date_start,
                self.date_finish,
                self.website,
                self.languages_for_update,
                self.video.file,
                self.video.name,
                self.document.file,
                self.document.name,
                self.logo.file,
                self.logo.name,
                self.tags_for_update,
                self.budget,
                self.user_glob_id,
                self.category,
                self.location,
            )
        }
        token = self.test_user1.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(
            self.description,
            response.json()["data"]["updateCrowdProject"]["crowdProject"][
                "description"
            ],
        )
        self.assertEqual(
            self.project_name,
            response.json()["data"]["updateCrowdProject"]["crowdProject"]["name"],
        )

    def test_delete_crowdfunding_project(self):
        crowd_project_glob_id = to_global_id(
            CrowdProjectType.__name__, self.crowd_project.id
        )
        data = {
            "query": """ mutation {
                            deleteCrowdProject (crowdProjectGlobId: "%s") {
                                deleted
                                }
                            }
                            """
            % (crowd_project_glob_id)
        }
        token = self.test_user1.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)
        self.assertFalse(
            CrowdfundingProject.objects.filter(pk=self.crowd_project.id).exists()
        )

    def test_get_crowd_project_rating(self):
        crowd_project_glob_id = to_global_id(
            CrowdProjectType.__name__, self.crowd_project.id
        )
        data = {
            "query": """ query {
                  crowdProjectRating (crowdProjectGlobId: "%s")
                      }
                      """
            % (crowd_project_glob_id)
        }
        response = self.client.post(reverse("api"), data=data)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["data"]["crowdProjectRating"]["avg"],
            float(self.crowd_project.rating.all().aggregate(Avg("rate"))["rate__avg"]),
        )

    def test_rate_crowd_project(self):
        crowd_project_glob_id = to_global_id(
            CrowdProjectType.__name__, self.crowd_project.id
        )
        token = self.test_user1.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        data = {
            "query": """ mutation {
                   rateCrowdProject (crowdProjectGlobId: "%s", rate: 1) {
                          crowdProjectRating
                       }
                   }
                   """
            % (crowd_project_glob_id)
        }
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertNotIn("errors", response.json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json()["data"]["rateCrowdProject"]["crowdProjectRating"]["avg"],
            float(self.crowd_project.rating.all().aggregate(Avg("rate"))["rate__avg"]),
        )

    def test_rate_crowd_project_project_without_authorization(self):
        crowd_project_glob_id = to_global_id(
            CrowdProjectType.__name__, self.crowd_project.id
        )
        data = {
            "query": """ mutation {
                   rateCrowdProject (crowdProjectGlobId: "%s", rate: 1) {
                          crowdProjectRating
                       }
                   }
                   """
            % (crowd_project_glob_id)
        }
        response = self.client.post(reverse("api"), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertIn("errors", response.json())
        self.assertEqual(
            response.json()["errors"][0]["message"],
            "You do not have permission to perform this action",
        )
        self.assertIsNone(response.json()["data"]["rateCrowdProject"])

    def test_rate_crowd_project_project_with_wrong_id(self):
        crowd_project_glob_id = "x" * 20
        token = self.test_user1.profile.active_token
        headers = {"HTTP_AUTHORIZATION": f"JWT {token}"}
        data = {
            "query": """ mutation {
                   rateCrowdProject (crowdProjectGlobId: "%s", rate: 1) {
                          crowdProjectRating
                       }
                   }
                   """
            % (crowd_project_glob_id)
        }
        response = self.client.post(reverse("api"), data=data, **headers)
        self.assertIn("errors", response.json())
        self.assertIsNone(response.json()["data"]["rateCrowdProject"])
