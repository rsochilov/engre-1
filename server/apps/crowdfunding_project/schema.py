from collections import Counter

import graphene
from accounts.models import User
from django.core.files.base import ContentFile
from django.db.models import Avg
from graphene.types.generic import GenericScalar
from graphene_django import DjangoConnectionField
from graphene_django.types import DjangoObjectType

from graphql_jwt.decorators import login_required
from core.inputs import FileInput
from core.models import Category, Language, Tag
from core.schema import CategoryType, UserType
from core.utils import retrieve_list, retrieve_object
from crowdfunding_project.models import CrowdfundingProject, CrowdfundingProjectRating
from market_location.models import City
from market_location.schema import CityType


class CrowdProjectType(DjangoObjectType):
    owner = graphene.Field(UserType)
    category = graphene.Field(CategoryType)
    location = graphene.Field(CityType)
    rating = GenericScalar()

    class Meta:
        model = CrowdfundingProject
        interfaces = (graphene.relay.Node,)

    def resolve_rating(self, *args, **kwargs):
        stat = Counter(self.rating.values_list("rate", flat=True))
        stat["avg"] = float(self.rating.all().aggregate(Avg("rate"))["rate__avg"])
        return stat


class CreateCrowdProjectMutation(graphene.Mutation):
    crowd_project = graphene.Field(CrowdProjectType)

    class Arguments:
        name = graphene.String(required=True)
        description = graphene.String(required=True)
        budget = graphene.Float(required=True)
        date_start = graphene.DateTime(required=True)
        date_finish = graphene.DateTime(required=True)
        website = graphene.String(required=True)

        owner = graphene.String()
        category = graphene.String(required=True)
        location = graphene.String(required=True)

        status = graphene.String(required=False, default_value="active")

        languages = graphene.List(required=True, of_type=graphene.String)
        tags = graphene.List(required=True, of_type=graphene.String)

        picture = FileInput(required=True)
        video = FileInput(required=True)
        document = FileInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        languages = retrieve_list(kwargs.pop("languages"))
        tags = retrieve_list(kwargs.pop("tags"))

        picture_data = kwargs.pop("picture")
        video_data = kwargs.pop("video")
        document_data = kwargs.pop("document")

        picture = ContentFile(picture_data["file"], picture_data["name"])
        video = ContentFile(video_data["file"], video_data["name"])
        document = ContentFile(document_data["file"], document_data["name"])

        kwargs["owner"] = (
            retrieve_object(kwargs.pop("owner"), User)
            if kwargs.get("owner")
            else info.context.user
        )
        kwargs["category"] = retrieve_object(kwargs.pop("category"), Category)
        kwargs["location"] = retrieve_object(kwargs.pop("location"), City)

        crowd_project = CrowdfundingProject.objects.create(**kwargs)
        crowd_project.languages.set(languages)
        crowd_project.tags.set(tags)
        crowd_project.picture.save(picture.name, picture)
        crowd_project.video.save(video.name, video)
        crowd_project.document.save(document.name, document)

        crowd_project.save()

        return CreateCrowdProjectMutation(crowd_project=crowd_project)


class UpdateCrowdProjectMutation(graphene.Mutation):
    crowd_project = graphene.Field(CrowdProjectType)

    class Arguments:
        crowdProjectGlobId = graphene.String(required=True)

        name = graphene.String(required=True)
        description = graphene.String(required=True)
        budget = graphene.Float(required=True)
        date_start = graphene.DateTime(required=True)
        date_finish = graphene.DateTime(required=True)
        website = graphene.String(required=True)

        owner = graphene.String(required=True)
        category = graphene.String(required=True)
        location = graphene.String(required=True)

        status = graphene.String(required=True)

        languages = graphene.List(required=True, of_type=graphene.String)
        tags = graphene.List(required=True, of_type=graphene.String)

        picture = FileInput(required=True)
        video = FileInput(required=True)
        document = FileInput(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        crowd_proj = retrieve_object(
            kwargs.pop("crowdProjectGlobId"), CrowdfundingProject
        )

        kwargs["owner"] = (
            retrieve_object(kwargs.pop("owner"), User)
            if kwargs.get("owner")
            else info.context.user
        )

        if kwargs.get("category"):
            kwargs["category"] = retrieve_object(kwargs.pop("category"), Category)

        if kwargs.get("location"):
            kwargs["location"] = retrieve_object(kwargs.pop("location"), City)

        if kwargs.get("languages", None):
            languages_ids = retrieve_list(kwargs.pop("languages"))
            crowd_proj.languages.clear()
            crowd_proj.languages.set(Language.objects.filter(pk__in=languages_ids))

        if kwargs.get("tags", None):
            tags_ids = retrieve_list(kwargs.pop("tags"))
            crowd_proj.tags.clear()
            crowd_proj.tags.set(Tag.objects.filter(pk__in=tags_ids))

        if kwargs.get("picture", None):
            picture_data = kwargs.pop("picture")
            picture = ContentFile(picture_data["file"], picture_data["name"])
            crowd_proj.picture.save(picture.name, picture)

        if kwargs.get("video", None):
            video_data = kwargs.pop("video")
            video = ContentFile(video_data["file"], video_data["name"])
            crowd_proj.video.save(video.name, video)

        if kwargs.get("document", None):
            document_data = kwargs.pop("document")
            document = ContentFile(document_data["file"], document_data["name"])
            crowd_proj.document.save(document.name, document)

        for attr in kwargs.keys():
            crowd_proj.__setattr__(attr, kwargs.get(attr))

        crowd_proj.save()
        return UpdateCrowdProjectMutation(crowd_project=crowd_proj)


class DeleteCrowdProjectMutation(graphene.Mutation):
    crowd_project = graphene.Field(CrowdProjectType)
    deleted = graphene.Boolean()

    class Arguments:
        crowdProjectGlobId = graphene.String(required=True)

    @login_required
    def mutate(self, info, **kwargs):
        crowd_proj = retrieve_object(
            kwargs.pop("crowdProjectGlobId"), CrowdfundingProject
        )
        crowd_proj.delete()
        return DeleteCrowdProjectMutation(deleted=True)


class RateCrowdProjectMutation(graphene.Mutation):
    crowd_project = graphene.Field(CrowdProjectType)
    crowdProjectRating = GenericScalar()

    class Arguments:
        crowdProjectGlobId = graphene.String(required=True)
        userGlobId = graphene.String()
        rate = graphene.Int(required=True)

    @staticmethod
    def resolve_crowd_project_rating(info, *args, **kwargs):
        crowd_proj = retrieve_object(
            kwargs.pop("crowdProjectGlobId"), CrowdfundingProject
        )
        stat = Counter(crowd_proj.rating.values_list("rate", flat=True))
        stat["avg"] = float(crowd_proj.rating.all().aggregate(Avg("rate"))["rate__avg"])
        return stat

    @login_required
    def mutate(self, info, **kwargs):
        _crowd_proj = retrieve_object(
            kwargs.get("crowdProjectGlobId"), CrowdfundingProject
        )
        _user = (
            retrieve_object(kwargs.get("userGlobId"), User)
            if kwargs.get("userGlobId")
            else info.context.user
        )
        _rate = kwargs.get("rate")
        rating, created = CrowdfundingProjectRating.objects.get_or_create(
            crowd_project=_crowd_proj, user=_user
        )
        rating.rate = _rate
        rating.save()
        return RateCrowdProjectMutation(
            crowdProjectRating=RateCrowdProjectMutation.resolve_crowd_project_rating(
                info, **kwargs
            )
        )


class Query(graphene.ObjectType):
    @staticmethod
    def resolve_all_crowd_project(info, **kwargs):
        return CrowdfundingProject.objects.all()

    all_crowd_projects = DjangoConnectionField(
        CrowdProjectType, resolver=resolve_all_crowd_project
    )
    crowd_project = graphene.relay.Node.Field(CrowdProjectType)
    crowd_project_rating = GenericScalar(
        crowdProjectGlobId=graphene.String(),
        resolver=RateCrowdProjectMutation.resolve_crowd_project_rating,
    )


class Mutations(graphene.ObjectType):
    create_crowd_project = CreateCrowdProjectMutation.Field()
    update_crowd_project = UpdateCrowdProjectMutation.Field()
    delete_crowd_project = DeleteCrowdProjectMutation.Field()
    rate_crowd_project = RateCrowdProjectMutation.Field()
