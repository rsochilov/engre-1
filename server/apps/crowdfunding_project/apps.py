from django.apps import AppConfig


class CrowdfundingProjectConfig(AppConfig):
    name = "crowdfunding_project"
