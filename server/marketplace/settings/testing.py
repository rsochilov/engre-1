from .dev import *

TESTING = True

TEMPLATE_DEBUG = False
CELERY_ALWAYS_EAGER = True
CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
BROKER_BACKEND = "memory"

ALLOWED_HOSTS = ["*"]
INTERNAL_IPS = ["127.0.0.1"]

PASSWORD_HASHERS = ("django.contrib.auth.hashers.MD5PasswordHasher",)

MIDDLEWARE.remove("django.middleware.locale.LocaleMiddleware")

SITEMAP_FILE = os.path.join(STATIC_ROOT, "sitemap_tests.xml")

SITE_URL = "localhost:8000"
