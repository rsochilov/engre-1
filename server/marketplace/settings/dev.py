from .base import *

DEBUG = True

HOSTS = [
    "django",
    "localhost",
    "ec2-3-16-99-137.us-east-2.compute.amazonaws.com",
    "3.16.99.137",
    "engre.co",
]
ALLOWED_HOSTS = HOSTS

INSTALLED_APPS += ("django_extensions",)

TESTING_MEDIA_ROOT = os.path.join(BASE_DIR, "testing_media")

SITEMAP_FILE = os.path.join(STATIC_ROOT, "sitemap_bak.xml")

SITE_URL = "localhost:8000"

EMAIL_TASK_DELAY = 15  # secs

EMAIL_VERIFIED_NOTIFICATOR_SLEEP_TIME = 15
ADMIN_EMAIL = "admin@admin.com"
