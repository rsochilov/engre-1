from .base import *

DEBUG = False
ALLOWED_HOSTS = ["engre.co", "test.engre.co"]
# MEDIA_ROOT = '/media'
SASS_OUTPUT_STYLE = "compressed"

CORS_ORIGIN_WHITELIST = ("engre.co", "ec2-3-16-99-137.us-east-2.compute.amazonaws.com")

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [
                (
                    os.environ.get("CHANNELS_HOST", "localhost"),
                    os.environ.get("CHANNELS_PORT", 6379),
                )
            ],
        },
    },
}

FILE_UPLOAD_PERMISSIONS = 0o644

SOCIAL_AUTH_REDIRECT_IS_HTTPS = True
SOCIAL_AUTH_FACEBOOK_API_VERSION = "3.2"

APPEND_SLASH = True

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

SITE_URL = "test.engre.co"
SITE_ID = 1
