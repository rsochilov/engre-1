import os
import sys
from datetime import timedelta

from django.utils.translation import gettext_lazy as _

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

sys.path.insert(0, os.path.join(BASE_DIR, "apps"))

SECRET_KEY = "u)uu_12)^qvyomvofthzri4f5hubng!8_k0u5oe*q=!q&_@s+n"

DEBUG = False

ALLOWED_HOSTS = [
    "0.0.0.0",
    "127.0.0.1",
    "3.16.99.137",
    "engre.co",
    "ec2-3-16-99-137.us-east-2.compute.amazonaws.com",
]

CORS_ORIGIN_WHITELIST = ("localhost:8080", "localhost:8000")

SITE_ID = 1

# Application definition

INSTALLED_APPS = [
    "djangocms_admin_style",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.sites",
    "django.contrib.postgres",
    "django.contrib.staticfiles",
    # local
    "accounts",
    "market_location",
    "company",
    "project",
    "blog",
    "news",
    "chat",
    "feedback",
    "core",
    "text",
    "education",
    "landing",
    "careers",
    "about",
    "user_settings",
    "partnership",
    "admin_dashboard",
    "forcustomers",
    "seo_tags",
    "chat_api",
    "accounts_api",
    "projects_api",
    "notifications_api",
    "user_filter_api",
    # third-party
    "django_celery_beat",
    "django_celery_results",
    "timezone_field",
    "corsheaders",
    "channels",
    "cities_light",
    "django_filters",
    "social_django",
    "storages",
    "cms",
    "menus",
    "sekizai",
    "treebeard",
    "djangocms_text_ckeditor",
    "filer",
    "easy_thumbnails",
    "djangocms_column",
    "djangocms_file",
    "djangocms_link",
    "djangocms_picture",
    "djangocms_style",
    "djangocms_snippet",
    "djangocms_googlemap",
    "djangocms_video",
    "sass_processor",
    "rest_framework",
    "django_social_share",
    "solo",
    "rangefilter",
]

MIDDLEWARE = [
    "cms.middleware.utils.ApphookReloadMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "marketplace.middleware.RestAuthMiddleware",
    "cms.middleware.user.CurrentUserMiddleware",
    "cms.middleware.page.CurrentPageMiddleware",
    "cms.middleware.toolbar.ToolbarMiddleware",
    "cms.middleware.language.LanguageCookieMiddleware",
    "social_django.middleware.SocialAuthExceptionMiddleware",
    "accounts.middleware.active_user_middleware",
    "core.middleware.IsRequestMobile",
    "core.middleware.XRobotsMiddleware",
]

AUTHENTICATION_BACKENDS = [
    # social auth
    "social_core.backends.google.GoogleOAuth2",
    "social_core.backends.facebook.FacebookOAuth2",
    "social_core.backends.linkedin.LinkedinOAuth2",
    "django.contrib.auth.backends.ModelBackend",
]

ROOT_URLCONF = "marketplace.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.csrf",
                "sekizai.context_processors.sekizai",
                "django.template.context_processors.static",
                "cms.context_processors.cms_settings",
                "social_django.context_processors.backends",
                "social_django.context_processors.login_redirect",
            ],
        },
    },
]

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [
                (
                    os.environ.get("CHANNELS_HOST", "localhost"),
                    os.environ.get("CHANNELS_PORT", 6379),
                )
            ],
        },
    },
}

ASGI_APPLICATION = "marketplace.routing.application"
WSGI_APPLICATION = "marketplace.wsgi.application"

# Database

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.environ.get("DATABASE_NAME", "marketplace"),
        "USER": os.environ.get("DATABASE_USER", "marketplace"),
        "PASSWORD": os.environ.get("DATABASE_PASSWORD", "~@dhjkUhgdge826dgFRFtt26"),
        "HOST": os.environ.get("DATABASE_HOST", "localhost"),
        "PORT": os.environ.get("DATABASE_PORT", "5432"),
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"

# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",},
]

# Internationalization

LANGUAGE_CODE = "en"

LANGUAGES = [("en", _("English")), ("de", _("German")), ("fr", _("French"))]

LOCALE_PATHS = (os.path.join(BASE_DIR, "locale"),)

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

CITIES_LIGHT_TRANSLATION_LANGUAGES = ["en", "fr", "de"]
CITIES_LIGHT_INCLUDE_CITY_TYPES = [
    "PPL",
    "PPLA",
    "PPLA2",
    "PPLA3",
    "PPLA4",
    "PPLC",
    "PPLF",
    "PPLG",
    "PPLL",
    "PPLR",
    "PPLS",
    "STLMT",
]
CITIES_LIGHT_APP_NAME = "market_location"

# Static files (CSS, JavaScript, Images)
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "sass_processor.finders.CssFinder",
]

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")

SITEMAP_FILE = os.path.join(STATIC_ROOT, "sitemap.xml")

STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"),)
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"

SASS_PROCESSOR_INCLUDE_DIRS = [
    os.path.join(BASE_DIR, "static", "scss"),
    os.path.join(STATIC_ROOT, "scss"),
    os.path.join(BASE_DIR, "..", "node_modules"),
]

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"
DEFAULT_FILE_STORAGE = "django.core.files.storage.FileSystemStorage"

# Thumbnail

THUMBNAIL_HIGH_RESOLUTION = True

THUMBNAIL_PROCESSORS = (
    "easy_thumbnails.processors.colorspace",
    "easy_thumbnails.processors.autocrop",
    "filer.thumbnail_processors.scale_and_crop_with_subject_location",
    "easy_thumbnails.processors.filters",
)

# CELERY

CELERY_BROKER_URL = os.getenv("CELERY_BROKER_URL", "redis://localhost:6379")
CELERY_RESULT_BACKEND = os.getenv("CELERY_RESULT_BACKEND", "redis://localhost:6379")
CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
CELERY_TIMEZONE = TIME_ZONE

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework_jwt.authentication.JSONWebTokenAuthentication",
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.BasicAuthentication",
    ],
    "DEFAULT_PERMISSION_CLASSES": [],
    "DEFAULT_FILTER_BACKENDS": ("django_filters.rest_framework.DjangoFilterBackend",),
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "PAGE_SIZE": 25,
}

# Google
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = (
    "830901559562-3nnkpbnkk09apq2hk6e58rnvm2060be1.apps.googleusercontent.com"
)
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.getenv("GOOGLE_SECRET")

# Facebook

SOCIAL_AUTH_FACEBOOK_KEY = "663530514220622"
SOCIAL_AUTH_FACEBOOK_SECRET = os.getenv("FACEBOOK_SECRET")
SOCIAL_AUTH_FACEBOOK_SCOPE = ["email"]
SOCIAL_AUTH_FACEBOOK_API_VERSION = "3.2"
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    "locale": "ru_RU",
    "fields": "id, name, email",
}
SOCIAL_AUTH_FACEBOOK_API_VERSION = "3.2"

# LinkedIn

SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = "77fekzhtomgc7w"
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = os.getenv("LINKEDIN_SECRET")
SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE = ["r_liteprofile", "r_emailaddress"]
SOCIAL_AUTH_LINKEDIN_OAUTH2_FIELD_SELECTORS = ["emailAddress"]

EDUCATION_LEARN_MORE_EMAIL = "no-reply.support@engre.co"
PARTNERSHIP_EMAIL = "no-reply.support@engre.co"

PHONE = "+16034848689"
SUPPORT_EMAIL = "support@engre.co"
PHONE_SEND_SUBJECT = "Call me back, please!"

AUTH_USER_MODEL = "accounts.User"
SOCIAL_AUTH_USER_MODEL = "accounts.User"
SOCIAL_AUTH_POSTGRES_JSONFIELD = True
SOCIAL_AUTH_FIELDS_STORED_IN_SESSION = ["is_blogger"]

SOCIAL_AUTH_PIPELINE = (
    "social_core.pipeline.social_auth.social_details",
    "social_core.pipeline.social_auth.social_uid",
    "social_core.pipeline.social_auth.social_user",
    "social_core.pipeline.user.get_username",
    "social_core.pipeline.user.create_user",
    "accounts.pipeline.get_name",
    "accounts.pipeline.blogger_create",
    "accounts.pipeline.activate_user",
    "social_core.pipeline.social_auth.associate_user",
    "social_core.pipeline.social_auth.load_extra_data",
    "social_core.pipeline.user.user_details",
    "social_core.pipeline.social_auth.associate_by_email",
)

CAREERS_APPLY_EMAIL = "no-reply.support@engre.co"

# CMS

CMS_LANGUAGES = {
    1: [
        {
            "redirect_on_fallback": True,
            "name": _("en"),
            "code": "en",
            "public": True,
            "hide_untranslated": False,
        },
        {
            "code": "de",
            "name": _("Deutsch"),
            "fallbacks": ["en", "fr"],
            "public": True,
        },
        {"code": "fr", "name": _("French"), "public": False,},
    ],
    "default": {
        "fallbacks": ["en", "de", "fr"],
        "hide_untranslated": False,
        "redirect_on_fallback": True,
        "public": True,
    },
}

CMS_TEMPLATES = (
    ("base.html", "Base"),
    ("landing.html", "Landing"),
    ("for_customers.html", "For a Customers"),
    ("about/base.html", "About base"),
    ("about/about-us.html", "About us"),
    ("about/people.html", "About people"),
    ("about/reviews.html", "About Review"),
    ("about/people.html", "About people"),
    ("about/achievements.html", "About achievements"),
    ("about/common-projects.html", "About common projects"),
    ("index.html", "Index"),
    ("education/base.html", "Education base"),
    ("education/undergraduates.html", "Education undergraduates"),
    ("education/graduates.html", "Education graduates"),
    ("education/professional-development.html", "Education professional development"),
    ("careers/description.html", "Careers base"),
    ("partnership/base.html", "Parntership base"),
    ("partnership/consulting.html", "Parntership consulting"),
    ("partnership/business_projects.html", "Parntership business projects"),
    ("partnership/commercial.html", "Partnership commercial"),
    ("partnership/software.html", "Parntership software"),
    ("partnership/software_detail.html", "Parntership software detail"),
    ("partnership/coworking.html", "Partnership Coworking"),
    ("partnership/investors.html", "Parntership for investor"),
    ("settings/base.html", "Settings base"),
    ("settings/profile.html", "Settings profile"),
    ("settings/company.html", "Settings company"),
    ("settings/project.html", "Settings project"),
    ("settings/service.html", "Settings service"),
    ("settings/chat.html", "Settings chat"),
    ("settings/document.html", "Settings documents"),
    ("admin_dashboard/base.html", "Admin base"),
    ("admin_dashboard/dashboard.html", "Admin dashboard"),
)

CMS_PERMISSION = True
CMS_PLACEHOLDER_CONF = {}

# Gulp
GULP_CWD = os.path.dirname(BASE_DIR)

# JWT
JWT_AUTH = {"JWT_EXPIRATION_DELTA": timedelta(days=1)}

LOGIN_URL = "/sign-in/"
LOGOUT_REDIRECT_URL = "/"
SOCIAL_AUTH_LOGIN_REDIRECT_URL = "/"

# EMAIL Settings

# SendGrid
SENDGRID_API_KEY = os.getenv("SENDGRID_API_KEY")
EMAIL_HOST = os.getenv("EMAIL_HOST")
EMAIL_HOST_USER = os.getenv("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = os.getenv("EMAIL_HOST_PASSWORD")

DEFAULT_FROM_EMAIL = os.getenv("DEFAULT_FROM_EMAIL", "no-reply.support@engre.co")
EMAIL_USE_TLS = os.getenv("EMAIL_USE_TLS", True)
EMAIL_PORT = os.getenv("EMAIL_PORT", 587)

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        "OPTIONS": {"min_length": 4,},
    },
]
INVITATION_TOKEN_EXPIRATION_DAYS = 7

# Google
GOOGLE_CREDENTIALS_PATH = os.path.join(os.path.dirname(BASE_DIR), "credentials.json")
GOOGLE_TOKEN_PATH = os.path.join(os.path.dirname(BASE_DIR), "token.pickle")

# Logging
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "file": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": os.path.join(os.path.dirname(BASE_DIR), "debug.log"),
        },
        "console": {"level": "INFO", "class": "logging.StreamHandler",},
    },
    "loggers": {
        "django": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": True,
        },
    },
}

SITE_URL = f"http://{os.getenv('HOST', 'localhost:8000')}"

# Cache

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": f"redis://{os.getenv('REDIS_HOST')}:6379/1",
        "OPTIONS": {"CLIENT_CLASS": "django_redis.client.DefaultClient"},
    }
}

# Number of seconds of inactivity before a user is marked offline
USER_ONLINE_TIMEOUT = 300

# Number of seconds that we will keep track of inactive users for before
# their last seen is removed from the cache
USER_LASTSEEN_TIMEOUT = 60 * 60 * 24 * 7

ADMIN_EMAIL = "support@engre.co"
GREETING_MESSAGE = "Welcome to the chat. In a chat it is forbidden to exchange contacts before the start of the project. Any attempt will be punishable."

# upload settings (set in nginx)
DATA_UPLOAD_MAX_MEMORY_SIZE = None

XROBOTSTAGS = ["all"]

SITEMAP_FILE = os.path.join(STATIC_ROOT, "sitemap.xml")
EMAIL_TASK_DELAY = 1200  # secs
EMAIL_VERIFIED_NOTIFICATOR_SLEEP_TIME = 300  # secs
EMAIL_NOT_VERIFIED_NOTIFICATOR_SLEEP_TIME = 86400  # secs
