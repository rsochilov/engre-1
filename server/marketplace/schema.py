import accounts.mutations
import accounts.schema
import blog.mutations
import blog.schema
import careers.schema
import chat.schema
import company.mutations
import company.schema
import core.password_reset_schema
import core.schema

# import crowdfunding_project.schema
import education.schema
import feedback.schema
import graphene
import landing.schema
import market_location.schema
import news.mutations
import news.schema
import project.mutations
import project.schema
import text.schema

# import investors.schema


class Query(
    chat.schema.Query,
    core.schema.Query,
    market_location.schema.Query,
    company.schema.Query,
    blog.schema.Query,
    news.schema.Query,
    project.schema.Query,
    feedback.schema.Query,
    accounts.schema.Query,
    text.schema.Query,
    careers.schema.Query,
    landing.schema.Query,
    graphene.ObjectType,
):
    pass


class Mutation(
    core.schema.Mutation,
    company.mutations.Mutation,
    blog.mutations.Mutation,
    news.mutations.Mutation,
    project.mutations.Mutation,
    feedback.schema.Mutation,
    text.schema.Mutation,
    education.schema.Mutation,
    accounts.mutations.Mutation,
    core.password_reset_schema.PasswordResetMutations,
    graphene.ObjectType,
):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
