from __future__ import absolute_import, unicode_literals

import os

import dotenv
from celery import Celery
from celery.signals import worker_ready

dotenv.read_dotenv(
    os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
        "config/.env",
    )
)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "marketplace.settings.base")

app = Celery("marketplace")

app.config_from_object("django.conf:settings", namespace="CELERY")

app.autodiscover_tasks()


@worker_ready.connect
def email_verified_users_by_project(sender, **kwarg):
    with sender.app.connection() as conn:
        sender.app.send_task(
            "user_filter_api.tasks.email_verified_users_by_project", connection=conn
        )
        sender.app.send_task(
            "user_filter_api.tasks.email_not_verified_users_by_project", connection=conn
        )
