class ParsingError:
    title: str
    exception: Exception

    def __init__(self, title: str, exception: Exception):
        self.title = title
        self.exception = exception

    def __str__(self):
        return f"""Parsing error:
    Type: {type(self.exception)}
    Info: {self.title}Base exception: {self.exception}
Traceback:
    file_name: {self.exception.__traceback__.tb_frame.f_code.co_filename}
    line: {self.exception.__traceback__.tb_lineno}"""
