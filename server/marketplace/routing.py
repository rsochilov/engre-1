from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter

import chat.routing
from notifications_api.routing import websocket_urlpatterns as notifications_routing

application = ProtocolTypeRouter(
    {
        "websocket": AuthMiddlewareStack(
            URLRouter(chat.routing.websocket_urlpatterns + notifications_routing)
        ),
    }
)
