#!/bin/sh
MANAGEPY_PATH="/var/www/marketplace/server/manage.py"
printf "Running load_about_achievements\n" &&
python3 $MANAGEPY_PATH load_about_achievements &&
printf "Running load_about_commonprojects\n" &&
python3 $MANAGEPY_PATH load_about_commonprojects &&
printf "Running load_about_persons\n" &&
python3 $MANAGEPY_PATH load_about_persons &&
printf "Running load_blog_articles\n" &&
python3 $MANAGEPY_PATH load_blog_articles &&
printf "Running load_contractors\n" &&
python3 $MANAGEPY_PATH load_contractors &&
printf "Running load_news_articles\n" &&
python3 $MANAGEPY_PATH load_news_articles &&
printf "Running load_startups\n" &&
python3 $MANAGEPY_PATH load_startups &&
printf "Done!\nExiting...\n"
