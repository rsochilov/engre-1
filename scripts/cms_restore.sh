#!/bin/bash
DUMP_PATH=$1;
echo "Deleting old tables";
docker exec marketplace_db psql -h localhost -d marketplace -U marketplace -c 'drop table cms_aliaspluginmodel, cms_cmsplugin, cms_globalpagepermission, cms_globalpagepermission_sites, cms_page, cms_pagepermission, cms_page_placeholders, cms_pageuser, cms_pageusergroup, cms_placeholder, cms_placeholderreference, cms_staticplaceholder, cms_title, cms_treenode, cms_urlconfrevision, cms_usersettings cascade;';
echo "Restoring from dump";
docker exec -i  marketplace_db psql -h localhost -d marketplace -U marketplace < $DUMP_PATH;
