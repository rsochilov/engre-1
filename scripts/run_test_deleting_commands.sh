#!/bin/sh
MANAGEPY_PATH="/var/www/marketplace/server/manage.py"
#MANAGEPY_PATH="../server/manage.py"
echo "Running delete_test_services";
python3 $MANAGEPY_PATH delete_test_services;
echo "Running delete_test_projects";
python3 $MANAGEPY_PATH delete_test_projects;
echo "Running delete_test_bloggers";
python3 $MANAGEPY_PATH delete_test_bloggers;
echo "Done!";
