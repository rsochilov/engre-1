FROM python:3.6

ENV PYTHONUNBUFFERED=1
WORKDIR /app

COPY requirements.txt /app

# Install requirements
RUN pip install -r requirements.txt

# copy app source code
COPY . /app
