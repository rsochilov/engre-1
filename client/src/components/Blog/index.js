import Layout from './Layout'
import List from './List'
import AskAQuestion from './AskAQuestion'

export { List, AskAQuestion }
export const filterMatcher = {
  category__slug: 'category',
  ordering: 'ordering',
  search: 'search',
  dates: 'dates',
  author: 'author'
}
export default Layout
