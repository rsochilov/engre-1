import Layout from './Layout'
import ItemList from './ItemList'

export { ItemList }
export const filterMatcher = {
  category__slug: 'category',
  order_by: 'order',
  search: 'search',
  industry: 'industry',
  core_skills__slug: 'coreSkill',
  company__ownership__slug: 'ownership',
  rate_range_type: 'rate',
  budget: 'budget',
  company__company_size: 'companySize',
  company__flexible_working_hours: 'workingHours',
}
export default Layout
