import ReviewPM from '@/components/Settings/ReviewPM'
import { prepareComponent } from '@/utils'
import Vue from 'vue'
import bModal from 'bootstrap-vue/es/components/modal/modal'
import bModalDirective from 'bootstrap-vue/es/directives/modal/modal'

Vue.component('b-modal', bModal)
Vue.directive('b-modal', bModalDirective)

prepareComponent(ReviewPM)
