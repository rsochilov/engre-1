import Vue from 'vue'
import NewsList from '@/components/News/List'
import { prepareComponent } from '@/utils'
import { VueMasonryPlugin } from 'vue-masonry'

Vue.use(VueMasonryPlugin)

prepareComponent(NewsList)
