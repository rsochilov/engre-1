import Vue from 'vue'
import { prepareComponent } from '@/utils'
import backToTopButton from '@/components/backToTopButton'

prepareComponent(backToTopButton)
