import ProjectsList from '@/containers/Settings/ProjectsList'
import Vue from 'vue'
import bModal from 'bootstrap-vue/es/components/modal/modal'
import bModalDirective from 'bootstrap-vue/es/directives/modal/modal'
import { prepareComponent } from '@/utils'

Vue.component('b-modal', bModal)
Vue.directive('b-modal', bModalDirective)

prepareComponent(ProjectsList)
