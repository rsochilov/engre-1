import AdminContent from '@/containers/Admin/Content'
import AdminAbout from '@/components/Admin/Content/About'
import AdminBlog from '@/components/Admin/Content/Blog'
import AdminNews from '@/components/Admin/Content/News'
import AdminPartnership from '@/components/Admin/Content/Partnership'
import AdminCareers from '@/components/Admin/Content/Careers'
import { prepareComponent, getProps } from '@/utils'
import Vue from 'vue'
import VueRouter from 'vue-router'
import VuejsClipper, {
  clipperFixed,
  clipperUpload,
  clipperPreview,
  clipperBasic
} from 'vuejs-clipper'

Vue.use(VueRouter)
Vue.use(VuejsClipper)
Vue.component('ClipperFixed', clipperFixed)
Vue.component('ClipperUpload', clipperUpload)
Vue.component('ClipperPreview', clipperPreview)
Vue.component('ClipperBasic', clipperBasic)

const routes = [
  {
    path: '/admin/content/about',
    component: AdminAbout,
    name: 'about',
    props: getProps()
  },
  {
    path: '/admin/content/blog',
    component: AdminBlog,
    name: 'blog',
    props: getProps()
  },
  {
    path: '/admin/content/news',
    component: AdminNews,
    name: 'news',
    props: getProps()
  },
  {
    path: '/admin/content/partnership',
    component: AdminPartnership,
    name: 'partnership',
    props: getProps()
  },
  {
    path: '/admin/content/careers',
    component: AdminCareers,
    name: 'careers',
    props: getProps()
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active'
})

prepareComponent(AdminContent, { router })
