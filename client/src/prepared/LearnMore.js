import LearnMore from '@/components/Education/LearnMore'
import Vue from 'vue'

// prepareComponent(LearnMore)
new Vue({
  el: '#{{ el }}',
  render: h => h(LearnMore)
})
