import PartnershipModal from '@/components/Partners/PartnershipModal'
import Vue from 'vue'

new Vue({
  el: '#{{ el }}',
  render: h => h(PartnershipModal)
})
