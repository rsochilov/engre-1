import Vue from 'vue'
import VueRouter from 'vue-router'
import Blog, { List, AskAQuestion } from '@/components/Blog'
import { prepareComponent, getProps } from '@/utils'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueRouter)

const routes = [
  {
    path: '/:base/ask-a-question',
    component: AskAQuestion,
    props: getProps()
  },
  {
    path: '/:base/:category',
    component: List,
    props: getProps()
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active'
})

prepareComponent(Blog, { router })
