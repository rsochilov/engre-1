import Vue from 'vue'
import AdminSoftware from '@/components/Admin/Software'
import { prepareComponent } from '@/utils'
import VuejsClipper, {
  clipperFixed,
  clipperUpload,
  clipperPreview,
  clipperBasic
} from 'vuejs-clipper'

Vue.use(VuejsClipper)
Vue.component('ClipperFixed', clipperFixed)
Vue.component('ClipperUpload', clipperUpload)
Vue.component('ClipperPreview', clipperPreview)
Vue.component('ClipperBasic', clipperBasic)

prepareComponent(AdminSoftware)
