import Vue from 'vue'
import VueRouter from 'vue-router'
import IntellectualServices, {
  ItemList
} from '@/components/IntellectualServices'
import { prepareComponent, getProps } from '@/utils'
import { VTooltip } from 'v-tooltip'

Vue.directive('tooltip', VTooltip)
Vue.use(VueRouter)

const routes = [
  {
    path: '/:base/:category/',
    component: ItemList,
    props: getProps()
  },
  {
    path: '/:base/:category/:industry/',
    component: ItemList,
    props: getProps()
  },
  {
    path: '/:base/:category/:industry/:coreSkill/',
    component: ItemList,
    props: getProps()
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active'
})

prepareComponent(IntellectualServices, { router })
