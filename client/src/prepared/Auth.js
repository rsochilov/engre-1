import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from '@/components/Auth'
import PasswordResetForm from '@/components/Auth/PasswordResetForm'
import { prepareComponent, getProps } from '@/utils'

Vue.use(VueRouter)

const routes = [
  {
    path: '/sign-in/',
    name: 'sign-in',
    component: Auth,
    props: getProps()
  },
  {
    path: '/sign-up/',
    name: 'sign-up',
    component: Auth,
    props: getProps()
  },
  {
    path: '/sign-up-blogger/',
    name: 'sign-up-blogger',
    component: Auth,
    props: getProps()
  },
  {
    path: '/password-reset/',
    name: 'password-reset',
    component: PasswordResetForm,
    props: getProps()
  },
  {
    path: '/invite/:uid/',
    name: 'invitee_password',
    component: Auth,
    props: getProps()
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active'
})

prepareComponent(Auth, { router })
