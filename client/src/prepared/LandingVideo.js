import Vue from 'vue'
import { prepareComponent } from '@/utils'
import LandingVideo from '@/components/Landing/LandingVideo'

prepareComponent(LandingVideo)
