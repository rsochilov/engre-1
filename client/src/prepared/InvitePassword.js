import Vue from "vue";
import VueRouter from "vue-router";
import InvitePassword from '@/components/Auth/InvitePassword'
import {getProps, prepareComponent} from '@/utils'

Vue.use(VueRouter)

const routes = [
    {
        path: '/invite/:uid/',
        name: 'invitee_password',
        component: InvitePassword,
        props: getProps()
    }
]

const router = new VueRouter({
    routes,
    mode: 'history',
    linkActiveClass: 'active'
})

prepareComponent(InvitePassword, { router })