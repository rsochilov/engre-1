import SendMessageButton from '@/components/IntellectualServices/SendMessageButton'
import { prepareComponent } from '@/utils'

prepareComponent(SendMessageButton)
